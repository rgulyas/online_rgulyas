//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Incidents.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_INCIDENTS_H
#define ONLINE_DATAFLOW_INCIDENTS_H

// C /C++ include files
#include <string>

///  Online namespace declaration
namespace Online  {

  /// Incident structure
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class DataflowIncident  {
  public:
    /// Incident data: Incident name
    std::string name;
    /// Incident data: Incident type
    std::string type;
    /// Default constructor
    DataflowIncident();
    /// Initializing constructor
    DataflowIncident(const std::string& n, const std::string& t);
    /// Default destructor
    virtual ~DataflowIncident();
    /// Inhibit copy construction
    DataflowIncident(const DataflowIncident& copy) = delete;
    /// Inhibit assignment
    DataflowIncident& operator=(const DataflowIncident& copy) = delete;
  };

  /// Default constructor
  inline DataflowIncident::DataflowIncident()  {
  }
  /// Initializing constructor
  inline DataflowIncident::DataflowIncident(const std::string& n, const std::string& t)
    : name(n), type(t)  {
  }

  /// Incident structure with optional user defined argument
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  template <typename T> class ContextIncident : public DataflowIncident  {
  public:
    T data;
    /// Default constructor
    ContextIncident();
    /// Initializing constructor
    ContextIncident(const std::string& n, const std::string& t, T data);
    /// Default destructor
    virtual ~ContextIncident()  {}
    /// Inhibit copy construction
    ContextIncident(const ContextIncident& copy) = delete;
    /// Inhibit assignment
    ContextIncident& operator=(const ContextIncident& copy) = delete;
  };

  /// Default constructor
  template <typename T> inline 
    ContextIncident<T>::ContextIncident() : DataflowIncident(), data()  {
  }
  /// Initializing constructor
  template <typename T> inline 
    ContextIncident<T>::ContextIncident(const std::string& n, const std::string& t, T d)
    : DataflowIncident(n,t), data(d)  {
  }
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_INCIDENTS_H
