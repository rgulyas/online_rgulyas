//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Transfer.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_ZMQ_TRANSFER_H
#define ONLINE_DATAFLOW_ZMQ_TRANSFER_H

#define TRANSFER_NS ZMQ
#include <NET/Transfer.h>
#undef  TRANSFER_NS

#endif  /* ONLINE_DATAFLOW_ZMQ_TRANSFER_H */
