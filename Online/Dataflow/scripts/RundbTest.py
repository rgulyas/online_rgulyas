from __future__ import print_function
#*****************************************************************************\
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
#                                                                             *
# This software is distributed under the terms of the GNU General Public      *
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
#                                                                             *
# In applying this licence, CERN does not waive the privileges and immunities *
# granted to it by virtue of its status as an Intergovernmental Organization  *
# or submit itself to any jurisdiction.                                       *
#*****************************************************************************/
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import object
from xmlrpc.server import SimpleXMLRPCServer, SimpleXMLRPCRequestHandler
import os, sys, time, socket, threading

# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

directory1 = '/markus/rundb_test'
directory2 = '/markus2/rundb_test'
count = 0

class File(object):
  def __init__(self, run, stream, source, seq):
    global count
    self.lock    = threading.Lock()
    self.run     = int(run)
    self.seq     = seq
    self.stream  = stream
    self.source  = source
    self.created = time.time()
    runno = int(run)
    sep = os.path.sep
    if count == 0:
      pass
    count = count + 1
    dir_name = directory1
    if count%2 == 0:
      dir_name = directory1
    self.name    = dir_name+sep+stream+sep+'%d'%(runno,)+sep+'Run%08d_%08d.mdf'%(runno,seq,)

class RunDBTest(object):
  def __init__(self):
    self.lock = threading.Lock()
    self.files = {}
    self.seq = {}

  def getNewFileName(self, run, stream, source):
    global count
    try:
      print('Rundb: getNewFileName(%s, %s, %s)'%(run, stream, source, ))
      self.lock.acquire()
      if stream not in self.seq:
        self.seq[stream] = {}
      if run not in self.seq[stream]:
        self.seq[stream][run] = 0
      seq = count ####self.seq[stream][run] + 1
      self.seq[stream][run] = seq
      f = File(run,stream,source,seq)
      self.files[f.name] = f
      self.lock.release()
      print('Rundb: getNewFileName(%s, %s, %s) = %s'%(run, stream, source, f.name, ))
      return f.name
    except Exception as X:
      print('Rundb: getNewFileName FAILED: Exception:',str(X))
      raise X

  def openFile(self, fname, run):
    try:
      print('Rundb: openFile(%s, %s)'%(fname, run, ))
      return fname
    except Exception as X:
      print('Rundb: openFile FAILED: Exception:',str(X))
      raise X

  def confirmFile(self, name, adler32, md5, bytes, events, physStat):
    try:
      print('Rundb: confirmFile(%s, %s, %s, %s, %s, %s)'%(name, adler32, md5, bytes, events, physStat, ))
      f = self.files[name]
      fmt = 'File[%s.%d]: %s Adler32:%0s MD5:%s Bytes:%s Events:%s Phys:%s '
      print(fmt%(f.stream,f.seq,name,adler32,md5,bytes,events,physStat,))
      del self.files[name]
      return 0
    except Exception as X:
      print('Rundb: confirmFile FAILED: Exception:',str(X))
      raise X

    
  def updateFile(self, name, values):
    try:
      f = self.files[name]
      print('Rundb: updateFile: File[%s.%d]: %s values:'%(f.stream,f.seq,f.name,), str(values))
      return 0
    except Exception as X:
      print('Rundb: updateFile FAILED: Exception:',str(X))
      raise X


# Create server
host   = socket.gethostname()
port   = 8050
server = SimpleXMLRPCServer((host, port),
                            requestHandler=RequestHandler)
server.register_introspection_functions()
server.register_instance(RunDBTest())
# Run the server's main loop
print('+'+(128*'=')+'+')
print('+%-128s+'%('     Run database test application running.... [%s:%d]'%(host,port,),))
print('+'+(128*'=')+'+')
server.serve_forever()
