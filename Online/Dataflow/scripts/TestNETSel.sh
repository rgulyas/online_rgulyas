#*****************************************************************************\
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
#                                                                             *
# This software is distributed under the terms of the GNU General Public      *
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
#                                                                             *
# In applying this licence, CERN does not waive the privileges and immunities *
# granted to it by virtue of its status as an Intergovernmental Organization  *
# or submit itself to any jurisdiction.                                       *
#*****************************************************************************/
#. setup.x86_64-centos7-gcc62-do0.vars;
export DATAINTERFACE=$HOST
export TAN_NODE=mona0801
export TAN_PORT=YES
export DATAFLOW_task="gentest.exe libDataflow.so dataflow_run_task -class=Class2 -mon=Dataflow_DIMMonitoring -opt=TestRCV.opts -auto"
export OPTS=/group/online/dataflow/templates/options;
export msg_svc=MessageSvc
export gaudi_task="GaudiOnlineExe.exe libGaudiOnline.so OnlineTask -msgsvc=$msg_svc "
export Class2_task="$gaudi_task -tasktype=LHCb::Class2Task -main=$GAUDIONLINEROOT/options/Main.opts ";
export UTGID=SEL_$1;
export EVENT_SERVER=mona0801::LHCb_MONA0801_SpyServer
${Class2_task} -opts=/home/frankm/cmtuser/OnlineDev_v6r12/Online/GaudiOnline/options/NetworkConsumer.opts -auto;
