//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  Fifo Logger implementation
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "DebugServer.h"

/// C/C++ include files
#include <mutex>
#include <regex>
#include <string>
#include <memory>
#include <thread>

class debugger::DebugServer::implementation   {
  typedef std::unique_ptr<std::thread> thread_t;
  bool        _is_running     { false };
  bool        _have_stdout    { false };
  int         _pipe_fd_in[2]  { 0, 0};
  int         _pipe_fd_out[2] { 0, 0};
  int         _gdb_stdin      { -1 };
  int         _gdb_stdout     { -1 };
  pid_t       _gdb_pid        { -1 };
  pid_t       _current_pid    { -1 };
  thread_t    _watchdog       { };
  thread_t    _input          { };
  thread_t    _output         { };

  std::mutex  _dim_vars_lock  { };
  long        _dim_dns_id     { 0L };
  int         _dim_cmd_id     { 0 };
  int         _dim_log_id     { 0 };
  std::string _dim_dns_name   { };
  std::string _dim_log_string { };
  std::string _dim_cmd_string { };

  std::string _command        { "/usr/bin/gdb" };
  std::string _current_node   { };
  std::string _current_task   { };

  /// DIM callback to supply serving log messages
  static void handle_log(void* tag, void** buff, int* size, int* first);

  /// DIM callback to process debugger commands
  static void handle_cmd(void* tag, void* address, int* size);

public:
  /// Default constructor
  implementation();

  /// Default destructor
  ~implementation();

  /// Set GDB command path
  void set_command(const std::string& cmd)   {
    this->_command = cmd;
  }

  void have_stdout(bool value)   {
    this->_have_stdout = value;
  }

  /// Generic output helper
  void output(FILE* file, const char* tag, const char* fmt, va_list& args);

  /// Output helper for logging messages
  void log(const char* fmt, ...);

  /// Output helper for error messages
  void error(const char* fmt, ...);

  /// Output helper to log messages with tag
  void print(const char* tag, const char* fmt, ...);

  /// Helper to call when a command is not understood
  void command_ignored(const std::string& cmd)  {
    this->error("Command ignored: %s", cmd.c_str());
  }

  /// Setup pipes to communicate to GDB child
  void setup_gdb_input();

  /// Re-establish command prompt
  void make_prompt();

  /// Run the remote debugger
  void run();

  /// Start GDB child
  void start_debugger();

  /// Stop GDB child
  void stop_debugger();

  /// Enable GDB watchdog to terminate child properly
  void watch_debugger();

  /// Shutdown the debugger application
  void shutdown();

  /// Server command: Publish status string
  void status_process();

  /// Server command: list processes with a UTGID matching a regular expression
  void list_processes(const std::string& match);

  /// Server command: Attach to a given process matching a regular expression
  void attach_process(const std::string& match);

  /// Process a single GDB command request
  bool process_gdb(const std::string& cmd);

  /// Process a single command request
  bool process_command(const std::string& cmd);

  /// Initialize dim communication layer
  void process_dim(const std::string& dns);

  /// Start the standard input message pump
  void process_stdin();

  /// Start the output message pump
  void process_stdout();
};

/// Framework include files
#include <RTL/readdir.h>
#include <RTL/strdef.h>
#include <RTL/Sys.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

/// C/C++ include files
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <csignal>
#include <system_error>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>

#define VAR_ARGS(args, fmt)    va_list args;  va_start(args, fmt)

namespace {
  std::string utgid_match_from_command(const std::string& cmd)   {
    auto req = RTL::str_replace(cmd,"  "," ");
    req = RTL::str_replace(req,"<node>",RTL::str_lower(RTL::nodeNameShort()));
    req = RTL::str_replace(req,"<NODE>",RTL::str_upper(RTL::nodeNameShort()));
    auto items = RTL::str_split(req, " ");
    std::string match;

    if ( items.size() > 1 )   {
      match = items[1];
    }
    return match;
  }

  std::map<pid_t, std::string> utgid_processes(const std::string& match)   {
    DIR *dir = 0;
    std::map<pid_t, std::string> procs;

    if ( match.empty() )  {
      return procs;
    }
    int flags = std::regex_constants::icase | std::regex_constants::ECMAScript;
    std::regex rg(match, (std::regex_constants::syntax_option_type)flags);

    if ( (dir = ::opendir("/proc")) )    {
      struct dirent* dp;
      RTL::UtgidProcess utgid;
      do {
	if ((dp = readdir(dir)) != 0) {
	  try {
	    const char* n = dp->d_name;
	    if ( *n && ::isdigit(*n) ) {
	      pid_t pid = ::atoi(n);
	      utgid.utgid = "";
	      RTL::read(utgid,pid);
	      if ( !utgid.utgid.empty() )   {
		std::smatch sm;
		bool stat = std::regex_match(utgid.utgid, sm, rg);
		if ( stat )    {
		  procs[pid] = utgid.utgid;
		}
	      }
	    }
	  }
	  catch(...) {
	  }
	}
      } while (dp != NULL);
      ::closedir(dir);
    }
    return procs;
  }
}

/// DimInfo overload to process debugger commands
void debugger::DebugServer::implementation::handle_cmd(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    auto* it = *(debugger::DebugServer::implementation**)tag;
    const char* add = (const char*)address;
    std::string cmd(add, add+*size);
    it->process_command( cmd.c_str() );
  }
}

/// DIM callback to supply serving log messages
void debugger::DebugServer::implementation::handle_log(void* tag, void** buff, int* size, int* /* first */)   {
  auto* it = *(debugger::DebugServer::implementation**)tag;
  *buff = (void*)it->_dim_log_string.c_str();
  *size = it->_dim_log_string.length();
}

/// Default constructor
debugger::DebugServer::implementation::implementation()   {
  this->_current_node = RTL::str_upper(RTL::nodeNameShort());
}

/// Default destructor
debugger::DebugServer::implementation::~implementation()   {
  if ( this->_dim_cmd_id > 0 )
    ::dis_remove_service(this->_dim_cmd_id);
  if ( this->_dim_log_id > 0 )
    ::dis_remove_service(this->_dim_log_id);
  if( _pipe_fd_in[0]  ) ::close(_pipe_fd_in[0]);
  if( _pipe_fd_in[1]  ) ::close(_pipe_fd_in[1]);
  if( _pipe_fd_out[0] ) ::close(_pipe_fd_out[0]);
  if( _pipe_fd_out[1] ) ::close(_pipe_fd_out[1]);
  _watchdog.reset();
  _output.reset();
  _input.reset();
}

/// Generic output helper
void debugger::DebugServer::implementation::output(FILE* file, const char* tag, const char* fmt, va_list& args)    {
  char text[4096];
  size_t len = ::snprintf(text, sizeof(text), "%-9s",this->_current_node.c_str());
  if ( !this->_current_task.empty() )  {
    len += ::snprintf(text+len, sizeof(text)-len, "%s ",this->_current_task.c_str());
  }
  len += ::snprintf(text+len, sizeof(text)-len, "%-9s",tag);
  len += ::vsnprintf(text+len, sizeof(text)-len, fmt, args);
  va_end(args);
  if ( this->_dim_log_id > 0 )    {
    text[len] = 0;   {
      std::lock_guard<std::mutex> protect(this->_dim_vars_lock);
      this->_dim_log_string = text;
    }
    ::dis_update_service(this->_dim_log_id);
  }
  if ( this->_have_stdout )   {
    text[len] = '\n';
    text[len+1] = 0;
    ::write(::fileno(file), text, len+1);
    ::fflush(file);
  }
}

/// Output helper for logging messages
void debugger::DebugServer::implementation::log(const char* fmt, ...)   {
  if ( fmt )   {
    VAR_ARGS(args, fmt);
    this->output(stdout, "INFO", fmt, args);
  }
}

/// Output helper for error messages
void debugger::DebugServer::implementation::error(const char* fmt, ...)   {
  if ( fmt )   {
    VAR_ARGS(args, fmt);
    this->output(stderr, "ERROR", fmt, args);
  }
}

/// Output helper to log messages with tag
void debugger::DebugServer::implementation::print(const char* tag, const char* fmt, ...)   {
  if ( fmt )   {
    VAR_ARGS(args, fmt);
    this->output(stdout, tag, fmt, args);
  }
}

/// Setup pipes to communicate to GDB child
void debugger::DebugServer::implementation::setup_gdb_input()   {
  if ( 0 == _pipe_fd_in[0] || 0 == _pipe_fd_in[1] )   {
    if( ::pipe(_pipe_fd_in) == -1 )    {
      std::error_code ec(errno, std::system_category());
      this->error("Pipe error: %s", ec.message().c_str());
      ::exit(errno);
    }
    if( ::pipe(_pipe_fd_out) == -1 )    {
      std::error_code ec(errno, std::system_category());
      this->error("Pipe error: %s", ec.message().c_str());
      ::exit(errno);
    }
  }
}

/// Re-establish command prompt
void debugger::DebugServer::implementation::make_prompt()   {
  if ( this->_have_stdout )    {
    ::fprintf(stdout, "%s %s ", (this->_gdb_stdin == -1) ? "No debugger>" : "(gdb)", _current_node.c_str());
    ::fflush(stdout);
  }
}

/// Shutdown the debugger application
void debugger::DebugServer::implementation::shutdown()   {
  this->_is_running = false;

  this->stop_debugger();

  /// Write something to the pipe, so that we exit the read call in the stdout thread
  ::write(_pipe_fd_out[1],"q",1);

  if ( this->_watchdog.get() )
    this->_watchdog->join();
  this->_watchdog.reset();

  if ( this->_input.get() )
    this->_input->join();
  this->_input.reset();

  if ( this->_output.get() )
    this->_output->join();
  this->_output.reset();
}

/// Start GDB child
void debugger::DebugServer::implementation::start_debugger()     {
  std::string name = RTL::processName();
  // Empty input pipe before starting new instance
  for(unsigned long ret=0; ::ioctl(_pipe_fd_in[0], FIONREAD, &ret) != -1; )   {
    if ( ret == 0 ) break;
    ::read(_pipe_fd_in[0], &ret, 1);
  }
  // Now for off child and switch to gdb executable
  ::setenv("UTGID",(name+"_GDB").c_str(),1);
  pid_t pid = ::fork();
  if ( 0 == pid )   {
    this->print("GDB","Child process starting debugger: %s", _command.c_str());
    ::close(STDIN_FILENO);
    ::dup2(_pipe_fd_in[0], STDIN_FILENO);
    ::close(STDOUT_FILENO);
    ::dup2(_pipe_fd_out[1], STDOUT_FILENO);
    ::close(STDERR_FILENO);
    ::dup2(_pipe_fd_out[1], STDERR_FILENO);
    ::execlp(this->_command.c_str(), this->_command.c_str(), nullptr);

    /// Call should never return unless error....
    std::error_code ec(errno, std::system_category());
    this->error("Fork failed. Cannot start the debugger: %s [%s]",
		this->_command.c_str(), ec.message().c_str());
    ::exit(errno);
  }
  ::setenv("UTGID",name.c_str(),1);
  this->_current_pid  = -1;
  this->_current_task = "";
  this->_gdb_pid      = pid;
  this->_gdb_stdin    = _pipe_fd_in[1];
  this->_gdb_stdout   = _pipe_fd_out[0];
}

/// Stop GDB child
void debugger::DebugServer::implementation::stop_debugger()     {
  if ( -1 != _gdb_pid )    {
    ::kill(this->_gdb_pid, SIGINT);
    this->print("QUIT","Sending QUIT to debugger....");
    ::write(this->_gdb_stdin, "detach\n", 2);
    ::write(this->_gdb_stdin, "q\n", 2);
    ::write(this->_gdb_stdin, "y\n", 2);
  }
}

/// Enable GDB watchdog to terminate child properly
void debugger::DebugServer::implementation::watch_debugger()    {
  this->_is_running = true;
  this->_watchdog = std::make_unique<std::thread>([this]{
      while( this->_is_running )    {
	int status = 0;
	pid_t pid = ::waitpid(-1, &status, 0);
	if ( pid > 0 && pid == this->_gdb_pid )    {
	  this->_gdb_stdin    = -1;
	  this->_gdb_stdout   = -1;
	  this->_gdb_pid      = -1;
	  this->_current_pid  = -1;
	  this->_current_task = "";
	  this->print("QUIT", "Debugger process with pid %d finished. Status: %d", pid, status);
	  if ( this->_is_running ) this->make_prompt();
	  continue;
	}
	::usleep(5000);
      }
      this->print("QUIT", "Debugger watchdog finished.");
      this->_current_pid  = -1;
      this->_current_task = "";
    });
}

/// Server command: list processes with a UTGID matching a regular expression
void debugger::DebugServer::implementation::list_processes(const std::string& match)   {
  auto procs = utgid_processes(match.empty() ? "(.*)" : match.c_str());
  this->print("LIST","Using match: %s --> %ld processes", match.c_str(), procs.size());
  for(const auto& p : procs)   {
    this->print("LIST","pid: %-5d   utgid: %s", p.first, p.second.c_str());
  }
  this->make_prompt();
}

/// Server command: Attach to a given process matching a regular expression
void debugger::DebugServer::implementation::attach_process(const std::string& match)   {
  auto procs = utgid_processes(match);
  if ( procs.empty() )  {
    this->error("No process selected for debugging match %s", match.c_str());
    this->make_prompt();
    return;
  }
  else if ( procs.size() > 1 )  {
    this->error("%ld processes selected for debugging match %s. Ambiguous command ignored.",
	      procs.size(), match.c_str());
    this->make_prompt();
    return;
  }
#if 0
  stop_debugger();
  while( this->_gdb_pid > 0 )   {
    ::usleep(100);
  }
  //this->start_debugger();
#endif
  char text[132];
  auto iproc = procs.begin();
  this->_current_pid  = iproc->first;
  this->_current_task = iproc->second;
  this->log("Debugging process: %s PID:%d", this->_current_task.c_str(), this->_current_pid);
  ::snprintf(text, sizeof(text),"attach %d", this->_current_pid);
  this->process_gdb(text);
}

/// Server command: Publish status string
void debugger::DebugServer::implementation::status_process()   {
  this->print("STATUS", "NODE:%s GDB:%d PID:%d TASK:'%s'",
	      this->_current_node.c_str(),
	      this->_gdb_pid, this->_current_pid,
	      this->_current_task.c_str());
  this->make_prompt();
}

/// Process a single GDB command request
bool debugger::DebugServer::implementation::process_gdb(const std::string& cmd)   {
  if ( this->_gdb_stdin == -1 )    {
    this->error("Underlying debugger not running. Need \".start\" before sending commands.");
    this->_current_pid  = -1;
    this->_current_task = "";
    this->make_prompt();
    return true;
  }
  std::string text = cmd + '\n';
  this->log("Sending debugger command: \"%s\"", cmd.c_str());
  ::write(this->_gdb_stdin, text.c_str(), text.length());
  if ( ::strncmp(cmd.c_str(), "detach", 3) == 0 )    {
    this->stop_debugger();
    this->_current_pid  = -1;
    this->_current_task = "";
  }
  this->make_prompt();
  return true;
}

/// Process a single command request
bool debugger::DebugServer::implementation::process_command(const std::string& cmd)   {
  if ( cmd.empty() )   {
    this->make_prompt();
    return true;
  }
  else if ( cmd[0] == '.' )    {
    if ( 0 == ::strncasecmp(cmd.c_str(),".quit",2) )   {
      this->stop_debugger();
      this->_is_running = false;
      return false;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".end",3) )   {
      this->stop_debugger();
      this->_is_running = false;
      return false;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".kill",4) )   {
      this->stop_debugger();
      this->_is_running = false;
      return false;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".stop",5) )   {
      this->stop_debugger();
      return true;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".start",6) )   {
      this->stop_debugger();
      this->start_debugger();
      return true;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".interrupt",6) )   {
      if ( this->_gdb_pid > 0 )
	::kill(this->_gdb_pid, SIGINT);
      else
	this->print("INTERRUPT","No gdb process running. Cannot send signal SIGINT.");
      return true;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".list",5) )   {
      this->list_processes(utgid_match_from_command(cmd));
      return true;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".attach",4) )   {
      this->attach_process(utgid_match_from_command(cmd));
      return true;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".status",7) )   {
      this->status_process();
      return true;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(),".help",4) )   {
      this->print("HELP", "Debug server commands:");
      this->print("HELP", ".q(uit)           Finalize the debug session end debugger and server. ");
      this->print("HELP", ".end              dto.                                                ");
      this->print("HELP", ".stop             Stop underlying debugger  %s.", this->_command.c_str());
      this->print("HELP", ".start            Start underlying debugger %s.", this->_command.c_str());
      this->print("HELP", ".status           Publish gdb connection status.                      ");
      this->print("HELP", ".attach <regex>   Attach debugger to process matching <regex>         ");
      this->print("HELP", "                  Note: Match must be unique!                         ");
      this->print("HELP", ".list <regex>     List all processes with a UTGID matching the regular expression.");
      this->print("HELP", "                  Regular expressions:                                ");
      this->print("HELP", "                  Use <node> <NODE> tags to dynamically modify <utgid>");
      this->make_prompt();
      return true;
    }
    std::error_code ec(EINVAL, std::system_category());
    this->error("Server command unknown: %s [%s]", cmd.c_str(), ec.message().c_str());
    this->make_prompt();
    return true;
  }
  return process_gdb(cmd);
}

/// Run the remote debugger
void debugger::DebugServer::implementation::run()   {
  this->process_stdout();
  while( this->_is_running )    {
    ::usleep(1000);
  }
  ::usleep(1000);
  this->shutdown();
}

/// Initialize dim communication layer
void debugger::DebugServer::implementation::process_dim(const std::string& dns)   {
  int port         = ::dis_get_dns_port();
  std::string svc;
  std::string name = RTL::processName();
  std::string node = RTL::str_upper(RTL::nodeNameShort());
  this->_dim_dns_name = dns;
  this->_dim_dns_id = ::dis_add_dns(this->_dim_dns_name.c_str(), port);

  svc  = name + "/command";
  this->_dim_cmd_id = ::dis_add_cmnd_dns(this->_dim_dns_id, svc.c_str(), "C", handle_cmd, (long)this);
  svc  = name + "/output";
  this->_dim_log_id = ::dis_add_service_dns(this->_dim_dns_id, svc.c_str(), "C", 0, 0, handle_log, (long)this);
  ::dis_start_serving_dns(this->_dim_dns_id, name.c_str());
}

/// Start the standard input message pump
void debugger::DebugServer::implementation::process_stdin()   {
  this->_input = std::make_unique<std::thread>([this] {
      std::string text;
      while( this->_is_running )    {
	char c = 0;
	int nb = ::read(STDIN_FILENO,&c,1);
	if ( nb == 1 )   {
	  if ( c == '\n' )   {
	    if ( !process_command(text) )    {
	      break;
	    }
	    text = "";
	    continue;
	  }
	  text += c;
	}
      }
    });
}

/// Start the output message pump
void debugger::DebugServer::implementation::process_stdout()   {
  this->_output = std::make_unique<std::thread>([this] {
      std::string text;
      while( this->_is_running )    {
	char c = 0;
	int nb = ::read(_pipe_fd_out[0],&c,1);
	if ( nb == 1 )   {
	  if ( c == '\n' )   {
	    this->print("GDB", "%s", text.c_str());
	    text = "";
	    continue;
	  }
	  text += c;
	}
      }
    });
}

/// Default constructor
debugger::DebugServer::DebugServer()   {
  this->imp = std::make_unique<implementation>();
}

/// Default destructor
debugger::DebugServer::~DebugServer()    {
  this->imp->print("QUIT","Shut down debug server: %s", RTL::processName().c_str());
  this->imp->stop_debugger();
  this->imp.reset();
}

/// (Re-)Configure object
void debugger::DebugServer::start(bool run)    {
  this->imp->setup_gdb_input();
  if ( run ) 
    this->imp->start_debugger();
  else
    this->imp->make_prompt();
  this->imp->watch_debugger();
}

static std::unique_ptr<debugger::DebugServer> s_server {nullptr};

/// Initialize the debug server
extern "C" void debug_server_initialize()   {
  if ( !s_server )   {
    s_server = std::make_unique<debugger::DebugServer>();
  }
}

/// Shutdown the debug server
extern "C" void debug_server_finalize()    {
  if ( s_server )   {
    s_server.reset();
  }
}

static void server_help()    {
  ::fprintf(stderr,"Usage: debug_server -arg [-arg...] \n"
	    "  -run                        Start the debugger immediately without attaching to a process.\n"
	    "  -command=<cmd>              Set specific gdb version. Default: /usr/bin/gdb        \n"
	    "  -stdin                      Read stdin to submit commands to the debuggee          \n"
	    "  -stdout                     Enable output to stdout.                               \n"
	    "  -dim                        Use DIM commands to submit commands to the debuggee    \n"
	    "  -fifo                       Use and initialize fifo logging.                       \n"
	    " \n"
	    );
  ::exit(EINVAL);
}

#include <LOG/FifoLog.inl.h>

int main(int argc, char** argv)   {
  RTL::CLI cli(argc, argv, server_help);
  std::string command;
  std::string dns;
  const char* cmd = ::getenv("DEBUG_COMMAND");
  bool run = cli.getopt("run",3) != nullptr;
  bool use_stdin  = cli.getopt("stdin",5)  != nullptr;
  bool use_stdout = cli.getopt("stdout",5) != nullptr;
  bool use_fifo   = cli.getopt("fifo",4)   != nullptr;

  if ( nullptr != cmd ) command = cmd;
  cli.getopt("dns", 3, dns);
  cli.getopt("command", 4, command);
  if ( !use_stdin && dns.empty() )   {
    ::fprintf(stderr,"Argument error: use_stdin: %s dns:'%s'\n",
	      use_stdin ? "1" : "0", dns.c_str());
    server_help();
  }
  auto server = std::make_unique<debugger::DebugServer>();
  if ( !command.empty() ) server->imp->set_command(command);
  if      ( use_fifo     ) ::fifolog_initialize_logger();
  server->start(run);
  if      ( use_stdout   ) server->imp->have_stdout(true);
  if      ( use_stdin    ) server->imp->process_stdin();
  else if ( !dns.empty() ) server->imp->process_dim(dns);
  server->imp->run();
  server.reset();
  return 0;
}
