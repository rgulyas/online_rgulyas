//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DIMMonitoring.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Include files
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram3D.h"
#include "AIDA/IProfile1D.h"
#include "AIDA/IProfile2D.h"

#include "TEnv.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TROOT.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "DIMMonitoring.h"
#include "Dataflow/Incidents.h"
#include "Dataflow/Plugins.h"
#include "Gaucho/MonitorInterface.h"
#include "RTL/rtl.h"
#include "RTL/strdef.h"
#include "dim/dis.hxx"

using namespace std;
using namespace Online;

// Factory for instantiation of service objects
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_DIMMonitoring,DIMMonitoring)

namespace Online  {
  template <typename TO> TO* cast_histo(const AIDA::IBaseHistogram* aidahist);
}

DIMMonitoring::DIMMonitoring(const string& nam, Context& ctxt)
  : DataflowComponent(nam,ctxt)
{
  m_started       = false;
  m_monsysrecover = 0;
  m_runno = 0;
  declareProperty("UniqueServiceNames",          m_uniqueServiceNames = 0);
  declareProperty("disableMonRate",              m_disableMonRate = 0);
  declareProperty("disableDimPropServer",        m_disableDimPropServer = 0);
  declareProperty("disableDimCmdServer",         m_disableDimCmdServer = 0);
  // declareProperty("disableDimRcpGaucho",      m_disableDimRcpGaucho = 0);

  declareProperty("disableMonObjectsForBool",    m_disableMonObjectsForBool = 1);
  declareProperty("disableMonObjectsForInt",     m_disableMonObjectsForInt = 0);
  declareProperty("disableMonObjectsForLong",    m_disableMonObjectsForLong = 0);
  declareProperty("disableMonObjectsForDouble",  m_disableMonObjectsForDouble = 0);
  declareProperty("disableMonObjectsForString",  m_disableMonObjectsForString = 1);
  declareProperty("disableMonObjectsForPairs",   m_disableMonObjectsForPairs = 1);
  declareProperty("disableMonObjectsForHistos",  m_disableMonObjectsForHistos = 0);

  declareProperty("disableDeclareInfoBool",      m_disableDeclareInfoBool = 1);
  declareProperty("disableDeclareInfoInt",       m_disableDeclareInfoInt = 0);
  declareProperty("disableDeclareInfoLong",      m_disableDeclareInfoLong = 0);
  declareProperty("disableDeclareInfoDouble",    m_disableDeclareInfoDouble = 0);
  declareProperty("disableDeclareInfoString",    m_disableDeclareInfoString = 1);
  declareProperty("disableDeclareInfoPair",      m_disableDeclareInfoPair = 1);
  declareProperty("disableDeclareInfoFormat",    m_disableDeclareInfoFormat = 0);
  declareProperty("disableDeclareInfoHistos",    m_disableDeclareInfoHistos = 0);
  declareProperty("maxNumCountersMonRate",       m_maxNumCountersMonRate = 1000);
  declareProperty("DimUpdateInterval",           m_updateInterval = 20);
  declareProperty("CounterUpdateInterval",       m_counterInterval = 0);
  declareProperty("ExpandCounterServices",       m_expandCounterServices=0);
  declareProperty("ExpandNameInfix",             m_expandInfix="");
  declareProperty("PartitionName",               m_partname="LHCb");
  declareProperty("ProcessName",                 m_ProcName="");
  declareProperty("ProgramName",                 m_ProgName="");
  declareProperty("DontResetCountersonRunChange",m_dontResetCountersonRunChange=false);
  declareProperty("UseDStoreNames",              m_useDStoreNames=false);
  declareProperty("CounterClasses",m_counterClasses);
  declareProperty("HistogramClasses",m_histogramClasses);
  TH1D::SetDefaultSumw2();
  TH2D::SetDefaultSumw2();
  TProfile::SetDefaultSumw2();
}

DIMMonitoring::~DIMMonitoring()    {
  m_MonIntf.reset();
}

int DIMMonitoring::initialize()   {
  int sc = Component::initialize();
  if (m_counterInterval == 0)
    m_counterInterval = m_updateInterval;
  if ( 0 != m_disableMonRate)  {
    debug("MonRate process is disabled.");
  }
  if (m_ProcName == "")    {
    m_ProcName = RTL::processName();
  }
  if (m_expandCounterServices)    {
    m_expandInfix = RTL::str_replace(m_expandInfix,"<part>",   m_partname);
    m_expandInfix = RTL::str_replace(m_expandInfix,"<proc>",   m_ProcName);
    m_expandInfix = RTL::str_replace(m_expandInfix,"<program>",m_ProgName);
  }
  SubSysParams cpars;
  cpars.ratePrefix   = "R_";
  cpars.dontclear    = m_dontResetCountersonRunChange;
  cpars.expandInfix  = m_expandInfix;
  cpars.expandnames  = m_expandCounterServices;
  cpars.updatePeriod = m_counterInterval;
  cpars.type         = MONSUBSYS_Counter;
  SubSysParams hpars;
  hpars.ratePrefix   = "R_";
  hpars.dontclear    = m_dontResetCountersonRunChange;
  hpars.updatePeriod = m_updateInterval;
  hpars.type         = MONSUBSYS_Histogram;
  m_MonIntf = std::make_unique<MonitorInterface>(RTL::processName(), move(cpars), move(hpars));
  m_MonIntf->applyCounterClasses(m_counterClasses);
  m_MonIntf->applyHistogramClasses(m_histogramClasses);

  subscribeIncident("DAQ_RUNNING");
  subscribeIncident("DAQ_STOPPED");
  subscribeIncident("DAQ_FINALIZE");
  return sc;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void DIMMonitoring::handle(const DataflowIncident& inc) {
  if ( inc.type == "DAQ_RUNNING" ) {
    //  CALLGRIND_START_INSTRUMENTATION
    debug("Starting publishing monitor information using DIM....");
    //  setProperties();
    //  dis_set_debug_on();
    if ( m_MonIntf )  {
      m_MonIntf->start();
    }
    setRunNo(m_runno);
    m_started = true;
  }
  else if ( inc.type == "DAQ_FINALIZE" ) {
    m_started = false;
    stopUpdate();
  }
  else if ( inc.type == "DAQ_STOPPED" ) {
  }
}

/// Monitoring interface: Undeclare single monitoring item
int DIMMonitoring::undeclare(const string& owner, const string& nam)  {
  if ( m_MonIntf )  {
    string oname = !owner.empty() ? owner : string("");
    m_MonIntf->undeclare(oname+"/"+nam);
  }
  return DF_SUCCESS;
} 

/** Undeclare monitoring information
    @param owner Owner identifier of the monitoring information
*/
int DIMMonitoring::undeclare(const string& owner)  {
  if ( m_MonIntf )  {
    string oname = !owner.empty() ? owner : string("");
    m_MonIntf->undeclareAll(oname);
  }
  return DF_SUCCESS;
}

void DIMMonitoring::stopUpdate()  {
  if ( m_MonIntf )  {
    m_MonIntf->stop();
    stopSaving();
  }
}

int DIMMonitoring::finalize()  {
  unsubscribeIncidents();
  dim_lock();
  if (m_started)    {
    m_started = false;
    if ( m_MonIntf )  {
      m_MonIntf->stop();
    }
  }
  dim_unlock();
  m_MonIntf.reset();
  debug("Finalized successfully");
  return Component::finalize();
}

void DIMMonitoring::setRunNo(uint32_t runno)  {
  m_runno = runno;
  m_MonIntf->setRunNo(runno);
}

template<class T> int
DIMMonitoring::i_declareCounter(const string& owner,
				const string& nam,
				T&  var,
				const string& desc)
{
  debug("Declare counter %s.%s of type %s",
	owner.c_str(),nam.c_str(),typeName(typeid(T)).c_str());
  string newName = nam;
  if (nam.find("COUNTER_TO_RATE") != string::npos)    {
    newName = extract("COUNTER_TO_RATE", nam);
    newName = owner+"/"+newName;
  }
  else if ( !nam.empty() && !owner.empty() )
    newName = owner+"/"+nam;
  else if ( nam.empty()   )
    newName = owner;
  else if ( owner.empty() )
    newName = nam;

  m_MonIntf->declareCounter(newName,var,desc);
  return DF_SUCCESS;
}

template<class T> int
DIMMonitoring::i_declarePair(const string& owner,
			     const string& nam,
			     T&  var1,
			     T&  var2,
			     const string& desc)
{
  debug("Declare Pair %s.%s of type %s",
	owner.c_str(),nam.c_str(),typeName(typeid(T)).c_str());
  string newName = nam;
  if (nam.find("COUNTER_TO_RATE") != string::npos)    {
    newName = extract("COUNTER_TO_RATE", nam);
    newName = owner+"/"+newName;
  }
  else if ( !nam.empty() && !owner.empty() )
    newName = owner+"/"+nam;
  else if ( nam.empty()   )
    newName = owner;
  else if ( owner.empty() )
    newName = nam;
  m_MonIntf->declarePair(newName,var1,var2,desc);
  return DF_SUCCESS;
}

int DIMMonitoring::i_unsupported(const string& own, const string& nam, const type_info& type)  {
  return error("Monitoring: %s/%s -> declare(%s)) not implemented",
	       own.c_str(),nam.c_str(),typeName(type).c_str());
}

template<class T>
int DIMMonitoring::declare_histogram(const string& owner, const string& nam, T* var, const string& descr)
{
  if ( 0 != m_disableDeclareInfoHistos )    {
    debug("m_disableDeclareInfoHistos DISABLED ");
    return DF_SUCCESS;
  }
  if ( m_disableMonObjectsForHistos != 0 )    {
    return error("DIMMonitoring doesn't have support for declaring histograms/profiles without using MonObjects ");
  }
  string hnam = (nam.find(owner) == string::npos) ? owner+"/"+nam : nam;
  m_MonIntf->declareHistogram(hnam,var,descr);
  return DF_SUCCESS;
}

int DIMMonitoring::declare(const string& owner,
			   const string& nam,
			   const void* var1,
			   const void* var2,
			   const type_info& typ,
			   const string& desc)
{
  if ( typ == typeid(int) )   {
    return i_declarePair(owner,nam,*(int*)var1,*(int*)var2,desc);
  }
  else if ( typ == typeid(long) )  {
    return i_declarePair(owner,nam,*(long*)var1,*(long*)var2,desc) ;
  }
  return i_unsupported(owner,nam,typ);
}

int DIMMonitoring::declare(const string& owner,
			   const string& nam,
			   const void* var,
			   const type_info& typ,
			   const string& desc)
{
  AIDA::IBaseHistogram *dyn_var = (AIDA::IBaseHistogram*)const_cast<void*>(var);
  if ( typ == typeid(int) )   {
    return (0 == m_disableDeclareInfoInt) ? i_declareCounter(owner,nam,*(int*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(long) )  {
    return (0 == m_disableDeclareInfoLong) ? i_declareCounter(owner,nam,*(long*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(unsigned int) )  {
    return (0 == m_disableDeclareInfoLong) ? i_declareCounter(owner,nam,*(unsigned int*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(unsigned long) )  {
    return (0 == m_disableDeclareInfoLong) ? i_declareCounter(owner,nam,*(unsigned long*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(double) )  {
    return i_declareCounter(owner,nam,*(double*)var,desc);
  }
  else if ( typ == typeid(float) )  {
    return i_declareCounter(owner,nam,*(float*)var,desc);
  }
  else if ( typ == typeid(atomic<int>) )  {
    return i_declareCounter(owner,nam,*(atomic<int>*)var,desc);
  }
  else if ( typ == typeid(atomic<float>) )  {
    return i_declareCounter(owner,nam,*(atomic<float>*)var,desc);
  }
  else if ( typ == typeid(atomic<double>) )  {
    return i_declareCounter(owner,nam,*(atomic<double>*)var,desc);
  }
  else if ( typ == typeid(atomic<long>) )  {
    return i_declareCounter(owner,nam,*(atomic<long>*)var,desc);
  }
  else if ( typ == typeid(bool) )  {
    return i_unsupported(owner,nam,typ);
  }
  else if ( typ == typeid(string) )  {
    return i_unsupported(owner,nam,typ);
  }
  else if ( typ == typeid(pair<double,double>) )  {
    return i_unsupported(owner,nam,typ);
  }
  //  else if ( typ == typeid(TH3D) )  {
  //    return declare_histogram(owner,nam,(TH3D*)var,desc);
  //  }
  else if ( typ == typeid(TH2D) )  {
    return declare_histogram(owner,nam,(TH2D*)var,desc);
  }
  else if ( typ == typeid(TH1D) )  {
    return declare_histogram(owner,nam,(TH1D*)var,desc);
  }
  else if ( typ == typeid(TProfile) )  {
    return declare_histogram(owner,nam,(TProfile*)var,desc);
  }
  else if ( typ == typeid(TProfile2D) )  {
    return declare_histogram(owner,nam,(TProfile2D*)var,desc);
  }
  //  else if ( typ == typeid(AIDA::IHistogram3D) && cast_histo<AIDA::IHistogram3D>(dyn_var) )  {
  //    TH3D* h = cast_histo<TH3D>(dyn_var);
  //    return declare_histogram(owner,nam,h,desc);
  //  }
  else if ( typ == typeid(AIDA::IHistogram2D) && cast_histo<AIDA::IHistogram2D>(dyn_var) )  {
    TH2D* h = cast_histo<TH2D>(dyn_var);
    return declare_histogram(owner,nam,h,desc);
  }
  else if ( typ == typeid(AIDA::IHistogram1D) && cast_histo<AIDA::IHistogram1D>(dyn_var) )  {
    TH1D* h = cast_histo<TH1D>(dyn_var);
    return declare_histogram(owner,nam,h,desc);
  }
  else if ( typ == typeid(AIDA::IProfile2D) && cast_histo<AIDA::IProfile2D>(dyn_var) )  {
    TProfile2D* h = cast_histo<TProfile2D>(dyn_var);
    return declare_histogram(owner,nam,h,desc);
  }
  else if ( typ == typeid(AIDA::IProfile1D) && cast_histo<AIDA::IProfile1D>(dyn_var) )  {
    TProfile* h = cast_histo<TProfile>(dyn_var);
    return declare_histogram(owner,nam,h,desc);
  }
  else if ( cast_histo<AIDA::IBaseHistogram>(dyn_var) )  {
    return error("Unknown histogram type (%s). Source:%s.%s",
		 typeName(typeid(var)).c_str(),owner.c_str(),nam.c_str());
  }
  return i_unsupported(owner,nam,typ);
}

//
int DIMMonitoring::declare(const string&    owner, 
			   const string&    nam, 
			   const string& ,
			   const void*      var,
			   size_t           size,
			   const type_info& typ,
			   const string&    desc)
{
  string format;
  string cname;
  if ( !nam.empty() && !owner.empty() ) cname = owner+"/"+nam;
  else if ( nam.empty()   ) cname = owner;
  else if ( owner.empty() ) cname = nam;

  //  char fch = ::tolower(format[0]);
  if ((typ == typeid(int*)) || (typ == typeid(unsigned int *)))    {
    format = "i";
    if (typ == typeid(int*))	{
      m_MonIntf->declareCounter<int*>(cname,format,( int*)var,size,desc);
    }
    else if (typ == typeid(unsigned int *))	{
      m_MonIntf->declareCounter<unsigned int*>(cname,format,( unsigned int*)var,size,desc);
    }
  }
  else if ((typ == typeid(long*)) || (typ == typeid(long long *)) || (typ == typeid(unsigned long *)))    {
    format = "x";
    if (typ == typeid(long *))	{
      m_MonIntf->declareCounter<long*>(cname,format,( long*)var,size,desc);
    }
    else if (typ == typeid(unsigned long *))	{
      m_MonIntf->declareCounter<unsigned long*>(cname,format,( unsigned long*)var,size,desc);
    }
    else if (typ == typeid(long long *))   {
      m_MonIntf->declareCounter<long*>(cname,format,( long*)var,size,desc);
    }
  }
  if ((typ == typeid(float*)) )    {
    format = "f";
    m_MonIntf->declareCounter<float*>(cname,format,( float*)var,size,desc);
  }
  if ((typ == typeid(double*)) )    {
    format = "d";
    m_MonIntf->declareCounter<double*>(cname,format,( double*)var,size,desc);
  }
  return DF_SUCCESS;
}

string DIMMonitoring::extract(const string mascara, string value)   const {
  if (value.find(mascara) != string::npos) {
    value.erase(0, mascara.size()+1);
    value.erase(value.size() - 1, 1);
  }
  return value;
}

string DIMMonitoring::infoOwnerName( const Component* owner )  const {
  return owner ? owner->name : string();
}

/// Monitoring interface: Update monitoring items
int DIMMonitoring::update(const string& , const string& , int runno)  {
  info("Monitor Service: Issuing EOR Update for Run %d",runno);
  m_MonIntf->eorUpdate(runno);
  return DF_SUCCESS;
}

/// Monitoring interface: Reset some/all monitoring items
int DIMMonitoring::reset(const string& owner, const string& what)  {
  if ( RTL::str_upper(what) == "HISTOS" )  {
    resetHistos(owner);
  }
  return DF_SUCCESS;
}

void DIMMonitoring::updatePerSvc(unsigned long ref)  {
  m_MonIntf->update(ref);
}

void DIMMonitoring::resetHistos(const string& )  {
  m_MonIntf->resetHistos();
}

void DIMMonitoring::lock(void)  {
  m_MonIntf->lock();
}

void DIMMonitoring::unlock(void)  {
  m_MonIntf->unlock();
}

void DIMMonitoring::startSaving(std::shared_ptr<TaskSaveTimer>& timer)   {
  m_MonIntf->startSaving(timer);
}

void DIMMonitoring::stopSaving()   {
  if (m_MonIntf!= 0)
    {
      m_MonIntf->stopSaving();
    }
}
