//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef ONLINE_DATAFLOW_HISTADDER_H
#define ONLINE_DATAFLOW_HISTADDER_H

/// Framework include files
#include <RTL/Logger.h>

/// C/C++ include files
#include <map>
#include <set>
#include <mutex>
#include <memory>
#include <vector>
#include <string>
#include <cstdint>
#include <filesystem>

// Forward declarations
class TH1;
class TFile;
class TDirectory;

/// Online namespace declaration
namespace Online   {

  /// Hists sub-namespace
  namespace histos  {

    class monitor;
    class file_generator;
    
    /// Generic adder interface
    /**
     *  \author  M.Frank
     *  \version 1.0
     */
    class hist_adder  {
    public:
      /// Monitor interface
      class monitor& mon;
      /// Output file generator
      class file_generator& out_file_gen;
      /// List of histogram files to be merged.
      std::map<int32_t, std::vector<std::string> > histogram_files;

    public:
      /// Initializing constructor
      hist_adder(monitor& mon, file_generator& output);
      /// Copy constructor
      hist_adder(const hist_adder& copy) = default;
      /// Move constructor
      hist_adder(hist_adder&& copy) = default;
      /// Default destructor
      virtual ~hist_adder() = default;
      /// Copy assignment
      hist_adder& operator= (const hist_adder& copy) = delete;
      /// Move assignment
      hist_adder& operator= (hist_adder&& copy) = delete;
      /// Merge all selected histogram files according to run number
      std::size_t merge();
    };
  }    // End namespace histos
}      // End namespace Online

#endif // ONLINE_DATAFLOW_HISTADDER_H
