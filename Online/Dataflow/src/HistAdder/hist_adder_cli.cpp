//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================

/// Framework include files
#include <hist_adder/monitor.h>
#include <hist_adder/hist_adder.h>
#include <hist_adder/file_selector.h>
#include <hist_adder/file_generator.h>
#include <dim/dis.h>

/// C/C++ include files
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cerrno>

namespace fs = std::filesystem;
#include <RPC/RpcClientHandle.h>


extern "C" int dataflow_utils_add_histogram_files(int argc, char** argv)    {
  RTL::CLI cli(argc, argv, [](int ac, char** av)  {
      std::cout << " dataflow_utils_add_histogram_files -opt [-opt]                             \n"
		<< "    -input=<input-directory>    Path to histogram data input directory      \n"
		<< "                                Will be recursively scanned for ROOT files  \n"
		<< "    -output=<output-directory>  Path to place summarized files              \n"
		<< "    -temporary=<temp-directory>  Path to place summarized files              \n"
		<< "    -task=<task-name>           Task name for output histograms             \n"
		<< "    -chunk=<number>             Chunk size for multi process execution      \n"
		<< "    -partition=<partition-name> Partition name for output histograms        \n"
		<< "    -run=<run-number>           Select single runnumber (default: all)      \n"
		<< "    -parallel                   Process runs in parallel                    \n"
		<< "    -print=<print-level>        Printout level VERBOSE, DEBUG,...,FATAL     \n"
		<< " \n"
		<< " \n"
		<< " \n";
      RTL::CLI::print_args(ac, av);
      ::exit(EINVAL);
    });
  int32_t runno = -1;
  std::size_t chunk_size = -1;
  std::string input, prt = "INFO";
  bool help = cli.getopt("help", 2) != 0;
  bool process_parallel = cli.getopt("parallel", 5) != 0;

  Online::histos::monitor        monitor;
  Online::histos::file_generator outgen(monitor);
  cli.getopt("input",     3, input);
  cli.getopt("run",       3, runno);
  cli.getopt("output",    3, outgen.path_pattern);
  cli.getopt("task",      3, outgen.task_name);
  cli.getopt("partition", 3, outgen.partition);
  cli.getopt("temporary", 5, outgen.temporary_directory);
  cli.getopt("chunk_size",3, chunk_size);
  cli.getopt("print",     3, prt);

  if ( help ||
       input.empty() ||
       outgen.partition.empty() ||
       outgen.task_name.empty() ||
       outgen.path_pattern.empty() ||
       outgen.temporary_directory.empty() )
    {
      cli.call_help();
    }

  RTL::Logger::LogDevice::setGlobalDevice(Online::histos::logger(), prt);

  std::string utgid = RTL::processName();
  Online::histos::file_selector selector(monitor, outgen);
  RTL::CLI::print_args(argc, argv);
  char c;
  std::cin >> c;

  ::dis_start_serving(utgid.c_str());
  selector.select(input, runno);
  if ( process_parallel )   {
    selector.multi_process = process_parallel;
    selector.chunk_size    = chunk_size;
    selector.merge(std::move(selector.histogram_files));
  }
  else   {
    selector.merge(std::move(selector.histogram_files));
  }
  return 0x0;
}

