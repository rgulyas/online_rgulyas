//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
// Framework include files
#include <hist_adder/processing_db.h>
#include <sqldb/sqlite.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ files
#include <system_error>

using namespace Online::histos;

/// Initializing constructor
processing_db::processing_db(const std::string& p, int dbg)
  : _debug(dbg), _path(p)
{
  this->init(_path);
}

/// Default destructor
processing_db::~processing_db() {
  this->fini();
  if ( this->database.is_valid() ) {
    this->database.commit();
    this->database.close();
  }
}

/// Initialize object
std::error_code processing_db::init(const std::string& db_name) {
  if ( !this->_inited )   {
    std::string err;
    const char* nam = db_name.c_str();
    this->database  = sqldb::database::open<sqlite>(db_name, err);
    if ( this->database.is_valid() )   {
      const char* sql = "CREATE TABLE IF NOT EXISTS Runs ("
	"RunNumber INT PRIMARY KEY, "
	"State     INT NOT NULL, "
	"StartTime INT NOT NULL, "
	"HistStart INT NOT NULL, "
	"HistTime  INT NOT NULL)";
      if ( this->database.execute(err, sql) != sqldb::OK )  {
	::lib_rtl_output(LIB_RTL_ERROR,"%s: Create table: SQL error: %s", nam, err.c_str());
	return std::make_error_code(std::errc::permission_denied);
      }
      ::lib_rtl_output(LIB_RTL_INFO,"FDB Database server: Database %s", nam);
      ::lib_rtl_output(LIB_RTL_INFO,"FDB Database server: Table FILES created successfully");
      /// Create prepared statement to insert records
      this->insert_record.prepare(database,
				  "INSERT INTO Runs (RunNumber, State, StartTime, HistStart, HistTime) "
				  "VALUES (?, ?, ?, ?, ?)");
      this->delete_record.prepare(database,
				  "DELETE FROM Runs WHERE RunNumber=?1 ");
      this->query_record_run.prepare (database,
				  "SELECT RunNumber, State, StartTime, HistStart, HistTime FROM Runs "
				  "WHERE RunNumber = ?1");
      this->query_record_state.prepare (database,
				  "SELECT RunNumber, State, StartTime, HistStart, HistTime FROM Runs "
				  "WHERE State = ?1");
      this->state_update_record.prepare (database,
				  "UPDATE Runs SET State=?1, HistTime=?2 WHERE RunNumber=?3 ");
      this->_inited = true;
      return std::make_error_code(std::errc(0));
    }
    ::lib_rtl_output(LIB_RTL_INFO,"%s: FAILED to access db: [%s]", nam, err.c_str());
    return std::make_error_code(std::errc::permission_denied);
  }
  return std::make_error_code(std::errc(0));
}

/// Finalize object
void processing_db::fini() {
  if ( this->_inited )   {
    this->insert_record.finalize();
    this->delete_record.finalize();
    this->query_record_run.finalize();
    this->query_record_state.finalize();
    this->state_update_record.finalize();
    this->database.close();
    this->_inited = false;
  }
}

/// Check the existence of a given object in the database
std::error_code
processing_db::query_run(int32_t runno, run_t& run) {
  this->query_record_run.reset();
  this->query_record_run.bind(0, runno);
  int ret = this->query_record_run.execute();
  while ( (ret = this->query_record_run.fetch_one()) == sqldb::OK ) {
    run.run_number = query_record_run.get<int>(0);
    run.state = query_record_run.get<int>(1);
    run.start = query_record_run.get<int>(2);
    run.enter = query_record_run.get<int>(3);
    run.time  = query_record_run.get<int>(4);
    this->query_record_run.reset();
    return std::make_error_code(std::errc(0));
  }
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Check the existence of a given object in the database
std::error_code
processing_db::query_runs_by_state(int32_t state, std::vector<run_t>& runs) {
  run_t run;
  this->query_record_state.reset();
  this->query_record_state.bind(0, state);
  int ret = this->query_record_state.execute();
  while ( (ret = this->query_record_state.fetch_one()) == sqldb::OK ) {
    run.run_number = query_record_state.get<int>(0);
    run.state = query_record_state.get<int>(1);
    run.start = query_record_state.get<int>(2);
    run.enter = query_record_state.get<int>(3);
    run.time  = query_record_state.get<int>(4);
    runs.emplace_back(run);
  }
  this->query_record_state.reset();
  if ( !runs.empty() )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Add a new object to the database
std::error_code processing_db::add(run_t run)  {
  this->insert_record.reset();
  if ( 0 == run.time ) run.time = ::time(0);
  this->insert_record.bind(0, run.run_number);
  this->insert_record.bind(1, run.state);
  this->insert_record.bind(2, run.start);
  this->insert_record.bind(3, run.enter);
  this->insert_record.bind(4, run.time);
  this->database.begin();
  int ret = this->insert_record.execute();
  this->database.commit();
  this->insert_record.reset();
  if ( ret == sqldb::DONE )   {
    return std::make_error_code(std::errc(0));
  }
  else if ( ret != sqldb::OK )    {
    return std::make_error_code(std::errc::file_exists);    
  }
  return std::make_error_code(std::errc(0));
}

/// Remove an object from the database
std::error_code processing_db::del(int32_t runno)  {
  this->delete_record.bind(0, runno);
  this->database.begin();
  int ret = this->delete_record.execute();
  this->delete_record.reset();
  this->database.commit();
  if ( ret == sqldb::OK )
    return std::make_error_code(std::errc(0));
  else if ( ret == sqldb::DONE )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Set new state to run record
std::error_code
processing_db::set_state(int32_t runno, int state)  {
  int32_t now = ::time(0);
  this->state_update_record.bind(0, state);
  this->state_update_record.bind(1, now);
  this->state_update_record.bind(2, runno);
  this->database.begin();
  int ret = this->state_update_record.execute();
  this->state_update_record.reset();
  this->database.commit();
  if ( ret == sqldb::DONE )
    return std::make_error_code(std::errc(0));
  else if ( ret == sqldb::OK )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Check if run record was entered previously
std::pair<std::error_code, bool> processing_db::exists(int32_t runno)    {
  run_t run;
  std::error_code ret = query_run(runno, run);
  if ( ret.value() == 0 || std::errc(ret.value()) == std::errc::no_such_file_or_directory )
    return std::make_pair(std::make_error_code(std::errc(0)), ret.value() == 0);
  return std::make_pair(ret, false);
}

#include <RTL/rtl.h>
#include <unistd.h>
#include <iostream>

namespace  {
  void help(int ac, char** av)  {
    std::cout <<
      "Usage:  histprocessing_db_init -arg [-arg]                                    \n\n"
      "     -p=<db-path>                     Create database if not existing.          \n"
      "                                                                                \n"
      "     -help              Print this help output                                  \n"       
      "     Arguments given: ";
    RTL::CLI::print_args(ac,av);
    std::cout << std::endl << std::flush;
    ::exit(EINVAL);
  }
  std::string get_path(int argc, char** argv)  {
    RTL::CLI cli(argc, argv, help);
    std::string path;
    cli.getopt("path", 1, path);
    if ( path.empty() || cli.getopt("help",4) != nullptr )   {
      cli.call_help();
      ::exit(EINVAL);
    }
    return path;
  }
}
//
/// Create new database
extern "C" int histprocessing_db_init(int argc, char** argv)   {
  std::string path = get_path(argc, argv);
  processing_db handler(path, true);
  processing_db::run_t run;
  handler.query_run(0, run);
  return 1;
}
//
/// Test filling the database
extern "C" int histprocessing_db_fill(int argc, char** argv)   {
  std::string path = get_path(argc, argv);
  processing_db handler(path, true);
  processing_db::run_t run;
  for(int32_t i=0; i<1000;++i)   {
    run.run_number = i+1000;
    run.state = 1;
    handler.add(run);
  }
  return 1;
}
//
/// Test reading back from the database
extern "C" int histprocessing_db_read(int argc, char** argv)   {
  std::string path = get_path(argc, argv);
  processing_db handler(path, true);
  for(int32_t i=0; i<1000;++i)   {
    processing_db::run_t run;
    int32_t run_number = i+1000;
    handler.query_run(run_number, run);
    std::string t1 = ctime(&run.enter);
    std::string t2 = ctime(&run.time);
    ::lib_rtl_output(LIB_RTL_ALWAYS,"Run: %6d state: %3d Time: %s %s",
		     run.run_number, run.state, 
		     t1.substr(0, t1.length()-1).c_str(),
		     t2.substr(0, t2.length()-1).c_str());
  }
  return 1;
}

/// Test updating records the database
extern "C" int histprocessing_db_update(int argc, char** argv)   {
  std::string path = get_path(argc, argv);
  processing_db handler(path, true);
  for(int32_t i=0; i<1000;++i)   {
    int32_t run_number = i+1000;
    handler.set_state(run_number, 123);
  }
  return 1;
}
