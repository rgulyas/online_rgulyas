//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Plugins.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/Plugins.h>
#include <DD4hep/Printout.h>

namespace Online {  namespace Plugins {
    using namespace std;

    /// Wrapper around DD4hep plugin mechanism to create data flow components
    DataflowComponent* createComponent(const string& id, DataflowContext& ctxt)   {
      pair<string,string> n(id,id);
      size_t idx = id.rfind('/');
      if ( idx != string::npos ) n = make_pair(id.substr(0,idx), id.substr(idx+1));
      DataflowComponent* object = dd4hep::PluginService::Create<DataflowComponent*>(n.first,n.second.c_str(),&ctxt);
      if ( !object ) {
	dd4hep::PluginDebug dbg;
	object = dd4hep::PluginService::Create<DataflowComponent*>(n.first,n.second.c_str(),&ctxt);
	if ( !object )  {
	  dd4hep::except("Dataflow","Failed to create component %s [Fatal]",id.c_str());
	}
      }
      return object;
    }
  }
}
