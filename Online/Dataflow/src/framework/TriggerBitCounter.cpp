//  ============================================================
//
//  FileWriterSvc.cpp
//  ------------------------------------------------------------
//
//  Package   : GaudiOnline
//
//  Author    : Markus Frank
//
//  ===========================================================
#include <Dataflow/TriggerBitCounter.h>
#include <MBM/Client.h>

using MBM::Requirement;
using MBM::EventDesc;
using namespace std;
using namespace Online;

TriggerBitCounter::TriggerBitCounter(const string& nam, Context& ctxt) :
  Processor(nam, ctxt)
{
  declareProperty("BackupFile",m_BackupFile="LumiCounter1.file");
  declareProperty("BackupDirectory",m_BackupDirectory="/localdisk/CounterFiles/");
  clearCounters();
}

// Standard Destructor
TriggerBitCounter::~TriggerBitCounter()
{
}

int TriggerBitCounter::initialize()  
{
  int sc = Processor::initialize();
  clearCounters();
  if ( DF_SUCCESS == sc )
    {
      declareMonitor("EvtsIn",  m_EvIn = 0, "Number of Events received.");
      return DF_SUCCESS;
    }
  return error("Failed to initialize component base class.");
}

int TriggerBitCounter::start()
{
  clearCounters();
  return Processor::start();
}

void TriggerBitCounter::clearCounters()
{
  m_EvIn = 0;
}

int TriggerBitCounter::stop()
{
  return Processor::stop();
}

int TriggerBitCounter::finalize()
{
  clearCounters();
  return Processor::finalize();
}
