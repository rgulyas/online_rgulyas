/*
 * HLTFileEqualizer.h
 *
 *  Created on: Jun 8, 2012
 *      Author: Beat Jost
 */

#ifndef HLTFILEEQUALIZER_H_
#define HLTFILEEQUALIZER_H_
#include <map>
#include <string>
#include <list>
#include <set>
#include <vector>
#include "dim/dic.hxx"
#include "dim/dis.hxx"
#define MBM_IMPLEMENTATION
#include "stdio.h"
#include "time.h"
#include <string>
using namespace std;
typedef std::pair<int,int> INCRPair;
enum ClassEnums
{
  CLASS_NONE =0,
  CLASS_SLOW ,
  CLASS_MEDIUM,
  CLASS_FAST,
  CLASS_FASTER,
  CLASS_GLOBAL,
  CLASS_SAFE
};
typedef struct
{
    float m_FDtime;
    int m_Limit;
    int m_credits;
} FDTime;
class myNode
{
  public:
    std::string m_name;
    std::string m_subfarm;
    float m_DiskUsage = 0.0;
    bool m_thrState = false;
    int m_thrDelay = 0;
    int m_DskCapacity = 0;
    int m_DskUsed = 0;
    myNode(std::string n)
    {
      m_name = n;
      m_subfarm = m_name.substr(0,6);
    };
    myNode() = default;
    myNode(const myNode& n) = default;
    myNode& operator=(const myNode& n) = default;
    float DiskUsage()
    {
      if (m_DskCapacity == 0)
      {
        return 0.0;
      }
      else
      {
        return float(m_DskUsed)/float(m_DskCapacity);
      }
    }
};
class SFarm
{
  public:
    std::string m_svcnam;
};
class DisNode
{
  public:
    DisNode(string nam, time_t t)
  {
      m_Name = nam;
      m_disTime=t;
  }
    string m_Name;
    time_t m_disTime;
};
template <class T> class MySet : public set<T>
{
  public:
    int contains(T &k)
    {
      return (this->find(k) != this->end());
    }
};
typedef std::map<std::string,myNode*> myNodeMap;
typedef std::map<std::string,std::list<std::pair<std::string,int> > > myActionMap; //list of nodes per subfarm to execute an action on.
typedef MySet<string> NodeSet;
typedef std::map<std::string,float> NodePerfMap;
typedef map<string,DisNode*> DisabledMap;
class THRCLSS
{
  public:
    ClassEnums m_type;
    float m_average;
    float m_average_prev;
    NodeSet *m_nodeSet;
    int m_totthrlimit;
    int m_NodethrLimit;
    std::string m_Name;
    int m_minThr;
    int m_maxThr;
    float m_ThrFactor;
    int m_totthrottle;
    int m_Nnodes;
    void fPrint(FILE *f)
    {
      fprintf(f,"Class %d Average disk usage %f Total Throttle %d (min %d max %d) Nodes %ld \n",
          m_type,m_average,m_totthrottle,m_minThr,m_maxThr,m_nodeSet->size());
    }
    void Clear()
    {
      m_totthrottle = 0;
      m_Nnodes = 0;
      m_minThr = 0;
      m_maxThr = 0;
    }
    THRCLSS (ClassEnums typ, NodeSet *nodeset): m_minThr(0), m_maxThr(0),m_totthrottle(0),m_Nnodes(0)
    {
      m_type = typ;
      m_nodeSet=nodeset;
      m_average = 0.0;
      m_average_prev = 0.0;
      m_NodethrLimit = 10000;
      switch (m_type)
      {
        case CLASS_SLOW:
        {
          m_Name = "Slow";
          m_totthrlimit = 0;
          m_ThrFactor = 1.0;
          break;
        }
        case CLASS_MEDIUM:
        {
          m_Name = "Medium";
          m_totthrlimit = 0;
          m_ThrFactor = 1.0;
          break;
        }
        case CLASS_FAST:
        {
          m_Name = "Fast";
          m_totthrlimit = 100000;
          m_ThrFactor = 4.0;
          break;
        }
        case CLASS_FASTER:
        {
          m_Name = "Faster";
          m_totthrlimit = 0;
          m_ThrFactor = 6.0;
          break;
        }
        case CLASS_GLOBAL:
        {
          m_Name = "Global";
          m_totthrlimit = 0;
          m_ThrFactor = 1.0;
          break;
        }
        case CLASS_SAFE:
        {
          m_Name = "Safe";
          m_totthrlimit = 0;
          m_ThrFactor = 1.0;
          break;
        }
        default:
        {
          m_Name = "None";
          m_totthrlimit = 0;
          break;
        }
      };
    };
};
typedef map<ClassEnums,THRCLSS*> ThrMap;
class HLTFileEqualizer
{
    typedef struct
    {
        int m_RunStatus;
        int m_inhibit_act;
        int m_deferState;
        int m_VeloPosition;
        int m_act;
        int m_freeze;
        int m_FreezeMEPs;
        int m_FreezeDeadt;
        int m_Holdoff;
    }ExtInpType;
  public:
    void ableAll (int value);
    void enableAll()
    {
      ableAll(0);
      return;
    }
    void disableAll(int value)
    {
      ableAll(value);
      return;
    }
    int m_Freeze;
    time_t m_holdofftime;
    ExtInpType m_Externals;
    ThrMap m_ThrottleMap;
    std::map<std::string,SFarm *> m_Farms;
    myNodeMap m_Nodes;
    NodeSet m_AllNodes;
    myNodeMap m_ThrottledNodes;
    DisabledMap m_disMap;
    std::map<std::string,DimUpdatedInfo*> m_infoMap;
    std::string m_StorageValue;
    NodeSet m_NodeSet;
    NodeSet m_SlowNodeSet;
    NodeSet m_MediumNodeSet;
    NodeSet m_FastNodeSet;
    NodeSet m_FasterNodeSet;
    int m_nnodes;
    int m_nfiles;
    long m_nfiles2;
    int m_low;
    int m_high;
    DimInfo *m_DefStateInfo;
    NodeSet m_enabledFarm;
    NodeSet m_recvNodes;
    NodeSet m_BufferrecvNodes;
    NodeSet m_exclNodes;
    NodeSet m_LHCbFarms;
    NodeSet m_AllpFarms;
    NodeSet m_AllpNodes;
    NodePerfMap m_nodePerf;
    HLTFileEqualizer();
    void Analyze();
    void Act();
    void Dump();
    void BufferDump();
    void updateServices();
    void setActing();
    void Freeze();
    void Thaw(int holdoff);
    FDTime m_FDtime;
    int m_thrDelay;
    float m_TargetSpread;
//    float m_NumRMS;
    myActionMap m_Actions;
    float m_lastGoodRMS;
    float m_RMSDecrement;
    THRCLSS *m_currentClass;
    THRCLSS *m_SafeClass;
    int m_TotLimit;
    int m_lastgoodThrLimit;
    DimService *m_configService;
    DimService *mSetService;
    DimService *m_AveragesService;
    DimService *m_Current_Settings;
    DimService *m_EnStat;
    DimService *m_Acting;
    DimService *m_VeloState;
    DimService *m_ExternalsSvc;
    DimService *m_ThrottleStatistics;
    DimService *m_IncrementParams;
    int m_totThrottle;
    float m_averageUsage;
    float m_averageUsage_prev;
    float m_L0Rate;
    float m_L0DeadTime;
    float m_LostMEP;
    bool m_frozen;
    bool m_thawing;
    std::vector<INCRPair> m_increments;
};
class LHCbThrottleInfo : public DimInfo
{
  public:
    HLTFileEqualizer *m_equalizer;
    LHCbThrottleInfo(char *name,HLTFileEqualizer *e);
    void infoHandler() override;
};
class LostMEPs:public DimInfo
{
  public:
    bool m_myfreeze;
    HLTFileEqualizer *m_equalizer;
    LostMEPs(char *name, float nolink, HLTFileEqualizer *e):DimInfo(name,nolink)
    {
      m_equalizer = e;
      m_myfreeze = false;
    }
    virtual void infoHandler() override;
};
class L0TriggerRate: public DimInfo
{
  public:
    bool m_myfreeze;
    L0TriggerRate(char *name, float nolink,HLTFileEqualizer *e):DimInfo(name,nolink)
    {
      m_equalizer = e;
      m_myfreeze=false;
    };
    HLTFileEqualizer *m_equalizer;
    virtual void infoHandler() override;
};
class L0DeadTime: public DimInfo
{
  public:
    bool m_myfreeze;
    L0DeadTime(char *name, float nolink,HLTFileEqualizer *e):DimInfo(name,nolink)
    {
      m_equalizer = e;
      m_myfreeze = false;
    };
    HLTFileEqualizer *m_equalizer;
    virtual void infoHandler() override;
};
class LHCb1RunStatus:public DimInfo
{
  public:
  LHCb1RunStatus(char *name, int nolink,HLTFileEqualizer *e);
  virtual void infoHandler() override;
  int m_nolink;
  HLTFileEqualizer *m_equalizer;
  int m_state;
  int m_prevstate;
};

//class LHCbDeferralState : public DimInfo
//{
//  public:
//    HLTFileEqualizer *m_equalizer;
//    int m_nolink;
//    LHCbDeferralState(char *name, int nolink,HLTFileEqualizer *e):DimInfo(name,nolink)
//    {
//      m_nolink = nolink;
//      m_equalizer=e;
//      return;
//    }
//    void infoHandler()
//    {
//
//      return;
//    }
//};

class DeferState : public DimInfo
{
  public:
    HLTFileEqualizer *m_equalizer;
    int m_nolink;
    DeferState(char *name, int nolink,HLTFileEqualizer *e):DimInfo(name,nolink)
    {
      m_nolink = nolink;
      m_equalizer = e;
    }
    void infoHandler() override
    {
      m_equalizer->m_Externals.m_deferState=getInt();
      return;
    }
};

class FarmStorageStatus : public DimInfo
{
  public:
    HLTFileEqualizer *m_equalizer;
    char *m_nolink;
    int m_state;
    FarmStorageStatus(char *name, char *nolink,HLTFileEqualizer *e);
    void infoHandler() override;
};
class VeloPosInfo : public DimInfo
{
  public:
    HLTFileEqualizer *m_equalizer;
    int m_VeloPos;
    VeloPosInfo(char *name, HLTFileEqualizer *e): DimInfo(name,0)
    {
      m_equalizer = e;
      m_VeloPos=0;
    }
    void infoHandler() override;
};
class ExclInfo : public DimInfo
{
  public:
    NodeSet *m_exclNodes;
    ExclInfo(char *name, NodeSet *nodeset);
    void infoHandler() override;
};
class LHCbFarms : public DimInfo
{
  public:
    HLTFileEqualizer *m_equalizer;
    LHCbFarms(char *name, HLTFileEqualizer *e): DimInfo(name,(char*)"\0")
    {
      m_equalizer=e;
    }
    void infoHandler() override;
};

class FarmDtime : public DimInfo
{
  public:
  HLTFileEqualizer *m_equalizer;
  int m_nlow;
  FarmDtime(char* name,HLTFileEqualizer *e): DimInfo(name,(void*)0,0)
  {
    m_nlow=0;
    m_equalizer=e;
    return;
  };
  void infoHandler() override;
};
class SettingsCommand : public DimCommand
{
  public:
    HLTFileEqualizer *m_equalizer;
    SettingsCommand(char *name,HLTFileEqualizer *e) : DimCommand((char*)name,(char*)"C")
    {
      m_equalizer = e;
    }
    void commandHandler() override;
};
class IncParamComm : public DimCommand
{
  public:
    HLTFileEqualizer *m_equalizer;
    IncParamComm(char *name,HLTFileEqualizer *e) : DimCommand((char*)name,(char*)"C")
    {
      m_equalizer = e;
    }
    void commandHandler() override;
};
class IntCommand : public DimCommand
{
  public:
    int *m_dest;
    IntCommand (const char *name,int *d) : DimCommand((char*)name, (char*)"I")
    {
      m_dest = d;
      return;
    };
    virtual void commandHandler() override
    {
      *m_dest = this->getInt();
      return;
    };
};
class DoCommand : public DimCommand
{
  public:
    HLTFileEqualizer *m_equalizer;
    DoCommand (const char *name,HLTFileEqualizer *e) : DimCommand((char*)name, (char*)"I")
    {
      m_equalizer = e;
      return;
    };
    virtual void commandHandler() override;
};
class FloatCommand : public DimCommand
{
  public:
    float *m_dest;
    FloatCommand (const char *name,float *d) : DimCommand((char*)name, (char*)"F")
    {
      m_dest = d;
      return;
    };
    virtual void commandHandler() override
    {
      *m_dest = this->getFloat();
      return;
    };
};
class dyn_string : public std::vector<std::string >
{
public:
  dyn_string() {};
  virtual ~dyn_string()
  {
  }
};

#endif /* HLTFILEEQUALIZER_H_ */
