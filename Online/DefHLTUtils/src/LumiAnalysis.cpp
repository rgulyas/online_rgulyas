/*
 * LumiAnalysis.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: beat
 */
#include <map>
#include <vector>
#include <list>
#include <ctime>
#include <string>
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include <set>
#include "errno.h"
#include <dirent.h> // directory header

#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
//#include <filesystem>
typedef unsigned long        CounterType[4];
using namespace std;
//using namespace std::experimental::filesystem;
using namespace boost::filesystem;
namespace
{
  template<typename T> class RunListItem
  {
    public:
      int m_runno;
      T m_count;
      time_t m_LastCnt;
      RunListItem(int runno, const T &count, time_t LastCnt)
      {
        m_runno = runno;
        m_count = count;
        m_LastCnt = LastCnt;
      }
      RunListItem(int runno)
      {
        ::memset(m_count, 0, sizeof(T));
        m_runno = runno;
        m_LastCnt = 0;
      }
    void reset(int runno)
      {
        ::memset(m_count, 0, sizeof(T));
        m_runno = runno;
        m_LastCnt = 0;
      }
  };

  typedef RunListItem<CounterType> LumiRunItem;
  template<typename T> class RunMap: public map<unsigned int, void*>
  {
    public:
      FILE *m_f;
      string m_fn;
      int m_Fdesc;
      void *m_RunPtr;
      size_t m_secsiz;
      RunListItem<T> *m_lastPtr;
      RunListItem<T> *m_CurrpTR;
      RunMap()
      {
        m_RunPtr = 0;
        m_lastPtr = 0;
        m_CurrpTR = 0;
        m_Fdesc = 0;
        size_t page_size = (size_t) sysconf (_SC_PAGESIZE);
        m_secsiz = (10000*sizeof(RunListItem<T>)+page_size)& ~page_size;
      }
      void SetFile(string fn)
      {
        using namespace boost::filesystem;
        m_fn = fn;
        string dyr = path(m_fn).parent_path().string();
        try
        {
          create_directories(dyr);
        } catch (...)
        {
          ::printf( "Cannot Create Directory %s\n",
              dyr.c_str());
        }
      }
      void OpenBackingStore()
      {
        size_t page_size = (size_t) sysconf (_SC_PAGESIZE);
        m_secsiz = (10000*sizeof(RunListItem<T>)+page_size)& ~page_size;
        m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT+O_EXCL,S_IRWXU+S_IRWXG+S_IRWXO);
        if (m_Fdesc >=0)  // File didn't exist, was created..
        {
          close(m_Fdesc);
          int status = truncate(m_fn.c_str(),m_secsiz);
          if (status != 0)
          {
            int ierr = errno;
            ::printf( "Cannot extend file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT,S_IRWXU+S_IRWXG+S_IRWXO);
          if (m_Fdesc < 0)
          {
            int ierr = errno;
            ::printf( "Cannot Open file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          m_RunPtr = mmap(0,m_secsiz,PROT_READ+PROT_WRITE,MAP_SHARED,m_Fdesc,0);
          if (m_RunPtr==MAP_FAILED)
          {
            int ierr = errno;
            ::printf( "Cannot map file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          memset(m_RunPtr,0,m_secsiz);
          this->clear();
          m_lastPtr = (RunListItem<T> *)m_RunPtr;
        }
        else
        {
          this->clear();
          m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT,S_IRWXU+S_IRWXG+S_IRWXO);
          if (m_Fdesc <0)
          {
            ::printf( "Cannot Open file %s\n",m_fn.c_str());
            m_RunPtr = 0;
          }
          else
          {
            m_RunPtr = mmap(0,m_secsiz,PROT_READ+PROT_WRITE,MAP_SHARED,m_Fdesc,0);
            RunListItem<T> *rptr = (RunListItem<T> *)m_RunPtr;
            m_lastPtr = rptr;
            while (rptr->m_runno >0)
            {
              insert(make_pair(rptr->m_runno,rptr));
              rptr++;
              m_lastPtr = rptr;
            }
          }
        }
      }
      void closeBackingStore()
      {
        munmap(0,m_secsiz);
        close(m_Fdesc);
      }
    RunListItem<T> *NewEntry(int rn)
    {
      if (m_Fdesc == 0 && m_lastPtr == 0)
      {
        m_lastPtr = (RunListItem<T> *)malloc(m_secsiz);
      }
      RunListItem<T> *p = m_lastPtr;
      m_lastPtr = p+1;
      p->reset(rn);
      return p;
    }
    void Sync()
    {
      msync((void*)m_RunPtr,m_secsiz,MS_ASYNC);
    }
  };
}
void ReadDir(string &dyr,vector<string> &fils)
{
  struct dirent *pent;
  DIR *pdir = opendir(dyr.c_str());
//  path dirP =path(dyr);
  while ((pent = readdir (pdir))) // while there is still something in the directory to list
  {
    string fname = pent->d_name;
    if (fname == "." || fname == "..")
    {
      continue;
    }
    fils.push_back(dyr+fname);
  }
  closedir(pdir);
}
void FillSummMap(vector<string> &fils, RunMap<CounterType> &Summ_RunMap)
{
  for (auto &i : fils)
  {
//    string fn=i.path().filename().string();
    RunMap<CounterType> *fRMap = new RunMap<CounterType>();
    fRMap->SetFile(i);
    fRMap->OpenBackingStore();
    for (auto j=fRMap->begin();j != fRMap->end();j++)
    {
      auto oit = Summ_RunMap.find(j->first);
      LumiRunItem *sri;
      if (oit == Summ_RunMap.end())
      {
        sri = Summ_RunMap.NewEntry(j->first);
        Summ_RunMap[j->first] = sri;
        for (size_t k = 0;k<sizeof(CounterType)/sizeof(unsigned long);k++)
        {
           sri->m_count[k] = ((LumiRunItem*)j->second)->m_count[k];
        }
      }
      else
      {
        sri = (LumiRunItem*)oit->second;
        for (size_t k = 0;k<sizeof(CounterType)/sizeof(unsigned long);k++)
        {
           sri->m_count[k] += ((LumiRunItem*)j->second)->m_count[k];
        }
      }
    }
    fRMap->closeBackingStore();
    delete fRMap;
  }
}

int main(int argc, char *argv[])
{
  vector<string> ffils;
  vector<string> sfils;
  RunMap<CounterType> farmSumm_RunMap;
  RunMap<CounterType> storeSumm_RunMap;
  set<unsigned int> PhysRuns;
  string dyr;
  string RDir;
  if (argc >1)
  {
    RDir = argv[1];
  }
  else
  {
   return 0;
  }
  dyr = RDir+"/Farm/";
  ReadDir(dyr,ffils);
  dyr = RDir+"/Storage/";
  ReadDir(dyr,sfils);
//  Summ_RunMap.SetFile(dyr+"LumiSummary");
//  directory_iterator diriter(dirp);
  FillSummMap(ffils, farmSumm_RunMap) ;
  FillSummMap(sfils, storeSumm_RunMap) ;
  FILE *f = fopen("/home/beat/PhysicsRuns.csv","r");
  unsigned int run;
  char xxx[2018];
  fgets(xxx,sizeof(xxx),f);
  while(fgets(xxx,sizeof(xxx),f) != 0)
  {
    sscanf(xxx,"%u",&run);
    PhysRuns.insert(run);
  }
  fclose(f);
  for (auto j=storeSumm_RunMap.begin();j!= storeSumm_RunMap.end();j++)
  {
    LumiRunItem* rj = (LumiRunItem*)(j->second);
    if (PhysRuns.find(j->first) == PhysRuns.end())
    {
      continue;
    }
    auto i=farmSumm_RunMap.find(j->first);
    if (i == farmSumm_RunMap.end())
    {
      printf ("Weird... Run %d in storage but not in farm...",j->first);
      continue;
    }
    LumiRunItem* ri = (LumiRunItem*)(i->second);
    unsigned long tots=0;
    unsigned long totf=0;
    printf ("Run %u Lumi Counters\tStorage\t",j->first);
    for(int k = 0;k<4;k++)
    {
      tots += rj->m_count[k];
      printf (" %9ld",rj->m_count[k]);
    }
    printf (" %9ld\n",tots);
    printf ("\t\t\t\tFarm\t");
    for(int k = 0;k<4;k++)
    {
      totf += ri->m_count[k];
      printf (" %9ld",ri->m_count[k]);
    }
    printf (" %9ld %f\n",totf,float(long(ri->m_count[3]-rj->m_count[3]))/ri->m_count[3]);
  }
  return 0;
}
