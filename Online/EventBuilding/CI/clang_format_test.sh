#!/bin/bash

modified_files=$(git ls-files -m)
if [[ -n $modified_files ]]; then
  echo "ERROR this script must be applied to a clean git state"
  exit 2
fi

find . -name '*.cpp' -o -name '*.hpp'  -exec clang-format -i {} \;

modified_files=$(git ls-files -m)

if [[ -z $modified_files ]]; then
  # send a negative message to gitlab
  echo "Excellent. **VERY GOOD FORMATTING!** :thumbsup:"
  exit 0;
else
  echo "The following files have clang-format problems (showing patches)";
  for f in $modified_files; do
      echo $f
      git diff $f
  done
fi

git reset HEAD --hard

exit 1
