#[=======================================================================[.rst:
Online/EventBuilding
--------------------
#]=======================================================================]

#

# TODO NO_CACHE is currently no supported by CMake < 3.21 check what it does in the future
# option(USE_SHARP "Enables SHARP support (experimental)" ON)

set(USE_SHAP OFF)

include(CheckSymbolExists)

if (PCIE40_FOUND AND MPI_CXX_FOUND AND HAVE_EB)
  message(STATUS "++ Building REAL EventBuilding library and components.....")

  list(APPEND CMAKE_PREFIX_PATH /usr)
  pkg_check_modules(glib2 glib-2.0 REQUIRED IMPORTED_TARGET)
  pkg_check_modules(numa numa REQUIRED IMPORTED_TARGET)
  pkg_check_modules(sharp sharp IMPORTED_TARGET) 
  list(REMOVE_ITEM CMAKE_PREFIX_PATH /usr)

  list(APPEND CMAKE_REQUIRED_LIBRARIES ibverbs)
  check_symbol_exists(ibv_reg_dmabuf_mr "infiniband/verbs.h" DMABUF_ENABLED)
  list(REMOVE_ITEM CMAKE_REQUIRED_LIBRARIES ibverbs)

  if(DMABUF_ENABLED)
    message(STATUS "++ DMABUFF supported")
  else()
    message(STATUS "++ DMABUFF not supported")
  endif()

  if(USE_SHARP)
    message(STATUS "++ using sharp")
  else()
    message(STATUS "++ NOT using sharp")
  endif()

  if(NOT sharp_FOUND AND USE_SHARP)
    message(WARNING "Sharp not found building without sharp.")
    set(USE_SHARP FALSE)
  endif()


  #If we want to globally rename the DD4hep namespace to Online, enable the line below !
  add_definitions(-DDD4hep=Online)
  
  #-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
  #EventBuilding basic client library
  #Note : The ROOT dependency is only required to declare Monitors of type THX
  #-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
  set(INFINIBAND_LIBRARIES ibverbs pthread)
  set(INFINIBAND_DIRS include/infiniband_net)

  online_library(EventBuildingLib
              src/BU.cpp
              src/buffer_interface.cpp
              src/buffer_writer_error.cpp
              src/bw_mon.cpp
              src/circular_buffer_status.cpp
              src/dummy_mep_buffer_writer.cpp
              src/dummy_mfp_buffer.cpp
              src/events_dispatch
              src/file_mmap.cpp
              src/generic_block.cpp
              src/infiniband_net
              src/logger.cpp
              src/mdf_reader.cpp
              src/mdf_tools.cpp
              src/MEP_injector.cpp
              src/MEP_tools.cpp
              src/MFP_builder.cpp
              src/MFP_generator.cpp
              src/MFP_preloader.cpp
              src/MFP_tools.cpp
              src/Parallel_Comm.cpp
              src/pcie40_reader.cpp
              src/pcie40_reader_error.cpp
              src/raw_tools.cpp
              src/RU.cpp
              src/shared_mem_buffer_backend.cpp
              src/shared_ptr_buffer_backend.cpp
              src/timer.cpp
              src/tools.cpp
              src/transport_unit.cpp
              # TODO this should removed from the standard EB build
              src/events_dispatch/Builder_dummy_unit.cpp
              src/events_dispatch/common.cpp
              src/events_dispatch/Filter_unit.cpp
              src/events_dispatch/Manager_unit.cpp
              src/events_dispatch/MBM_reader_dummy_unit.cpp
              src/events_dispatch/Output_unit.cpp
              src/infiniband_net/ib_collective.cpp
              src/infiniband_net/ib_comms.cpp
              src/infiniband_net/ib.cpp
              src/infiniband_net/ib_polling.cpp
              src/infiniband_net/ib_rdma_ops.cpp
              src/infiniband_net/ib_sync.cpp
              src/infiniband_net/parser.cpp
              src/infiniband_net/proc.cpp
              src/infiniband_net/sock.cpp
          )
  target_link_libraries(EventBuildingLib
      PUBLIC  Online::dim
              Online::OnlineBase
              Online::DataflowLib
              Online::EventData
              PkgConfig::glib2
              PkgConfig::numa
              MPI::MPI_CXX
              lhcb_pcie40
              ${INFINIBAND_LIBRARIES}
      PRIVATE -lrt)
  target_include_directories(EventBuildingLib BEFORE PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/private_include
    ${CMAKE_CURRENT_SOURCE_DIR}/private_include/infiniband_net)
  target_compile_definitions(EventBuildingLib PRIVATE HAVE_PCIE40)
  # enable sharp build
  if(USE_SHARP)
    target_link_libraries(EventBuildingLib PUBLIC PkgConfig::sharp)
    target_compile_definitions(EventBuildingLib PRIVATE EB_USE_SHARP)
  endif()

  if(DMABUF_ENABLED)
    target_compile_definitions(EventBuildingLib PRIVATE IB_DMABUF_SUPPORT)
  endif()

  target_compile_options(EventBuildingLib PRIVATE -Wno-sign-compare -Wno-unused -Wno-range-loop-construct)
  target_compile_features(EventBuildingLib PRIVATE cxx_std_20)
  if(NOT CMAKE_CXX_COMPILER_ID STREQUAL  "Clang")
    target_compile_options(EventBuildingLib PRIVATE -fconcepts)
  endif()

  # ---------------------------------------------------------------------------------------
  #   Commonly used DataflowExample components
  # ---------------------------------------------------------------------------------------
  online_module(EventBuilding components/Components.cpp)
  target_link_libraries(EventBuilding PRIVATE Online::EventBuildingLib Online::DataflowLib)
  target_include_directories(EventBuilding BEFORE PRIVATE
    ${PCIE_40_INCLUDE_DIRS}
    ${CMAKE_CURRENT_SOURCE_DIR}/private_include
    ${CMAKE_CURRENT_SOURCE_DIR}/private_include/infiniband_net)
  target_compile_features(EventBuilding PRIVATE cxx_std_20)
  #
  if(NOT CMAKE_CXX_COMPILER_ID STREQUAL  "Clang")
    target_compile_options(EventBuilding PRIVATE -fconcepts)
  endif()
  message(STATUS "++ PCIE40 found: ${PCIE_40_INCLUDE_DIRS}")
else()
  message(STATUS "++ PCIE40 NOT found: Building only dummy EventBuildingLib.....")
  online_library(EventBuildingLib
                      src/mdf_tools.cpp
                      src/MEP_tools.cpp
                      src/MFP_tools.cpp
                      src/raw_tools.cpp
                      src/generic_block.cpp
                      src/tools.cpp
                      src/file_mmap.cpp
		    )
  target_link_libraries(EventBuildingLib PUBLIC  Online::EventData)
endif()
foreach(name IN ITEMS MFP_decoder MEP_decoder MEP_analizer MEP_to_csv)
    add_executable(${name} main/${name}.cpp src/options.cpp)
    target_link_libraries(${name} Online::EventBuildingLib) 
    target_include_directories(${name} BEFORE PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/private_include)
    target_compile_features(${name} PRIVATE cxx_std_20)
    install(TARGETS ${name} RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}")
endforeach(name IN ITEMS)
online_env(SET "EVENTBUILDINGROOT" "${CMAKE_CURRENT_SOURCE_DIR}")
online_install_includes(include)

