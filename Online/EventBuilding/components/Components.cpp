//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Author     : M.Frank & R.Krawczyk
//  Created    : 10/6/2020
//
//==========================================================================

// Framework includes
#include "Dataflow/Plugins.h"

#include "EventBuilding/RU.hpp"
#include "EventBuilding/BU.hpp"
#include "EventBuilding/MFP_generator.hpp"
#include "events_dispatch/Builder_dummy_unit.hpp"
#include "events_dispatch/Manager_unit.hpp"
#include "events_dispatch/Filter_unit.hpp"
#include "events_dispatch/Output_unit.hpp"
#include "events_dispatch/MBM_reader_dummy_unit.hpp"

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(EB, EB_RU, RU)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(EB, EB_BU, BU)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(EB, MFP_generator, MFP_generator)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online, EB_dummy_builder_unit, Builder_dummy_unit)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online, EB_manager_unit, Manager_unit)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online, EB_filter_unit, Filter_unit)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online, EB_output_unit, Output_unit)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online, EB_MBM_reader_dummy_unit, MBM_reader_dummy_unit)
