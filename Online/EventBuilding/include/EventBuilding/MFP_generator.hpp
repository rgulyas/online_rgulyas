#ifndef MFP_GENERATOR_H
#define MFP_GENERATOR_H 1

#include <string>
#include <random>
#include <functional>
#include <mutex>

#include "Dataflow/DataflowComponent.h"
#include "MFP_tools.hpp"
#include "shmem_buffer_writer.hpp"
#include "timer.hpp"
#include "logger.hpp"

namespace EB {
  class MFP_generator : public Online::DataflowComponent {
  public:
    MFP_generator(const std::string& nam, Context& ctxt);
    virtual ~MFP_generator();

    /// Service implementation: initialize the service
    virtual int initialize() override;
    /// Service implementation: start of the service
    virtual int start() override;
    /// Service implementation: stop the service
    virtual int stop() override;
    /// Service implementation: finalize the service
    virtual int finalize() override;

    /// Cancel I/O operations of the dataflow component
    virtual int cancel() override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause() override;
    /// Incident handler implementation: Inform that a new incident has occurredd
    virtual void handle(const Online::DataflowIncident& inc) override;
    /// IRunable implementation : Run the class implementation
    virtual int run() override;

  private:
    void configure();

    Log_stream logger;

    bool _generate = false;

    // properties
    unsigned _prop_n_banks;
    std::string _prop_src_id;
    unsigned _prop_align;
    unsigned _prop_block_version;
    unsigned _prop_bank_type;
    unsigned int _run_number;
    // In the dummy MFPs the banks have all the same size
    unsigned _prop_bank_size;
    // Enable the test pattern generation (This mode may slow down performance)
    bool _enable_test_pattern;
    int _max_gen_per_node;
    // TODO implement a proper run change
    // changes run every n_MFPs, 0 disables the run change
    unsigned _n_MFPs_per_run;
    // number of seconds between the ond of a run and the beginning the next one in seconds
    unsigned _change_run_sleep;

    // Event rate in Hz
    double _event_rate;

    // UTGID of the SODIN data generator
    std::string _sodin_utgid;
    unsigned _prop_sodin_block_version;
    double _sodin_error_injection_prob;
    unsigned _sodin_error_injection_burst;
    bool _sodin_error_injection;

    // Random sizes props
    bool _random_sizes;
    unsigned _n_preloaded_sizes; // number of preloaded sizes used when generating the banks
    unsigned _random_seed;
    int _lambda; // lambda of the poisson distributions
    int _offset; // offset fo the poisson distribution
    int _multi;  // multiplier of the poisson distribution
    // total function _offset + poisson(_lambda)*_multi

    // subdetector specific random props
    bool _sub_detector_specific_run;

    // Override list for the various SDs, the UTGID will use the specified SD instead the default one
    std::vector<std::string> _velo_utigid_override;
    std::vector<std::string> _scifi_utigid_override;
    std::vector<std::string> _ut_utigid_override;
    std::vector<std::string> _muon_utigid_override;
    std::vector<std::string> _hcal_utigid_override;
    std::vector<std::string> _ecal_utigid_override;
    std::vector<std::string> _rich1_utigid_override;
    std::vector<std::string> _rich2_utigid_override;

    int _velo_lambda; // lambda of the poisson distributions
    int _velo_offset; // offset fo the poisson distribution
    int _velo_multi;  // multiplier of the poisson distribution

    int _scifi_lambda; // lambda of the poisson distributions
    int _scifi_offset; // offset fo the poisson distribution
    int _scifi_multi;  // multiplier of the poisson distribution

    int _ut_lambda; // lambda of the poisson distributions
    int _ut_offset; // offset fo the poisson distribution
    int _ut_multi;  // multiplier of the poisson distribution

    int _muon_lambda; // lambda of the poisson distributions
    int _muon_offset; // offset fo the poisson distribution
    int _muon_multi;  // multiplier of the poisson distribution

    int _hcal_lambda; // lambda of the poisson distributions
    int _hcal_offset; // offset fo the poisson distribution
    int _hcal_multi;  // multiplier of the poisson distribution

    int _ecal_lambda; // lambda of the poisson distributions
    int _ecal_offset; // offset fo the poisson distribution
    int _ecal_multi;  // multiplier of the poisson distribution

    int _rich1_lambda; // lambda of the poisson distributions
    int _rich1_offset; // offset fo the poisson distribution
    int _rich1_multi;  // multiplier of the poisson distribution

    int _rich2_lambda; // lambda of the poisson distributions
    int _rich2_offset; // offset fo the poisson distribution
    int _rich2_multi;  // multiplier of the poisson distribution

    // prop buffer size is in GB
    size_t _prop_buffer_size;
    size_t _buffer_size;
    std::string _shmem_name;
    std::vector<int> _numa_layout;
    bool _reconnect;
    int _stop_timeout;

    // Performance counters
    uint32_t _throttle_counter;

    // MFP header fields
    uint16_t _n_banks;
    uint64_t _ev_id;
    uint16_t _src_id;
    uint8_t _align;
    uint8_t _block_version;

    unsigned int _sleep_interval_ns;

    EB::bank_type_type _bank_type;
    // In the dummy MFPs the banks have all the same size
    EB::bank_size_type _bank_size;

    std::vector<EB::bank_size_type> _bank_sizes;
    std::minstd_rand _generator;

    int init_sd();
    void init_sodin();
    void init_sizes();
    void init_sd_run();
    void write_MFP();
    void write_ODIN_banks(EB::MFP* MFP);
    void write_test_pattern(EB::MFP* MFP);
    void write_end_run();
    int config_numa();
    void reset_counters();

    int _numa_node;

    bool _is_sodin;

    // DF monitoring counters
    // TODO remove duplicates
    uint64_t _DF_events_out;
    uint64_t _DF_events_in;
    uint64_t _DF_events_err;

    // number of MFP counter
    size_t _n_MFPs_run;

    Shmem_buffer_writer<EB::MFP> _buffer;

    Timer _total_timer;
    Timer _random_gen_timer;
    Timer _MFP_write_timer;
    Timer _dead_time_timer;
    uint64_t _last_report_ev_id;
    int _local_id;
    std::string _sub_detector;

    std::timed_mutex _loop_lock;
    std::timed_mutex _start_lock;
  };
} // namespace EB

#endif // MFP_GENERATOR_H
