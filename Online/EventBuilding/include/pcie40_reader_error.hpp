#ifndef PCIE40_READER_ERROR_H
#define PCIE40_READER_ERROR_H 1
#include <system_error>
#include <string>

namespace EB {
  enum class PCIe40_errors {
    ID_OPEN = 1,
    ID_FIND,
    STREAM_OPEN,
    META_STREAM_OPEN,
    META_STREAM_ENABLE,
    META_STREAM_DISABLE,
    CTRL_STREAM_OPEN,
    STREAM_GET_ENABLED,
    STREAM_ENABLED,
    STREAM_LOCK,
    STREAM_SIZE,
    STREAM_MAP,
    DMABUF_OPEN,
    HOST_READ_OFF,
    HOST_FREE_BUFF,
    HOST_BYTES_USED,
    GET_NAME,
    DEVICE_NOT_OPEN,
    SET_META_PACKING,
    LOGIC_RESET,
    CORRUPTED_DATA,
    CORRUPTED_EV_ID,
    INVALID_PACKING_FACTOR,
    INVALID_TRUNCATION_THRESHOLD,
    INTERNAL_BUFFER_FULL,
    INTERNAL_BUFFER_FULL_FLUSH,
  };

  std::error_code make_error_code(PCIe40_errors ec);

  // enum class

} // namespace EB

namespace std {
  template<>
  struct is_error_code_enum<EB::PCIe40_errors> : true_type {};

} // namespace std

#endif // PCIE40_READER_ERROR_H
