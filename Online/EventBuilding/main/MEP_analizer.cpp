#include "EventBuilding/MEP_tools.hpp"
#include "EventBuilding/MFP_tools.hpp"
#include "EventBuilding/file_mmap.hpp"
#include "PCIE40Data/RawBank40.h"
#include "PCIE40Data/sodin.h"
#include "options.hpp"
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <tuple>
#include <numeric>
#include <cmath>
#include <bitset>
#include <fstream>
#include <sys/mman.h>

// TODO this is temporary
template<class T>
struct stats {
  T sum = 0;
  T sum_square = 0;
  T min = std::numeric_limits<T>::max();
  T max = std::numeric_limits<T>::min();
  size_t n_elems = 0;
  void append(T value)
  {
    sum += value;
    sum_square += value * value;
    min = std::min(min, value);
    max = std::max(max, value);
    n_elems++;
  }

  double calc_mean() const { return static_cast<double>(sum) / n_elems; }

  double calc_stdev() const
  {
    double mean = calc_mean();
    return std::sqrt(static_cast<double>(sum_square) / n_elems - mean * mean);
  }

  std::string print() const
  {
    std::stringstream ret;
    ret << "mean " << calc_mean() << "\n"
        << "stdev " << calc_stdev() << "\n"
        << "min " << min << "\n"
        << "max " << max << "\n"
        << "n elements " << n_elems;

    return ret.str();
  }

  std::string print_csv() const
  {
    std::stringstream ret;
    ret << calc_mean() << "," << calc_stdev() << "," << min << "," << max << "," << n_elems;

    return ret.str();
  }

  static std::string print_csv_header(const std::string& prefix = "")
  {
    std::stringstream ret;

    ret << prefix << "mean"
        << "," << prefix << "stdev"
        << "," << prefix << "min"
        << "," << prefix << "max"
        << "," << prefix << "n_elems";

    return ret.str();
  }
};

template<class T>
class Histo {
public:
  Histo() = default;
  Histo(T min, T max, size_t n_bins) : _bin_edges(n_bins), _bin_contents(n_bins + 2, 0)
  {
    _bin_width = (max - min) / n_bins;
    std::generate(_bin_edges.begin(), _bin_edges.end(), [n = 0, min, bin_width = this->_bin_width]() mutable {
      return min + n++ * bin_width;
    });
  }
  //   Histo(T min, T max, T bin_width) : _bin_width(bin_width)
  // {

  //   _bin_width =
  //     (max - min) / n_bins std::generate(_bin_edges.first(), _bin_edges.end(), [n = 0, min, _bin_width]() mutable {
  //       return min + n++ * _bin_width
  //     });
  // }
  // Histo(std::vector<T>bins) : _bin_edges(bins) {}

  void add_value(T value)
  {
    size_t bin;
    if (value < _bin_edges[0]) {
      bin = _bin_edges.size();
    } else if (value > _bin_edges[_bin_edges.size() - 1] + _bin_width) {
      bin = _bin_edges.size() + 1;
    } else {
      bin = static_cast<size_t>((value - _bin_edges[0]) / _bin_width);
    }
    _bin_contents[bin]++;
  }
  std::vector<size_t> get_bins_content(bool get_overflow = true, bool get_underflow = true) const
  {
    std::vector<size_t> ret_val(_bin_contents.size());

    std::copy(_bin_contents.begin(), _bin_contents.end(), ret_val.begin());
    if (!get_overflow) {
      // remove overflow and underflow accordingly
      ret_val.pop_back();
      if (!get_underflow) {
        ret_val.pop_back();
      }
    } else if (!get_underflow) {
      // swaps underflow and overflow bins and discards underflow
      std::iter_swap(ret_val.end() - 1, ret_val.end() - 2);
      ret_val.pop_back();
    }

    return ret_val;
  }
  std::vector<T> get_bins_edges() const { return _bin_edges; }

  std::string print_csv() const
  {
    std::stringstream ret_val;
    ret_val << "bin_edges,bin_content\n";
    ret_val << "underflow," << _bin_contents[_bin_contents.size() - 2] << "\n";
    for (size_t bin_idx = 0; bin_idx < _bin_edges.size(); bin_idx++) {
      ret_val << _bin_edges[bin_idx] << "," << _bin_contents[bin_idx] << "\n";
    }
    ret_val << "overflow," << _bin_contents[_bin_contents.size() - 1] << "\n";

    return ret_val.str();
  }

private:
  T _bin_width;
  std::vector<T> _bin_edges;
  std::vector<size_t> _bin_contents;
};

template<class T>
stats<T> combine_stats(const stats<T>& a, const stats<T>& b)
{
  double mean_weight_a = (1 / a.stdev) * (1 / a.stdev);
  double mean_weight_b = (1 / b.stdev) * (1 / b.stdev);
  size_t total_size = a.n_elems + b.n_elems;
  double mean = (a.mean * mean_weight_a + b.mean * mean_weight_b) / (mean_weight_a + mean_weight_b);
  double stdev = std::sqrt(
    (a.n_elems * a.stdev * a.stdev + b.n_elems * b.stdev * b.stdev) / (total_size) +
    a.n_elems * b.n_elems * (a.mean - b.mean) * (a.mean - b.mean) / (total_size * total_size));
  return stats(mean, stdev, std::min(a.min, b.min), std::max(a.max, b.max), total_size);
}

template<class T>
stats<T> calc_stats(const T* v, size_t size)
{

  double sum = std::accumulate(v, v + size, 0.0);
  double mean = sum / size;

  double sq_sum = std::inner_product(v, v + size, v, 0.0);
  double stdev = std::sqrt(sq_sum / size - mean * mean);

  T min = *(std::min_element(v, v + size));
  T max = *(std::max_element(v, v + size));

  return {mean, stdev, min, max, size};
}

typedef std::map<EB::src_id_type, stats<size_t>> sys_stat_map;
typedef std::map<EB::src_id_type, Histo<double>> sys_histo_map;

int main(int argc, char** argv)
{
  int ret_val = 0;
  std::string file_names;
  std::string histo_base_name("");
  int bin_min = 1;
  int bin_max = 500;
  int n_bins = 500;
  bool source_stats = false;
  bool bx_types = false;
  bool csv_out = false;
  void* read_ptr;
  EB::File_mmap file;
  EB::Options opt_parser(argc, argv);
  opt_parser.add_option("file_name", 'f', "MEP file names csv", true, file_names);
  // opt_parser.add_counter("verbose", 'v', "Increase verbosity", false, verbose);
  // opt_parser.add_flag("sodin", 's', "Decodes ODIN banks (if present)", false, SODIN_decode);
  // opt_parser.add_flag("sizes", 'S', "Prints statistics on the bank sizes", false, sizes_stats);
  opt_parser.add_flag("bx_types", 'b', "Prints statistics on the bx types", false, bx_types);
  opt_parser.add_flag("source_stats", 's', "Prints statistics on the source ids", false, source_stats);
  opt_parser.add_flag("csv", 'c', "Outputs in csv format", false, csv_out);
  opt_parser.add_option("histo", 'H', "Size distribution histograms base name", false, histo_base_name);
  opt_parser.add_option("min", 'm', "Size distribution histograms min bin", false, bin_min);
  opt_parser.add_option("max", 'M', "Size distribution histograms max bin", false, bin_max);
  opt_parser.add_option("n_bins", 'n', "Size distribution histograms number of bins", false, n_bins);
  // opt_parser.add_flag("dump_sizes", 'S', "Prints statistics on the bank sizes", false, sizes_stats);
  // opt_parser.add_flag("error", 'e', "Detects Tell40 error banks", false, detect_tell40_err);
  // opt_parser.add_flag(
  //   "error_only", 'E', "Detects Tell40 error banks and suppresses other output", false, detect_tell40_err);
  if (!opt_parser.parse_args()) {
    opt_parser.print_usage();
    return 1;
  }

  std::vector<std::string> files = EB::str_split(file_names, ",");

  sys_stat_map system_sizes_MFP_stats;
  sys_histo_map system_sizes_MFP_histo;
  sys_stat_map system_sizes_events_stats;
  sys_histo_map system_sizes_events_histo;
  std::array<sys_stat_map, 4> system_types_sizes_events_stats;
  std::array<sys_histo_map, 4> system_types_sizes_events_histo;
  for (const auto& file_name : files) {
    try {
      file = EB::File_mmap(file_name);
    } catch (std::exception& e) {
      std::cerr << argv[0] << ": " << e.what() << std::endl;
      return errno;
    } catch (...) {
      std::cerr << argv[0] << ": unable to open file " << file_name << " unexpected exception" << std::endl;
      return -2;
    }

    read_ptr = file.get_read();
    while (file.update_read(sizeof(EB::MEP_header))) {
      const EB::MEP* curr_MEP = reinterpret_cast<const EB::MEP*>(read_ptr);
      if (file.update_read(curr_MEP->bytes() - sizeof(EB::MEP_header)) == NULL) {
        std::cout << "End of file reached truncated MEP" << std::endl;
        break;
      }
      auto bx_types_list = curr_MEP->get_bx_types();

      for (const auto& curr_MFP : *curr_MEP) {
        auto sys_id = EB::get_sys_id(curr_MFP.header.src_id);
        auto MFP_stats_it =
          system_sizes_MFP_stats.insert(sys_stat_map::value_type(sys_id, sys_stat_map::mapped_type()));
        MFP_stats_it.first->second.append(curr_MFP.bytes());

        auto MFP_histo_it = system_sizes_MFP_histo.insert(
          sys_histo_map::value_type(sys_id, sys_histo_map::mapped_type(bin_min * 30000, bin_max * 30000, n_bins)));
        MFP_histo_it.first->second.add_value(curr_MFP.bytes());

        auto events_stats_it =
          system_sizes_events_stats.insert(sys_stat_map::value_type(sys_id, sys_stat_map::mapped_type()));

        auto events_histo_it = system_sizes_events_histo.insert(
          sys_histo_map::value_type(sys_id, sys_histo_map::mapped_type(bin_min, bin_max, n_bins)));

        auto sizes = curr_MFP.header.bank_sizes();

        std::array<sys_stat_map::iterator, 4> bx_types_stats_it;
        std::array<sys_histo_map::iterator, 4> bx_types_histo_it;
        if (bx_types) {
          for (size_t bx_type = 0; bx_type < bx_types_stats_it.size(); bx_type++) {
            bx_types_stats_it[bx_type] = system_types_sizes_events_stats[bx_type]
                                           .insert(sys_stat_map::value_type(sys_id, sys_stat_map::mapped_type()))
                                           .first;

            bx_types_histo_it[bx_type] =
              system_types_sizes_events_histo[bx_type]
                .insert(sys_histo_map::value_type(sys_id, sys_histo_map::mapped_type(bin_min, bin_max, n_bins)))
                .first;
          }
        }
        for (size_t bank_num = 0; bank_num < curr_MFP.header.n_banks; bank_num++) {
          events_stats_it.first->second.append(sizes[bank_num]);
          events_histo_it.first->second.add_value(sizes[bank_num]);
          if (bx_types) {
            bx_types_stats_it[bx_types_list[bank_num]]->second.append(sizes[bank_num]);
            bx_types_histo_it[bx_types_list[bank_num]]->second.add_value(sizes[bank_num]);
          }
        }
      }

      read_ptr = file.get_read();
    }
  }

  if (!csv_out) {
    std::cout << "-----MFP stats-------" << std::endl;
    for (const auto& elem : system_sizes_MFP_stats) {
      std::cout << EB::sys_id_to_str_long(EB::sys_id(elem.first)) << "\n" << elem.second.print() << std::endl;
    }

    std::cout << "-----events stats---------" << std::endl;
    for (const auto& elem : system_sizes_events_stats) {
      std::cout << EB::sys_id_to_str_long(EB::sys_id(elem.first)) << "\n" << elem.second.print() << std::endl;
    }

    if (bx_types) {
      for (size_t bx_t = 0; bx_t < system_types_sizes_events_stats.size(); bx_t++) {
        std::cout << "-------BX type " << std::bitset<2>(bx_t) << "--------" << std::endl;
        for (const auto& elem : system_types_sizes_events_stats[bx_t]) {
          std::cout << EB::sys_id_to_str_long(EB::sys_id(elem.first)) << "\n" << elem.second.print() << std::endl;
        }
      }
    }
  } else {
    std::cout << "data_type," << stats<size_t>::print_csv_header() << std::endl;

    for (const auto& elem : system_sizes_MFP_stats) {
      std::cout << "MFP_" << EB::sys_id_to_str_long(EB::sys_id(elem.first)) << "," << elem.second.print_csv()
                << std::endl;
    }

    for (const auto& elem : system_sizes_events_stats) {
      std::cout << "event_" << EB::sys_id_to_str_long(EB::sys_id(elem.first)) << "," << elem.second.print_csv()
                << std::endl;
    }

    if (bx_types) {
      for (size_t bx_t = 0; bx_t < system_types_sizes_events_stats.size(); bx_t++) {
        for (const auto& elem : system_types_sizes_events_stats[bx_t]) {
          std::cout << "BX_type_" << std::bitset<2>(bx_t) << "_" << EB::sys_id_to_str_long(EB::sys_id(elem.first))
                    << "," << elem.second.print_csv() << std::endl;
        }
      }
    }
  }

  if (histo_base_name != "") {
    for (const auto& elem : system_sizes_events_histo) {
      std::ofstream sys_sizes_file(
        histo_base_name + EB::sys_id_to_str_long(EB::sys_id(elem.first)) + "_sizes_histo.csv");
      sys_sizes_file << elem.second.print_csv();
    }

    for (const auto& elem : system_sizes_MFP_histo) {
      std::ofstream sys_MFP_sizes_file(
        histo_base_name + EB::sys_id_to_str_long(EB::sys_id(elem.first)) + "_MFP_sizes_histo.csv");
      sys_MFP_sizes_file << elem.second.print_csv();
    }

    if (bx_types) {
      for (size_t bx_t = 0; bx_t < system_types_sizes_events_histo.size(); bx_t++) {
        for (const auto& elem : system_types_sizes_events_histo[bx_t]) {
          std::stringstream file_name;
          file_name << histo_base_name << "BX_type_" << std::bitset<2>(bx_t) << "_" << EB::sys_id(elem.first)
                    << "_sizes_histo.csv";
          std::ofstream sys_bx_types_sizes_file(file_name.str());
          sys_bx_types_sizes_file << elem.second.print_csv();
        }
      }
    }
  }

  return ret_val;
}
