#include "EventBuilding/MEP_tools.hpp"
#include "EventBuilding/MFP_tools.hpp"
#include "EventBuilding/file_mmap.hpp"
#include "PCIE40Data/RawBank40.h"
#include "PCIE40Data/sodin.h"
#include "options.hpp"
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <tuple>
#include <numeric>
#include <cmath>
#include <bitset>
#include <sys/mman.h>

// TODO this is temporary
typedef std::tuple<double, double, double, double, size_t> stat_tuple;

stat_tuple combine_stats(const stat_tuple& a, const stat_tuple& b)
{
  double mean_weight_a = (1 / std::get<1>(a)) * (1 / std::get<1>(a));
  double mean_weight_b = (1 / std::get<1>(b)) * (1 / std::get<1>(b));
  size_t total_size = std::get<size_t>(a) + std::get<size_t>(b);
  double mean = (std::get<0>(a) * mean_weight_a + std::get<0>(b) * mean_weight_b) / (mean_weight_a + mean_weight_b);
  double stdev = std::sqrt(
    (std::get<size_t>(a) * std::get<1>(a) * std::get<1>(a) + std::get<size_t>(b) * std::get<1>(b) * std::get<1>(b)) /
      (total_size) +
    std::get<size_t>(a) * std::get<size_t>(b) * (std::get<0>(a) - std::get<0>(b)) * (std::get<0>(a) - std::get<0>(b)) /
      (total_size * total_size));
  return stat_tuple(
    mean, stdev, std::min(std::get<2>(a), std::get<2>(b)), std::max(std::get<3>(a), std::get<3>(b)), total_size);
}

template<class ForwardIt>
stat_tuple calc_stats(const ForwardIt& begin, const ForwardIt& end)
{
  size_t n_elem = std::distance(begin, end);
  double sum = std::accumulate(begin, end, 0.0);
  double mean = sum / n_elem;

  double sq_sum = std::inner_product(begin, end, begin, 0.0);
  double stdev = std::sqrt(sq_sum / n_elem - mean * mean);

  auto min = *(std::min_element(begin, end));
  auto max = *(std::max_element(begin, end));

  return {mean, stdev, min, max, n_elem};
}

template<class T>
stat_tuple calc_stats(const T* v, size_t size)
{
  return calc_stats(v, v + size);
}

int main(int argc, char** argv)
{
  int ret_val = 0;
  std::string file_name;
  int verbose = 0;
  bool SODIN_decode = false;
  bool sizes_stats = false;
  bool bx_types = false;
  bool detect_tell40_err = false;
  void* read_ptr;
  EB::File_mmap file;
  EB::MEP* curr_MEP;
  EB::Options opt_parser(argc, argv);
  opt_parser.add_option("file_name", 'f', "MEP file name", true, file_name);
  opt_parser.add_counter("verbose", 'v', "Increase verbosity", false, verbose);
  opt_parser.add_flag("sodin", 's', "Decodes ODIN banks (if present)", false, SODIN_decode);
  opt_parser.add_flag("sizes", 'S', "Prints statistics on the bank sizes", false, sizes_stats);
  opt_parser.add_flag("bx_types", 'b', "Prints statistics on the bx types", false, bx_types);
  opt_parser.add_flag("dump_sizes", 'S', "Prints statistics on the bank sizes", false, sizes_stats);
  opt_parser.add_flag("error", 'e', "Detects Tell40 error banks", false, detect_tell40_err);
  // opt_parser.add_flag(
  //   "error_only", 'E', "Detects Tell40 error banks and suppresses other output", false, detect_tell40_err);
  if (!opt_parser.parse_args()) {
    opt_parser.print_usage();
    return 1;
  }

  try {
    file = EB::File_mmap(file_name);
  } catch (std::exception& e) {
    std::cerr << argv[0] << ": " << e.what() << std::endl;
    return errno;
  } catch (...) {
    std::cerr << argv[0] << ": unable to open file " << file_name << " unexpected exception" << std::endl;
    return -2;
  }

  read_ptr = file.get_read();
  while (file.update_read(sizeof(EB::MEP_header))) {
    std::map<EB::src_id_type, stat_tuple> system_stat_map;
    std::array<size_t, 4> bx_types_stats{};
    curr_MEP = reinterpret_cast<EB::MEP*>(read_ptr);
    if (file.update_read(curr_MEP->bytes() - sizeof(EB::MEP_header)) == NULL) {
      std::cout << "End of file reached truncated MEP" << std::endl;
      break;
    }
    size_t n_banks = curr_MEP->cbegin()->header.n_banks;

    std::cout << curr_MEP->print() << std::endl;

    if (SODIN_decode || bx_types || detect_tell40_err || sizes_stats || (verbose > 0)) {
      auto src_ids = curr_MEP->header.src_ids();
      for (size_t k = 0; k < curr_MEP->header.n_MFPs; k++) {
        EB::MFP* curr_MFP = curr_MEP->at(k);
        if ((verbose > 0)) {
          std::cout << curr_MFP->print(verbose > 1) << std::endl;
        }

        auto curr_sys_id = EB::get_sys_id(curr_MFP->header.src_id);

        if (detect_tell40_err) {
          auto errors = curr_MFP->tell40_errors();
          ret_val += errors.size();
          if (errors.size() > 0) {
            std::cout << "Detected " << errors.size() << " error banks from source 0x" << std::hex
                      << curr_MFP->header.src_id << std::dec << " sys id "
                      << EB::sys_id_to_str_long(static_cast<EB::sys_id>(curr_sys_id)) << " idx " << k << std::endl;
            if (verbose > 0) {
              for (const auto& elem : errors) {
                std::cout << "Detected error bank from source 0x" << std::hex << curr_MFP->header.src_id << std::dec
                          << " type " << std::get<EB::BankType>(elem) << " in bank number " << std::get<int>(elem)
                          << " of size " << std::get<size_t>(elem) << std::endl;
              }
            }
          }
        }

        if (sizes_stats) {
          auto stats = calc_stats<EB::bank_size_type>(curr_MFP->header.bank_sizes(), curr_MFP->header.n_banks);
          auto map_it = system_stat_map.find(curr_sys_id);

          if (map_it != system_stat_map.end()) {
            map_it->second = combine_stats(map_it->second, stats);
          } else {
            system_stat_map[curr_sys_id] = stats;
          }

          std::cout << EB::sys_id_to_str_long(static_cast<EB::sys_id>(curr_sys_id)) << " "
                    << "Size statistics: " << std::get<0>(stats) << " " << std::get<1>(stats) << " "
                    << std::get<2>(stats) << " " << std::get<3>(stats) << " " << std::get<4>(stats) << std::endl;
        }

        if (
          (SODIN_decode || bx_types) &&
          (EB::get_sys_id(src_ids[k]) == static_cast<EB::src_id_type>(EB::sys_id::ODIN))) {
          for (auto bank_it = curr_MFP->begin(); bank_it != curr_MFP->end(); bank_it++) {
            if (SODIN_decode) {
              std::cout << *(reinterpret_cast<Online::pcie40::sodin_t*>(&(*bank_it))) << std::endl;
              std::cout << "--------" << std::endl;
            }
            if (bx_types) {
              bx_types_stats[reinterpret_cast<Online::pcie40::sodin_t*>(&(*bank_it))->bx_type()]++;
            }
          }
          if (SODIN_decode) {
            std::cout << "------ODIN MFP END--------" << std::endl;
          }
        }
      }
    }

    if (sizes_stats) {
      for (const auto& elem : system_stat_map) {
        std::cout << EB::sys_id_to_str_long(static_cast<EB::sys_id>(elem.first)) << " "
                  << "Size statistics: " << std::get<0>(elem.second) << " " << std::get<1>(elem.second) << " "
                  << std::get<2>(elem.second) << " " << std::get<3>(elem.second) << " " << std::get<4>(elem.second)
                  << std::endl;
      }
    }

    auto MFP_sizes = curr_MEP->get_mfp_sizes();
    auto event_sizes = curr_MEP->get_ev_sizes();
    auto MFP_stats = calc_stats(MFP_sizes.begin(), MFP_sizes.end());
    auto event_stats = calc_stats(event_sizes.begin(), event_sizes.end());

    std::cout << "MFP Size statistics: " << std::get<0>(MFP_stats) << " " << std::get<1>(MFP_stats) << " "
              << std::get<2>(MFP_stats) << " " << std::get<3>(MFP_stats) << " " << std::get<4>(MFP_stats) << std::endl;
    std::cout << "event Size statistics: " << std::get<0>(event_stats) << " " << std::get<1>(event_stats) << " "
              << std::get<2>(event_stats) << " " << std::get<3>(event_stats) << " " << std::get<4>(event_stats)
              << std::endl;
    std::cout << "MEP average ev size " << static_cast<double>(curr_MEP->bytes()) / curr_MEP->cbegin()->header.n_banks
              << std::endl;

    if (bx_types) {
      std::cout << "BX types: " << std::endl;
      for (size_t k = 0; k < bx_types_stats.size(); k++) {
        std::cout << std::bitset<2>(k) << " " << bx_types_stats[k] << " " << (100. * bx_types_stats[k]) / n_banks << "%"
                  << std::endl;
      }
    }

    read_ptr = file.get_read();
  }

  return ret_val;
}
