#include "EventBuilding/MEP_tools.hpp"
#include "EventBuilding/MFP_tools.hpp"
#include "EventBuilding/file_mmap.hpp"
#include "PCIE40Data/RawBank40.h"
#include "PCIE40Data/sodin.h"
#include "options.hpp"
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <tuple>
#include <numeric>
#include <cmath>
#include <bitset>
#include <sys/mman.h>

int main(int argc, char** argv)
{
  int ret_val = 0;
  std::string file_name;
  bool decode_sizes = false;
  bool decode_bx_types = false;
  bool decode_bank_types = false;
  void* read_ptr;
  EB::File_mmap file;
  EB::MEP* curr_MEP;
  EB::Options opt_parser(argc, argv);
  opt_parser.add_option("file_name", 'f', "MEP file name", true, file_name);
  opt_parser.add_flag("sizes", 'S', "Decodes bank sizes", false, decode_sizes);
  opt_parser.add_flag("bx_types", 'b', "Decodes the bx types", false, decode_bx_types);
  opt_parser.add_flag("type", 't', "Decodes bank types", false, decode_bank_types);
  if (!opt_parser.parse_args()) {
    opt_parser.print_usage();
    return 1;
  }

  try {
    file = EB::File_mmap(file_name);
  } catch (std::exception& e) {
    std::cerr << argv[0] << ": " << e.what() << std::endl;
    return errno;
  } catch (...) {
    std::cerr << argv[0] << ": unable to open file " << file_name << " unexpected exception" << std::endl;
    return -2;
  }

  read_ptr = file.get_read();

  // print csv header
  std::cout << "ev_id";
  std::cout << ",src_id";
  if (decode_sizes) {
    std::cout << ",size";
  }
  if (decode_bx_types) {
    std::cout << ",bx_type";
  }
  if (decode_bank_types) {
    std::cout << ",bank_type";
  }
  std::cout << std::endl;

  while (file.update_read(sizeof(EB::MEP_header))) {
    curr_MEP = reinterpret_cast<EB::MEP*>(read_ptr);
    if (file.update_read(curr_MEP->bytes() - sizeof(EB::MEP_header)) == NULL) {
      std::cout << "End of file reached truncated MEP" << std::endl;
      break;
    }
    size_t n_events;
    EB::ev_id_type base_ev_id;
    try {
      n_events = curr_MEP->at(0)->header.n_banks;
      base_ev_id = curr_MEP->at(0)->header.ev_id;
    } catch (std::exception& e) {
      std::cerr << e.what() << " Aborting." << std::endl;
      return -3;
    } catch (...) {
      std::cerr << "Exception while reading the number of banks. Aborting." << std::endl;
      return -3;
    }
    size_t n_sources = curr_MEP->header.n_MFPs;

    auto src_ids = curr_MEP->header.src_ids();

    std::vector<uint8_t> bx_types;
    if (decode_bx_types) {
      bx_types = curr_MEP->get_bx_types();
    }

    std::vector<EB::bank_size_type*> bank_sizes(n_sources);
    std::vector<EB::bank_type_type*> bank_types(n_sources);
    // std::vector<std::vector<EB::bank_size_type>> bank_sizes(n_sources);
    // std::vector<std::vector<EB::bank_type_type>> bank_types(n_sources);

    if (decode_sizes || decode_bank_types) {
      for (size_t k = 0; k < curr_MEP->header.n_MFPs; k++) {
        EB::MFP* curr_MFP = curr_MEP->at(k);
        bank_sizes[k] = curr_MFP->header.bank_sizes();
        bank_types[k] = curr_MFP->header.bank_types();
        // bank_sizes[k] =
        //   std::vector<EB::bank_size_type>(curr_MFP->header.bank_sizes(), curr_MFP->header.bank_sizes() + n_events);
        // bank_types[k] =
        //   std::vector<EB::bank_type_type>(curr_MFP->header.bank_types(), curr_MFP->header.bank_types() + n_events);
      }
    }

    for (size_t event = 0; event < n_events; event++) {
      for (size_t src = 0; src < n_sources; src++) {
        std::cout << base_ev_id + event << "," << src_ids[src];
        if (decode_sizes) {
          std::cout << "," << bank_sizes[src][event];
        }
        if (decode_bx_types) {
          std::cout << "," << std::bitset<2>(bx_types[event]);
        }
        if (decode_bank_types) {
          std::cout << "," << static_cast<EB::BankType>(bank_types[src][event]);
        }
        std::cout << std::endl;
      }
    }

    read_ptr = file.get_read();
  }

  return ret_val;
}
