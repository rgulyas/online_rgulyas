#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H 1

#include "buffer_interface.hpp"
#include "circular_buffer_status.hpp"
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <string>
#include <tuple>

namespace EB {
  template<class B>
  class Circular_buffer {
  public:
    virtual ~Circular_buffer() {}
    Circular_buffer() {}
    Circular_buffer(B&& backend) : _backend(std::move(backend)) {}

    void reset_backend(B&& backend = std::move(B())) { _backend = std::move(backend); }
    bool is_set() { return _backend.is_set(); }

  protected:
    B _backend;
  };
} // namespace EB

#endif // CIRCULAR_BUFFER_H