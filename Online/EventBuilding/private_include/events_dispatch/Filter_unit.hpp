#ifndef FILTERUNIT_HPP
#define FILTERUNIT_HPP

#include <assert.h>
#include <chrono>
#include <ctime>
#include <glib.h>
#include <malloc.h>
#include <mpi.h>
#include <random>
#include <stdio.h>
#include <stdlib.h>
#include <tuple>
#include <unistd.h>
#include <vector>

#include "Unit.hpp"
#include "common.hpp"
#include "Dataflow/DataflowComponent.h"
#include "MBM/Producer.h"

#include "buffer_interface.hpp"
#include "EventBuilding/MEP_tools.hpp"
#include "mbm_writer.hpp"

// Filter Unit class
// Used to implement Data Consumer functions

namespace Online {

  class Filter_unit : public DataflowComponent, public Unit {

  public:
    // main method to perform data consuming operations
    Filter_unit(const std::string& nam, DataflowContext& ctxt);
    /// Default destructor
    virtual ~Filter_unit() = default;

  protected:
    /// Service implementation: initialize the service
    virtual int initialize() override;
    /// Service implementation: start of the service
    virtual int start() override;
    /// Service implementation: stop the service
    virtual int stop() override;
    /// Service implementation: finalize the service
    virtual int finalize() override;
    /// Cancel I/O operations of the dataflow component
    virtual int cancel() override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause() override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc) override;
    /// IRunable implementation : Run the class implementation
    virtual int run() override;

    // methods
  private:
    // start transmissions to force necessary MPI resource allocation
    // invoked to mitigate / eliminate MPI init impact on the actual performance
    // measurements
    void mpi_warmup_run();
    // Method for nofyfing Event Manager that FU is ready fo send
    void signal_readiness(DISPATCH::fu_status stat = DISPATCH::fu_status::TRANSMISSION_DONE);
    // test if any of commenced MPI sends has finished
    bool probe_for_completion(int* index);
    // Commence data receive from ANY BU source
    void recv_from_bu(size_t size, int index, int sender_rank, int tag = DISPATCH::BU_RU_DATA_TAG);
    bool service_mu_alloc_call();
    // Generate new period to simulate processing time on the data consumer side
    size_t get_new_processing_period(size_t new_mep_size);
    // calculate index for data for next transmission
    long unsigned int get_entry_address(int entry_index);
    void probe_print_throughput();
    bool exchange_message_size();
    void run_recv();
    void get_meta(size_t size);
    void init_new_transmission(int sender_rank);

    // variables
  private:
    /// Property: Buffer name for data output
    std::string _mbm_name;
    EB::Buffer_writer<EB::MEP>* _write_buff;
    // buffer of records current pointer
    unsigned int _data_buffer_current_ptr;
    // counter of all finished transmissions granted to RU
    int64_t _all_finished_granted_transmission;
    // timestamp when the current record processing in queue was commenced
    int64_t _event_started_processing_timestamp;
    // MPI request for FU signalling readiness
    MPI_Request _mpi_fu_ready_request;
    MPI_Request _mpi_acq_request;
    MPI_Request _mpi_mu_fu_alloc_request;
    // MPI Message to signal FU readiness
    DISPATCH::mpi_fu_ready_msg _fu_ready_message;
    // MPI Message to establish message size
    DISPATCH::mpi_bu_fu_mep_size_msg _ou_fu_mep_size_msg;
    DISPATCH::mu_fu_alloc_msg _alloc_msg;

    // counter of all completed irecv requests to count total average FU
    // throughput
    size_t _all_completed_bytes;

    // counter of all completed irecv requests for the current probing period to
    // caltulate current FU throughput
    size_t _current_probe_completed_bytes;

    // array for holding MPI_Request structures for pending transmisisons from BU
    // to FU
    // Currently dimensions 1 x WINDOW_LENGTH
    MPI_Request* _mpi_data_sent_requests;

    // current start time of the latest throughput probe
    int64_t _probe_start;
    // global start time when Event Building transmissions were commenced
    int64_t _global_start;

    int64_t _next_probing;
    char* _acquired_data_ptr;
    DISPATCH::current_transmission_metadata _current_transmission_meta;
    size_t _last_allocated_message_size;
  };
} // namespace Online

#endif
