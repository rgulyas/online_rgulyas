#include "events_dispatch/common.hpp"

using namespace DISPATCH;
template<class T>
std::vector<mep_entry> FBUFF::get_mep(EB::Buffer_reader<T>* _recv_buff)
{
  std::vector<mep_entry> vector_of_entries;
  int i;
  for (i = 0; i != MAX_MEPS_IN_ONE_RUN; i++) {
    EB::MEP* new_elem = _recv_buff->try_get_element();
    if (new_elem == NULL) {
      printf("ELEMENT NULL\n");
      break;
    }
    char* ptr = reinterpret_cast<char*>(new_elem);
    // size_t size = new_elem->_size;
    size_t size = new_elem->bytes();
    // printf("NONZERO ELEMENT. SIZE %ld. ADDR %ld \n",size,ptr);
    vector_of_entries.push_back(mep_entry(ptr, size));
  }

  return vector_of_entries;
}

// size_t FBUFF::get_buffer_size(shmem_buff& buff)  {
// return (buff._local_buffer_status.get_size());
//};

// char* FBUFF::get_buffer_data(shmem_buff& buff)  {
//  return (buff.get_head());
//};

template<class T>
void FBUFF::sync_data_read(EB::Buffer_reader<T>* _recv_buff)
{
  _recv_buff->read_complete();
}

template<class T>
void FBUFF::sync_data_write(EB::Buffer_writer<T>* _write_buff)
{
  _write_buff->write_complete();
}

template<class T>
void FBUFF::generate_data(EB::Buffer_writer<T>* _write_buff, int rank, int amount_in_gib)
{

  if (DISPATCH::constant_size == true) {
    size_t gen_size = DISPATCH::MB_IN_GRANT * 1024UL * 1024UL;
    size_t total_data_amount = static_cast<size_t>(amount_in_gib) * 1024UL * 1024UL * 1024UL / 8UL;

    unsigned int how_many_to_generate = total_data_amount / gen_size;
    // printf("TRYING TO GENERATE %d MEPS \n",how_many_to_generate);
    unsigned int i = 0;
    for (i = 0; i != how_many_to_generate; i++) {
      EB::MEP* new_elem = NULL;
      new_elem = _write_buff->try_write_next_element(gen_size);
      if (new_elem == NULL) {
        printf(
          "BUFFER FOR BDU RANK %d NOT ENOUGH SPACE ! GENERATED %d MEPS INSTEAD OF %d\n", rank, i, how_many_to_generate);
        break;
      } else {
        new_elem->set_magic_valid();
        new_elem->header.p_words = gen_size / 4;
        _write_buff->write_complete();
      }
    }
    // printf("BDU rank %d :  GENERATED %d MEPS OUT OF OF %d\n", rank, i, how_many_to_generate);
  } else {
    size_t total_data_amount =
      static_cast<size_t>(amount_in_gib) * 1024UL * 1024UL * 1024UL / static_cast<size_t>(BITS_PER_BYTE);
    long remaining_data_amount = total_data_amount;

    int all_meps = 0;
    while (remaining_data_amount > 0) {

      size_t amount = (rand() % 96 + 32) * 1024 * 1024 * 4;
      amount += (4096 * rand()) % (1024 * 1024 * 4);
      EB::MEP* new_elem = NULL;
      new_elem = _write_buff->try_write_next_element(amount);
      if (new_elem == NULL) {
        printf(
          "BUFFER FOR BDU RANK %d NOT ENOUGH SPACE ! GENERATED %ld B  (in %d MEPs) INSTEAD OF %ld\n",
          rank,
          total_data_amount - remaining_data_amount,
          all_meps,
          total_data_amount);
        break;
      }
      all_meps++;
      new_elem->set_magic_valid();
      new_elem->header.p_words = amount / 4;
      remaining_data_amount -= amount;

      _write_buff->write_complete();
    }
  }

  return;
}
