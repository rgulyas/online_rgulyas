/**
 * @file proc.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Process Class
 * @version 0.1.1
 * @date 16/11/2020
 *
 * @copyright Copyright (c) 2020
 *
 */

#pragma once
#if !defined(PROC_H)
#define PROC_H
#include <fstream>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include <infiniband/verbs.h>
#include <iostream>
#include <memory>
#include <string>
#include <functional>
#include <memory> 

namespace IB_verbs {
  class Proc {
  private:
    /* data */
  public:
    struct MRInfo {
      uint32_t rkey;
      uintptr_t addr;
    };
    int sockFd;
    std::string ibDevString;
    int ibDev;
    uint16_t local_lid;
    uint16_t remote_lid;
    uint32_t remote_qp_num;      // must be 32 bit
    uint32_t remote_sync_qp_num; // must be 32 bit
    uint8_t qp_rdma;
    uint8_t next_qp;
    typedef std::unique_ptr<ibv_qp, std::function<void(ibv_qp*)>> unique_qp_t;
    unique_qp_t qp;
    unique_qp_t sync_qp;
    MRInfo rdma;
    MRInfo sync;
    Proc();
    Proc(Proc&&) = default;
    Proc& operator=(Proc&&) = default;
    Proc(const Proc&) = delete;
    Proc& operator=(const Proc&) = delete;
    ~Proc() = default;

    void set_addrInfo(const std::string& node, const std::string& service);

    addrinfo* get_addrInfo();

  private:
    std::unique_ptr<addrinfo, decltype(&freeaddrinfo)> _addrInfo;
  };

} // namespace IB_verbs
#endif // PROC_H
