#include "mbm_writer.hpp"
#include "RTL/rtl.h"
#include <stdexcept>
#include "EventBuilding/tools.hpp"
#include <unistd.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h>    /* For O_* constants */
#include <sys/mman.h>
#include <cstring>
#include <sstream>
#include <string>
#include <memory>
#include <iostream>
#include <tuple>

template<class T>
EB::Mbm_reader<T>::Mbm_reader(
  Online::DataflowComponent::Context& context,
  const std::string& instance,
  const std::string& name,
  const std::string& req) :
  EB::Buffer_reader<T>(),
  _context(&context), _consumer(context.mbm->createConsumer(name, instance)), _m_req(req), _ack_pending(false)
{
  _m_requirement.parse(req);
  _consumer->addRequest(_m_requirement);
}

template<class T>
EB::Mbm_reader<T>::~Mbm_reader()
{}

template<class T>
T* EB::Mbm_reader<T>::try_get_element()
{
  T* data = NULL;
  int status = _consumer->getEvent();
  if (status == MBM_NORMAL) {
    const MBM::EventDesc& dsc = _consumer->event();
    data = (T*) dsc.data;
    // size_t size = dsc.len;
    // info("Got MEP size %ld\n", size);
    _ack_pending = true;
  }
  return data;
}

template<class T>
void EB::Mbm_reader<T>::read_complete()
{
  if (_ack_pending) {
    _consumer->freeEvent();
    _ack_pending = false;
  }
}

template<class T>
void EB::Mbm_reader<T>::flush()
{
  // TODO check if there is a way of removing all the events from the buffer
  if (_ack_pending) {
    _consumer->freeEvent();
    _ack_pending = false;
  }
}

template<class T>
std::vector<std::tuple<void*, size_t>> EB::Mbm_reader<T>::get_full_buffer()
{
  // TODO verify
  void* mbm_data = (void*) mbm_buffer_address(_consumer->id());
  size_t mbm_size;
  mbm_buffer_size(_consumer->id(), &mbm_size);
  std::vector<std::tuple<void*, size_t>> ret_val;
  ret_val.emplace_back(std::tuple<void*, size_t>(mbm_data, mbm_size));
  return ret_val;
}

template<class T>
void EB::Mbm_reader<T>::cancel()
{
  EB::Buffer_reader<T>::cancel();
  _consumer->cancel();
  _ack_pending = false;
}
