#ifndef PCIE40_READER_H
#define PCIE40_READER_H 1
#include <memory>
#include <vector>
#include <exception>
#include <deque>
#include <lhcb/pcie40/id.h>
#include <lhcb/pcie40/daq.h>
#include <lhcb/pcie40/pcie40.hpp>
#include "EventBuilding/MFP_tools.hpp"
#include "buffer_interface.hpp"
#include "circular_buffer_reader.hpp"
#include "circular_buffer_writer.hpp"
#include "shared_ptr_buffer_backend.hpp"
#include "file_writer.hpp"
#include "pcie40_reader_error.hpp"

namespace EB {
  // alignments are expressed as a power of 2
  constexpr int default_PCIe40_alignment = 12;     // 4096 B
  constexpr int default_PCIe40_frag_alignment = 5; // 32 B
  constexpr int default_PCIe40_frag_thr = 32;
  constexpr int default_PCIe40_packing_factor = 3000;
  // max number of character in the PCIe40 name
  constexpr int PCIe40_name_length = 8;
  // minimum str length to sto a PCIe40 name including the str terminator
  constexpr int PCIe40_name_str_length = PCIe40_name_length + 1;
  // max number of character in the PCIe40 unique name including stream
  constexpr int PCIe40_unique_name_length = PCIe40_name_length + 2;
  // minimum str length to sto a PCIe40 unique name including the str terminator
  constexpr int PCIe40_unique_name_str_length = PCIe40_unique_name_length + 1;

  class PCIe40_reader {
  public:
    PCIe40_reader();
    PCIe40_reader(
      int id,
      int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN,
      bool enable_MFP = true,
      int packing_factor = default_PCIe40_packing_factor);

    PCIe40_reader(
      const std::string& name,
      int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN,
      bool enable_MFP = true,
      int packing_factor = default_PCIe40_packing_factor);

    PCIe40_reader(const PCIe40_reader& other) = delete;
    PCIe40_reader(PCIe40_reader&& other);
    virtual ~PCIe40_reader();

    PCIe40_reader& operator=(const PCIe40_reader& other) = delete;
    PCIe40_reader& operator=(PCIe40_reader&& other);

    std::error_code open(int id, int stream);
    std::error_code open(const std::string& name, int stream);

    std::error_code set_packing_factor(int packing_factor);
    std::error_code set_truncation_threshold(int trunc_thr);

    std::error_code enable_MFP_stream();
    std::error_code disable_MFP_stream();

    std::error_code configure_MFP(int packing_factor);
    std::error_code configure_fragment(int trunc_thr = default_PCIe40_frag_thr);

    // Resets the the cards and flush the buffer
    std::error_code reset();

    bool is_open();

    void close();

    template<class T>
    T* extract_element();
    void ack_read();
    void update_usage();
    void cancel_pending();

    std::string get_name() const;
    int get_dmabuf_fd() const;

    size_t get_buffer_occupancy();
    size_t get_buffer_size() const;

    uint16_t _src_id;
    uint8_t _block_version;

  protected:
    static constexpr int src_version_mask = 0xFF0000;
    static constexpr int src_version_shift = 16;
    static constexpr int src_subsystem_mask = 0xF800;
    static constexpr int src_subsystem_shift = 11;
    static constexpr int src_num_mask = 0x7FF;
    static constexpr int src_num_shift = 0;
    static constexpr ev_id_type default_next_ev_id = 0;

    void* _buffer;
    size_t _buffer_size;

    size_t _internal_read_off;
    size_t _device_read_off;
    size_t _available_data;
    size_t _requested_size;

    int _id;
    P40_DAQ_STREAM _stream;
    ev_id_type _next_ev_id;
    int _id_fd;
    int _stream_fd;
    int _ctrl_fd;
    int _meta_fd;
    int _dmabuf_fd;

    std::string _name;

    // TODO this should be controlled by the ECS
    // alignment between MFPs into the PCIe40 buffer as a power of 2
    int PCIe40_alignment = default_PCIe40_alignment;

    std::error_code update_device_ptr();

    virtual void init_internal_status();

    int update_read_off(size_t size);
  };

  class PCIe40_MFP_reader : public PCIe40_reader, public EB::Buffer_reader<EB::MFP> {
  public:
    PCIe40_MFP_reader();
    PCIe40_MFP_reader(int id, int stream, int packing_factor);
    PCIe40_MFP_reader(const std::string& name, int stream, int packing_factor);

    EB::MFP* try_get_element() override;
    void read_complete() override;
    void flush() override;

    int get_src_id() const override;

    std::string get_name() const override;
    int get_dmabuf_fd() const;

    size_t get_buffer_size() const override;
    size_t get_buffer_occupancy() override;

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;
  };

  class PCIe40_frag_reader : public EB::Buffer_reader<EB::MFP> {
  public:
    PCIe40_frag_reader();
    PCIe40_frag_reader(
      int id,
      size_t buffer_size,
      int packing_factor = 1,
      size_t buffer_alignment = default_PCIe40_alignment,
      int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN);
    PCIe40_frag_reader(
      const std::string& name,
      size_t buffer_size,
      int packing_factor = 1,
      size_t buffer_alignment = default_PCIe40_alignment,
      int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN);

    EB::MFP* try_get_element() override;
    void read_complete() override;
    void flush() override;

    int get_src_id() const override;

    void set_n_frags(int n_MFPs);
    int get_n_frags() const;

    void set_align(int n_MFPs);
    int get_align() const;

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;

    size_t get_buffer_size() const override;
    size_t get_buffer_occupancy() override;

    std::string get_name() const override;

  protected:
    PCIe40_reader _pcie40_reader;
    Circular_buffer_reader<EB::MFP, Shared_ptr_buffer_backend> _internal_buffer_reader;
    Circular_buffer_writer<EB::MFP, Shared_ptr_buffer_backend> _internal_buffer_writer;

    uint16_t _packing_factor;
    // TODO we may want to modify this
    uint8_t _align = default_PCIe40_frag_alignment;
    // add more vectors for sizes and offsets
    // add a function that builds all the MFPs in the buffer
    std::vector<void*> _frag_list;
    std::vector<uint8_t> _type_list;
    std::vector<uint16_t> _size_list;
    uint64_t _ev_id;
    uint8_t _version;
    uint32_t _total_size = 0;

    // Set to true when a flush is received, deasserted when the consecutive sequenque of flushes ends
    bool _flush = false;
    // Set to true if a flush MFP is pending because the internal buffer is full, deasserted when the flush MFP is sent
    bool _flush_pending = false;

    void scan_frag_list();
    std::error_code build_MFP();
  };
} // namespace EB

#endif // PCIE40_READER_H
