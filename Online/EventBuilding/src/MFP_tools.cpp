#include "EventBuilding/MFP_tools.hpp"
#include "EventBuilding/tools.hpp"
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <bitset>

EB::src_id_type EB::get_sys_id(EB::src_id_type src_id) { return (src_id & EB::sys_id_mask) >> EB::sys_id_shift; }

void EB::set_sys_id(EB::src_id_type& src_id, EB::src_id_type sys_id)
{
  src_id = (src_id & EB::src_num_mask) | ((sys_id << EB::sys_id_shift) & EB::sys_id_mask);
}

void EB::set_sys_id(EB::src_id_type& src_id, EB::sys_id sys_id)
{
  set_sys_id(src_id, static_cast<src_id_type>(sys_id));
}

EB::src_id_type EB::get_src_num(EB::src_id_type src_id) { return (src_id & EB::src_num_mask) >> EB::src_num_shift; }

void EB::set_src_num(EB::src_id_type& src_id, EB::src_id_type src_num)
{
  src_id = (src_id & EB::sys_id_mask) | ((src_num << EB::src_num_shift) & EB::src_num_mask);
}

std::string EB::src_id_sys_to_str_long(EB::src_id_type src_id)
{
  return sys_id_to_str_long(static_cast<sys_id>(get_sys_id(src_id)));
}

std::string EB::sys_id_to_str_long(EB::sys_id id)
{
  switch (id) {
  case sys_id::ODIN: return "ODIN";
  case sys_id::VELO_A: return "VELO_A";
  case sys_id::VELO_C: return "VELO_C";
  case sys_id::RICH_1: return "RICH_1";
  case sys_id::UT_A: return "UT_A";
  case sys_id::UT_C: return "UT_C";
  case sys_id::SCIFI_A: return "SCIFI_A";
  case sys_id::SCIFI_C: return "SCIFI_C";
  case sys_id::RICH_2: return "RICH_2";
  case sys_id::PLUME: return "PLUME";
  case sys_id::ECAL: return "ECAL";
  case sys_id::HCAL: return "HCAL";
  case sys_id::MUON_A: return "MUON_A";
  case sys_id::MUON_C: return "MUON_C";
  case sys_id::TDET: return "TDET";
  default: return "Unknown sys ID";
  }
}

std::string EB::src_id_sys_to_str(EB::src_id_type src_id)
{
  return sys_id_to_str(static_cast<sys_id>(get_sys_id(src_id)));
}

std::string EB::sys_id_to_str(EB::sys_id id)
{
  switch (id) {
  case sys_id::ODIN: return "OD";
  case sys_id::VELO_A: return "VA";
  case sys_id::VELO_C: return "VC";
  case sys_id::RICH_1: return "R1";
  case sys_id::UT_A: return "UA";
  case sys_id::UT_C: return "UC";
  case sys_id::SCIFI_A: return "SA";
  case sys_id::SCIFI_C: return "SC";
  case sys_id::RICH_2: return "R2";
  case sys_id::PLUME: return "PL";
  case sys_id::ECAL: return "EC";
  case sys_id::HCAL: return "HC";
  case sys_id::MUON_A: return "MA";
  case sys_id::MUON_C: return "MC";
  case sys_id::TDET: return "TD";
  default: return "NA";
  }
}

EB::sys_id EB::str_to_sys_id(const std::string& str)
{
  if (str == "OD") {
    return sys_id::ODIN;
  } else if (str == "VA") {
    return sys_id::VELO_A;
  } else if (str == "VC") {
    return sys_id::VELO_C;
  } else if (str == "R1") {
    return sys_id::RICH_1;
  } else if (str == "UA") {
    return sys_id::UT_A;
  } else if (str == "UC") {
    return sys_id::UT_C;
  } else if (str == "SA") {
    return sys_id::SCIFI_A;
  } else if (str == "SC") {
    return sys_id::SCIFI_C;
  } else if (str == "R2") {
    return sys_id::RICH_2;
  } else if (str == "PL") {
    return sys_id::PLUME;
  } else if (str == "EC") {
    return sys_id::ECAL;
  } else if (str == "HC") {
    return sys_id::HCAL;
  } else if (str == "MA") {
    return sys_id::MUON_A;
  } else if (str == "MC") {
    return sys_id::MUON_C;
  } else if (str == "TD") {
    return sys_id::TDET;
  } else {
    throw std::runtime_error("Invalid system string " + str);
  }
}

std::ostream& operator<<(std::ostream& os, const EB::sys_id& id) { return os << EB::sys_id_to_str_long(id); }

size_t EB::MFP_header_size(int packing_fraction)
{
  return sizeof(MFP_header) +
         aligned_array_end(reinterpret_cast<bank_type_type*>(sizeof(MFP_header)), packing_fraction, MFP_alignment) +
         aligned_array_size<bank_size_type>(packing_fraction, MFP_alignment);
}

EB::bank_type_type* EB::MFP_header::bank_types()
{
  return reinterpret_cast<uint8_t*>(reinterpret_cast<uintptr_t>(this) + sizeof(MFP_header));
}

const EB::bank_type_type* EB::MFP_header::bank_types() const { return const_cast<MFP_header*>(this)->bank_types(); }

EB::bank_size_type* EB::MFP_header::bank_sizes()
{
  return reinterpret_cast<bank_size_type*>(
    reinterpret_cast<uintptr_t>(bank_types()) + aligned_array_size<bank_type_type>(n_banks, MFP_alignment));
  // this should be used if the ftype array is not aligned
  // bank_types() + aligned_array_end(reinterpret_cast<bank_type_type*>(&align), n_banks, MFP_alignment)));
}

const EB::bank_size_type* EB::MFP_header::bank_sizes() const { return const_cast<MFP_header*>(this)->bank_sizes(); }

size_t EB::MFP_header::header_size() const { return MFP_header_size(n_banks); }

size_t EB::MFP_header::bytes() const { return packet_size; }

void* EB::MFP::payload()
{
  return reinterpret_cast<void*>(
    reinterpret_cast<uintptr_t>(header.bank_sizes()) +
    aligned_array_size<bank_size_type>(header.n_banks, MFP_alignment));
}

const void* EB::MFP::payload() const { return const_cast<MFP*>(this)->payload(); }

std::string EB::MFP_header::print(bool verbose) const
{
  std::stringstream os;
  os << "\n------MFP HEADER------\n";
  os << "Magic " << std::hex << "0x" << magic << std::dec << "\n";
  os << "event ID " << ev_id << "\n";
  os << "source ID " << std::hex << "0x" << src_id << std::dec << "\n";
  os << "n banks " << n_banks << "\n";
  os << "packet size " << packet_size << "\n";
  if (verbose) {
    auto types = bank_types();
    auto sizes = bank_sizes();
    for (int k = 0; k < n_banks; k++) {
      os << "bank type " << EB::BankType(types[k]) << " bank size " << sizes[k] << "\n";
    }
  }

  return os.str();
}

std::ostream& operator<<(std::ostream& os, const EB::MFP_header& header) { return os << header.print(); }

bool EB::MFP::is_header_valid(uint16_t* magic) const
{
  uint16_t junk;
  if (magic == NULL) {
    magic = &junk;
  }
  *magic = header.magic;
  return (*magic == MFP_magic);
}

void EB::MFP::set_header_valid() { header.magic = MFP_magic; }

bool EB::MFP::is_valid(uint16_t* magic, size_t* size) const
{
  size_t junk;
  if (size == NULL) {
    size = &junk;
  }
  *size = bytes();
  return is_header_valid(magic) && (*size >= header.header_size());
}

bool EB::MFP::is_end_run() const { return is_header_valid() && (header.ev_id == MFP_end_run); }
void EB::MFP::set_end_run()
{
  // TODO set the correct size
  set_header_valid();
  header.ev_id = MFP_end_run;
  header.n_banks = 0;
}

void* EB::MFP::get_bank_n(int n)
{
  void* ret_val;
  if (n >= header.n_banks) {
    ret_val = nullptr;
  } else {
    auto it = this->begin();
    for (int k = 0; k < n; k++) {
      it++;
    }
    ret_val = &(*it);
  }
  return ret_val;
}

const void* EB::MFP::get_bank_n(int n) const
{
  const void* ret_val;
  if (n >= header.n_banks) {
    ret_val = nullptr;
  } else {
    auto it = this->cbegin();
    for (int k = 0; k < n; k++) {
      it++;
    }
    ret_val = &(*it);
  }
  return ret_val;
}

std::vector<EB::tell40_error_report_type> EB::MFP::tell40_errors() const
{
  std::vector<tell40_error_report_type> ret_vec;
  ret_vec.reserve(header.n_banks);

  auto types = header.bank_types();
  auto sizes = header.bank_sizes();

  auto it = cbegin();
  for (size_t k = 0; k < header.n_banks; k++) {
    BankType type(static_cast<BankType>(types[k]));
    if (
      (type == DaqErrorFragmentThrottled) || (type == DaqErrorBXIDCorrupted) || (type == DaqErrorSyncBXIDCurrupted) ||
      (type == DaqErrorFragmentMissing) || (type == DaqErrorFragmentTruncated) || (type == DaqErrorIdleBXIDCorrupted)) {
      ret_vec.emplace_back(reinterpret_cast<const void*>(&(*it)), type, sizes[k], k);
    }
    it++;
  }

  // most of the allocated space is probably empty
  ret_vec.shrink_to_fit();

  return ret_vec;
}

void* EB::MFP::operator[](int n)
{
  auto it = this->begin();
  for (int k = 0; k < n; k++) {
    it++;
  }
  return &(*it);
}

const void* EB::MFP::operator[](int n) const
{
  auto it = this->cbegin();
  for (int k = 0; k < n; k++) {
    it++;
  }
  return &(*it);
}

bool EB::MFP::is_wrap() const { return header.magic == MFP_wrap; }
void EB::MFP::set_wrap() { header.magic = MFP_wrap; }

std::string EB::MFP::print(bool verbose) const
{
  std::stringstream os;
  os << header.print(verbose);
  os << "Payload " << payload() << '\n';

  if (verbose) {
    // TODO add something for the banks
  }

  return os.str();
}
std::ostream& operator<<(std::ostream& os, const EB::MFP& header) { return os << header.print(); }

// iterator implementation

EB::MFP::iterator::reference EB::MFP::iterator::operator*() const { return *_bank; }
EB::MFP::iterator::pointer EB::MFP::iterator::operator->() { return _bank; }
EB::MFP::iterator& EB::MFP::iterator::operator++()
{
  auto padded_size = *_size + get_padding(*_size, 1 << _align);
  _bank += padded_size;
  _size++;
  return *this;
}

EB::MFP::iterator EB::MFP::iterator::operator++(int)
{
  iterator tmp = *this;
  ++(*this);
  return tmp;
}
bool EB::operator==(const EB::MFP::iterator& a, const EB::MFP::iterator& b) { return a._size == b._size; }
bool EB::operator!=(const EB::MFP::iterator& a, const EB::MFP::iterator& b) { return !(a == b); }

// const iterator implementation
EB::MFP::const_iterator::reference EB::MFP::const_iterator::operator*() const { return *_bank; }
EB::MFP::const_iterator::pointer EB::MFP::const_iterator::operator->() { return _bank; }
EB::MFP::const_iterator& EB::MFP::const_iterator::operator++()
{
  auto padded_size = *_size + get_padding(*_size, 1 << _align);
  _bank += padded_size;
  _size++;
  return *this;
}

EB::MFP::const_iterator EB::MFP::const_iterator::operator++(int)
{
  const_iterator tmp = *this;
  ++(*this);
  return tmp;
}
bool EB::operator==(const EB::MFP::const_iterator& a, const EB::MFP::const_iterator& b) { return a._size == b._size; }
bool EB::operator!=(const EB::MFP::const_iterator& a, const EB::MFP::const_iterator& b) { return a._size != b._size; }

std::ostream& operator<<(std::ostream& os, const Online::pcie40::sodin_t& elem)
{
  os << "ODIN BANK:"
     << "\n";
  os << "run_number: " << elem.run_number() << "\n";
  os << "event_type: " << elem.event_type() << "\n";
  os << "step_number: " << elem.step_number() << "\n";
  os << "gps_time: " << elem.gps_time() << "\n";
  os << "tck: " << elem.tck() << "\n";
  os << "partition_id: " << elem.partition_id() << "\n";
  os << "bunch_id: " << elem.bunch_id() << "\n";
  os << "bx_type: " << std::bitset<2>(elem.bx_type()) << "\n";
  os << "nzs_mode: " << elem.nzs_mode() << "\n";
  os << "tae_central: " << elem.tae_central() << "\n";
  os << "tae_first: " << elem.tae_first() << "\n";
  os << "tae_window: " << (int) elem.tae_window() << "\n";
  // stream << "calib_type: "<< elem.calib_type()   << "\n";
  os << "trigger_type: " << static_cast<unsigned int>(elem.trigger_type()) << "\n";
  os << "orbit_id: " << elem.orbit_id() << "\n";
  os << "event_id: " << elem.event_id() << "\n";
  os << "is_tae : " << elem.is_tae();

  return os;
}