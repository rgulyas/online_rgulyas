#include "buffer_interface.hpp"
#include <stdexcept>

EB::Buffer_element::Buffer_element() { _status = 0; }

EB::Buffer_element::Buffer_element(int payload, size_t size)
{
  _status = 1;
  _payload = payload;
  _size = sizeof(Buffer_element) + size;
}

size_t EB::Buffer_element::bytes() { return _size; }

bool EB::Buffer_element::is_valid() { return _status == 1; }

bool EB::Buffer_element::is_wrap() { return _status == 2; }

void EB::Buffer_element::set_wrap()
{
  _status = 2;
  _size = 0;
  _payload = 0;
}

void EB::Buffer_element::init(int payload, size_t size)
{
  _status = 1;
  _payload = payload;
  _size = sizeof(Buffer_element) + size;
}