#include "dummy_mfp_buffer.hpp"
#include <iostream>

EB::Dummy_MFP_buffer::Dummy_MFP_buffer(std::shared_ptr<char>& buffer, size_t buffer_size, int src_id)
{
  set_buffer(buffer, buffer_size, src_id);
}

void EB::Dummy_MFP_buffer::set_buffer(std::shared_ptr<char>& buffer, size_t buffer_size, int src_id)
{
  _buffer = buffer;
  _size = buffer_size;

  _read_ptr = _buffer.get();

  _src_id = src_id;
}

EB::MFP* EB::Dummy_MFP_buffer::try_get_element()
{
  EB::MFP* ret_val;
  ret_val = reinterpret_cast<EB::MFP*>(get_ptr_at());
  update_read_ptr();

  return ret_val;
}

std::vector<std::tuple<void*, size_t>> EB::Dummy_MFP_buffer::get_full_buffer()
{
  std::vector<std::tuple<void*, size_t>> ret_val;

  ret_val.emplace_back(std::make_tuple(reinterpret_cast<void*>(_buffer.get()), _size));

  return ret_val;
}

void EB::Dummy_MFP_buffer::read_complete() {}

void EB::Dummy_MFP_buffer::flush() { _read_ptr = _buffer.get(); }

int EB::Dummy_MFP_buffer::get_src_id() const { return _src_id; }

// This is a dummy buffer it always considered empty
size_t EB::Dummy_MFP_buffer::get_buffer_occupancy() { return 0; }

char* EB::Dummy_MFP_buffer::get_ptr_at(int MFP_number)
{
  std::uintptr_t offset = reinterpret_cast<std::uintptr_t>(_read_ptr) - reinterpret_cast<std::uintptr_t>(_buffer.get());
  char* ret_val = NULL;
  for (int k = 0; k < MFP_number; k++) {
    if (offset + sizeof(EB::MFP) > _size) {
      offset = 0;
#ifdef DEBUG
      std::cerr << "buffer end restarting from the beginning" << std::endl;
#endif // DEBUG
    }
    ret_val = _buffer.get() + offset;
    size_t MFP_size = reinterpret_cast<EB::MFP*>(ret_val)->header.bytes();
    if (offset + MFP_size > _size) {
      std::cerr << "ERROR truncated event resuming from the beginning" << std::endl;
      offset = 0;
      k--;
    } else {
      size_t tmp_size = MFP_size + get_padding(MFP_size, 1 << reinterpret_cast<EB::MFP*>(ret_val)->header.align);
      // page aligned MFPs
      offset += tmp_size + get_padding(tmp_size, 4096);
      assert(get_padding(MFP_size, 1 << reinterpret_cast<EB::MFP*>(ret_val)->header.align) == 0);
    }
  }
  return ret_val;
}

size_t EB::Dummy_MFP_buffer::get_size_at(int MFP_number)
{
  return (reinterpret_cast<EB::MFP*>(get_ptr_at(MFP_number)))->header.bytes();
}

std::tuple<char*, size_t> EB::Dummy_MFP_buffer::get_MFP_at(int MFP_number)
{
  char* ptr = get_ptr_at(MFP_number);

  size_t size = (reinterpret_cast<EB::MFP*>(ptr))->header.bytes();

  return {ptr, size};
}

std::tuple<char*, size_t> EB::Dummy_MFP_buffer::operator[](int MFP_number) { return get_MFP_at(MFP_number); }

void EB::Dummy_MFP_buffer::update_read_ptr(int MFP_number) { _read_ptr = get_ptr_at(MFP_number + 1); }