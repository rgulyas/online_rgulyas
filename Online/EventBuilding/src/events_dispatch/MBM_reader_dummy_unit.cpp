#include "Dataflow/Incidents.h"
#include "Dataflow/DataflowComponent.h"
#include "events_dispatch/common.hpp"
#include "events_dispatch/MBM_reader_dummy_unit.hpp"

#include <memory>
#include <mutex>
#include <glib.h>

using namespace std;
using namespace Online;
using namespace DISPATCH;
using namespace EB;

/// Standard Constructor
MBM_reader_dummy_unit::MBM_reader_dummy_unit(const string& nam, DataflowContext& ctxt) :
  DataflowComponent(nam, ctxt), Unit()
{
  info("MBM READER: IN CONSTRUCTOR");
  this->_delay_timer = 0;

  declareProperty("how_many_ous", how_many_ous = 1);
  declareProperty("how_many_fus", how_many_fus = 1);
  declareProperty("Buffer", _m_buffer = "Events");
  declareProperty("Requirements", _m_req);
}

/// IService implementation: initialize the service
int MBM_reader_dummy_unit::initialize()
{
  info("MBM READER: IN INTIALIZE");
  int sc = Component::initialize();
  if (sc != DF_SUCCESS) return error("Failed to initialize base-class");
  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &(this->_rank));
  MPI_Comm_size(MPI_COMM_WORLD, &(this->_worldSize));

  info("MBM READER: MY RANK: %d, OUs NUMBER: %d, FUs NUMBER: %d", this->_rank, how_many_ous, how_many_fus);
  info("MBM Consumer connecting to %s\n", _m_buffer.c_str());

  // disabled for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);
  return sc;
}

/// IService implementation: finalize the service
int MBM_reader_dummy_unit::finalize()
{
  MPI_Finalize();
  return DF_SUCCESS;
}

/// IService implementation: finalize the service
/// TODO
int MBM_reader_dummy_unit::stop()
{
  // TODO implement this
  return DF_SUCCESS;
}

/// Cancel I/O operations of the dataflow component
int MBM_reader_dummy_unit::cancel()
{
  // TODO implement this
  _m_receiveEvts = false;
  return DF_SUCCESS;
}

/// Stop gracefully execution of the dataflow component
int MBM_reader_dummy_unit::pause()
{
  // TODO implement this
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void MBM_reader_dummy_unit::handle(const DataflowIncident& inc)
{
  info("MBM_reader_dummy Got incident: %s of type %s", inc.name.c_str(), inc.type.c_str());
}

/// IRunable implementation : Run the class implementation
int MBM_reader_dummy_unit::run()
{
  info("MBM READER: IN RUN");

  int64_t probe_start;
  this->_probe_global_start = g_get_real_time();
  probe_start = _probe_global_start;
  this->_now = probe_start;
  this->_timer_start = probe_start;

  // fpisani removing unused variable
  // int64_t probe_now = g_get_real_time();
  info("MBM READER rank %d IN RUN: Warmup done.  Starting main loop \n", this->_rank);

  while (true) {
    // if (time_elapsed_check() == true) {
    // probe_now = g_get_real_time();
    read_meps();
    //}
  }
  return DF_SUCCESS;
}

int MBM_reader_dummy_unit::start()
{
  info("MBM READER: IN START");
  _m_receiveEvts = true;

  _recv_buff = new Mbm_reader<EB::MEP>(context, RTL::processName(), _m_buffer, _m_req);

  // disabled for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);
  return DF_SUCCESS;
}

void MBM_reader_dummy_unit::read_meps() { std::vector<mep_entry> mep_entries = FBUFF::get_mep<EB::MEP>(_recv_buff); }

bool MBM_reader_dummy_unit::time_elapsed_check()
{
  _delay_timer++;
  if (_delay_timer >= PROBE_EVERY_IT) {
    _delay_timer = 0;
    _now = g_get_real_time();
    if (_now - _timer_start >= TIME_ELAPSED_FOR_GENERATION) {
      _timer_start += TIME_ELAPSED_FOR_GENERATION;
      return true;
    }
  }
  return false;
}
