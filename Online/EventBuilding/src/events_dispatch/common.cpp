#include "events_dispatch/common.hpp"

namespace DISPATCH {
  int how_many_fus = 1;

  int how_many_ous = 1;

  //	int how_many_processes = 1;

  int window_length = 40;

  size_t entry_size_kb = 4096;

  size_t entry_size_b = entry_size_kb * 1024;

  //	size_t bundled_transmissions = MB_IN_GRANT * 1024 / (entry_size_kb);

  bool perform_data_check = false;

  bool constant_size = true;

  bool use_debug_mpi_barriers = true;

  unsigned int gibit_events_in_one_quantum = 86;

  std::stringstream get_shmem_name(std::string shmem_name, int rank)
  {
    std::stringstream buff_name;
    buff_name << shmem_name << "_" << (rank);
    return buff_name;
  }

} // namespace DISPATCH
