#include "timer.hpp"
#include "EventBuilding/tools.hpp"
#include <system_error>
#include <cstring>

EB::Timer::Timer(clockid_t clk_id)
{
  _clock_id = clk_id;
  reset();
  _is_running = false;
  _is_enabled = true;
}

clockid_t EB::Timer::get_clk_id() const { return _clock_id; }

void EB::Timer::set_clk_id(clockid_t clk_id)
{
  _clock_id = clk_id;
  reset();
}

void EB::Timer::start()
{
  if (_is_enabled) {
    // if the clock is running no action is needed
    if (!_is_running) {
      _is_running = true;
      int error = clock_gettime(_clock_id, &_start_time);
      if (error != 0) {
        throw std::system_error(errno, std::generic_category(), "Timer start clock_gettime");
      }
    }
  }
}

void EB::Timer::stop()
{
  if (_is_enabled) {
    // if the clock is not running no action is needed
    if (_is_running) {
      _elapsed_time = get_elapsed_time_ns();
      _is_running = false;
    }
  }
}

void EB::Timer::reset()
{
  if (_is_enabled) {
    // if the clock is running we reset the start time
    if (_is_running) {
      int error = clock_gettime(_clock_id, &_start_time);
      if (error != 0) {
        throw std::system_error(errno, std::generic_category(), "Timer reset clock_gettime");
      }
    }

    _elapsed_time = 0;
  }
}

clock_t EB::Timer::get_elapsed_time_ns() const
{
  clock_t ret_val = 0;
  if (_is_enabled) {
    ret_val = _elapsed_time;
    if (_is_running) {
      timespec stop_time;
      int error = clock_gettime(_clock_id, &stop_time);
      if (error != 0) {
        throw std::system_error(errno, std::generic_category(), "Timer get_elapsed_time_ns clock_gettime");
      }

      ret_val += timespec_diff_ns(stop_time, _start_time);
    }
  }
  return ret_val;
}

double EB::Timer::get_elapsed_time_s() const { return get_elapsed_time_ns() / 1.e9; }

void EB::Timer::disable() { _is_enabled = false; }

void EB::Timer::enable() { _is_enabled = true; }

bool EB::Timer::is_enabled() const { return _is_enabled; }

bool EB::Timer::is_running() const { return _is_running; }