//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  EventHeader.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_EVENTHEADER
#define ONLINE_EVENTDATA_EVENTHEADER

#include <EventData/event_header_t.h>
namespace Online   {
  using EventHeader = event_header_t;
}
#endif // ONLINE_EVENTDATA_EVENTHEADER
