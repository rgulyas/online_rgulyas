//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  run3_detector_bits.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_RUN3_DETECTOR_BITS_H
#define ONLINE_EVENTDATA_RUN3_DETECTOR_BITS_H 1

/// Online namespace declaration
namespace Online    {

  /// Enumeration containing the detector identifier bits in the top 5 bits of the source ID
  namespace run3  {
    enum run3_detector_bits  {
      RO_BIT_ODIN   = 0,
      RO_BIT_VELOA  = 2,
      RO_BIT_VELOC  = 3,
      RO_BIT_UTA    = 5,
      RO_BIT_UTC    = 6,
      RO_BIT_SFA    = 7,
      RO_BIT_SFC    = 8,
      RO_BIT_RICH1  = 4,
      RO_BIT_RICH2  = 9,
      RO_BIT_PLUME  = 10,
      RO_BIT_ECAL   = 11,
      RO_BIT_HCAL   = 12,
      RO_BIT_MUONA  = 13,
      RO_BIT_MUONC  = 14,
      RO_BIT_TDET   = 15,
    };
  }
}      // End namespace Online
#endif /* ONLINE_EVENTDATA_RUN3_DETECTOR_BITS_H  */
