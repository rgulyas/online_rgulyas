//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//==========================================================================
#ifndef ONLINE_PCIE40DATA_PCIE40DATA_RAWBANK40_H_
#define ONLINE_PCIE40DATA_PCIE40DATA_RAWBANK40_H_

// Framework include files
#include <EventData/raw_bank_online_t.h>

/// Online namespace declaration
namespace Online {
  using RawBank40 = raw_bank_online_t;
}    // namepace Online
#endif /* ONLINE_PCIE40DATA_PCIE40DATA_RAWBANK40_H_ */
