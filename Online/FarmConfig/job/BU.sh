#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
unset PYTHONPATH;
unset PYTHONHOME;

export BU_OPTIONS=$(echo "$UTGID.opts" | cut -d _ -f 3- -)
if test "${PARTITION}" = "TDET"; then
	. /group/online/dataflow/EventBuilder/EventBuilderDev/install.ebonly.x86_64-centos9-gcc12-dbg/bin/thisonline.sh;
elif test "${PARTITION}" = "FEST"; then
	. /group/online/dataflow/EventBuilder/EventBuilderDev/install.ebonly.x86_64-centos9-gcc12-dbg/bin/thisonline.sh;
else
	. /group/online/dataflow/EventBuilder/EventBuilderRelease/install.ebonly.LCG_103.x86_64-centos9-gcc12-opt/bin/thisonline.sh;
fi;

cd ${FARMCONFIGROOT}/job;
`dataflow_task Class1` -opts=${EVENTBUILDINGROOT}/options/${BU_OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
