#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the storage/monitoring nodes
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
##echo "[ERROR] exec -a ${UTGID} ${DATAFLOW_task} -class=${TASK_CLASS} -opts=${TASK_OPTIONS}";
test_release=/home/frankm/cmtuser/OnlineDev_v6r4;
if test -z "$test_release";then
    if test -f ${test_release}/Online/FarmConfig/options/${TASKTYPE}.opts; then
	cd ${test_release};
	. setup.${CMTCONFIG}.vars;
	cd ${FARMCONFIGROOT}/job;
	echo "[WARN] +++ Starting experimental task ${TASKTYPE} from ${test_release}";
    fi;
fi;
###echo "${UTGID} DATAINTERFACE: ${DATAINTERFACE} Opts: ${TASK_OPTIONS}"
exec -a ${UTGID} ${DATAFLOW_task} -class=${TASK_CLASS} -opts=${TASK_OPTIONS};
