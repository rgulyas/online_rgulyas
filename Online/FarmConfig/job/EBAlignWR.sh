#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
execute `dataflow_task Class2` -opts=${STATIC_OPTS}/EBAlignWR.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
