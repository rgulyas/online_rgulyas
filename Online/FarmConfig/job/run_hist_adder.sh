#!/bin/bash
rm procdb.dat;
#
gdb --args gentest libHistAdder.so run_hist_app \
    -rundb-scan-interval=10 \
    -procdb-scan-interval=10 \
    -days-to-scan=5 \
    -procdb-file=procdb.dat   \
    -input=/hist/HLT2test/2023/LHCb2 \
    -output=/group/online/dataflow/cmtuser/Savesets \
    -partition=LHCb           \
    -task=HLT2                \
    -temporary=/tmp           \
    -chunk_size=100           \
    -print=INFO
 
