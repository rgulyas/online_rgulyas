#if 0
//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_ADDTIMER_H
#define ONLINE_GAUCHO_ADDTIMER_H

#include "Gaucho/GenTimer.h"

/// Online namespace declaration
namespace Online   {

  class MonAdder;

  class AddTimer : public GenTimer    {
    MonAdder *m_owner;
    bool      m_stopped;
  public:
    AddTimer(MonAdder *tis, int period = 5, int typ = 0);
    virtual ~AddTimer(void);
    void timerHandler ( void ) override;
    void stop()  override;
    void startPeriodic(int dt)  override;
  };
}
#endif  // ONLINE_GAUCHO_ADDTIMER_H
#endif
