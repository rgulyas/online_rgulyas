#if 0
//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <GauchoAppl/AddTimer.h>
#include <GauchoAppl/MonAdder.h>
#include <dim/dic.hxx>

using namespace Online;

AddTimer::AddTimer(MonAdder *tis, int period, int typ) : GenTimer((void*)this,period*1000,typ)   {
  this->m_owner = tis;
  this->m_dueTime = 0;
  this->m_dontdimlock = true;
  this->m_stopped = false;
}

AddTimer::~AddTimer()  {
}

void AddTimer::timerHandler()   {
  int arg = 1;
  if (!this->m_stopped)  {
    DimClient::sendCommandNB(this->m_owner->cmdName.c_str(),&arg,sizeof(arg));
  }
}

void AddTimer::startPeriodic(int dt)  {
  this->m_stopped = false;
  this->GenTimer::startPeriodic(dt);
}

void AddTimer::stop()   {
  this->m_stopped = true;
  this->GenTimer::stop();
}
#endif
