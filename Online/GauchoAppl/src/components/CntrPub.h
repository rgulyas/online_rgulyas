//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_CNTRPUB_H
#define ONLINE_GAUCHO_CNTRPUB_H

#include "PubSvc.h"
#include <GaudiKernel/Service.h>
#include <GaudiKernel/IToolSvc.h>
#include <GauchoAppl/MonAdder.h>
#include <regex>


// Forward declarations
class DimService;

class CntrPub: public PubSvc {
  struct  SVCDescr {
    int type;
    std::string name;
    int idata;
    float rdata;
    DimService *svc;
  };
public:
  CntrPub(const std::string& name, ISvcLocator* sl);
  virtual ~CntrPub() = default;
  virtual StatusCode start() override;
  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;
  virtual void analyze(mem_buff& buffer, Online::MonitorItems* mmap)  override;

  std::regex   m_counterPatternRegex;
  std::string  m_SvcInfix;
  std::string  m_counterPattern;
  std::map<std::string,SVCDescr*> m_cntrSvcMap;

  DimService  *m_noEvtsSvc  { nullptr };
  DimService  *m_EvtRateSvc { nullptr };

  int    m_NoEvts   {0};
  double m_EvtRate  {0e0};
};
#endif // ONLINE_GAUCHO_CNTRPUB_H
