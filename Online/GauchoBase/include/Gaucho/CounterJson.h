//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_COUNTERJSON_H
#define ONLINE_GAUCHO_COUNTERJSON_H

#include <Gaucho/MonTypes.h>
#include <nlohmann/json.hpp>

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class DimBuffBase;

  class JsonCounterDeserialize   {
  public:
    typedef nlohmann::json json;

  protected:
    template <typename TYPE> static json _scalar(const DimBuffBase* p, const std::string& typ);
    template <typename TYPE> static json _numbers(const DimBuffBase* b, const std::string& typ);
    template <typename FIRST, typename SECOND> static json _pair(const DimBuffBase* b, const std::string& typ);
    template <typename ENTRIES, typename SUM, typename MEAN=double>  
      static json _average(const DimBuffBase* b, const std::string& typ);
    template <typename ENTRIES, typename SUM, typename MEAN=double>  
      static json _sigma(const DimBuffBase* b, const std::string& typ);
    template <typename ENTRIES, typename SUM, typename MEAN=double>  
      static json _stat(const DimBuffBase* b, const std::string& typ);
    template <typename FIRST, typename SECOND> 
      static json _binomial(const DimBuffBase* b, const std::string& typ);

  public:
    static json de_serialize(const void* ptr);
  };
}
#endif // ONLINE_GAUCHO_COUNTERDESERIALIZE_H
