//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_COUNTERTASKJSON_H_
#define ONLINE_GAUCHO_COUNTERTASKJSON_H_

#include <Gaucho/TaskRPC.h>
#include <Gaucho/CounterJson.h>
#include <regex>

/// Online namespace declaration
namespace Online  {

  class CounterTaskJson : public TaskRPC  {
  protected:
    typedef nlohmann::json json;
  public:
    CounterTaskJson(const std::string& task,const std::string& dns="", int tmo=3);
    virtual ~CounterTaskJson() = default;
    int counters(const std::vector<std::string>& names, std::vector<json>& Cntrs);
    int counters(const std::vector<std::string>& names, std::map<std::string, json>& Cntrs);
    json counter_directory();
    json counters();
    json counters(const std::string& selection);
    template <typename CONT> static int taskList(const std::string& dns, CONT& tasks)
      {   return TaskRPC::_taskList("Counter", dns, tasks);  }
    static json taskList(const std::string& dns);
  };
}
#endif // ONLINE_GAUCHO_COUNTERTASKJSON_H_
