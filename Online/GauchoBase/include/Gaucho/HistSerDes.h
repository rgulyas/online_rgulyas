//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHOBASE_GAUCHO_HISTSERDES_H_
#define ONLINE_GAUCHOBASE_GAUCHO_HISTSERDES_H_

#include <string>
#include <vector>
class TAxis;

/// Online namespace declaration
namespace Online   {

  class BinNAxis;

  class HistSerDes  {
  public:
    static void *de_serialize(const void *ptr, const char *nam=0);
    static void  cpyBinLabels(char *ptr, const BinNAxis& axis);
    static void  SetBinLabels(TAxis *ax, const char *l);
    static int   GetBinLabels(TAxis *ax, std::vector<std::string>&);
    static std::string optsep;
  };
}

#endif /* ONLINE_GAUCHOBASE_GAUCHO_HISTSERDES_H_ */
