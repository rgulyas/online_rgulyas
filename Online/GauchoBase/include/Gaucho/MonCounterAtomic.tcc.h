//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

#include <atomic>
#include <typeinfo>

/// Online namespace declaration
namespace Online  {

  /// specialization of MonCounter class for atomic types
  template <typename T> struct MonCounter<std::atomic<T>> : MonCounterBase {

  MonCounter(const std::string& nam, const std::string& tit, const std::atomic<T>& data) :
    MonCounterBase{Type(), nam, tit, std::max(8u, serialSize()), serialSize(), (std::atomic<T>*)&data} {}

    constexpr static MONTYPE Type() {
      if constexpr (std::is_same_v<T, int>) {
	  return C_ATOMICINT;
	} else if constexpr (std::is_same_v<T, long>) {
	  return C_ATOMICLONG;
	} else if constexpr (std::is_same_v<T, float>) {
	  return C_ATOMICFLOAT;
	} else if constexpr (std::is_same_v<T, double>) {
	  return C_ATOMICDOUBLE;
	} else {
	static_assert(false_v<T>, "Unsupported type for atomic specialization of MonCounter");
      }
    }

    constexpr static unsigned int serialSize() {
      std::atomic<T> q;
      return sizeof( q.load() );
    }
    unsigned int serializeData(DimBuffBase *pp, const void *src, int )  const override {
      long *dst = add_ptr<long>(pp, pp->dataoff);
      std::atomic<T> *s = (std::atomic<T>*)src;

      *dst = 0;
      auto q = s->load();
      decltype(q) *a = (decltype(q)*)dst;
      *a = q;
      a = (decltype(q)*)&m_atomData;
      *a = q;
      return pp->reclen;
    }

    void create_OutputService(const std::string & infix) override {
      this->dim_service.reset();
      std::string nam = this->m_srvcprefix + infix + this->name;
      if (nam.length() <= 128 /* maximum length of a DIM service Name*/) {
	this->m_atomData = 0;
	std::atomic<T> *s = (std::atomic<T>*)this->m_contents;
	auto q = s->load();
	decltype(q) *a = (decltype(q)*)&this->m_atomData;
	*a = q;
	this->dim_service.reset(new DimService(nam.c_str(),typeid(q).name(), &this->m_atomData,sizeof(*a)));
      }
    }
  };
}
