//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef OBJRRPC_H
#define OBJRRPC_H
#include <dim/dis.hxx>
#include <Gaucho/RPCdefs.h>
#include <Gaucho/Serializer.h>

#include <memory>

/// Online namespace declaration
namespace Online   {

  class BRTLLock;

  class ObjRPC : public DimRpc    {
  protected:
    std::shared_ptr<DimServerDns>  dns;
    std::unique_ptr<Serializer>    serializer;
    BRTLLock *maplock  { nullptr };
    BRTLLock *objlock  { nullptr };

  public:
    ObjRPC() = delete;
    ObjRPC(std::unique_ptr<Serializer>&& serial, const std::string& name, const char *f_in, const char *f_out, BRTLLock* = nullptr, BRTLLock * = nullptr);
    ObjRPC(std::shared_ptr<DimServerDns> dns,std::unique_ptr<Serializer>&& serial, const std::string& name, const char *f_in, const char *f_out, BRTLLock* = nullptr, BRTLLock * = nullptr);
    virtual ~ObjRPC() = default;
    void rpcHandler() override;
  };
}
#endif
