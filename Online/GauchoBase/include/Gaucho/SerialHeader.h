//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * SerialHeader.h
 *
 *  Created on: Aug 26, 2010
 *      Author: beat
 */

#ifndef SERIALHEADER_H_
#define SERIALHEADER_H_

#include <cstring>

#define SERHEADER_Compress 1<<0
#define SERHEADER_Version  2
#define SERIAL_MAGIC       0xfeedbabe

namespace Online   {

  class SerialHeader_V1   {
  public:
    unsigned int m_magic;
    int flags;
    int version;
    int comp_version;
    long ser_tim;
    long run_number;
    int buffersize;
  };

  class SerialHeader_V2   {
  public:
    unsigned int m_magic;
    int flags;
    int version;
    int comp_version;
    long ser_tim;
    long run_number;
    int buffersize;
    unsigned int updateInterval;
    int level;
  };

  class SerialHeader   {
  public:
    unsigned int m_magic        { SERIAL_MAGIC };
    int          flags          { 0 };
    int          version        { SERHEADER_Version };
    int          comp_version   { 0 };
    long         ser_tim        { 0 };
    long         run_number     { 0 };
    int          buffersize     { 0 };
    unsigned int updateInterval { 0 };
    int          level          { 0 };

    SerialHeader() = default;
    SerialHeader(const SerialHeader& copy) = default;
    SerialHeader & operator = (const SerialHeader &copy);

    template <typename RETURN_TYPE=void> RETURN_TYPE* endPtr()  {
      switch(version)    {
      case 1:
      return (RETURN_TYPE*)((char*)this + sizeof(SerialHeader_V1));
      case 2:
      return (RETURN_TYPE*)((char*)this + sizeof(SerialHeader_V2));
      default:
      return (RETURN_TYPE*)((char*)this + sizeof(SerialHeader));
      }
    }

    template <typename RETURN_TYPE=void> const RETURN_TYPE* endPtr()  const {
      switch(version)    {
      case 1:
      return (RETURN_TYPE*)((char*)this + sizeof(SerialHeader_V1));
      case 2:
      return (RETURN_TYPE*)((char*)this + sizeof(SerialHeader_V2));
      default:
      return (RETURN_TYPE*)((char*)this + sizeof(SerialHeader));
      }
    }

    size_t SizeOf() const  {
      switch(version)   {
      case 1:
	return sizeof(SerialHeader_V1);
      case 2:
	return sizeof(SerialHeader_V2);
      default:
	return sizeof(SerialHeader);
      }
    }
  };

  inline SerialHeader & SerialHeader::operator = (const SerialHeader &copy)  {
    ::memcpy(this, &copy, copy.SizeOf());
    return *this;
  }
}
#endif /* SERIALHEADER_H_ */
