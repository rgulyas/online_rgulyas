//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_TASKRPC_H_
#define ONLINE_GAUCHO_TASKRPC_H_

#include <Gaucho/RPCRec.h>
#include <vector>
#include <string>
#include <memory>
#include <list>
#include <set>
#include <map>

/// Online namespace declaration
namespace Online  {

  class RPCRec;
  class CntrDescr;

  class TaskRPC   {
  protected:
    std::string m_dns;
    std::string m_task;
    std::unique_ptr<RPCRec> m_RPC;
    template <typename CONT> static int _taskList(const std::string& type, const std::string& dns, CONT& tasks);

  public:
    TaskRPC(const std::string& type, const std::string& task,const std::string& dns="", int tmo=0);
    virtual ~TaskRPC();
    const std::string& taskName()  const   {  return m_task;  }
    int directory (std::vector<std::string>& Cntr);
    int items(const std::vector<std::string>& names);
    int items(const std::string& selection);
  };
}
#endif // ONLINE_GAUCHO_TASKRPC_H_
