//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * Utilities.h
 *
 *  Created on: Jan 25, 2011
 *      Author: beat
 */

#ifndef ONLINE_GAUCHO_UTILITIES_H_
#define ONLINE_GAUCHO_UTILITIES_H_

#include <string>

namespace    {

  template <typename T=void> inline T* add_ptr(void* ptr, unsigned long offs)   {
    return reinterpret_cast<T*>(((char*)ptr + offs));
  }
  template <typename T=void> inline const T* add_ptr(const void* ptr, unsigned long offs)   {
    return reinterpret_cast<const T*>(((const char*)ptr + offs));
  }
  template <typename T=void> inline T* ptr_as(void* ptr)   {
    return reinterpret_cast<T*>(ptr);
  }
  template <typename T=void> inline const T* ptr_as(const void* ptr)   {
    return reinterpret_cast<const T*>(ptr);
  }
  inline size_t sum_string_length(size_t length, const std::string& str)           {
    return length + str.length();
  }

  template <class T> class ObjectLock {
  private:
    T* m_lockable;
  public:
    ObjectLock() = delete;
    ObjectLock(ObjectLock&& copy) = delete;
    ObjectLock(const ObjectLock& copy) = delete;
    ObjectLock& operator=(ObjectLock&& copy) = delete;
    ObjectLock& operator=(const ObjectLock& copy) = delete;

    ObjectLock(T* object) : m_lockable(object) { if (m_lockable) m_lockable->lock();   }
    ~ObjectLock()                              { if (m_lockable) m_lockable->unlock(); }
  };
  template <typename LOCKABLE, typename T> void locked_execution(LOCKABLE* lockable_object, T func)    {
    ObjectLock<LOCKABLE> lock(lockable_object);
    func();
  }
  template <typename LOCKABLE, typename T> void locked_execution(LOCKABLE& lockable_object, T func)    {
    ObjectLock<typename LOCKABLE::element_type> lock(lockable_object.get());
    func();
  }
}
#endif /* ONLINE_GAUCHO_UTILITIES_H_  */

