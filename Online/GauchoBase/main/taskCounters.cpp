//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework includes
#include <Gaucho/CounterTask.h>
#include <Gaucho/MonCounter.h>
#include <RTL/rtl.h>

/// C/C++ includes
#include <iomanip>
#include <iostream>

using namespace Online;

static void print_tasks( const std::set<std::string>& tasks ) {
  size_t i = 0;
  for ( const auto& t : tasks ) ::printf( "Task[%3ld]  %s\n", ++i, t.c_str() );
}

int main( int argc, char* argv[] ) { // Taskname, DNS Node
  std::string dns, task;
  int         continuous = 0;
  RTL::CLI    cli( argc, argv, []() {
      std::cout << " Usage: taskCounters -arg [-arg]  \n"
		<< "  -d(ns)=<dns>            DNS name to interrogate for services\n"
		<< "  -t(ask)=<task-name>     Supply task name for detailed counter printout\n"
		<< "  -c(continuous)=<number> Continuous mode. Give seconds between updates." << std::endl;
    } );
  bool        help = cli.getopt( "help", 1 );
  cli.getopt( "continuous", 1, continuous );
  cli.getopt( "task", 1, task );
  cli.getopt( "dns", 1, dns );
  if ( help ) { cli.call_help(); }
  if ( dns.empty() ) {
    std::cout << "Insufficient Number of arguments" << std::endl;
    cli.call_help();
  }
  if ( continuous == 1 ) { continuous = 2; }

  std::set<std::string> tasks;
  CounterTask::taskList( dns, tasks );
  if ( task.empty() ) {
    print_tasks( tasks );
  } else {
    auto                     h = std::make_unique<CounterTask>( task, dns );
    std::vector<std::string> counters;
    std::vector<std::string> hsts;
    std::vector<CntrDescr*>  objs;

    int status = h->directory( counters );
    if ( status == 1 ) {
      std::cout << "Task " << task << " does not exist..." << std::endl;
      return 1;
    }
    std::cout << "Number of Counters for Task " << task << " " << counters.size() << std::endl;
    std::cout << "Trying to retrieve the following Counters:" << std::endl;

    for ( size_t i = 0; i < counters.size(); i++ ) {
      hsts.push_back( counters[i] );
      //std::cout << hsts[i] << std::endl;
    }
    std::cout << std::flush;
  Again:
    objs.clear();
    h->counters( hsts, objs );
    std::cout << std::endl
              << RTL::timestr() << "  Retrieved " << objs.size() << " Counters  from " << task << std::endl;
    for ( size_t i = 0; i < objs.size(); i++ ) {
      char text[128];
      switch ( objs[i]->type ) {
      case C_GAUDIACCCHAR:
      case C_GAUDIACCuCHAR:
        ::snprintf( text, sizeof( text ), "(CHAR)         %d", int((signed char)objs[i]->scalars.i_data) );
        break;
      case C_GAUDIACCSHORT:
      case C_GAUDIACCuSHORT:
        ::snprintf( text, sizeof( text ), "(SHORT)        %d", int((signed short)objs[i]->scalars.i_data) );
        break;
      case C_INT:
      case C_ATOMICINT:
      case C_GAUDIACCINT:
        ::snprintf( text, sizeof( text ), "(INT)          %d", (int)objs[i]->scalars.i_data );
        break;
      case C_UINT:
      case C_GAUDIACCuINT:
        ::snprintf( text, sizeof( text ), "(UINT)         %d", int((unsigned int)objs[i]->scalars.ui_data) );
        break;
      case C_LONGLONG:
      case C_ATOMICLONG:
      case C_GAUDIACCLONG:
        ::snprintf( text, sizeof( text ), "(LONG)         %ld", long( objs[i]->scalars.l_data ) );
        break;
      case C_ULONG:
      case C_GAUDIACCuLONG:
        ::snprintf( text, sizeof( text ), "(ULONG)        %ld", (unsigned long)( objs[i]->scalars.ul_data ) );
        break;
      case C_FLOAT:
      case C_ATOMICFLOAT:
        ::snprintf( text, sizeof( text ), "(FLOAT)        %f", objs[i]->scalars.f_data );
        break;
      case C_DOUBLE:
      case C_ATOMICDOUBLE:
        ::snprintf( text, sizeof( text ), "(DOUBLE)       %f", objs[i]->scalars.d_data );
        break;
      case C_RATEDOUBLE:
        ::snprintf( text, sizeof( text ), "(RATEDOUBLE)   %f Hz", objs[i]->scalars.d_data );
        break;
      case C_RATEFLOAT:
        ::snprintf( text, sizeof( text ), "(RATEFLOAT)    %f Hz", objs[i]->scalars.f_data );
        break;
      case C_INTSTAR:
      case C_UINTSTAR:
        for ( int j = 0, nel = objs[i]->nel, *p = (int*)objs[i]->ptr.get(); j < nel; j++ ) {
          ::snprintf( text, sizeof( text ), "(INT*) index  %d value %d", j, p[j] );
        }
        break;
      case C_LONGSTAR:
      case C_ULONGSTAR: {
        long* p   = (long*)objs[i]->ptr.get();
        int   nel = objs[i]->nel;
        for ( int j = 0; j < nel; j++ ) { ::snprintf( text, sizeof( text ), "(LONG*) index   %d value %li", j, p[j] ); }
      } break;
      case C_FLOATSTAR: {
        float* p   = (float*)objs[i]->ptr.get();
        int    nel = objs[i]->nel;
        for ( int j = 0; j < nel; j++ ) { ::snprintf( text, sizeof( text ), "(FLOAT*) index  %d value %f", j, p[j] ); }
      } break;
      case C_DOUBLESTAR:
      case C_RATEDOUBLESTAR: {
        double* pp  = (double*)objs[i]->ptr.get();
        int     nel = objs[i]->nel;
        for ( int j = 0; j < nel; j++ ) {
          double p = pp[j];
          ::snprintf( text, sizeof( text ), "(DOUBLE*) index %d value %f", j, p );
        }
      } break;
      case C_INTPAIR:
        ::snprintf( text, sizeof( text ), "(INT Pair) value  %d %d", objs[i]->ip_data.first, objs[i]->ip_data.second );
        break;
      case C_LONGPAIR:
        ::snprintf( text, sizeof( text ), "(LONG Pair) value %ld %ld", objs[i]->lp_data.first, objs[i]->lp_data.second );
        break;

      case C_GAUDIAVGACCc:
      case C_GAUDIAVGACCcu:
      case C_GAUDIAVGACCs:
      case C_GAUDIAVGACCsu:
      case C_GAUDIAVGACCi:
      case C_GAUDIAVGACCiu:
      case C_GAUDIAVGACCl:
      case C_GAUDIAVGACClu:
      case C_GAUDIAVGACCf:
      case C_GAUDIAVGACCd:
        ::snprintf( text, sizeof( text ), "(Gaudi Averaging Counter) Entries: %ld Sum: %f Mean: %f", objs[i]->gaudi.average.entries,
		    objs[i]->gaudi.average.sum, objs[i]->gaudi.average.mean );
        break;

      case C_GAUDISTATACCc:
      case C_GAUDISTATACCcu:
      case C_GAUDISTATACCs:
      case C_GAUDISTATACCsu:
      case C_GAUDISTATACCi:
      case C_GAUDISTATACCiu:
      case C_GAUDISTATACCl:
      case C_GAUDISTATACClu:
      case C_GAUDISTATACCf:
      case C_GAUDISTATACCd:
        ::snprintf( text, sizeof( text ), "(Gaudi Stat Counter) Entries: %ld Sum: %f Sum2: %f Mean: %f",
		    objs[i]->gaudi.stat.entries,
		    objs[i]->gaudi.stat.sum, 
		    objs[i]->gaudi.stat.sum2,
		    objs[i]->gaudi.stat.mean );
        break;

      case C_GAUDISIGMAACCc:
      case C_GAUDISIGMAACCcu:
      case C_GAUDISIGMAACCs:
      case C_GAUDISIGMAACCsu:
      case C_GAUDISIGMAACCi:
      case C_GAUDISIGMAACCiu:
      case C_GAUDISIGMAACCl:
      case C_GAUDISIGMAACClu:
      case C_GAUDISIGMAACCf:
      case C_GAUDISIGMAACCd:
        ::snprintf( text, sizeof( text ), "(Gaudi Sigma Counter) Entries: %ld Sum: %f Sum2: %f Mean: %f",
		    objs[i]->gaudi.sigma.entries,
		    objs[i]->gaudi.sigma.sum,
		    objs[i]->gaudi.sigma.sum2,
		    objs[i]->gaudi.sigma.mean );
        break;

      case C_GAUDIBINACCc:
      case C_GAUDIBINACCcu:
      case C_GAUDIBINACCs:
      case C_GAUDIBINACCsu:
      case C_GAUDIBINACCi:
      case C_GAUDIBINACCiu:
      case C_GAUDIBINACCl:
      case C_GAUDIBINACClu:
      case C_GAUDIBINACCf:
      case C_GAUDIBINACCd:
        ::snprintf( text, sizeof( text ), "(Gaudi Binomial Counter) Entries: %ld True: %ld False: %ld",
		    objs[i]->gaudi.binomial.entries,
		    objs[i]->gaudi.binomial.entries_true,
		    objs[i]->gaudi.binomial.entries_false); 
        break;

      case C_GAUDIMSGACClu:
        ::snprintf( text, sizeof( text ), "(Gaudi Msg Counter) %lu", long( objs[i]->scalars.ul_data ) );
        break;
      default:
        snprintf( text, sizeof( text ), "Unknown Counter" );
        break;
      }
      std::cout << std::left << std::setw(48) << objs[i]->name << std::hex << objs[i]->type
                << std::scientific << " @ " << (void*)objs[i] << " data " << text << std::endl;
      std::cout << std::flush;
    }
    if ( continuous > 0 ) {
      std::cout << "========================================================================================="
                << std::endl;
      ::lib_rtl_sleep( continuous * 1000 );
      goto Again;
    }
  }
  return 0;
}
