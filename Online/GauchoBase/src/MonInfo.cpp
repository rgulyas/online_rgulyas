//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/MonInfo.h>
#include <CPP/mem_buff.h>

using namespace Online;

namespace  {
  static int  s_empty =-1;
  static bool s_shutdowninProgress = false;
}

void MonInfo::setShutdownInProgress(bool value)   {
  s_shutdowninProgress = value;
}

MonInfo::MonInfo(const std::string& target, MonInfoHandler* hdlr)
  : DimUpdatedInfo(target.c_str(), &s_empty, sizeof(int)), handler(hdlr)
{
}

MonInfo::MonInfo(const std::string& target, int period, MonInfoHandler* hdlr)
  : DimUpdatedInfo(target.c_str(), period, &s_empty, sizeof(int)), handler(hdlr)
{
}

void MonInfo::infoHandler()   {
  void *valin = getData();
  int   size  = getSize();
  if (size == sizeof(int) && *(int*)valin == s_empty )    {
    return;
  }
  else if ( s_shutdowninProgress )   {
    return;
  }
  else if ( this->handler != nullptr )   {
    mem_buff buffer(valin, size);
    (*this->handler)(buffer.begin(), size, this);
  }
}
