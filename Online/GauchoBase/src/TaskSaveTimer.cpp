//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/TaskSaveTimer.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/MonitorClass.h>
#include <Gaucho/ObjService.h>

/// Initializing constructor
Online::TaskSaveTimer::TaskSaveTimer(int timer_period)
  : GenTimer(timer_period * 1000, TIMER_TYPE_PERIODIC)
{
  m_dueTime = 0;
  m_dontdimlock = true;
}

/// Default constructor
Online::TaskSaveTimer::~TaskSaveTimer()  {
}

/// Timer callback
void Online::TaskSaveTimer::timerHandler(void)   {
  // Need protection against sub_system==0:
  // If the save time runs, but no histograms were
  // ever published, sub_system (histograms) is NULL!
  if ( this->sub_system )   {
    if ( this->sub_system->runNumber == 0 ) return;
    if ( !this->m_dontdimlock )
      dim_lock();
    this->makeDirs(this->sub_system->runNumber);
    auto file = openFile();
    if ( !this->m_dontdimlock )
      dim_unlock();
    for (const auto& i : this->sub_system->classMgr.getMap() )   {
      locked_execution(this->sub_system, [this,i]() {
	  if (i.second->genSrv) {
            i.second->genSrv->setRunNo(this->sub_system->runNumber);
	    i.second->genSrv->serialize(this->buffer, 0);
	  }
	});
      // TODO the serialize() -> savetoFile() chain should be replaced with the logic from the monitoring sink
      if ( this->buffer.used() > 0 )  {
	file->cd();
	this->savetoFile(this->buffer.begin());
      }
    }
    this->closeFile(std::move(file));
    this->updateSaveSet();
    this->sub_system->reset();
  }
}
