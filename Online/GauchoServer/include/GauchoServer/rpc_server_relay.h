//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================
#ifndef ONLINE_GAUCHOSERVER_RPC_SERVER_RELAY_H
#define ONLINE_GAUCHOSERVER_RPC_SERVER_RELAY_H

// C++ include files
#include <condition_variable>
#include <cstdint>
#include <memory>
#include <string>
#include <mutex>
#include <regex>
#include <map>

// Framework includes

namespace Online  {

  class rpc_server_relay   {
  public:
    struct server_t;
    typedef std::map<std::string,std::shared_ptr<server_t> > srv_map_t;

    struct rpc_t   {
    public:
      std::string   service         { };
      std::string   command         { };
      std::mutex    lock_mutex      { };
      server_t*     server          { nullptr };
      unsigned long rpc_type        { 0 };

    private:
      int           connected       { 0 };
      int           response_svc_id { 0 };

    private:
      static void rpc_submission_response(void* tag, int* success);
      static void rpc_response_call(void* tag, void* buffer, int* size);

    public:
      rpc_t(const std::string& svc, unsigned long type, server_t* srv);
      rpc_t(const rpc_t& copy) = delete;
      rpc_t(rpc_t&& copy) = delete;
      rpc_t() = delete;
      virtual ~rpc_t();

      bool is_connected()  const   {   return this->connected != 0; }
      void call(const void* data, size_t len);
      void call_with_feedback(const void* data, size_t len);
      void get_time_stamp(int& secs, int& milli);

      virtual void on_submission_response(bool succeeded);
      /// Handle RPC response from server
      virtual void on_response(const void* data, size_t len);
      /// Handle no-link RPC response
      void on_nolink();
      /// Lock RPC handler
      bool lock();
      /// Unlock RPC handler
      bool unlock();
    };
    typedef std::map<std::string,std::shared_ptr<rpc_t> > rpc_map_t;

    struct dns_t    {
    public:
      std::string       name     { };
      srv_map_t         servers  { };
      rpc_server_relay* listener { nullptr };
      long              dns_id   { 0 };
      int               info_id  { 0 };

    public:
      dns_t(const std::string& n, rpc_server_relay* l, long d, int i)
	: name(n), listener(l), dns_id(d), info_id(i) {}
      dns_t(const dns_t& copy) = delete;
      dns_t(dns_t&& copy) = delete;
      dns_t() = delete;
    };

    struct server_t    {
    public:
      std::string            name        { };
      std::string            node        { };
      rpc_map_t              connections { };
      std::shared_ptr<dns_t> dns         { };
      rpc_server_relay*      listener    { nullptr };
      int                    info_id     { 0 };

    public:
      server_t(const std::string& nam, const std::string& nod, std::shared_ptr<dns_t>& d, rpc_server_relay* l)
	: name(nam), node(nod), dns(d), listener(l), info_id(0) {}
      server_t(const server_t& copy) = delete;
      server_t(server_t&& copy) = delete;
      server_t() = delete;
      ~server_t();

      /// Handle RPC response
      void on_response(rpc_t* rpc, const void* data, size_t len);
      /// Handle no-link RPC response
      void on_nolink(rpc_t* rpc);

      void clear_all_rpc();
      bool remove_rpc(const std::string& service);
      bool add_rpc(const std::string& service, unsigned long cookie);
      /// Invoke RPC call
      bool call_rpc(const std::string& service, const void* data, size_t len);
    };

    /// User client interface
    /**
     *   @author  M.Frank
     */
    struct client_t   {
    public:
      typedef rpc_server_relay::rpc_t rpc_t;
    public:

      /// Default destructor
      virtual ~client_t() = default;
      /// Handle RPC response from server
      virtual void on_response  (rpc_t* rpc, const void* data, size_t length) = 0;
      /// Informational callback when a rpc no-link
      virtual void on_nolink    (rpc_t* /* rpc */)  {}
      /// Informational callback when a rpc service appears
      virtual void on_connect_rpc   (rpc_t* /* rpc */)  {}
      /// Informational callback when a rpc service disappears
      virtual void on_disconnect_rpc(rpc_t* /* rpc */)  {}
    };

    typedef std::map<std::string, std::shared_ptr<server_t> > server_map_t;
    typedef std::map<std::string, std::shared_ptr<dns_t> >    dns_map_t;
    typedef std::map<unsigned long, std::regex>               rpc_matches_t;

    rpc_matches_t    service_matches;
    dns_map_t        known_dns_servers;
    //server_map_t     known_dim_servers;
    client_t*        client     { nullptr };
    int              timeout    { 0 };

  private:
    /// DimInfo overload to process messages
    static void dnsinfo_callback(void* tag, void* address, int* size);
    /// DimInfo overload to process messages
    static void taskinfo_callback(void* tag, void* address, int* size);
    /// Process task information from DNS
    void analyze_tasks(const char* input, std::shared_ptr<dns_t>& dns);
    /// Process service information from task
    void analyze_services(const char* input, server_t* dns);

  public:
    /// Default constructor
    rpc_server_relay();
    /// Default destructor
    virtual ~rpc_server_relay();

    /// Handle RPC response
    void on_response(rpc_t* rpc, const void* data, size_t len);
    /// Handle no-link RPC response
    void on_nolink(rpc_t* rpc);
    /// Informational callback when a rpc service appears
    void on_connect_rpc     (rpc_t* rpc);
    /// Informational callback when a rpc service disappears
    void on_disconnect_rpc  (rpc_t* rpc);

    /// Add new DNS server list 
    bool add_dns(const std::string& dns);
    /// Add new server to list 
    bool add_server(const std::string& server_node, std::shared_ptr<dns_t>& dns);
    /// Remove server from list 
    bool remove_server(const std::string& server_node, std::shared_ptr<dns_t>& dns);

    /// Access all servers from a given DNS node
    std::vector<server_t*> get_servers(const std::string& dns, const std::regex& match)  const;
    std::shared_ptr<rpc_t> get_server_rpc(const std::string& dns, const std::string& server, const std::regex& rpc_match)  const;
    std::shared_ptr<rpc_t> get_server_rpc(const std::string& dns, const std::regex& server_match, const std::regex& rpc_match)  const;
    std::vector<rpc_t*>    get_servers_rpc(const std::string& dns, const std::regex& server_match, const std::regex& rpc_match)  const;
  };
}
#endif /* ONLINE_GAUCHOSERVER_RPC_SERVER_RELAY_H  */
