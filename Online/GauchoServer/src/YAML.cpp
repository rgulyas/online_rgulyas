//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <nlohmann/json.hpp>
#include <GauchoServer/YAML.h>

/// C/C++ include files
#include <filesystem>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>

namespace  {

  /// Basic YAML file parser base class
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class YAMLParser   {
  public:
    int debug { 0 };
  public:
    YAMLParser() = default;
    virtual ~YAMLParser() = default;
    void       print_key(const std::string& prefix, const YAML::Node& o, const std::string& key);
    YAML::Node open(const std::string& file_name);
  };

  /// Monet histogram page parser and json converter
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class MonetPageParser : public YAMLParser   {
  public:
    typedef nlohmann::json json;
    class DirectoryScanner;
    class PageWriter;
  public:
    MonetPageParser() = default;
    MonetPageParser(int dbg)  {
      this->debug = dbg; 
    }
    virtual ~MonetPageParser() = default;

    json parse_hist_page(const std::string& fname);
    void parse_page_definitions(const std::string& input, const std::string& output);
  };

  YAML::Node YAMLParser::open(const std::string& file_name)     {
    return YAML::LoadFile(file_name.c_str());
  }

  void YAMLParser::print_key(const std::string& prefix, const YAML::Node& o, const std::string& thekey)    {
    try  {
      YAML::Node n = o[thekey];
      if ( n.IsDefined() )    {
	std::cout << prefix
		  << "Key "       << std::setw(16) << std::left << ('"'+thekey+'"') << "  "
		  << " Defined: " << n.IsDefined()
		  << " Scalar: "  << n.IsScalar()
		  << " Seq: "     << n.IsSequence()
		  << " Map: "     << n.IsMap();
	if ( n.IsScalar() )   {
	  std::cout << " value '" << n.as<std::string>() << "'";
	}
	std::cout << "\n";
	if ( n.IsMap() )   {
	  for( YAML::iterator i=n.begin(); i != n.end(); ++i ) {
	    auto key = i->first.as<std::string>();
	    print_key(prefix+"    ",n, key.c_str());
	  }
	}
	return;
      }
      std::cout << "print_key: Object has no key : " << thekey << std::endl;
    }
    catch(const std::exception& e)   {
      std::cout << "print_key: Exception: " << e.what() << std::endl;
    }
  }

  /// Monet page parser's output writer
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class MonetPageParser::PageWriter  {
  public:
    std::filesystem::path output_location;

  public:
    int write(const std::string& output_path, json&& page);
  };

  /// Monet page parser's directory scanner
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class MonetPageParser::DirectoryScanner  {
  public:
    MonetPageParser& parser;
    PageWriter&      writer;
    std::filesystem::path input_location;
  public:
    void scan(const std::filesystem::directory_entry& entry);
  };

#define LINE1 "+==================================================================================="
#define LINE2 "+-----------------------------------------------------------------------------------"
#define LINE3 "    +-------------------------------------------------------------------------------"

  int MonetPageParser::PageWriter::write(const std::string& output_extension, json&& page)     {
    std::filesystem::path output = this->output_location.string()+output_extension;
    std::filesystem::path parent = std::filesystem::path(output).parent_path();
    std::error_code ec;

    if ( !std::filesystem::exists(parent, ec) )   {
      if ( !std::filesystem::create_directories(parent, ec) )   {
	std::cout << "Failed to create output directory: " 
		  << parent.string() << " [" << ec.message() << "]" << std::endl;
	return ec.value();
      }
    }
    if ( std::filesystem::exists(parent, ec) )   {
      std::ofstream out(output.string(), std::ofstream::out);
      if ( out.good() )   {
	out << page.dump(4) << std::endl;
	if ( out.good() )   {
	  return 0;
	}
	return errno;
      }
      else   {
	std::cout << "Failed to create output file: " << output.string() 
		  << " [" << ec.message() << "]" << std::endl;
	return ec.value();
      }
    }
    std::cout << "Failed to access output directory: " << parent.string() 
	      << " [" << ec.message() << "]" << std::endl;
    return ec.value();
  }

  void MonetPageParser::DirectoryScanner::scan(const std::filesystem::directory_entry& entry)     {
    if ( entry.exists() )   {
      if ( entry.is_directory() )   {
	for(auto const& e : std::filesystem::directory_iterator{entry.path()})
	  this->scan(std::filesystem::directory_entry(e.path()));
      }
      else if ( entry.is_symlink() )   {
	std::filesystem::path link = std::filesystem::read_symlink(entry.path());
	this->scan(std::filesystem::directory_entry(link));
      }
      else if ( entry.is_regular_file() )   {
	std::string input = entry.path().string();
	std::string extension = input.substr(this->input_location.string().length());
	if ( parser.debug > 0 )    {
	  std::cout << LINE1 << std::endl;
	  std::cout << "| Input:  " << input << std::endl;
	}
	json page = parser.parse_hist_page(input);
	this->writer.write(extension, std::move(page));
      }
    }
  }

  MonetPageParser::json MonetPageParser::parse_hist_page(const std::string& fname)    {
    char timestr[256];
    struct tm tm;
    std::time_t now = ::time(nullptr);

    ::localtime_r(&now, &tm);
    ::strftime(timestr, sizeof(timestr),"%a %b %d %Y %H:%M:%S",&tm);

    YAML::Node doc = this->open(fname);
    YAML::Node histograms = doc["histograms"];
    if ( debug > 1 )    {
      std::cout << "=====    " << fname << std::endl;
      print_key("",doc, "pagedoc");
      print_key("",doc, "pagename");
      std::cout << LINE2 << std::endl;
    }
    json page   = {{"name", ""}, {"date", timestr}};
    json layout = {{"columns", 1}, {"rows", 1}};
    json items  = json::array();

    YAML::Node ydoc = doc["pagedoc"];
    YAML::Node ynam = doc["pagename"];

    std::vector<double> size_x, size_y;
    for( YAML::iterator it=histograms.begin(); it != histograms.end(); ++it ) {
      YAML::Node n = *it;
      if ( debug > 0 )   {
	if ( debug > 1 )   {
	  std::cout << LINE3 << std::endl;
	}
	std::cout << "| Histogram: " << n["name"].as<std::string>() << std::endl;
      }
      double sx  = 1e0, sy = 1e0;
      json hist  = {};
      json style = {{"fLineWidth", 1}, {"fLineColor", 1}};

      std::string opt;
      for( YAML::iterator i=n.begin(); i != n.end(); ++i ) {
	auto key = i->first.as<std::string>();
	if ( debug > 1 )   {
	  print_key("    ", n, key.c_str());
	}
	if ( key == "name" )
	  hist["histogram"] = i->second.as<std::string>();
	else if ( key == "size_x" )
	  sx = i->second.as<double>();
	else if ( key == "size_y" )
	  sy = i->second.as<double>();
	else if ( key == "taskname" )   {
	  hist["task"]      = "<part>_MON010._"+i->second.as<std::string>();
	  hist["dns"]       = "mon01";
	  hist["partition"] = "FEST";
	}
	else if ( key == "creation" )  {
	  auto val = i->second.as<std::string>();
	  if ( !val.empty() ) hist["created"] = val;
	}
	else if ( key == "display_options" )  {
	  YAML::Node o = n[key];
	  if ( o["label_x"].IsDefined() )
	    style["fXAxis"]["fName"] = o["label_x"].as<std::string>();
	  else if ( o["xmin"].IsDefined() && o["xmin"].IsScalar() )
	    style["fXAxis"]["fXMin"] = o["xmin"].as<std::string>();
	  else if ( o["xmax"].IsDefined() && o["xmax"].IsScalar() )
	    style["fXAxis"]["fXMax"] = o["xmax"].as<std::string>();
	  else if ( o["ndivx"].IsDefined() )
	    opt += "NDIVX";
	  else if ( o["gridx"].IsDefined() )
	    opt += "GRIDX";

	  else if ( o["label_y"].IsDefined() )
	    style["fYAxis"]["fName"] = o["label_y"].as<std::string>();
	  else if ( o["ymin"].IsDefined() && o["ymin"].IsScalar() )
	    style["fYAxis"]["fXMin"] = o["ymin"].as<std::string>();
	  else if ( o["ymax"].IsDefined() && o["ymax"].IsScalar() )
	    style["fYAxis"]["fXMax"] = o["ymax"].as<std::string>();
	  else if ( o["ndivy"].IsDefined() )
	    opt += "NDIVY";
	  else if ( o["gridy"].IsDefined() )
	    opt += "GRIDY";

	  else if ( o["label_z"].IsDefined() )
	    style["fZAxis"]["fName"] = o["label_z"].as<std::string>();
	  else if ( o["zmin"].IsDefined() && o["zmin"].IsScalar() )
	    style["fZAxis"]["fXMin"] = o["zmin"].as<std::string>();
	  else if ( o["zmax"].IsDefined() && o["zmax"].IsScalar() )
	    style["fZAxis"]["fXMax"] = o["zmax"].as<std::string>();
	  else if ( o["ndivz"].IsDefined() )
	    opt += "NDIVZ";
	  else if ( o["gridz"].IsDefined() )
	    opt += "GRIDZ";

	  else if ( o["norm"].IsDefined() && o["norm"].IsScalar() )
	    style["fNormFactor"] = o["norm"].as<std::string>();
	  else if ( o["linecolor"].IsDefined() && o["linecolor"].IsScalar() )
	    style["fLineColor"] = o["linecolor"].as<int>();
	  else if ( o["fillcolor"].IsDefined() && o["fillcolor"].IsScalar() )
	    style["fFillColor"] = o["fillcolor"].as<int>();
	  else if ( o["fillstyle"].IsDefined() && o["fillstyle"].IsScalar() )
	    style["fFillStyle"] = o["fillstyle"].as<int>();

	  else if ( o["stats"].IsDefined() && o["stats"].IsScalar() )   {
	    auto val = o["stats"].as<std::string>();
	    if ( !val.empty() ) style["fBinStatErrOpt"] = val;
	  }
	}
      }
      size_x.emplace_back(sx);
      size_y.emplace_back(sy);
      hist["style"] = style;
      items.push_back(hist);
    }
    double xmi = 1e6, xma = 0e0;
    double ymi = 1e6, yma = 0e0;
    for(size_t i=0; i < size_x.size(); ++i)   {
      xmi = std::min(xmi, size_x[i]);
      xma = std::max(xma, size_x[i]);
      ymi = std::min(ymi, size_y[i]);
      yma = std::max(yma, size_y[i]);
    }
    int tot_cols = (xma+xmi/2e0)/xmi, tot_rows = (yma+ymi/2e0)/ymi;
    for(size_t i=0; i < size_x.size(); ++i)   {
      int cols = (size_x[i]+xmi/2e0)/xmi;
      int rows = (size_y[i]+ymi/2e0)/ymi;
      items[i]["style"]["colspan"] = cols;
      items[i]["style"]["rowspan"] = rows;
    }
    layout["rows"]     = tot_rows;
    layout["columns"]  = tot_cols;
    page["name"]       = ydoc.IsDefined() ? ydoc.as<std::string>() : std::string();
    page["identifier"] = ynam.IsDefined() ? ynam.as<std::string>() : std::string();;
    page["items"]      = items;
    page["layout"]     = layout;

    if ( debug > 1 )    {
      std::cout << LINE2 << std::endl;
      std::cout << page.dump(4) << std::endl;
    }
    return page;
  }

  void MonetPageParser::parse_page_definitions(const std::string& input, const std::string& output)    {
    PageWriter writer        { output };
    DirectoryScanner scanner { *this, writer, input };
    scanner.scan(std::filesystem::directory_entry(scanner.input_location));
  }
}

#include <RTL/rtl.h>

extern "C" int test_yaml(int argc, char** argv)    {
  if ( argc && argv )   {
  }
  MonetPageParser parser { 1 };
  //parser.debug = 1;
  parser.parse_page_definitions("/home/frankm/tmp/Monet/histoyml/OnlineMon", "/home/frankm/Presenter/web/Pages/Monet");

  //parse_hist_page("/home/frankm/tmp/Monet/histoyml/OnlineMon/Reco/TrackMonitor.yml");
  //parse_hist_page("/home/frankm/tmp/Monet/histoyml/OnlineMon/ODIN/BXType.yml");
  return 0;
}
