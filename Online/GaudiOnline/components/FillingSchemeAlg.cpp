//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_FILLINGSCHEMEALG_H
#define GAUDIONLINE_FILLINGSCHEMEALG_H

/// Framework include files
#include <GaudiKernel/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>

/// C/C++ include files
#include <mutex>

/// Forward declarations

/// Online namespace declaration
namespace Online  {

  /// Helper service to pass options to non-service clients
  /** @class FillingSchemeAlg FillingSchemeAlg.h
   *
   * 
   * \author  M.Frank
   * \version 1.0
   * \date    25/04/2019
   */
  class FillingSchemeAlg : public Gaudi::Algorithm  {
  public:
    using BeamSchema    = std::pair<size_t, std::string>;
    using FillingScheme = std::pair<BeamSchema, BeamSchema>;

    /// Property: Directory to place for vectors of raw banks for TAE subevents
    Gaudi::Property<std::string> m_dns {this, "DNS", "mon01", "DNS node to pick up filling scheme"};

    /// Data handle to put fiulling schema on the TES:
    DataObjectWriteHandle<std::shared_ptr<FillingScheme> >  m_scheme {this, "FillingScheme", "DAQ/FillingScheme"};

    /// Helper structure
    struct DP  {
      int               id   { 0 };
      time_t            last { 0 };
      FillingSchemeAlg* algo { nullptr };
      size_t*           size { nullptr };
      std::string*      str  { nullptr };
    };

    DP   m_svcB1FS;
    DP   m_svcB1NB;
    DP   m_svcB2FS;
    DP   m_svcB2NB;
    long m_dns_ID { 0L };

    FillingScheme                  m_raw  { };
    std::shared_ptr<FillingScheme> m_data { };
    mutable std::mutex             m_lock { };
    mutable bool                   m_changed { false };

    /// DimInfo overload to process messages
    static void fs_str_handler(void* tag, void* address, int* size);
    /// DimInfo overload to process messages
    static void fs_size_handler(void* tag, void* address, int* size);

    void subscribe();
    void unsubscribe();

  public:
    /// Constructors
    using Algorithm::Algorithm;

    /// Initialize the algorithm
    virtual StatusCode initialize()   override;
    /// Finalize the algorithm
    virtual StatusCode finalize()   override;
    /// Execute single event
    virtual StatusCode execute(EventContext const& ctxt)  const  override  final;
  };   // class FillingSchemeAlg
}      // namespace Online
#endif // GAUDIONLINE_ONLINEFILLINGSCHEMEALG_H
//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
//#include "FillingSchemeAlg.h"
#include <GaudiKernel/MsgStream.h>
#include <RTL/rtl.h>
#include <dim/dic.h>

using namespace Online;

/// Factory instantiation
DECLARE_COMPONENT( FillingSchemeAlg )

/// DimInfo overload to process messages
void FillingSchemeAlg::fs_str_handler(void* tag, void* address, int* size) {
  if ( tag && address && size && *size ) {
    int len = *size;
    FillingSchemeAlg::DP* dp = *(FillingSchemeAlg::DP**)tag;
    std::lock_guard<std::mutex> lock(dp->algo->m_lock);
    const char* val = (const char*)address;
    dp->last = ::time(0);
    dp->algo->m_changed = true;
    dp->str->assign(val, val+len);
  }
}

/// DimInfo overload to process messages
void FillingSchemeAlg::fs_size_handler(void* tag, void* address, int* size) {
  if ( tag && address && size && *size ) {
    FillingSchemeAlg::DP* dp = *(FillingSchemeAlg::DP**)tag;
    std::lock_guard<std::mutex> lock(dp->algo->m_lock);
    dp->last = ::time(0);
    dp->algo->m_changed = true;
    if ( *size == sizeof(unsigned int) )
      *dp->size = *(unsigned int*)address;
    else if ( *size == sizeof(size_t) )
      *dp->size = *(size_t*)address;
  }
}

void FillingSchemeAlg::subscribe()   {
  if ( m_dns.empty() )   {
    m_dns = RTL::nodeNameShort();
  }
  if ( 0 == m_dns_ID )    {
    m_dns_ID = ::dic_add_dns(m_dns.value().c_str(), ::dim_get_dns_port());
  }
  if ( 0 == m_svcB1FS.id )   {
    m_svcB1FS.last  = 0;
    m_svcB1FS.algo  = this;
    m_svcB1FS.str   = &m_raw.first.second;
    m_svcB1FS.id    = ::dic_info_service_dns(m_dns_ID, "LHC/B1FillingScheme", MONITORED, 0,0,0, fs_str_handler, (long)&m_svcB1FS, 0, 0);
  }
  if ( 0 == m_svcB1NB.id )   {
    m_svcB1NB.last  = 0;
    m_svcB1NB.algo  = this;
    m_svcB1NB.size  = &m_raw.first.first;
    m_svcB1NB.id    = ::dic_info_service_dns(m_dns_ID, "LHC/B1NBunches", MONITORED, 0,0,0, fs_size_handler, (long)&m_svcB1NB, 0, 0);
  }
  if ( 0 == m_svcB2FS.id )   {
    m_svcB2FS.last  = 0;
    m_svcB2FS.algo  = this;
    m_svcB2FS.str   = &m_raw.second.second;
    m_svcB2FS.id    = ::dic_info_service_dns(m_dns_ID, "LHC/B2FillingScheme", MONITORED, 0,0,0, fs_str_handler, (long)&m_svcB2FS, 0, 0);
  }
  if ( 0 == m_svcB2NB.id )   {
    m_svcB2NB.last  = 0;
    m_svcB2NB.algo  = this;
    m_svcB2NB.size  = &m_raw.second.first;
    m_svcB2NB.id    = ::dic_info_service_dns(m_dns_ID, "LHC/B2NBunches", MONITORED, 0,0,0, fs_size_handler, (long)&m_svcB2NB, 0, 0);
  }
}

void FillingSchemeAlg::unsubscribe()   {
  if ( 0 != m_svcB1FS.id )   {
    ::dic_release_service(m_svcB1FS.id);
    m_svcB1FS.id = 0;
  }
  if ( 0 == m_svcB1NB.id )   {
    ::dic_release_service(m_svcB1FS.id);
    m_svcB1FS.id = 0;
  }
  if ( 0 == m_svcB2FS.id )   {
    ::dic_release_service(m_svcB1FS.id);
    m_svcB1FS.id = 0;
  }
  if ( 0 == m_svcB2NB.id )   {
    ::dic_release_service(m_svcB1FS.id);
    m_svcB1FS.id = 0;
  }
}

/// Initialize the algorithm
StatusCode FillingSchemeAlg::initialize()    {
  StatusCode sc = this->Algorithm::initialize();
  if ( !m_data.get() ) m_data = std::make_shared<FillingScheme>();
  if ( sc.isSuccess() ) this->subscribe();
  return sc;
}

/// Finalize the algorithm
StatusCode FillingSchemeAlg::finalize()    {
  this->unsubscribe();
  return this->Algorithm::finalize();
}

/// Execute single event
StatusCode FillingSchemeAlg::execute(EventContext const& /* ctxt */)  const  {
  if ( m_svcB1FS.last == 0 || m_svcB1NB.last == 0 || m_svcB2FS.last == 0 || m_svcB2NB.last == 0 )    {
    error() << "Cannot supply filling scheme. Some of the DIM services have not yet answered.";
    return StatusCode::FAILURE;
  }
  if ( m_changed )   {
    std::lock_guard<std::mutex> lock(m_lock);
    *m_data = m_raw;
    m_changed = false;
  }
  std::shared_ptr<FillingScheme> fs = m_data;
  m_scheme.put(std::move(fs));
  return StatusCode::SUCCESS;
}

