//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_INPUTALG_H
#define GAUDIONLINE_INPUTALG_H

/// Framework include files
#include <GaudiOnline/IRawEventCreator.h>
#include <GaudiKernel/IMonitorSvc.h>
#include "EventProcessor.h"
#include "IOService.h"

/// Online namespace declaration
namespace Online  {

  class bank_header_t;
  class raw_bank_online_t;

  /// Online algorithm to feed the TES with the basic raw event.
  /**
    *
    * \author  M.Frank
    * \version 1.0
    * \date    25/04/2019
    */
  class InputAlg : public EventProcessor   {

  protected:
    using daq_error_t = std::vector<const raw_bank_online_t*>;
    class Counters;

    Gaudi::Property<bool>                 m_declareData{this, "DeclareData",  true,  "Declare or drop data"};
    Gaudi::Property<bool>                 m_declareErrs{this, "DeclareErrors",false, "Declare error data"};
    Gaudi::Property<bool>                 m_declareEvt {this, "DeclareEvent", false, "Declare raw data"};
    Gaudi::Property<bool>                 m_makeRawEvt {this, "MakeRawEvent", false, "Declare RawEvent structures"};
    Gaudi::Property<bool>                 m_enableHalt {this, "EnableHalt",   true,  "Enable halt the event loop"};
    Gaudi::Property<std::string>          m_ioService  {this, "IOServiceName","Online::IOService/IOService","Name of IO service"};
    /// Property: Directory to place for vectors of raw banks for TAE subevents
    Gaudi::Property<std::string>          m_bankDir    {this, "BankDirectory","Banks","Directory to place raw banks"};

    DataObjectWriteHandle<lb_evt_data_t>  m_rawData    {this, "RawData",   "Banks/RawData"};
    DataObjectWriteHandle<evt_data_t>     m_daqError   {this, "DAQErrors", "Banks/RawDataErrors"};
    DataObjectWriteHandle<evt_desc_t>     m_rawGuard   {this, "RawGuard",  "Banks/RawDataGuard"};

    /// Reference to the I/O service
    SmartIF<IOService>                    m_io;
    /// Reference to the I/O service
    SmartIF<IMonitorSvc>                  m_monitor;
    /// Reference to RawEvent tool
    std::unique_ptr<IRawEventCreator>     m_evtTool;
    /// Reference to internal monitoring counters
    std::unique_ptr<Counters>             m_counters;

    /// Halt the algorithm and allow for debugging
    void halt()  const;

    /// Expand TAE record from MDF
    StatusCode expand_tae_tell1_event(const event_header_t* hdr)  const;

    /// Allow to escape halt
    mutable int m_halt;

  public:
    using EventProcessor::EventProcessor;
    /// Initialize the algorithm
    virtual StatusCode initialize()   override;
    /// Finalize the algorithm
    virtual StatusCode finalize()   override;
    /// Start the algorithm
    virtual StatusCode start()   override;
    /// Stop the algorithm
    virtual StatusCode stop()    override;
    /// Execute single event
    virtual StatusCode process(EventContext const& ctxt)  const  override;
  };    // class InputAlg
}       // Endnamespace Online
#endif  // GAUDIONLINE_INPUTALG_H
