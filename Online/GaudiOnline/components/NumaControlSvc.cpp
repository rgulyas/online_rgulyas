//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  NumaControlSvc.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_NUMACONTROLSVC_H
#define ONLINE_DATAFLOW_NUMACONTROLSVC_H

// Framework include files
#include <GaudiKernel/Service.h>
#include <RTL/strdef.h>
#include <RTL/Numa.h>

///  Online namespace declaration
namespace Online  {

  /// NumaControlSvc component to be applied in the manager sequence
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class NumaControlSvc : public Service  {
  protected:
    /// Property: Option to steer the printout.
    std::string      printOption;
    /// Property: String representation of the key to be added to the event context
    std::string      when;
    /// Numa actor
    RTL::Numa        numa;

    /// Apply NUMA configuration settings according to job options
    void apply_numa_config()   {
      if ( -1 != numa.available() )   {
	std::string opt = RTL::str_lower(printOption);
	std::pair<int, std::vector<std::string> > ret = { -1, { } };
	/// Now optional printout
	if ( opt.find("all") != std::string::npos ) ret = numa.printSettings();
	warning(ret.second);
	if ( opt.find("initial") != std::string::npos ) ret = numa.printStatus();
	warning(ret.second);
	ret = numa.apply_config();
	warning(ret.second);
	if ( opt.find("result") != std::string::npos ) ret = numa.printStatus();
	warning(ret.second);
      }
    }

    /// Warning printout handling
    void warning(const std::vector<std::string>& msg)   const   {
      MsgStream err(msgSvc(), name());
      err << MSG::INFO;
      if ( err.isActive() )  {
	for( const auto& m : msg )
	  err << MSG::INFO << m << endmsg;
      }
    }
    
  public:
    /// Initializing constructor
    NumaControlSvc(const std::string& nam, ISvcLocator* ctxt)
      : Service(nam, ctxt)
    {
      declareProperty("When",       when="initialize");
      declareProperty("Print",      printOption);
      declareProperty("BindCPU",    numa.bindCPU);
      declareProperty("BindMemory", numa.bindMemory);
      declareProperty("CPUSlots",   numa.cpuSlots);
      declareProperty("CPUMask",    numa.cpuMask);
    }

    /// Default destructor
    virtual ~NumaControlSvc() = default;

    /// Initialize the numa controls component
    virtual StatusCode initialize()  override  {
      StatusCode sc = Service::initialize();
      if ( when.find("initialize") != std::string::npos )
	apply_numa_config();
      return sc;
    }

    /// Start the numa controls component
    virtual StatusCode start()  override  {
      StatusCode sc = Service::start();
      if ( when.find("start") != std::string::npos )
	apply_numa_config();
      return sc;
    }

    /// Stop the numa controls component
    virtual StatusCode stop()  override  {
      StatusCode sc = Service::stop();
      if ( when.find("stop") != std::string::npos )
	apply_numa_config();
      return sc;
    }

    /// Finalize the numa controls component
    virtual StatusCode finalize()  override  {
      StatusCode sc = Service::finalize();
      if ( when.find("finalize") != std::string::npos )
	apply_numa_config();
      return sc;
    }
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_NUMACONTROLSVC_H

using Online::NumaControlSvc;
DECLARE_COMPONENT( NumaControlSvc )
