"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from OnlineApplication import *

class ReadDatafile(Application):
  def __init__(self, outputLevel, partitionName, partitionID):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID)
    
  def setup_algorithms(self):
    import Gaudi.Configuration as Gaudi
    import Configurables
    self.config.numEventThreads = 1
    self.config.numStatusThreads= 1
    self.config.events_HighMark = 0
    self.config.events_LowMark  = 0

    input                       = Configurables.Online__InputAlg('EventInput')
    input.DeclareData           = True
    explorer                    = Configurables.StoreExplorerAlg('Explorer')
    explorer.Load               = 1
    explorer.PrintFreq          = 1.0
    explorer.OutputLevel        = 1
    dump                        = Configurables.Online__Tell1BankDump('Dump')
    dump.RawLocation            = '/Event/DAQ/RawEvent'
    dump.CheckData              = 0
    dump.DumpData               = 1
    dump.FullDump               = 1
    dump.OutputLevel            = 1
    tae                         = Configurables.Online__TAETestCreator('TAECreate')
    tae.TAEEvents               = 5
    tae.RemoveDAQStatus         = True
    ask                         = Configurables.Online__InteractiveAlg('DumpHandler')
    ask.Prompt                  = "Press <ENTER> to dump banks, q or Q to quit :"
    ctrl                        = Configurables.Online__InteractiveAlg('CommandHandler')
    ctrl.Prompt                 = "Press <ENTER> to continue, q or Q to quit :"
    sequence                    = Gaudi.GaudiSequencer('Output')
    sequence.Members            = [tae,explorer,ask,dump,ctrl]
    self.input                  = input
    self.dump                   = dump
    self.sequence               = sequence
    self.app.TopAlg             = [input, sequence]
    self.broker.DataProducers   = self.app.TopAlg
    return self
