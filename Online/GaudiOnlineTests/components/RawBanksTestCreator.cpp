//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Include files from the framework
#include <Gaudi/PluginService.h>
#include <EventData/odin_t.h>
#include <EventData/event_header_t.h>
#include <EventData/raw_bank_offline_t.h>
#include <EventHandling/memory_data_guard_t.h>

/// Online namespace declaration
namespace Online    {

  namespace {
    std::pair<const bank_header_t*, const void*> make_bank(uint8_t*& start, 
							   uint8_t  type,
							   uint16_t source,
							   uint16_t len,
							   uint8_t  version,
							   bool set=true)
    {
      raw_bank_offline_t* b = (raw_bank_offline_t*)start;
      b->setMagic();
      b->setType(type);
      b->setSize(len * sizeof(int));
      b->setVersion(version);
      b->setSourceID(source);
      if ( set )   {
	uint32_t* p = b->begin<uint32_t>();
	for(uint16_t i=0; i<len; ++i)
	  *p = i+1;
      }
      start += b->totalSize();
      return { b, b->begin<uint32_t>() };
    }
  }

  class Configuration;

  /// Generic MDF File I/O class to be used by the OnlineEvtApp
  /**
   *
   * \version 1.0
   * \author  M.Frank
   */
  class GenerateEventAccess : public EventAccess  {
  public:

    using Factory = Gaudi::PluginService::Factory<EventAccess*(std::unique_ptr<RTL::Logger>&& logger,
							       const Configuration* cfg)>;

    /// Helper class containing the configuration parameters of this object 
    /**
     *
     * \version 1.0
     * \author  M.Frank
     */
    struct gen_config_t : public EventAccess::config_t  {
      /// Default constructor
      gen_config_t() = default;
      /// Move constructor
      gen_config_t(gen_config_t&& copy) = default;
      /// Copy constructor
      gen_config_t(const gen_config_t& copy) = default;
      /// Default destructor
      ~gen_config_t() = default;
      /// Assignment operator
      gen_config_t& operator=(gen_config_t&& copy) = default;
      /// Move assignment operator
      gen_config_t& operator=(const gen_config_t& copy) = default;
      /// Property: Half window size for TAE generation
      std::size_t   half_window  { 0 };
    } config;

    const Online::Configuration* online_config { nullptr };
    int run_id                       { 123 };
    int event_id                     { 0 };

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(GenerateEventAccess);

    /// Default constructor
    GenerateEventAccess(std::unique_ptr<RTL::Logger>&& logger,
			const Configuration* cfg);
    /// Standard destructor
    virtual ~GenerateEventAccess() = default;
    /// Fill the event data cache if necessary
    virtual int fill_cache()  override;
    /// Cancel all pending I/O requests to the buffer manager
    virtual int cancel()  override;
    /// EventAccess overload: Close connection to event data source
    virtual void close()  override;
  };
}

#include <GaudiOnline/Configuration.h>

using namespace Online;

/// Initializing constructor
GenerateEventAccess::GenerateEventAccess(std::unique_ptr<RTL::Logger>&& logger,
					 const Configuration* cfg)
  : EventAccess(move(logger)), online_config(cfg)
{
  base_config = &config;
}

/// Close filework connection
void GenerateEventAccess::close()  {
  cancel();
  m_cancelled = false;
}

/// Cancel all pending I/O requests to the buffer manager
int GenerateEventAccess::cancel()   {
  m_cancelled = true;
  return ONLINE_OK;
}

/// Fill the event data cache if necessary
int GenerateEventAccess::fill_cache()   {
  while (1)   {
    std::size_t packing     = online_config->generic.packingFactor;
    std::size_t max_events  = online_config->generic.maxEventsIn;
    std::size_t half_window = online_config->generic.halfWindow;
    long        buff_size   = online_config->generic.bufferSize;
    auto* mem = (uint8_t*)::operator new(buff_size);
    auto* ptr = (uint8_t*)mem;

    for( std::size_t pkg = 0; pkg < packing; ++pkg )   {
      for( std::size_t hw = -half_window; hw <= half_window; ++hw )   {
	auto* e = new(ptr) event_header_t();
	ptr += event_header_t::sizeOf(3);
	/// Add banks ....
	for(int i=0; i<16; ++i)  {
	  make_bank(ptr, bank_types_t::Rich, i, (i+1)*64, 0);
	}
	/// Add banks ....
	for(int i=0; i<9; ++i)  {
	  make_bank(ptr, bank_types_t::PrsE, i, (i+1)*32, 0);
	}
	/// Add bank ODIN
	++this->event_id;
	auto bank = (raw_bank_offline_t*)ptr;
	bank->setMagic();
	bank->setType(bank_types_t::ODIN);
	bank->setSize(sizeof(run3_odin_t));
	bank->setVersion(7);
	bank->setSourceID(0);
	auto* run = bank->begin<run3_odin_t>();
	::memset(run,0,sizeof(run3_odin_t));
	run->_run_number = this->run_id;
	run->_orbit_id   = this->event_id/10;
	run->_event_id   = this->event_id;
	run->_bunch_id   = this->event_id%100;
	run->_trigger_type = 0;

	run->_tae_first   = 0;
	run->_tae_central = 0;
	run->_tae_window  = 0;
	if ( half_window > 0 )   {
	  if ( hw == 0 ) run->_tae_central = 1;
	  else if ( hw == -half_window ) run->_tae_first = 1;
	  run->_tae_window = hw + half_window + 1;
	}
	ptr += bank->totalSize();

	/// Fix event header
	std::size_t evt_len = ptr - mem;
	e->setChecksum(0);
	e->setCompression(0);
	e->setHeaderVersion(3);
	e->setSize(evt_len);
	auto* h = event_header_t::SubHeader(e).H1;
	h->setTriggerMask({~0x0U, ~0x0U, ~0x0U, ~0x0U});
	h->setRunNumber(run->_run_number);
	h->setOrbitNumber(run->_orbit_id);
	h->setBunchID(run->_bunch_id);
	if ( ptr - mem > buff_size )   {
	  m_logger->except("Allocated buffer size of %ld bytes too small. %s",
			   buff_size, "[Memory-corruption]");
	  return ONLINE_ERROR;
	}
      }
    }
    auto type = event_traits::tell1::data_type;
    auto evts = convertMultiMDF(mem, ptr-mem);
    auto burst = std::make_shared<memory_data_guard_t<event_collection_t> >
      (move(evts.second), move(evts.first), type, mem);
    this->queueBurst(move(burst));
    this->printInfo();
    if ( this->monitor.eventsIn > max_events )   {
      this->cancel();
      return ONLINE_END_OF_DATA;
    }
    return ONLINE_OK;
  }
};

DECLARE_COMPONENT( GenerateEventAccess )
