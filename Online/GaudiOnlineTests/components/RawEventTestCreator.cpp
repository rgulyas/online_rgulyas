//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <Tell1Data/Tell1Decoder.h>
#include <EventData/raw_bank_offline_t.h>
#include <EventData/odin_t.h>
#include <Event/RawEvent.h>

/// Online namespace declaration
namespace Online    {

  /// Determine length of the sequential buffer from RawEvent object
  inline size_t rawEventLength(const LHCb::RawEvent* evt)    {
    size_t  len = 0;
    for(size_t i=LHCb::RawBank::L0Calo; i<LHCb::RawBank::LastType; ++i)  {
      len += rawEventLengthBanks(evt->banks(LHCb::RawBank::BankType(i)));
    }
    return len;
  }

 /** @class RawEventTestCreator RawEventTestCreator.cpp
  *  Creates and fills dummy RawEvent
  *
  *  @author Markus Frank
  *  @date   2005-10-13
  */
  class RawEventTestCreator : public Gaudi::Algorithm {
    /// Flag to test bank removal
    Gaudi::Property<bool>                 m_removeBank{this, "RemoveBank", false, ""};
    DataObjectWriteHandle<LHCb::RawEvent> m_rawEvent{this, "RawLocation", LHCb::RawEventLocation::Default};
  public:
    using Algorithm::Algorithm;

    /// Main execution
    StatusCode execute(EventContext const& /* ctxt */) const override  {
      int i, cnt, *p;
      static int trNumber = -1;
      auto raw  = std::make_unique<LHCb::RawEvent>();
      raw_bank_offline_t*  bank = 0;
      for(i=0; i<16; ++i)  {
        bank = (raw_bank_offline_t*)raw->createBank(i, LHCb::RawBank::Rich, 1, (i+1)*64, 0);
        for(p=bank->begin<int>(), cnt=0; p != bank->end<int>(); ++p)  {
          *p = cnt++;
        }
        raw->adoptBank((LHCb::RawBank*)bank, true);
      }
      if ( m_removeBank ) raw->removeBank((LHCb::RawBank*)bank);
      for(i=0; i<9; ++i)  {
        bank = (raw_bank_offline_t*)raw->createBank(i, LHCb::RawBank::PrsE, 1, (i+1)*32, 0);
        for(p=bank->begin<int>(), cnt=0; p != bank->end<int>(); ++p)  {
          *p = cnt++;
        }
        raw->adoptBank((LHCb::RawBank*)bank, true);
      }

      bank = (raw_bank_offline_t*)raw->createBank(0, LHCb::RawBank::ODIN, 2, sizeof(run2_odin_t), 0);
      run2_odin_t* run = bank->begin<run2_odin_t>();
      ::memset(run,0,sizeof(run2_odin_t));
      run->_run_number     = 123;
      run->_orbit_id   = trNumber/10;
      run->_event_id    = trNumber;
      run->_bunch_id = trNumber%100;
      run->_trigger_type = 0;
      raw->adoptBank((LHCb::RawBank*)bank, true);

      m_rawEvent.put(std::move(raw));
      return StatusCode::SUCCESS;
    }
  };
}

DECLARE_COMPONENT( Online::RawEventTestCreator )
