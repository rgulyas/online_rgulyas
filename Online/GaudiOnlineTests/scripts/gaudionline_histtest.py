import os, sys, socket, GaudiOnline
import OnlineEnvBase as OnlineEnv
import Configurables

print_level = 3
application = GaudiOnline.Passthrough(outputLevel=print_level,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=0xFFFF,
                                      classType=GaudiOnline.Class1)
#
application.setup_monitoring_service()
hist_test    = Configurables.Online__HistogramTypeTest()
counter_test = Configurables.Online__CounterTypeTest()
application.setup_hive(GaudiOnline.FlowManager("EventLoop"), 40)
application.setup_algorithms([hist_test, counter_test], 0.01)
#
sink = Configurables.OnlMonitorSink()
sink.HistogramsToPublish = [(".*", ".*")]
sink.CountersToPublish = [(".*", ".*")]
application.app.ExtSvc.append(sink)
application.config.inputType = 'None'
application.config.execMode  = 4
if OnlineEnv.PartitionName != 'FEST':
  application.config.autoStart       = True
print('Setup complete....')
