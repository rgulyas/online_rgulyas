#!/bin/bash
#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
opts_file=./CreateMEP${1}.opts;
if test -f ${opts_file};
then
  rm -f ${opts_file};
fi

CREATETAE=false;
FILE_PREFIX=mep
if test "${1}" = "-tae";
then
    CREATETAE=true;
    FILE_PREFIX=tae
    echo "We are creating TAE events.....";
fi

cat >${opts_file} <<END-OF-OPTS
#pragma print off
#include "$GAUDIONLINETESTSROOT/options/CreateMEP.opts"
ApplicationMgr.OutStream  = { "Online::MEPWriter/Writer_0" };
ApplicationMgr.EvtMax   = 2002;
StoreExplorer.PrintFreq = 0.01;
Writer_0.Compress       = 0;
Writer_0.ChecksumType   = 0;
Writer_0.PackingFactor  = 11;
Writer_0.Connection     = "file://${FILE_PREFIX}Data_0.dat";
Writer_0.MakeTAE        = ${CREATETAE};
Writer_0.OutputLevel    = 2;
END-OF-OPTS
#
cat ${opts_file};
`which gentest` libGaudiOnlineTestsComp.so GaudiMain ${opts_file};
if test -f ${opts_file}; then
    rm ${opts_file};
fi;
