//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "RTL/rtl.h"
#include "RTL/bits.h"

namespace {
  struct Test {
    char* txt;
    int txt_len, bf_len;
    Test(char* buff, int txlen, int len) : txt(buff), txt_len(txlen), bf_len(len) {}
    int alloc(int len)  {
      int pos = 0;
      ::BF_alloc2(txt,bf_len,len,&pos);
      printf("BF_alloc(txt,%d,%d,%d);\n",bf_len,len,pos);
      ::BF_print(txt,txt_len);
      return pos;
    }
    void free(int pos, int len)  {
      ::BF_free2(&txt[0], pos, len);
      printf("BF_free(txt,%d,%d);\n",pos,len);
      ::BF_print(txt,txt_len);
    }
  };
}

extern "C" int rtl_testbits2(int,char **) {
  int  pos = 0, size = 15;
  char txt[32*sizeof(int)];
  int  bf_len = 8*sizeof(txt);

  Test test(txt, sizeof(txt), bf_len);  

  ::memset(txt,0,sizeof(txt));
  ::printf("Filled size: %ld bits\n",long(sizeof(txt)*8));
  ::BF_set2(txt,20,96);
  ::printf("BF_set(txt,20,%d);\n",96);
  ::BF_print(&txt[0],sizeof(txt));
  ::BF_count2(txt,sizeof(txt),&pos,&size);
  ::printf("BFCount: pos:%d size:%d\n",pos,size);
  ::BF_count2(txt+8,bf_len-8*8,&pos,&size);
  ::printf("BFCount: start:%d pos:%d size:%d  %d\n",8*8,pos,size, size+8*8+pos);
  ::BF_count2(txt+16,bf_len-16*8,&pos,&size);
  ::printf("BFCount: start:%d pos:%d size:%d  %d\n",16*8,pos,size, size+16*8+pos);
  ::BF_count2(txt+32,bf_len-32*8,&pos,&size);
  ::printf("BFCount: start:%d pos:%d size:%d  %d\n",32*8,pos,size,size+32*8+pos);

  test.free(21,94);
  test.alloc(256);
  test.free(test.alloc(11), 11);
  test.alloc(11+32);
  test.alloc(32);
  test.alloc(20);
  test.alloc(19);
  test.alloc(5*4*32);

  return 0;
}

extern "C" int rtl_testbits3(int,char **) {
  char txt[32*sizeof(int)];
  int  bf_len = 8*sizeof(txt);
  Test test(txt, sizeof(txt), bf_len);  

  ::memset(txt,0,sizeof(txt));
  ::printf("Filled size: %ld bits\n",long(sizeof(txt)*8));

  test.alloc(512);
  test.free(60,16);
  test.alloc(15);
  test.free(120,122);
  test.alloc(108);
  test.alloc(13);
#if 0
  test.free(test.alloc(11), 11);
  test.alloc(11+32);
  test.alloc(32);
  test.alloc(20);
  test.alloc(19);
  test.alloc(5*4*32);
#endif
  return 0;
}
