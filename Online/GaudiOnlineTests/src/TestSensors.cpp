//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <CPP/Interactor.h>
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <WT/wtdef.h>

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

namespace {

  class SensorTester : public Interactor {
    typedef vector<SensorTester*> Probes;
    SensorTester* m_parent;
    int    m_turns=0, m_sleep=0, m_id=0;
    Probes m_probes;

  public:
    int count = 0;

  public:
    SensorTester(SensorTester* parent, int turns)
      : m_parent(parent), m_turns(turns), count(turns)
    {
      m_sleep = 100%(turns>0 ? turns : 1);
    }
    virtual ~SensorTester() {
      for(Probes::iterator i=m_probes.begin(); i!=m_probes.end();++i)
        delete (*i);
      m_probes.clear();
    }
    void add(SensorTester* test) {
      count  += test->m_turns;
      m_turns  += test->m_turns+1;
      test->m_id = m_probes.size()+1;
      m_probes.push_back(test);
    }
    void check_finish(const char* test)  {
      --m_turns;
      if ( 0 == m_turns )   {
	cout << "[" << test << "] Finished processing after " << count << " turns." << endl;
	for(size_t i=0; i<m_probes.size(); ++i) {
	  cout << "[" << test << "] ---> Probe " << i << " had " << m_probes[i]->count << " turns." << endl;
	}
	::wtc_insert(WT_FACILITY_EXIT,0);
      }
    }
    void handleTimer(long value) {
      switch(value)   {
      case 0:
        --m_parent->m_turns;
	//cout << "[TIMERTEST] " << m_turns << " turns." << m_parent->m_turns << endl;
	TimeSensor::instance().add(this,m_sleep,(void*)long((--m_turns)>0 ? 0 : 1));
	break;
      case 1:
	TimeSensor::instance().add(m_parent,m_sleep,(void*)2);
	break;
      case 2:
	check_finish("TIMERTEST");
	break;
      }
    }
    void handleIoc(int typ) {
      switch(typ) {
      case 0:
        --m_parent->m_turns;
	//cout << "[IOCTEST] " << m_turns << " turns. Parent:" << m_parent->m_turns << endl;
	IocSensor::instance().send(this,(--m_turns)>0 ? 0 : 1,this);
	break;
      case 1:
	IocSensor::instance().send(m_parent,2,this);
        break;
      case 2:
	check_finish("IOCTEST");
	break;
      }
    }
    void handle(const Event& ev) override {
      switch(ev.eventtype) {
      case IocEvent:
        handleIoc(ev.type);
        break;
      case TimeEvent:
        handleTimer((long)ev.timer_data);
        break;
      default:
        break;
      }
    }
    static int start_ioc() {
      SensorTester test(0,0);
      for (size_t i=0; i<10; ++i) {
        SensorTester* probe = new SensorTester(&test, 1000);
        test.add(probe);
        IocSensor::instance().send(probe,0,probe);
      }
      IocSensor::instance().run();
      IocSensor::instance().shutdown();
      cout << "[IOCTEST]  Sensor test finished after " << test.count << " calls." << endl;
      return 0;
    }
    static int start_timer() {
      SensorTester test(0,0);
      for (size_t i=0; i<10; ++i) {
        SensorTester* probe = new SensorTester(&test, i+1);
        test.add(probe);
        TimeSensor::instance().add(probe,i+1,0);
      }
      TimeSensor::instance().run();
      TimeSensor::instance().shutdown();
      cout << "[TIMERTEST]  Sensor test finished after " << test.count << " calls." << endl;
      return 0;
    }
  };
}

extern "C" int cpp_test_iocsensor(int, char**) {
  return SensorTester::start_ioc();
}

extern "C" int cpp_test_timesensor(int, char**) {
  return SensorTester::start_timer();
}
