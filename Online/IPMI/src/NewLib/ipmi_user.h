/*
 * Copyright (c) 2003 Sun Microsystems, Inc.  All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * Redistribution of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * Redistribution in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * Neither the name of Sun Microsystems, Inc. or the names of
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 * SUN MICROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL NOT BE LIABLE
 * FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.  IN NO EVENT WILL
 * SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA,
 * OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR
 * PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF
 * LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE,
 * EVEN IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

#ifndef IPMI_USER_H
#define IPMI_USER_H

#if HAVE_CONFIG_H
# include "config.h"
#endif
#include "ipmi.h"

#define IPMI_PASSWORD_DISABLE_USER  0x00
#define IPMI_PASSWORD_ENABLE_USER   0x01
#define IPMI_PASSWORD_SET_PASSWORD  0x02
#define IPMI_PASSWORD_TEST_PASSWORD 0x03

#define IPMI_USER_ENABLE_UNSPECIFIED 0x00
#define IPMI_USER_ENABLE_ENABLED 0x40
#define IPMI_USER_ENABLE_DISABLED 0x80
#define IPMI_USER_ENABLE_RESERVED 0xC0

/* (22.27) Get and (22.26) Set User Access */
struct user_access_t {
	uint8_t callin_callback=0;
	uint8_t channel=0;
	uint8_t enabled_user_ids=0;
	uint8_t enable_status=0;
	uint8_t fixed_user_ids=0;
	uint8_t ipmi_messaging=0;
	uint8_t link_auth=0;
	uint8_t max_user_ids=0;
	uint8_t privilege_limit=0;
	uint8_t session_limit=0;
	uint8_t user_id=0;
	void zero()
	{
	    callin_callback=0;
	    channel=0;
	    enabled_user_ids=0;
	    enable_status=0;
	    fixed_user_ids=0;
	    ipmi_messaging=0;
	    link_auth=0;
	    max_user_ids=0;
	    privilege_limit=0;
	    session_limit=0;
	    user_id=0;

	};
};

/* (22.29) Get User Name */
struct user_name_t {
	uint8_t user_id=0;
	uint8_t user_name[17]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	void zero()
	{
	  user_id = 0;
	  memset(user_name,0,sizeof(user_name));
	}
};
template <class T> class ipmi_intf;
template <class T> class ipmi_helper;
template <class T> class ipmi_user
{
  public:
//    ipmi_user(ipmi_intf<T> *i);
    ipmi_helper<T> *h;
    ipmi_intf<T> *intf;
    ipmi_user(ipmi_intf<T> *i);
    int get_user_access( struct user_access_t *user_access_rsp);
    int get_user_name( struct user_name_t *user_name_ptr);
    int set_user_access( struct user_access_t *user_access_req,
        uint8_t change_priv_limit_only);
    int set_user_password( uint8_t user_id, uint8_t operation,    const char *password, uint8_t is_twenty_byte);
    void dump_user_access(const char *user_name, struct user_access_t *user_access);
    void dump_user_access_csv(const char *user_name, struct user_access_t *user_access);
    int print_user_list(uint8_t channel_number);
    int print_user_summary(uint8_t channel_number);
    int set_user_name(uint8_t user_id, const char *name);
    int user_test_password(uint8_t user_id, const char *password,uint8_t is_twenty_byte_password);
    int user_summary( int argc, char **argv);
    int user_test( int argc, char **argv);
    int user_priv( int argc, char **argv);
    int user_mod( int argc, char **argv);
    int user_password( int argc, char **argv);
    int user_name( int argc, char **argv);


};
#endif /* IPMI_USER_H */
