//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//  Author     : Markus Frank
//==========================================================================
extern "C" int mbmdump(int, char**);
int main(int argc, char** argv)  {
  return mbmdump(argc,argv);
}
