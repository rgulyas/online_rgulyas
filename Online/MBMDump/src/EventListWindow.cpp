//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//  ========================================================================
//  EventListWindow.cpp
//  ------------------------------------------------------------------------
//
//  Package   : MBMDump: Dump utility for MBM buffer events
//
//  Author    : Markus Frank
//
//  ========================================================================
//
// C++ include files
#include <string>

// Framework include files
#include "MBMDump/MBMDump.h"

using namespace Online;

/// Initializing constructor
EventListWindow::EventListWindow(BaseMenu* par,int cmd_id, const Format& f, Events&& e, bool bld)
  : BaseMenu(par), m_fmt(f), m_events(e), m_bankWindow(0)
{
  m_parentCmd = cmd_id;
  if ( bld ) build();
}

/// Default destructor
EventListWindow::~EventListWindow()  {
  drop(m_bankWindow);
  deleteMenu();
}

/// Build the menu
void EventListWindow::build()  {
  char txt[256];
  openDetached(0,0,"Display window"," Event list ",procName());
  addCommand(C_DISMISS,"Dismiss");
  addComment(C_COM2,"");
  addComment(C_COM3," Hit return on subevent item to see data");
  addComment(C_COM4,"");
  addComment(C_COM5,"+------------------------------------------------------------------------------------------------------------------------+");
  addComment(C_COM6,"| EventID  <--------- Size -------->  Checksum  Flags <---------- Trigger Mask --------->      Run      Bunch      Orbit |");
  addComment(C_COM7,"+------------------------------------------------------------------------------------------------------------------------+");
  for(size_t cnt=0; cnt<m_events.size();++cnt)  {
    unsigned int eid = m_events[cnt].first;
    const auto* h = m_events[cnt].second;
    if ( h->is_mdf() )  {
      const auto* sh = h->subHeader().H1;
      auto        m = sh->triggerMask();
      ::snprintf(txt,sizeof(txt),"| %4u -> %8d %8d %8d  %08X %02X%02X%02X %08X %08X %08X %08X %8d   %08X   %08X |",
		 eid, h->size0(), h->size1(), h->size2(), h->checkSum(), h->compression(), 
		 h->hdr(), h->dataType(), m[0], m[1], m[2], m[3], 
		 sh->runNumber(), sh->orbitNumber(), sh->bunchID());
      addCommand(C_ITEMS+cnt,txt);
      continue;
    }
    ::snprintf(txt,sizeof(txt),"| EID:%u -> Corrupted MDF header encountered  [%p]", eid, (void*)h);
    addComment(C_ITEMS+cnt,txt);
  }
  addCommand(C_DISMISS2,"Dismiss");
  closeMenu();
  setCursor(C_DISMISS,1);
}

/// Virtual overload to handle menu interaction(s)
void EventListWindow::handleMenu(int cmd_id)    {
  switch(cmd_id)  {
  case C_DISMISS:
  case C_DISMISS2:
    hideMenu();
    parent().setCursor(m_parentCmd,1);
    break;
  default:
    if ( cmd_id >= C_ITEMS )  {
      for(size_t cnt=0; cnt<m_events.size(); ++cnt)  {
	if ( cnt+C_ITEMS == size_t(cmd_id) )  {
	  BankListWindow::Banks   banks;
	  const auto* h = m_events[cnt].second;
	  checkRecord(h, h->recordSize(),true,false);
	  const uint8_t* start = h->data();
	  const uint8_t* end   = start + h->size();
	  while(start < end)    {
	    auto* bank = (raw_bank_offline_t*)start;
	    std::size_t len = bank->totalSize();
	    banks.emplace_back(0,bank);
	    start += len;
	    if ( 0 == len ) break;
	  }
	  replace(m_bankWindow,new BankTypesWindow(this,cmd_id,m_fmt,std::move(banks)));
	  return;
	}
      }
    }
    break;
  }
}

