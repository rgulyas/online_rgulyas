//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_ONLINEALIGN_FITTER_H
#define ONLINE_ONLINEALIGN_FITTER_H

/// Framework include files
#include <GaudiKernel/AlgTool.h>
#include <OnlineAlign/IAlignUser.h>
#include <RTL/rtl.h>

/// Forward declarations
class TMinuit;

/// Online namespace declaration
namespace Online  {

  class IAlignDrv;
  class CounterTask;

  class Fitter : public extends<AlgTool, IAlignIterator>   {
  public:
    /// Tool constructor
    Fitter(const std::string& type, const std::string& name, const IInterface* parent);

    /// AlgTool overloads
    virtual StatusCode initialize() override;
    virtual StatusCode finalize() override;

    /// IAlignIterator overloads
    virtual StatusCode begin() override;
    virtual StatusCode end() override;
    virtual StatusCode run() override;

    /// Implementation
    std::vector<double> readParams()  const;
    double              getIterationResult();
    StatusCode          writeParams(const std::vector<double> &params) const;
    void                setPartitionName(const std::string& partition);

    SmartIF<IAlignDrv>       parent;

  protected:

    static void Chi2(int& npar, double* grad, double& fval, double* params, int flag);
    
    lib_rtl_thread_t         m_thread;
    std::unique_ptr<TMinuit> m_minuit;
    std::string              m_paramFileName;
    std::string              m_partitionName;
    std::string              m_counterTaskDNS;
    std::string              m_counterTaskName;
    std::vector<std::string> m_counterNames;
  };
}      // End namespace Online
#endif // ONLINE_ONLINEALIGN_FITTER_H
