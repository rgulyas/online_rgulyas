//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

#ifndef ONLINE_ASIO_BOOST_H
#define ONLINE_ASIO_BOOST_H

// Boot include files
#include <boost/asio.hpp>

#if BOOST_ASIO_VERSION >= 101200
// Helpers for backwards compatibility
namespace boost   {
  namespace asio  {
    namespace detail  {
      inline void* buffer_cast_helper(const mutable_buffer& b)       {  return b.data();      }
      inline const void* buffer_cast_helper(const const_buffer& b)   {  return b.data();      }
      inline std::size_t buffer_size_helper(const mutable_buffer& b) {  return b.size();      }
      inline std::size_t buffer_size_helper(const const_buffer& b)   {  return b.size();      }
    }
  }
}
#endif

#endif  /* ONLINE_ASIO_BOOST_H */
