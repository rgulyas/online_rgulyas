//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINE_ONLINEBASE_CPP_CONFIG_H
#define ONLINE_ONLINEBASE_CPP_CONFIG_H

#ifndef dd4hep
#define dd4hep Online
#endif

#endif  // ONLINE_ONLINEBASE_CPP_CONFIG_H
