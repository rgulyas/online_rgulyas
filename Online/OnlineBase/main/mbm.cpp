//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
extern "C" int MBM_FUNCTION (int argc, char** argv);

int main (int argc, char** argv)  {
  return MBM_FUNCTION (argc, argv);
}
