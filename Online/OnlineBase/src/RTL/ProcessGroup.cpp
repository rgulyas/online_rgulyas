//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : OnlineKernel
//
//  Author     : Markus Frank
//==========================================================================
// Include files
#include "RTL/ProcessGroup.h"

#include <cmath>
#include <cstdio>
#include <signal.h>
#include "RTL/rtl.h"
#ifndef _WIN32
#include <sys/wait.h>
#endif
#include <iostream>
#include <sstream>
#include <cerrno>

#include "RTL/DllAccess.h"
using OnlineBase::currentCommand;

using namespace std;
using namespace RTL;

/// Standard constructor
ProcessGroup::ProcessGroup() : Process(0)   {
}

/// Standard destructor
ProcessGroup::~ProcessGroup() {
}

/// Get a process by PID
Process* ProcessGroup::get(int pid) const {
  for(Processes::const_iterator i=m_procs.begin(); i!=m_procs.end();++i) {
    if ( pid == (*i)->pid() ) return *i;
  }
  return 0;
}

/// Start process
int ProcessGroup::start()    {
  int status = 1;
  for(Processes::iterator i=m_procs.begin(); i!=m_procs.end();++i) {
    int iret = (*i)->start();
    if ( !lib_rtl_is_success(iret) ) {
      status = iret;
    }
  }
  return status;
}

/// Start process
int ProcessGroup::start(bool new_process_group)    {
  int status = 1;
  for(Processes::iterator i=m_procs.begin(); i!=m_procs.end();++i) {
    int iret = (*i)->start(new_process_group);
    if ( !lib_rtl_is_success(iret) ) {
      status = iret;
    }
  }
  return status;
}

/// Wait for process group.end
int ProcessGroup::wait(int flag)    {
#ifdef __linux
  int status = 1;
  int cnt = m_procs.size();
  int opt = WNOHANG;
  switch(flag) {
  case WAIT_BLOCK:
    opt = WUNTRACED|WCONTINUED;
  case WAIT_NOBLOCK:
  default:
    break;
  }
  while(cnt>=0) {
    int sc = 0;
    pid_t pid = -1;
    do {
      pid = waitpid(0,&sc,opt);
    } while (pid == -1 && errno == EINTR);
    if ( pid != -1 ) {
      Process* p = get(pid);
      if ( p && Process::debug() ) {
	std::stringstream str;
	str << "Process " << p->name() << " ended. Status=" << sc;
	if ( !p->output().empty() ) str << " output redirected to:" << p->output();
	str << endl;
	::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
      }
    }
    else if ( pid == -1 ) {
      return -1;
    }
  }
  return status;
#else
  return 0;
#endif
}

/// Add a new process
void ProcessGroup::add(Process* p) {
#ifdef __linux
  if ( p ) m_procs.push_back(p);
#else

#endif
}

/// Remove a process from the list
int ProcessGroup::remove(const string& name, Process** proc) {
  if ( proc ) *proc = 0;
  for(Processes::iterator i=m_procs.begin(); i!=m_procs.end();++i) {
    if ( name == (*i)->name() ) {
      if ( proc ) *proc = (*i);
      else        delete (*i);
      m_procs.erase(i);
      return 1;
    }
  }
  return 0;
}

/// Remove all processes
int ProcessGroup::removeAll() {
  for(Processes::iterator i=m_procs.begin(); i!=m_procs.end();++i)
    delete (*i);
  m_procs.clear();
  return 1;
}
