//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/Numa.h>
#if defined(ONLINE_HAVE_NUMA)
#include <numa.h>
#include <RTL/Logger.h>
#include <set>
#endif

#if !defined(ONLINE_HAVE_NUMA)

/// Check availability of NUMA
int RTL::Numa::available()  {
  return -1;
}

/// Short summary of process status
std::pair<int, std::vector<std::string> > RTL::Numa::printStatus()  {
  return { -1, { } };
}

/// Print the current NUMA environment the process is executing on
std::pair<int, std::vector<std::string> > RTL::Numa::printSettings()  {
  std::vector<std::string> lines;
  lines.emplace_back("Numa is not available on this node. No settings applied.");
  return { -1, lines };
}

/// Apply NUMA configuration settings according to job options
std::pair<int, std::vector<std::string> >  RTL::Numa::apply_config()   {
  return { -1, { } };
}

#else

using RTL::Logger;

/// Print bitmask content
static std::string c_mask(bitmask* m)  {
  std::string msk;
  for(unsigned int i=0; i < m->size; ++i)   {
    if ( i > 64 ) continue;
    msk += char(::numa_bitmask_isbitset(m,i) ? '1' : '0');
  }
  return msk;
}

/// Check availability of NUMA
int RTL::Numa::available()  {
  return ::numa_available();
}

/// Short summary of process status
std::pair<int, std::vector<std::string> > RTL::Numa::printStatus()  {
  std::vector<std::string> lines;
  pid_t pid = ::lib_rtl_pid();

  lines.emplace_back(Logger::format("| numa_preferred:              %16d",numa_preferred()));
  lines.emplace_back(Logger::format("| numa_get_membind:            %s",c_mask(numa_get_membind()).c_str()));
  bitmask* m = numa_allocate_cpumask();
  ::numa_sched_getaffinity(pid, m);
  lines.emplace_back(Logger::format("| numa_sched_getaffinity:      %s",c_mask(m).c_str()));
  ::numa_free_cpumask(m);
  lines.emplace_back(Logger::format("+--------------------------------------- PID:%d",pid));
  return { 0, lines };
}

/// Print the current NUMA environment the process is executing on
std::pair<int, std::vector<std::string> > RTL::Numa::printSettings()  {
  std::vector<std::string> lines;

  if ( -1 == RTL::Numa::available() )   {
    lines.emplace_back("Numa is not available on this node. No settings applied.");
    return { -1, lines };
  }
  lines.emplace_back("+-------------------- Numa Node info ---------------------------");
  lines.emplace_back("| numa is available on this node. Properties are as follows:");
  lines.emplace_back(Logger::format("| numa_max_possible_node:      %16d",::numa_max_possible_node()));
  lines.emplace_back(Logger::format("| numa_num_possible_nodes:     %16d",::numa_num_possible_nodes()));
  lines.emplace_back(Logger::format("| numa_max_node:               %16d",::numa_max_node()));
  lines.emplace_back(Logger::format("| numa_num_configured_nodes:   %16d",::numa_num_configured_nodes()));
  lines.emplace_back(Logger::format("| numa_num_configured_cpus:    %16d",::numa_num_configured_cpus()));
  lines.emplace_back(Logger::format("| numa_num_task_cpus:          %16d",::numa_num_task_cpus()));
  lines.emplace_back(Logger::format("| numa_num_task_nodes:         %16d",::numa_num_task_nodes()));
  lines.emplace_back(Logger::format("| numa_get_mems_allowed:       %s",c_mask(::numa_get_mems_allowed()).c_str()));
  lines.emplace_back(Logger::format("| numa_all_nodes_ptr:          %s",c_mask(::numa_all_nodes_ptr).c_str()));
  lines.emplace_back(Logger::format("| numa_no_nodes_ptr:           %s",c_mask(::numa_no_nodes_ptr).c_str()));
  lines.emplace_back(Logger::format("| numa_all_cpus_ptr:           %s",c_mask(::numa_all_cpus_ptr).c_str()));
  lines.emplace_back(Logger::format("| numa_get_run_node_mask:      %s",c_mask(::numa_get_run_node_mask()).c_str()));
  lines.emplace_back(Logger::format("| numa_get_interleave_mask:    %s",c_mask(::numa_get_interleave_mask()).c_str()));

  bitmask* cm = ::numa_allocate_cpumask();
  for(size_t i=0, n=::numa_max_node(); i<=n && i; ++i)  {
    long long int free_mem = 0;
    long long int total_mem = ::numa_node_size64(i,&free_mem);
    ::numa_bitmask_clearall(cm);
    ::numa_node_to_cpus(i,cm);
    lines.emplace_back(Logger::format("| numa_node_size(%2ld):          %16lld   Free:%lld",
				      i, total_mem, free_mem));
    lines.emplace_back(Logger::format("| numa_node_to_cpus(%2ld):       %s",i,c_mask(cm).c_str()));
  }
  ::numa_bitmask_free(cm);
  lines.emplace_back(Logger::format("+-------------------- Numa Task info ---------------------------"));
  auto other_lines = Numa::printStatus();
  lines.insert(lines.end(), other_lines.second.begin(), other_lines.second.end());
  return { 0, lines };
}

/// Apply NUMA configuration settings according to job options
std::pair<int, std::vector<std::string> > RTL::Numa::apply_config()   {
  std::vector<std::string> lines;
  if ( -1 == available() )  {
    lines.emplace_back(Logger::format("Numa is not available on this node. No settings applied."));
    return { -1, lines };
  }
  int status = 0;
  /// Setup the bitmasks for the nodes to run on:
  bitmask* nm     = ::numa_allocate_nodemask();
  bitmask* cm     = ::numa_allocate_cpumask();
  ::numa_bitmask_clearall(nm);
  ::numa_bitmask_clearall(cm);

  if ( !cpuMask.empty() )  {
    std::set<int> slots;  
    for(size_t i=0; i < cpuMask.size(); ++i )   {
      if ( cpuMask[i] == 0 )  {
	::numa_bitmask_clearbit(cm, i);
      }
      else   {
	int node = ::numa_node_of_cpu(i);
	::numa_bitmask_setbit(cm, i);
	::numa_bitmask_setbit(nm, node);
	slots.insert(node);
      }
    }
    cpuSlots.clear();
    for(int i : slots)  {
      cpuSlots.push_back(i);
    }
  }
  else if ( !cpuSlots.empty() )  {
    ::numa_bitmask_clearall(cm);
    for(int i : cpuSlots )   {
      if ( 0 == ::numa_node_to_cpus(i,cm) )  {
	::numa_bitmask_setbit(nm, i);
      }
    }
  }

  int ret;
  /// Propagate the CPU bindings unless switched off
  if ( bindCPU )  {
    lines.emplace_back(Logger::format("Set numa node     mask to: %s",c_mask(nm).c_str()));
    lines.emplace_back(Logger::format("Set numa affinity mask to: %s",c_mask(cm).c_str()));
    ret = ::numa_run_on_node_mask(nm);
    if ( ret != 0 )  {
      lines.emplace_back(Logger::format("NUMA failed bind task to CPU slots %s: [%s]",
					c_mask(nm).c_str(), RTL::errorString().c_str()));
      status = -1;
    }
    ret = ::numa_sched_setaffinity(::lib_rtl_pid(),cm);
    if ( ret != 0 )  {
      lines.emplace_back(Logger::format("NUMA failed set task affinity to mask %s: [%s]",
					c_mask(cm).c_str(), RTL::errorString().c_str()));
      status = -1;
    }
  }
  /// Apply the memory binding unless switched off
  if ( bindMemory )  {
    bitmask* nmfree = ::numa_allocate_nodemask();
    ::numa_set_localalloc();
    ::numa_set_membind(nm);
    /// Setup the bitmask with all the nodes to free when migrating pages:
    ::numa_bitmask_clearall(nmfree);
    for(size_t b=0, n=::numa_max_node(); b<=n && b<nm->size; ++b)  {
      if ( !::numa_bitmask_isbitset(nm,b) )  {
	::numa_bitmask_setbit(nmfree,b);
      }
    }
    ret = ::numa_migrate_pages(::lib_rtl_pid(), nmfree, nm);
    if ( ret != 0 )  {
      lines.emplace_back(Logger::format("NUMA failed to migrate pages: %s",
					RTL::errorString().c_str()));
      status = -1;
    }
    ::numa_bitmask_free(nmfree);
  }
  ::numa_bitmask_free(cm);
  ::numa_bitmask_free(nm);
  return { status, lines };
}
#endif

