// ===========================================================================
// Include files
// ===========================================================================
#include "Options/Utils.h"
// ===========================================================================
// Boost
// ===========================================================================
#include <regex>
#include <boost/algorithm/string.hpp>
// ===========================================================================
#include "Options/sys.h"
// ===========================================================================

namespace gpu=Gaudi::Parsers::Utils;

std::string gpu::replaceEnvironments(const std::string& input) {
  static const std::regex expression("\\$(([A-Za-z0-9_]+)|\\(([A-Za-z0-9_]+)\\))");

    std::string result=input;
    auto start = input.begin();
    auto end = input.end();
    std::match_results<std::string::const_iterator> what;
    std::regex_constants::match_flag_type flags = std::regex_constants::match_default;
    while ( std::regex_search(start, end, what, expression, flags ) )
    {
      std::string var{what[2].first,what[2].second};
      if (var.empty()) var = std::string{what[3].first,what[3].second};
      std::string env;
      if ( System::getEnv( var, env ) ) {
        boost::algorithm::replace_first(result,
                                        std::string{ what[0].first, what[0].second },
                                        env);
      }
      start = what[0].second;
      // update flags:
      flags |= std::regex_constants::match_prev_avail;
      // Really ever need to deal with \A (LF)  ?
      // flags |= std::regex_constants::match_not_bob;
    }
    return result;
}
// ===========================================================================
