#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/ROLogger
---------------
#]=======================================================================]

online_library(ROLogger
	src/FifoWriter.cpp
	src/StdoutLogger.cpp
	src/DimLogger.cpp
	src/DimWriter.cpp
	src/DimConsumer.cpp
	src/DimNodeConsumer.cpp
	src/LogServer.cpp
	src/StdoutWriter.cpp
	src/PublishLogger.cpp
	src/FifoLogger.cpp
	src/StdinConsumer.cpp)
target_link_libraries(ROLogger PRIVATE Online::dim Online::OnlineBase)
