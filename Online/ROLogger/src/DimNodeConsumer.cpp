//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_DIMNODECONSUMER_H
#define ROLOGGER_DIMNODECONSUMER_H

/// Framework include files
#include "LogServer.h"

/// C/C++ include files
#include <string>
#include <vector>
#include <regex>

/// rologger namespace declaration
namespace rologger  {

  namespace  {

    class NodeConsumerImp : public Consumer::Implementation  {
    public:
      typedef std::vector<std::regex> Matches;
      typedef std::map<std::string, unsigned int> ServiceList;
      ServiceList    service_list;
      Matches        server_match;
      std::string    broker;
      unsigned int   brokerServers = 0;
      bool           print_stats = false;

      /// Signal handler to stop
      static void sig_stop (int sig);
      /// DimInfo overload to process messages
      static void analyze_servers(void* tag, void* address, int* size);
      /// Analyze the DIM service list of the broker
      void        analyze_servers(char* services, size_t len);
      /// DimInfo overload to process messages
      static void process_messages(void* tag, void* address, int* size);
      /// Analyze a single payload messages
      void        process_messages(char* payload, size_t len);

      /// Add logging handler for a single server process
      void _add_server(const std::string& server);
      /// Add a set of host services
      void _add_servers(char* services);
      /// Remove set of host services
      void _remove_servers(char* services);

    public:
      /// Default constructor
      NodeConsumerImp() = default;
      /// Default destructor
      virtual ~NodeConsumerImp() = default;
      /// Base consumer overload: Add new listening topic
      virtual void add_topic(const std::string& name)  override;
      /// Base consumer overload: Start listening to rologger topics
      virtual int  listen(const std::string& brokers)  override;
      /// Base consumer overload: Shutdown processing
      virtual void shut_down()  override;
    };
  }
}
#endif // ROLOGGER_DIMNODECONSUMER_H

/// Framework include files
#include "DimConsumer.h"
#include <WT/wtdef.h>
#include <RTL/rtl.h>
#include <dim/dic.hxx>

/// C/C++ include files
#include <csignal>

using namespace rologger;

namespace   {
  void get_service_node(char* s, std::string& svc, std::string& node) {
    char* at = strchr(s,'@');
    *at = 0;
    svc = s;
    node = at+1;
  }
}

/// Signal handler to stop
void NodeConsumerImp::sig_stop (int /* sig */) {
  ::wtc_insert(WT_FACILITY_EXIT,nullptr);
}

/// DimInfo overload to process messages
void NodeConsumerImp::process_messages(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    NodeConsumerImp* it = *(NodeConsumerImp**)tag;
    it->process_messages((char*)address, *size);
  }
}

/// Analyze a single payload messages
void NodeConsumerImp::process_messages(char* payload, size_t len)   {
  /// Start measuring from first message received
  if ( !monitor.t_start )   {
    monitor.t_start = monitor.t_last = rd_clock();
  }
  monitor.messages++;
  monitor.bytes += len;
  for( auto l : listeners )
    l->handle_payload("???", payload, len, 0, 0);
  auto now = rd_clock();
  if ( monitor_interval > 0 && now - monitor.t_last > monitor_interval )   {
    for( auto l : listeners )
      l->handle_monitoring(monitor);
    monitor.t_last        = now;
    monitor.messages_last = monitor.messages;
    monitor.bytes_last    = monitor.bytes;
  }
}

/// DimInfo overload to process messages
void NodeConsumerImp::analyze_servers(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    NodeConsumerImp* it = *(NodeConsumerImp**)tag;
    it->analyze_servers((char*)address, *size);
  }
}

/// Remove set of host services
void NodeConsumerImp::_remove_servers(char* msg)   {
  char *at, *p = msg, *last = msg;
  std::string svc, node;
  while ( last != 0 && (at=strchr(p,'@')) != 0 )  {
    last = strchr(at,'|');
    if ( last ) *last = 0;
    get_service_node(p,svc,node);
    auto it  = this->service_list.find(svc);
    if ( it != this->service_list.end() )    {
      ::dic_release_service((*it).second);
      this->service_list.erase(it);
    }
    p = last+1;
  }
}

/// Add a set of host services
void NodeConsumerImp::_add_servers(char* msg)   {
  char *at, *p = msg, *last = msg;
  std::string svc, node;
  while ( last != 0 && (at=strchr(p,'@')) != 0 )  {
    last = strchr(at,'|');
    if ( last ) *last = 0;
    get_service_node(p, svc, node);
    _add_server(svc);
    p = last+1;
  }
}

/// Add logging handler for a single server process
void NodeConsumerImp::_add_server(const std::string& server)    {
  bool match = server_match.empty();
  if ( !match )   {
    for ( const auto& m : server_match )  {
      if ( std::regex_match(server, m) )  {
	match = true;
	break;
      }
    }
  }
#if 0
  ::lib_rtl_output(LIB_RTL_VERBOSE,"+++ Check: %s", server.c_str());
#endif
  if ( match && this->service_list.find(server) == this->service_list.end() )   {
    std::string svc = server + "/log";
    unsigned int id = ::dic_info_service_dns(this->in_dns_ID, svc.c_str(), MONITORED,
					     0, 0, 0, process_messages, 
					     (long)this, 0, 0);
    this->service_list.insert(std::make_pair(server, id));
    ::lib_rtl_output(LIB_RTL_VERBOSE,"+++ Adding message source: %s", server.c_str());
  }
}

/// Analyze the DIM service list of the broker
void NodeConsumerImp::analyze_servers(char* msg, size_t len)   {
  if ( msg && *msg && len>0 )  {
    if ( *msg == '+' )   {
      _add_servers(msg + 1);
    }
    else if ( *msg == '!' )   {
      _add_servers(msg + 1);
    }
    else if ( *msg == '-' )   {
      _remove_servers(msg + 1);
    }
    else  {
      _add_servers(msg);
    }
  }
}

/// Base server overload: Add new listening topic
void NodeConsumerImp::add_topic(const std::string& /* name */)   {
}

/// Base server overload: Start listening to rologger topics
int NodeConsumerImp::listen(const std::string& broker_name)   {
  this->broker = broker_name;
  if ( this->broker.empty() )    {
    char text[132];
    ::dic_get_dns_node(text);
    this->broker = text;
  }
  int port            = ::dic_get_dns_port();
  std::string svc     = "DIS_DNS/SERVER_LIST";
  ::lib_rtl_output(LIB_RTL_VERBOSE,"+++ Set dns input node to %s", this->broker.c_str());
  this->in_dns_ID     = ::dic_add_dns(this->broker.c_str(), port);
  this->brokerServers = ::dic_info_service_dns(this->in_dns_ID, svc.c_str(), MONITORED,
					       0, 0, 0, analyze_servers, 
					       (long)this, 0, 0);
  ::signal(SIGIO, NodeConsumerImp::sig_stop);
  return 1;
}

/// Base consumer overload: Shutdown processing
void NodeConsumerImp::shut_down()   {
}

/// rologger namespace declaration
namespace rologger  {

  template <> std::unique_ptr<Consumer::Implementation>
  Consumer::create<DimNodeConsumer>(int argc, char** argv)  {
    auto consumer = std::make_unique<NodeConsumerImp>();
    bool print_help = false;
    std::string val;

    for( int i=0; i<argc; ++i )   {
      char c = argv[i][0];
      if ( c == '-' )   {
	c = argv[i][1];
	switch(c)   {
	case 'h':
	  print_help = true;
	  break;
	case 'i':
	  consumer->monitor_interval = 1000 * ::atoi(argv[++i]);
	  break;
	case 'm':
	case 'S':
	  val = argv[++i];
	  ::lib_rtl_output(LIB_RTL_VERBOSE,"+++ Adding server match: %s", val.c_str());
	  consumer->server_match.emplace_back(std::regex(val));
	  break;
	default:
	  break;
	}
	continue;
      }
    }
    if ( print_help )   {
      ::printf("     -S <reg-ex>    Select messages from nodes matching the   \n"
	       "                    specified regular expression.             \n"
	       "     -i <msecs>     Monitor time interval in milli seconds.   \n");
      ::exit(0);
    }
    return std::unique_ptr<Consumer::Implementation>(move(consumer));
  }
}
