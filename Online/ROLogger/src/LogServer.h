//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_LOGSERVER_H
#define ROLOGGER_LOGSERVER_H

/// C/C++ include files
#include <vector>
#include <string>
#include <memory>
#include <list>

/// kafka namespace declaration
namespace rologger   {

  typedef uint64_t rd_ts_t;

  rd_ts_t rd_clock (void);
  std::string error(int err_num);

  /// Monitoring structure
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class Monitor final  {
  public:
    rd_ts_t  t_start          = 0;
    rd_ts_t  t_end            = 0;
    rd_ts_t  t_end_send       = 0;
    rd_ts_t  t_last           = 0;
    uint64_t messages         = 0;
    uint64_t messages_last    = 0;
    uint64_t messages_dr_ok   = 0;
    uint64_t messages_dr_err  = 0;
    uint64_t messages_dr_wait = 0;
    uint64_t bytes_dr_ok      = 0;
    uint64_t bytes            = 0;
    uint64_t bytes_last       = 0;
    uint64_t offset           = 0;
    uint32_t partitions       = 0;
    Monitor() = default;
    Monitor(Monitor&& copy) = delete;
    Monitor(const Monitor& copy) = delete;
    ~Monitor() = default;
    Monitor& operator=(Monitor&& copy) = default;
    Monitor& operator=(const Monitor& copy) = default;
  };

  /// Writer base class
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class Writer   {
  public:
    /// Flag to print (or not) statistics information in 'handle_monitoring'
    bool    print_stats { false };
    bool    print_raw   { false };

  public:
    /// Default constructor
    Writer() = default;
    /// Default destructor
    virtual ~Writer();

    /// Second level object initialization
    virtual int initialize();
    /// Handler for all messages
    virtual void handle_payload( const char* topic,
				 char* payload, size_t plen,
				 char* key,     size_t klen);  
    /// Handler for all messages
    virtual void handle_raw(     const char* topic,
				 char* payload, size_t plen,
				 char* key,     size_t klen);  
    /// Handle monitoring 
    virtual void handle_monitoring(const Monitor& monitor);
    /// Print raw message
    virtual void print_raw_message(const char* payload, std::size_t len)   const;

    /** Utility functions      */
    /// Print monitoring information in table form
    void print_statistics(const Monitor& monitor)  const;
    /// Print monitoring information in table form
    void print_summary(const Monitor& monitor)   const;
  };

  /// Consumer wrapper
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class Consumer   {
  public:
    /// Implementation base
    class Implementation;

  private:
    /// Reference to implementation
    std::unique_ptr<Implementation> consumer {};

  public:
    /// Access to the monitoring structure
    const Monitor& monitor()  const;

  public:

    /// Specific consumer creator
    template <typename T> static 
      std::unique_ptr<Implementation> create(int argc, char** argv);
    /// Consumer creator
    static std::unique_ptr<Implementation> create(int consumer_type, std::vector<char*>& args);
  protected:
    /// Default constructor
    Consumer() = default;

  public:
    /// Initializing constructor
    Consumer(std::unique_ptr<Implementation>&& impl);
    /// Default destructor
    virtual ~Consumer();
    /// Start publishing monitoring information using DIM
    virtual void publish_monitor(const std::string& dns_name,
				 const std::string& name,
				 const std::string& tag);
    /// Start publishing monitoring information using DIM
    virtual void publish_monitor(long               dns_id,
				 const std::string& name,
				 const std::string& tag);
    /// Add a writer
    void add_writer(Writer* writer);
    /// Remove a writer
    void remove_writer(Writer* writer);
    /// Start listening to topics
    bool listen(const std::string& brokers);
    /// Run the instance
    void run();
  };

  /// Consumer implementation base
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class Consumer::Implementation  {
  public:
    /// Writer queue definition
    typedef std::list<Writer*> Writers;
    /// Message listeners
    Writers    listeners;
    /// Monitoring data block
    Monitor      monitor;
    /// DIM service ID if the monitor information is published
    unsigned int monitorID = 0;
    /// DIM output DNS id for publishing DIM information
    long         out_dns_ID = 0;
    /// DIM input DNS id for accessing data
    long         in_dns_ID = 0;
    /// Monitoring printout interval
    size_t       monitor_interval = 0; /* = 1000000 = 1 second. units are usec! */

  public:
    /// DIM callback to supply the monitoring information
    static void feed_monitor(void* tag, void** buf, int* size, int* first);

  public:
    /// Default constructor
    Implementation() = default;
    /// Default destructor
    virtual ~Implementation() = default;
    /// Add a writer
    virtual void add_writer(Writer* writer);
    /// Remove a writer
    virtual void remove_writer(Writer* writer);
    /// Add new listening topic
    virtual void add_topic(const std::string& name) = 0;
    /// Start listening to kafka topics
    virtual int  listen(const std::string& brokers) = 0;
    /// Shutdown processing
    virtual void shut_down() = 0;
    /// Start publishing monitoring information using DIM
    virtual void publish_monitor(const std::string& dns_name,
				 const std::string& name,
				 const std::string& tag);
    /// Start publishing monitoring information using DIM
    virtual void publish_monitor(long               dns_id,
				 const std::string& name,
				 const std::string& tag);
    /// Run the instance
    virtual void run();
  };

  enum consumer_types   {
    DIMLOG_CONSUMER  = 1<<0,
    DIMNODE_CONSUMER = 1<<1,
    STDIN_CONSUMER   = 1<<3
  };

}      // End namespace kafka

class DimLogConsumer;
class DimNodeConsumer;
class StdinConsumer;
#endif // ROLOGGER_LOGSERVER_H
