//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_CPUMONOSTREAM_H
#define ROMON_CPUMONOSTREAM_H 1

#include <iomanip>
#include <iostream>

// Framework include files
#include "ROMon/CPUMon.h"
namespace ROMon  {
  std::ostream& operator<<(std::ostream& os, const CPU::Stat& s);
  std::ostream& operator<<(std::ostream& os, const CPU& n);
  std::ostream& operator<<(std::ostream& os, const CPUset& n);
  std::ostream& operator<<(std::ostream& os, const CPUfarm& n);

  std::ostream& operator<<(std::ostream& os, const Process& n);
  std::ostream& operator<<(std::ostream& os, const Procset& n);
  std::ostream& operator<<(std::ostream& os, const ProcFarm& n);

  std::ostream& operator<<(std::ostream& os, const Memory& n);
  std::ostream& operator<<(std::ostream& os, const NodeStats& n);
}
#endif /* ROMON_CPUMONOSTREAM_H */

