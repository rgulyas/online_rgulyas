//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_CTRLSUBFARMDISPLAY_H
#define ROMON_CTRLSUBFARMDISPLAY_H 1

// Framework includes
#include "ROMon/ClusterDisplay.h"
#include "ROMon/TaskSupervisor.h"

// C++ include files
#include <set>

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class CtrlSubfarmDisplay ROMon.h GaudiOnline/CtrlSubfarmDisplay.h
   *
   *   Monitoring display for the LHCb storage system.
   *
   *   @author M.Frank
   */
  class CtrlSubfarmDisplay : public ClusterDisplay  {
  protected:
    /// Typedef for exclded nodes container
    typedef std::set<std::string> StrSet;

    /// reference to the node display
    MonitorDisplay*      m_nodes;

    /// Extracted cluster information for all contained nodes
    Cluster              m_cluster;

    /// Variable size data buffer
    Descriptor           m_clusterData;

    /// Excluded nodes
    StrSet               m_excluded;

    /// Initialize window
    void init(int argc, char** arv);

  public:
    /// Initializing constructor
    CtrlSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv);

    /// Standard constructor
    CtrlSubfarmDisplay(int argc, char** argv);

    /// Standard destructor
    ~CtrlSubfarmDisplay();

    /// Number of nodes in the dataset
    size_t numNodes()                   override { return m_cluster.nodes.size(); }

    /// Retrieve cluster name from cluster display
    std::string clusterName() const override;

    /// Retrieve node name from cluster display by offset
    std::string nodeName(size_t offset) override;

    /// Access to data buffer
    Descriptor& data()             override {    return m_clusterData; }

    /// Access to data buffer
    const Descriptor& data() const override {    return m_clusterData; }

    /// Allow client access to cluster data
    const Cluster& cluster() const      { return m_cluster; }

    /// Access Node display
    MonitorDisplay* nodeDisplay() const override { return m_nodes; }

    /// Show the display header information (title, time, ...)
    void showHeader();

    /// Display the node information
    void showNodes();

    /// Update all displays
    void update() override;

    /// DIM command service callback
    static void excludedHandler(void* tag, void* address, int* size);
  };
  /// Static abstract object creator.
  ClusterDisplay*  createCtrlSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv);
}      // End namespace ROMon
#endif /* ROMON_CTRLSUBFARMDISPLAY_H */

