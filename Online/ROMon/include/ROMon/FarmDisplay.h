//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//  ROMon
//-------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//=========================================================================
#ifndef ROMON_FARMDISPLAY_H
#define ROMON_FARMDISPLAY_H 1

// Framework includes
#include "ROMon/FarmDisplayBase.h"

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class FarmDisplay ROMon.h GaudiOnline/FarmDisplay.h
   *
   *   Monitoring display for the LHCb storage system.
   *
   *   @author M.Frank
   */
  class FarmDisplay : public FarmDisplayBase  {
  protected:
    typedef std::map<std::string, InternalDisplay*> SubDisplays;
    typedef std::vector<std::string> Farms;
    SubDisplays                      m_farmDisplays;
    std::unique_ptr<PartitionListener> m_listener;
    std::string                      m_match;
    int                              m_height;
    int                              m_width;
    int                              m_dense;
    /// Cursor position in sub display array
    int                              m_subDisplayHeight;

    /// Keyboard rearm action
    static int key_rearm (unsigned int fac, void* param);
    /// Keyboard action
    static int key_action(unsigned int fac, void* param);

  public:
    /// Standard constructor
    FarmDisplay(int argc, char** argv);

    /// Standard destructor
    virtual ~FarmDisplay();

    /// Get farm <partition>/<display name> from cursor position
    std::string currentCluster()  const override;

    /// Get farm display name from cursor position
    std::string currentDisplayName()  const override;

    /// Get the name of the currently selected cluster
    std::string selectedCluster() const override;

    /// Get the name of the currently selected cluster and node
    std::pair<std::string,std::string> selectedNode() const override;

    /// Number of sub-nodes in a cluster
    size_t selectedClusterSize() const;

    /// Handle keyboard interrupts
    int handleKeyboard(int key) override;

    /// Get farm display from cursor position
    InternalDisplay* currentDisplay()  const;

    /// Accessor to sub-displays of main panel
    SubDisplays& subDisplays() {  return m_farmDisplays; }

    /// Set cursor to position
    void set_cursor() override;

    /// Set cursor to position
    void set_cursor(InternalDisplay* updater) override;
    /// Interactor overload: Display callback handler
    void handle(const CPP::Event& ev) override;
    /// Connect to data resources
    void connect()  override {  InternalDisplay::connect(); }
    /// Connect to data sources
    void connect(const std::string& section, const std::vector<std::string>& farms);
    /// DIM command service callback
    void update(const void* /* data */ )       override {                                          }
    /// Update display content
    void update(const void* data, size_t len)  override { this->InternalDisplay::update(data,len); }

    /// DIM command service callback
    static void dnsDataHandler(void* tag, void* address, int* size);

  };
}      // End namespace ROMon
#endif /* ROMON_FARMDISPLAY_H */
