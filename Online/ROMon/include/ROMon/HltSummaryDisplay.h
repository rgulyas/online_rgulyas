//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ONLINE_ROMON_HLTSUMMARYDISPLAY_H
#define ONLINE_ROMON_HLTSUMMARYDISPLAY_H 1

// Framework includes
#include "ROMon/InternalDisplay.h"
#include "CPP/Interactor.h"

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class HltSummaryDisplay ROMon.h GaudiOnline/FarmDisplay.h
   *
   *   HltSummary display. Dislay shows up on CTRL-h
   *   HltSummary content is in $ROMONROOT/doc/farmMon.hlp
   *
   *   @author M.Frank
   */
  class HltSummaryDisplay : public InternalDisplay {
  public:
    /// Initializing constructor with default file
    HltSummaryDisplay(InternalDisplay* parent);
    /// Standard destructor
    ~HltSummaryDisplay() {}
    /// Interactor overload: Display callback handler
    void handle(const CPP::Event& ev) override;
    /// Update display content
    void update(const void*) override;
    /// Update display content
    void update(const void* data, size_t len)  override { this->InternalDisplay::update(data,len); }
  };

}      // End namespace ROMon
#endif /* ONLINE_ROMON_HLTSUMMARYDISPLAY_H */
