//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_ROMONDIMFSMLISTENER_H
#define ROMON_ROMONDIMFSMLISTENER_H 1

// Framework includes
#include "ROMon/RODimListener.h"

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class RODimFSMListener ROMon.h GaudiOnline/ROMon.h
   *
   *   DIM FSM status listener to collect FSM information from tasks
   *
   *   @author M.Frank
   */
  class RODimFSMListener : public RODimListener {
  public:
    /// Standard constructor
    RODimFSMListener(bool verbose = false) : RODimListener(verbose) {}
    /// Standard destructor
    virtual ~RODimFSMListener();
    /// Add handler for a given message source
    void addHandler(const std::string& node,const std::string& svc) override;
    /// Remove handler for a given message source
    void removeHandler(const std::string& node, const std::string& svc) override;
    /// DimInfoHandler overload
    static void infoHandler(void* tag, void* address, int* size);
  };
}      // End namespace ROMon
#endif /* ROMON_ROMONDIMFSMLISTENER_H */

