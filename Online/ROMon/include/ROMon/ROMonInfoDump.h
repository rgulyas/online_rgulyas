//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_ROMONINFODUMP_H
#define ROMON_ROMONINFODUMP_H 1

// Framework include files
#include "dim/dic.hxx"

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class ROMonInfoDump ROMon.h GaudiOnline/ROMon.h
   *
   *
   * @author M.Frank
   */
  class ROMonInfoDump : public DimInfoHandler {
  protected:
    DimInfo*    m_info;
  public:
    /// Standard constructor
    ROMonInfoDump(int argc, char** argv);
    /// Default destructor
    virtual ~ROMonInfoDump();
    /// Help printout in case of -h /? or wrong arguments
    static void help();
    /// DIM info callback
    void infoHandler() override;
  };

}      // End namespace ROMon
#endif /* ROMON_ROMONINFODUMP_H */

