//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMonServiceDump.cpp
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "ROMon/ROMonPrint.h"
#include "ROMon/TaskSupervisor.h"
#include "ROMon/TaskSupervisorParser.h"
#include "MBM/bmstruct.h"
#include "ROMon/CPUMon.h"
#include "ROMonDefs.h"
#include "ROMon/ROMon.h"
#include "CPP/TimeSensor.h"
#include "CPP/IocSensor.h"
#include "CPP/Event.h"
#include "RTL/rtl.h"
#include "RTL/Logger.h"
#include "dim/dis.h"
#include "dim/dic.h"

// C/C++ include files
#include <iomanip>
#include <stdexcept>
#include <iostream>
#include <cstring>
#include <sstream>
#include <cerrno>

using namespace std;
using namespace ROMon;


namespace  {

#define s_line_seperator "+=================================================================================================================\n"
  static bool s_debug = true;

  void help_run_romon_service_dump() {
    ::printf("run_romon_service_dump -arg [-arg]                                \n"
	     "  -service=<name>            Service name to be analysed.         \n"
	     "  -dns=<dim-dns-name>        DIM_DNS_NAME for service interaction.\n"
	     "  -debug                     Run with debug flag.                 \n\n"
	     "  Examples:                                                       \n\n"
	     "  run_romon_service_dump -dns=hltb04 -service=/hltb0426/ROcollect/Statistics\n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=hltb0526 -service=/HLTB05/TaskSupervisor/Status \n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=hlta0326 -service=/hlta03/ROpublish \n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=hlta0326 -service=/hlta03/ROpublish/Tasks \n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=hlta0326 -service=/hlta03/ROpublish/ROTasks \n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=* -service=/hlta03/ROpublish/ROTasks \n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=* -service=/hlta03/ROpublish/HLT2 \n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=* -service=/hlta03/ROpublish/ROTasks \n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=* -service=/hlta03/ROpublish/CPU \n"
	     "  run_romon_service_dump -dns=ecs03.lbdaq.cern.ch -node=hlta0328 -service=/hlta03/ROpublish/CPU \n"
	     );
    ::exit(0);
  }

  /// Service interface to access the published data
  /** 
   *   \author  M.Frank
   *   \version 1.0
   */
  class ServiceInterface   {
  protected:
    bool continuous = false;
    bool running = true;
    int service = 0;
    std::string node;
    /// DimInfo overload to process messages
    static void infoHandler(void* tag, void* address, int* size);
  public:
    /// Initializing constructor
    ServiceInterface(bool continuous, const std::string& service, const std::string& node="");
    /// Default destructor
    virtual ~ServiceInterface();
    virtual void operator()(const char* /* data */) const { }
    virtual int run() const  {
      while(running) ::lib_rtl_sleep(100);
      ::exit(0);
    }
    bool use_node(const char* name)  const {
      const char* dot = strchr(name,'.');
      bool use = 0 == ::strncmp(name,node.c_str(),3+(dot ? dot-name : node.length()));
      use |= node == "*";
      return use;
    }
  };

  /// Initializing constructor
  ServiceInterface::ServiceInterface(bool cont, const std::string& svc, const std::string& nod)
    : continuous(cont), running(true), node(nod)
  {
    service = ::dic_info_service(svc.c_str(),MONITORED,0,0,0,infoHandler,(long)this,0,0);
  }

  /// Default destructor
  ServiceInterface::~ServiceInterface()   {
    if ( service ) ::dic_release_service(service);
    service = 0;
  }
  /// DimInfo overload to process messages
  void ServiceInterface::infoHandler(void* tag, void* address, int* size) {
    char time_str[255];
    time_t tim = ::time(0);
    ::strftime(time_str,sizeof(time_str),"%d/%m/%Y %H:%M:%S",::localtime(&tim));
    ::lib_rtl_output(LIB_RTL_INFO," %s Received data of size:%d bytes.",time_str,*size);
    if ( address && tag && size && *size>0 ) {
      try {
	ServiceInterface* it = *(ServiceInterface**)tag;
	(*it)((const char*)address);
	if ( !it->continuous ) {
	  it->running = false;
	  ::dic_release_service(it->service);
	  it->service = 0;
	}
      }
      catch(const exception& e) {
	cout << "Exception in DIM callback processing:" << e.what() << endl;
      }
      catch(...)  {
	cout << "UNKNOWN exception in DIM callback processing." << endl;
      }
    }
  }

  template<typename T> class analyse_service  : public ServiceInterface {
  public:
    analyse_service(bool continuous, const string& nod, const string& service)
      :  ServiceInterface(continuous, service,nod){}
    virtual void operator()(const char* ptr)  const override;
  };

  /// Analyse services of type: "/ROcollect/Statistics"
  class rocollect_statistics;
  template <> void analyse_service<rocollect_statistics>::operator()(const char* p)  const {
    NodeStats* ns = (NodeStats*)p;
    printer(*ns->procs())();
  }

  /// Analyse services of type: "/ROpublish/Tasks"
  class ropublish_tasks;
  template <> void analyse_service<ropublish_tasks>::operator()(const char* p)  const {
    typedef ProcFarm::Nodes    _N;
    ProcFarm*        pf    = (ProcFarm*)p;
    ProcFarm::Nodes& nodes = pf->nodes;
    for(_N::const_iterator ip=nodes.begin(); ip!=nodes.end(); ip=nodes.next(ip))  {
      const Procset& ps = (*ip);
      if (use_node(ps.name)) {
	printer(ps)();
	if ( node != "*" ) goto Done;
      }
    }
    if ( node != "*" ) {
      ::printf(s_line_seperator);
      ::printf("|\n|\n|   Unknown Node '%s'. "
	       "No corresponding information found.\n|\n|\n",node.c_str());
    }
  Done:
    ::printf(s_line_seperator);
  }

  /// Analyse services of type: "/ROpublish/HLTx"
  class ropublish_hltstats;
  template <> void analyse_service<ropublish_hltstats>::operator()(const char* p)  const {
    typedef DeferredHLTSubfarmStats _S;
    const _S& hs = *(_S*)p;
    printer(hs)();
  }

  /// Analyse services of type: "/ROpublish"
  class ropublish;
  template <> void analyse_service<ropublish>::operator()(const char* p)  const {
    const Nodeset& ns = *(Nodeset*)p;
    ::printf(s_line_seperator);
    for(Nodeset::Nodes::const_iterator i=ns.nodes.begin(); i!=ns.nodes.end(); i=ns.nodes.next(i)) {
      const Node& n = (*i);
      if (use_node(n.name)) {
	printer(n)();
	if ( node != "*" ) goto Done;
      }
    }
    if ( node != "*" )  {
      ::printf("|\n|\n|   Unknown Node '%s'. No corresponding information found.\n|\n|\n",node.c_str());
    }
  Done:
    ::printf(s_line_seperator);
  }

  /// Analyse services of type: "/ROpublish/CPU"
  class ropublish_cpu;
  template <> void analyse_service<ropublish_cpu>::operator()(const char* ptr)  const {
    const CPUfarm& c = *(CPUfarm*)ptr;
    const CPUfarm::Nodes& n = c.nodes;
    ::printf(s_line_seperator);
    for(CPUfarm::Nodes::const_iterator ic=n.begin(); ic != n.end(); ic=n.next(ic))  {
      const CPUset& s = (*ic);
      if (use_node(s.name)) {
	printer(s)();
	if ( node != "*" ) goto Done;
      }
    }
    if ( node != "*" )  {
      ::printf("|\n|\n|   Unknown Node '%s'. No corresponding information found.\n|\n|\n",node.c_str());
    }
  Done:
    ::printf(s_line_seperator);
  }

  /// Analyse services of type: "/TaskSuperviser/Status"
  class tasksupervisor_status;
  template <> void analyse_service<tasksupervisor_status>::operator()(const char* ptr)  const {
    XML::TaskSupervisorParser ts;
    char txt[255], time_str[255], line[255];
    time_t tim = ::time(0);
    cout << "Buffer:" << (char*)ptr << endl;
    if ( ts.parseBuffer(node, ptr, ::strlen(ptr)+1) ) {
      float GB = 1.0/float(1024*1024*1024);
      int cnt=0;
      
      char* p = new char[sizeof(Cluster)+sizeof(int)];
      *(int*)(p+sizeof(int)) = sizeof(Cluster)+sizeof(int);
      Cluster* c = ::new(p+sizeof(int)) Cluster();
      c->nodes.clear();
      ts.getClusterNodes(*c);
      const Cluster::Nodes& ns = c->nodes;

      ::printf("+==============================================================================================\n");
      ::printf("|  Cluster:%s  status:%12s  last update:%s\n",c->name.c_str(),c->status.c_str(),c->time.c_str());

      for(Cluster::Nodes::const_iterator i=ns.begin(); i!=ns.end();++i) {
	const Cluster::Node& n = (*i).second;
	if ( n.name == node || node.empty() || node == "*" )  {
	  Cluster::Node::Tasks::const_iterator j;
	  float blk_size = float(n.blk_size != 0 ? n.blk_size : 4096)*GB;
	  bool  ctrl_pc = (0 == ::strcasecmp(n.name.c_str(),c->name.c_str()));
	  bool  dead = n.status == "DEAD";

	  ::snprintf(line,sizeof(line),
		     "+-- %s ----------------------------------------------------------------------------------",
		     n.name.c_str());
	  ::strftime(time_str,sizeof(time_str),"%H:%M:%S",::localtime(&tim));
	  ::printf("%s\n",line);
	  ::printf("| Task Control display for node:%s  [%s]\n",n.name.c_str(),time_str);
	  ::printf("| Node:%s  last update:%s\n|\n",n.name.c_str(),n.time.c_str());
	  ::printf("| %-12s %8s    Tasks       Connections  %6s %6s %6s %6s %3s %3s\n",
		   "","","RSS","Stack","Data","VSize","CPU","MEM");
	  ::printf("| %-12s %8s found/missing found/missing %6s %6s %6s %6s %3s %3s %s\n",
		   "Node","Status","[MB]","[MB]","[MB]","[MB]","[%]","[%]","Boot time");
	  ::printf("| %-12s %8s %5ld/%-7ld %5ld/%-7ld %6ld %6ld %6ld %6ld %3.0f %3.0f %s\n|\n",
		   n.name.c_str(),n.status.c_str(),
		   long(n.taskCount),long(n.missTaskCount),long(n.connCount),long(n.missConnCount),
		   long(n.rss/1024),long(n.stack/1024),long(n.data/1024),long(n.vsize/1024),
		   n.perc_cpu, n.perc_mem, n.boot.c_str());
	  if ( !ctrl_pc && !dead )  {
	    ::printf("| Local-Disks       Disk-Space [GB]\n");
	    ::printf("| %-6s %-6s     %6s %6s     %s\n", "Total","Good","Total","Free","Summary");
	    ::printf("|   %-4d   %-4d     %6.0f %6.0f     %s\n",
		     n.dev_num, n.dev_good, 
		     float(n.blk_total)*blk_size, float(n.blk_availible)*blk_size,
		     n.dev_num==n.dev_good ? "Disk(s) OK" : "Disk(s) broken");
	  }
	  if ( dead ) {
	    ::printf("|\n|                        -------------------------------------\n");
	    ::printf(   "|                        Node is DEAD. Severe problem present.\n");
	    ::printf(   "|                        -------------------------------------\n|\n");
	  }
	  else {
	    typedef map<string,bool> _O;
	    _O ord;
	    _O::iterator k;
	    ::printf("%s\n",line);
	    if ( n.taskCount > 0 ) {
	      ::printf("|  Task Summary:\n");
	      for(j=n.tasks.begin(); j != n.tasks.end(); ++j)  {
		ord[(*j).first]=(*j).second;
	      }
	      for(k=ord.begin(),cnt=0; k !=ord.end(); ++k,++cnt) {
		bool ok = (*k).second;
		::printf("|   [%03d]  %-32s %-10s\n",
			 cnt,(*k).first.c_str(),ok ? "OK" : "MISSING");
	      }
	      ord.clear();
	    }
	    else {
	      ::printf("  No tasks found\n");
	    }
	    if ( n.projects.size() > 0 ) {
	      cnt=0;
	      ::printf("%-15s %-16s %-14s %-14s %-14s %-14s %-14s\n",
		       "PVSS Summary:","Project name","Event Mgr","Data Mgr","Dist Mgr","FSM Server","Dev Handler");
	      for(Cluster::Projects::const_iterator q=n.projects.begin(); q != n.projects.end(); ++q,++cnt)  {
		const Cluster::PVSSProject& p = *q;
		::printf("   [%03d]        %-16s %-14s %-14s %-14s %-14s %-14s\n",cnt,p.name.c_str(),
			 p.state(p.eventMgr), p.state(p.dataMgr), p.state(p.distMgr),
			 p.state(p.fsmSrv), p.state(p.devHdlr));
	      }
	      ::printf("|\n");
	    }
	    if ( n.connCount > 0 ) {
	      ::printf("|  Connection Summary:\n");
	      for(j=n.conns.begin(); j != n.conns.end(); ++j) ord[(*j).first]=(*j).second;
	      for(k=ord.begin(),cnt=0; k !=ord.end(); ++k,++cnt) {
		bool ok = (*k).second;
		::printf("|   [%03d]  %-32s %-10s\n",cnt,(*k).first.c_str(),ok ? "OK" : "MISSING");
	      }
	      ord.clear();
	    }
	    else {
	      ::printf("|  No connections found\n");
	    }
	    ::printf("%s\n",line);
	    if ( n.missTaskCount == 0 && n.missConnCount == 0 )
	      ::printf("|  Node looks fine. No missing tasks/network connections found.\n");
	    else if ( n.missTaskCount == 0 )
	      ::printf("|  No missing tasks found, but missing network connections .....\n");
	    else if ( n.missConnCount == 0 )
	      ::printf("|  No missing network connections found, but missing tasks .....\n");
	  }
	}
      }
      delete [] p;
    }
    else {
      ::printf("   Unknown Node. No data found.\n");
      ::strftime(txt,sizeof(txt),"           %H:%M:%S",::localtime(&tim));
      ::printf("%s\n",txt);
    }
    ::printf("+==============================================================================================\n");
  }

  /// Analyse services of type: "/TaskSuperviser/Summary"
  class tasksupervisor_summary;
  template <> void analyse_service<tasksupervisor_summary>::operator()(const char*)  const {
  }

  int analyse(bool continuous, const string& dns, const string& node, const string& service)   {
    size_t idx;
    if ( !dns.empty() )   {
      ::lib_rtl_output(LIB_RTL_INFO,"Set DIC dns node to:%s",dns.c_str());
      ::dic_set_dns_node(dns.c_str());
    }
    if ( (idx=service.find("/ROpublish/Tasks")) != string::npos )
      return analyse_service<ropublish_tasks>(continuous, node, service).run();
    else if ( (idx=service.find("/ROpublish/ROTasks")) != string::npos )
      return analyse_service<ropublish_tasks>(continuous, node, service).run();
    else if ( (idx=service.find("/ROpublish/CPU")) != string::npos )
      return analyse_service<ropublish_cpu>(continuous, node, service).run();  // TODO
    else if ( (idx=service.find("/ROpublish/HLT")) != string::npos )
      return analyse_service<ropublish_hltstats>(continuous, node, service).run();
    else if ( (idx=service.find("/ROpublish")) != string::npos )
      return analyse_service<ropublish>(continuous, node, service).run();
    else if ( (idx=service.find("/ROcollect/Statistics")) != string::npos )
      return analyse_service<rocollect_statistics>(continuous, node, service).run();
    else if ( (idx=service.find("/TaskSupervisor/Status")) != string::npos )
      return analyse_service<tasksupervisor_status>(continuous, node, service).run();
    else if ( (idx=service.find("/TaskSupervisor/Summary")) != string::npos )
      return analyse_service<tasksupervisor_summary>(continuous, node, service).run(); // TODO

    ::lib_rtl_output(LIB_RTL_ERROR,
		     "Services of type: %s are not understood by the dumper!",
		     service.c_str());
    return 0;
  }
}

extern "C" int run_romon_service_dump(int argc, char** argv) {
  string dns, node, service;
  const char* dns_env = ::lib_rtl_getenv("DIM_DNS_NODE");
  RTL::CLI cli(argc,argv,help_run_romon_service_dump);
  bool continuous = cli.getopt("continuous",1) != 0;
  cli.getopt("service",3,service);
  cli.getopt("node",3,node);
  cli.getopt("dns",3,dns);
  if ( dns_env && dns.empty() ) dns = dns_env;
  if ( service.empty() ) help_run_romon_service_dump();
  RTL::Logger::install_rtl_printer(LIB_RTL_INFO);

  if ( s_debug ) {
    cout << "Dns:" << dns << " Node:" << node << " Service:" << service << endl;  
  }
  return analyse(continuous, dns, node, service);
}


