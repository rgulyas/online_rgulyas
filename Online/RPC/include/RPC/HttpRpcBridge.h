//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef HTTP_HTTP_RPCBRIDGE_H
#define HTTP_HTTP_RPCBRIDGE_H

// Framework include files
#include <CPP/Callback.h>
#include <HTTP/HttpRequest.h>

// C/C++ include files
#include <string>

/// Namespace for the http based bridge implementation
namespace rpc  {

  ///  HTTP Bridge class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class HttpRpcBridge {
  public:
    /// Handler class specification
    class Handler;
    /// Definition of the Callback signature
    typedef void (*HttpCall)(const http::Request* request);
    /// Callback type declarations
    typedef dd4hep::Callback   Callback;

    /// Reference to the implementation interface
    Handler*        implementation = 0;
    /// Bridge bind address
    std::string     host;
    /// Bridge bind port
    std::string     port;

  public:
    /// Initializing constructor
    HttpRpcBridge(const std::string& address, int port);
    /// Default destructor
    virtual ~HttpRpcBridge();
    /// Modify debug flag
    int setDebug(int value);
    /// Define the mount point
    void setMount(const std::string& mnt);
    /// Add server redirection request to other node
    void addServerRedirection(const std::string& req, const std::string& server, const std::string& port);
    /// Add URL redirection request to other node and new URL
    void addUrlRedirection(const std::string& url, const std::string& server, const std::string& port, const std::string& new_url);

    /// Start the http service
    void start(bool detached = false, int num_additional_threads = 0);

    /// Run the http service (only use in conjunction with start(false); ) 
    /** Use only to execute the callback loop in a seperate thread.
     *  Run the bridge's io_service loop. If specified additional workers may be used
     */
    void run(int num_additional_threads = 0);
    /// Stop the http service
    void stop();

    /// Bind default callbacks to all successfully handled function requests.
    void onHandled(const Callback& call);
    /// Bind default callbacks to all unhandled functions. Call signature must be HttpCall.
    void onUnhandled(const Callback& call);
    /// Bind default callbacks to processing errors. Call signature must be HttpCall.
    void onError(const Callback& call);
  };
}       /* End  namespace http      */
#endif  /* HTTP_HTTP_RPCBRIDGE_H    */
