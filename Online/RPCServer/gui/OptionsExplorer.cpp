//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/OptionsExplorer.h"
#include "RPC/GUI/GuiException.h"
#include "RPC/GUI/GuiCommand.h"
#include "RPC/GUI/GuiMsg.h"

#include "RPC/DataflowRPC.h"
#include "RPC/DimClient.h"
#include "HTTP/HttpClient.h"

#include "CPP/TimeSensor.h"
#include "CPP/IocSensor.h"
#include "CPP/Event.h"

/// ROOT include files
#include "TSystem.h"
#include "TGLabel.h"
#include "TGButton.h"
#include "TGMsgBox.h"
#include "TGTextEntry.h"
#include "TGComboBox.h"
#include "TApplication.h"
#include "TGTableLayout.h"
#include "TGFileDialog.h"

ClassImp(xmlrpc::OptionsExplorer)

using namespace std;
using namespace xmlrpc;

namespace {
#if 0
  string dns_service(const string& d)   {
    string dd = d;
    for(size_t i=0; i<dd.size(); ++i) dd[i] = ::tolower(dd[i]);
    return "/RO/"+dd+"/ROpublish";
  }
#endif
  string _selectedText(TGComboBox* c)  {
    TGLBEntry* selected = c->GetSelectedEntry();
    return selected ? selected->GetTitle() : "";
  }

  void _clr(TGComboBox* c)  {
    TGTextEntry* e = c->GetTextEntry();
    if ( e ) e->SetText("");
  }
  string str_upper(string d)   {
    for(size_t i=0; i<d.size(); ++i) d[i] = ::toupper(d[i]);
    return d;
  }

  const char *Option_filetypes[] = { "All files",     "*",
				     "Options files", "*.opts",
				     "Text files",    "*.[tT][xX][tT]",
				     0,               0 };
}

/// Standard initializing constructor
OptionsExplorer::OptionsExplorer(TGFrame* pParent, CPP::Interactor* g)
  : TGCompositeFrame(pParent, 100, 100, kVerticalFrame), gui(g)
{
  TGCompositeFrame* envelope   = new TGCompositeFrame(this, 60, 20, kVerticalFrame);

  gClient->GetColorByName("#c0c0c0", disabled);
  gClient->GetColorByName("white", enabled);

  group   = new TGGroupFrame(envelope, "Select client from DNS inventory");
  group->SetLayoutManager(new TGTableLayout(group, 8, 6));

  dns.label = new TGLabel(group,      "Enter DNS node",     DNS_LABEL);
  dns.input = new TGComboBox(group,   DNS_INPUT);
  //dns.clear = new TGTextButton(group, "Clear",              DNS_CLEAR);
  dns.load  = new TGTextButton(group, "Load Nodes",         DNS_LOAD);
  dns.fill  = new TGLabel(group,      "",                   DNS_FILL);
  dns.input->Connect("Selected(Int_t)",    "xmlrpc::OptionsExplorer", this, "dnsChanged(Int_t)");
  //dns.clear->Connect("Clicked()",          "xmlrpc::OptionsExplorer", this, "clearDnsField()");
  dns.load->Connect("Clicked()",           "xmlrpc::OptionsExplorer", this, "loadNodes()");
  dns.input->SetMaxWidth(100);
  group->AddFrame(dns.label, new TGTableLayoutHints(0, 1, 0, 1, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(dns.input, new TGTableLayoutHints(1, 2, 0, 1, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  //group->AddFrame(dns.clear, new TGTableLayoutHints(2, 3, 0, 1, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(dns.load,  new TGTableLayoutHints(3, 4, 0, 1, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(dns.fill,  new TGTableLayoutHints(4, 5, 0, 1, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  node.label = new TGLabel(group,      "Select node",        NODE_LABEL);
  node.input = new TGComboBox(group,   NODE_INPUT);
  node.clear = new TGTextButton(group, "Clear",              NODE_CLEAR);
  node.load  = new TGTextButton(group, "Load Clients",       NODE_LOAD);
  node.fill  = new TGLabel(group,      "",                   NODE_FILL);
  node.input->Connect("Selected(Int_t)",  "xmlrpc::OptionsExplorer", this, "nodeChanged(Int_t)");
  node.clear->Connect("Clicked()",        "xmlrpc::OptionsExplorer", this, "clearNodeField()");
  node.load->Connect("Clicked()",         "xmlrpc::OptionsExplorer", this, "loadClients()");
  node.input->SetMaxWidth(100);
  group->AddFrame(node.label, new TGTableLayoutHints(0, 1, 1, 2, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(node.input, new TGTableLayoutHints(1, 2, 1, 2, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  group->AddFrame(node.clear, new TGTableLayoutHints(2, 3, 1, 2, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(node.load,  new TGTableLayoutHints(3, 4, 1, 2, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(node.fill,  new TGTableLayoutHints(4, 5, 1, 2, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  part.label = new TGLabel(group,      "Enter partition",    PART_LABEL);
  part.input = new TGComboBox(group,  PART_INPUT);
  part.clear = new TGTextButton(group, "Clear",              PART_CLEAR);
  part.load  = new TGTextButton(group, "Load Clients",       PART_LOAD);
  part.fill  = new TGLabel(group,      "",                   PART_FILL);
  part.input->Connect("Selected(Int_t)",  "xmlrpc::OptionsExplorer", this, "partitionChanged(Int_t)");
  part.clear->Connect("Clicked()",        "xmlrpc::OptionsExplorer", this, "clearPartField()");
  part.load->Connect("Clicked()",         "xmlrpc::OptionsExplorer", this, "loadClients()");
  part.input->SetMaxWidth(100);
  group->AddFrame(part.label, new TGTableLayoutHints(0, 1, 2, 3, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(part.input, new TGTableLayoutHints(1, 2, 2, 3, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  group->AddFrame(part.clear, new TGTableLayoutHints(2, 3, 2, 3, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(part.load,  new TGTableLayoutHints(3, 4, 2, 3, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(part.fill,  new TGTableLayoutHints(4, 5, 2, 3, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  part.input->AddEntry("----", 255);
  part.input->AddEntry("LHCb", 0);
  part.input->AddEntry("LHCb2",1);
  part.input->AddEntry("LHCbA",2);
  part.input->AddEntry("VELO", 3);
  part.input->AddEntry("VELOA",4);
  part.input->AddEntry("VELOC",5);
  part.input->AddEntry("IT",   6);
  part.input->AddEntry("TT",   7);
  part.input->AddEntry("RICH1",8);
  part.input->AddEntry("RICH2",9);
  part.input->AddEntry("HCAL", 10);
  part.input->AddEntry("ECAL", 11);
  part.input->AddEntry("PRS",  12);
  part.input->AddEntry("CALO", 13);
  part.input->AddEntry("MUONA",14);
  part.input->AddEntry("MUONC",15);
  part.input->AddEntry("MUON", 16);

  rpcClients.isValid = true;
  rpcClients.label = new TGLabel(group,         "Select Options Client (Direct Xml-RPC data exchange)", CLIENTS_LABEL);
  rpcClients.input = new TGComboBox(group,      CLIENTS_INPUT);
  rpcClients.clear = new TGTextButton(group,    "Clear",                 CLIENTS_CLEAR);
  rpcClients.load  = new TGTextButton(group,    "Load options",          CLIENTS_LOAD);
  rpcClients.fill  = new TGLabel(group,         "",                      CLIENTS_FILL);
  rpcClients.input->Connect("Selected(Int_t)",  "xmlrpc::OptionsExplorer", this, "loadRpcClientOptions()");
  rpcClients.load->Connect("Clicked()",         "xmlrpc::OptionsExplorer", this, "loadRpcClientOptions()");
  rpcClients.input->SetMaxWidth(100);
  group->AddFrame(rpcClients.label, new TGTableLayoutHints(0, 1, 3, 4, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(rpcClients.input, new TGTableLayoutHints(1, 2, 3, 4, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  group->AddFrame(rpcClients.clear, new TGTableLayoutHints(2, 3, 3, 4, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(rpcClients.load,  new TGTableLayoutHints(3, 4, 3, 4, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(rpcClients.fill,  new TGTableLayoutHints(4, 5, 3, 4, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  bridgeClients.isValid = true;
  bridgeClients.label = new TGLabel(group,      "Select Options Client (Xml-RPC data exchange using Bridge)", BRIDGECLIENTS_LABEL);
  bridgeClients.input = new TGComboBox(group,   BRIDGECLIENTS_INPUT);
  bridgeClients.clear = new TGTextButton(group, "Clear",                 BRIDGECLIENTS_CLEAR);
  bridgeClients.load  = new TGTextButton(group, "Load options",          BRIDGECLIENTS_LOAD);
  bridgeClients.fill  = new TGLabel(group,      "",                      BRIDGECLIENTS_FILL);
  bridgeClients.load->Connect("Clicked()",      "xmlrpc::OptionsExplorer", this, "loadBridgeClientOptions()");
  bridgeClients.input->SetMaxWidth(100);
  group->AddFrame(bridgeClients.label,  new TGTableLayoutHints(0, 1, 4, 5, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(bridgeClients.input,  new TGTableLayoutHints(1, 2, 4, 5, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  group->AddFrame(bridgeClients.clear,  new TGTableLayoutHints(2, 3, 4, 5, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(bridgeClients.load,   new TGTableLayoutHints(3, 4, 4, 5, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(bridgeClients.fill,   new TGTableLayoutHints(4, 5, 4, 5, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  dimClients.isValid = true;
  dimClients.label = new TGLabel(group,         "Select Options Client (WinCCOA data exchange over DIM)", DIMCLIENTS_LABEL);
  dimClients.input = new TGComboBox(group,      DIMCLIENTS_INPUT);
  dimClients.clear = new TGTextButton(group,    "Clear",                 DIMCLIENTS_CLEAR);
  dimClients.load  = new TGTextButton(group,    "Load options",          DIMCLIENTS_LOAD);
  dimClients.fill  = new TGLabel(group,         "",                      DIMCLIENTS_FILL);
  dimClients.load->Connect("Clicked()",         "xmlrpc::OptionsExplorer", this, "loadDimClientOptions()");
  dimClients.input->SetMaxWidth(100);
  group->AddFrame(dimClients.label,  new TGTableLayoutHints(0, 1, 5, 6, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(dimClients.input,  new TGTableLayoutHints(1, 2, 5, 6, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  group->AddFrame(dimClients.clear,  new TGTableLayoutHints(2, 3, 5, 6, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(dimClients.load,   new TGTableLayoutHints(3, 4, 5, 6, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(dimClients.fill,   new TGTableLayoutHints(4, 5, 5, 6, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  victimOpts.isValid = true;
  victimOpts.label = new TGLabel(group,         "Select Options file for Victim Node", VICTIMOPTS_LABEL);
  victimOpts.input = new TGComboBox(group,      VICTIMOPTS_INPUT);
  victimOpts.clear = new TGTextButton(group,    "Open",                  VICTIMOPTS_OPEN);
  victimOpts.load  = new TGTextButton(group,    "Load options",          VICTIMOPTS_LOAD);
  victimOpts.fill  = new TGLabel(group,         "",                      VICTIMOPTS_FILL);
  victimOpts.clear->Connect("Clicked()",        "xmlrpc::OptionsExplorer", this, "openVictimOptions()");
  victimOpts.load->Connect("Clicked()",         "xmlrpc::OptionsExplorer", this, "loadVictimOptions()");
  victimOpts.input->SetMaxWidth(100);
  group->AddFrame(victimOpts.label,  new TGTableLayoutHints(0, 1, 6, 7, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(victimOpts.input,  new TGTableLayoutHints(1, 2, 6, 7, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  group->AddFrame(victimOpts.clear,  new TGTableLayoutHints(2, 3, 6, 7, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(victimOpts.load,   new TGTableLayoutHints(3, 4, 6, 7, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(victimOpts.fill,   new TGTableLayoutHints(4, 5, 6, 7, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  envelope->AddFrame(group,  new TGLayoutHints(kLHintsRight|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY, 1,1,1,1));
  //group->AddFrame(victim, new TGLayoutHints(kLHintsRight|kLHintsCenterY|kLHintsExpandX, 1,1,1,1));
  AddFrame(envelope, new TGLayoutHints(kLHintsLeft|kLHintsTop|kLHintsExpandX, 1, 1, 1, 1));
  clearClientSelector();
  dns.input->Resize(200,22);
  node.input->Resize(200,22);
  part.input->Resize(200,22);

  if ( rpcClients.isValid    ) rpcClients.input->Resize(300,22);
  if ( dimClients.isValid    ) dimClients.input->Resize(300,22);
  if ( bridgeClients.isValid ) bridgeClients.input->Resize(300,22);
  if ( victimOpts.isValid    ) victimOpts.input->Resize(300,22);

  auto cl = rpc::client<http::HttpClient>();
  // Use bridge to RPC server:
  (*cl)->userRequestHeaders.emplace_back("Dim-DNS","ecs03");
  (*cl)->userRequestHeaders.emplace_back("Dim-Server","/RPCDomainInfo");
  (*cl)->userRequestHeaders.emplace_back("RPC-Port","2601");
  (*cl)->open("ecs03.lbdaq.cern.ch", 2600);
  domain_client.handler = std::move(cl);
  IocSensor::instance().send(this,DNS_LOAD);
}

/// Default destructor
OptionsExplorer::~OptionsExplorer()   {
}

/// Access the list of selected nodes
set<string> OptionsExplorer::nodeList()  const    {
  set<string> items;
  TGListBox*  lb = node.input->GetListBox();
  for(Int_t i=0, n=lb->GetNumberOfEntries(); i<n; ++i)
    items.insert(((TGTextLBEntry*)lb->GetEntry(i))->GetText()->GetString());
  return items;
}

/// Access the list of selected tasks
set<string> OptionsExplorer::taskList()  const    {
  set<string> items;
  TGListBox*  lb = rpcClients.input->GetListBox();
  for(Int_t i=0, n=lb->GetNumberOfEntries(); i<n; ++i)
    items.insert(((TGTextLBEntry*)lb->GetEntry(i))->GetText()->GetString());
  return items;
}

/// Deferred execution using wtc
void OptionsExplorer::exec_call(Int_t cmd)   {
  printf("Running exec: %d\n",cmd);
  CPP::IocSensor::instance().send(this,cmd);
}

/// Interactor interrupt handler callback
void OptionsExplorer::handle(const CPP::Event& ev)   {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case DNS_LOAD:
      loadDns();
      break;
    case NODE_LOAD:
      loadNodes();
      break;
    case CLIENTS_LOAD:
      loadClients();
      break;
    default:
      break;
    }
    break;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    case DNS_LOAD:
      IocSensor::instance().send(this,cmd);
      break;
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}

/// Load the DNS domains into the widget
void OptionsExplorer::loadDns()   {
  try {
    set<string> domains = domain_client.domains();
    Int_t cnt = 0;
    dns.input->RemoveAll();
    for(auto in=domains.begin(); in!=domains.end(); ++in, ++cnt)
      dns.input->AddEntry((*in).c_str(), cnt);
    dns.input->SetEnabled(kTRUE);
    if ( !domains.empty() )
      dns.input->Select(0, kFALSE);
  }
  catch(const std::exception& e)    {
    GuiException(kMBOk,kMBIconAsterisk,"Options explorer exception",e.what()).send(gui);
  }
}

/// Callback when the text of the dns node changed
void OptionsExplorer::dnsChanged(Int_t selected_id)  {  
  string dns_node = _selectedText(dns.input);
  GuiMsg("OptionsExplorer::dnsChanged: ID=%d [%s]",selected_id,dns_node.c_str()).send(gui);
  if ( !dns_node.empty() )  {
    CPP::IocSensor::instance().send(this,NODE_LOAD);
  }
}

/// Callback to clear text field
void OptionsExplorer::clearDnsField()   {
  _clr(dns.input);
}

/// Button callback to load nodes of a domain
void OptionsExplorer::loadNodes()  {
  string dns_node = _selectedText(dns.input);
  clearClientSelector();
  if ( !dns_node.empty() )   {
    try  {
      set<string> nodes  = domain_client.nodes(dns_node);
      Int_t cnt = 0, sel = node.input->GetSelected();
      node.input->RemoveAll();
      GuiMsg("loadNodes: Got %ld nodes",nodes.size()).send(gui);
      for(auto in=nodes.begin(); in != nodes.end(); ++in, ++cnt)  {
	node.input->AddEntry((*in).c_str(), cnt);
      }
      if ( sel >= 0 ) node.input->Select(sel);
      else if ( !nodes.empty() ) node.input->Select(0);
    }
    catch(const std::exception& e)    {
      GuiException(kMBOk,kMBIconAsterisk,
		   "Options explorer exception",
		   e.what()).send(gui);
    }
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,
	       "Options explorer exception",
	       "No valid DNS node selected!").send(gui);
}

/// Callback to clear text field
void OptionsExplorer::clearNodeField()   {
  node.input->RemoveAll();
  if ( rpcClients.isValid    ) rpcClients.input->RemoveAll();
  if ( dimClients.isValid    ) dimClients.input->RemoveAll();
  if ( bridgeClients.isValid ) bridgeClients.input->RemoveAll();
}

/// Callback when the text of the dns node changed
void OptionsExplorer::nodeChanged(Int_t /* selected_id */)  {
  clearClientSelector();
  CPP::IocSensor::instance().send(this,CLIENTS_LOAD);
}

/// Callback to clear text field
void OptionsExplorer::clearPartField()   {
  part.input->Select(255, kFALSE);
  if ( rpcClients.isValid    ) rpcClients.input->RemoveAll();
  if ( dimClients.isValid    ) dimClients.input->RemoveAll();
  if ( bridgeClients.isValid ) bridgeClients.input->RemoveAll();
}

/// Load all DNS clients of a DNS (callback of "Load")
void OptionsExplorer::loadClients()    {
  string         d = _selectedText(dns.input);
  string         n = _selectedText(node.input);
  string         p = _selectedText(part.input);
  if ( d.empty() )  {
    /// Error
    return;
  }
  
  if ( n.empty() )  {
    /// Error
    //return;
  }

  if ( p.empty() || p == "----" )   {
    p = "(*)";
  }
  string match = p+"_"+str_upper(n)+"_(*)";
  try  {
    GuiMsg("loadClients: Match: %s [%s]",match.c_str(),n.empty() ? "NOT used" : "used").send(gui);
    set<string> tasks = n.empty() ? domain_client.tasksByRegex(d, match) : domain_client.tasks(d, n);

    Int_t cnt = 0, sel = rpcClients.input->GetSelected();
    if ( rpcClients.isValid    ) rpcClients.input->RemoveAll();
    if ( dimClients.isValid    ) dimClients.input->RemoveAll();
    if ( bridgeClients.isValid ) bridgeClients.input->RemoveAll();
    GuiMsg("loadClients: Got: %ld tasks",tasks.size()).send(gui);
    for(auto in=tasks.begin(); in!=tasks.end(); ++in, ++cnt)   {
      if ( rpcClients.isValid    ) rpcClients.input->AddEntry((*in).c_str(), cnt);
      if ( dimClients.isValid    ) dimClients.input->AddEntry((*in).c_str(), cnt);
      if ( bridgeClients.isValid ) bridgeClients.input->AddEntry((*in).c_str(), cnt);
      //GuiMsg("Process: %3d: %s\n", cnt, (*in).c_str()).send(gui);
    }
    if ( rpcClients.isValid    )  {
      rpcClients.input->Select(sel>0 ? sel : 0, kFALSE);
      rpcClients.input->SetEnabled(kTRUE);
      rpcClients.load->SetEnabled(kTRUE);
    }
    if ( bridgeClients.isValid )  {
      bridgeClients.input->Select(sel>0 ? sel : 0, kFALSE);
      bridgeClients.input->SetEnabled(kTRUE);
      bridgeClients.load->SetEnabled(kTRUE);
    }
    if ( dimClients.isValid )  {
      dimClients.input->Select(sel>0 ? sel : 0, kFALSE);
      dimClients.input->SetEnabled(kTRUE);
      dimClients.load->SetEnabled(kTRUE);
    }
  }
  catch(const std::exception& e)    {
    GuiException(kMBOk,kMBIconAsterisk,
		 "Options explorer exception",
		 e.what()).send(gui);
  }
}

/// Callback when the text of the dns node changed
void OptionsExplorer::partitionChanged(Int_t /* selected_id */)  {
  string pname = _selectedText(part.input);
  GuiMsg("partitionChanged: Widget %s", pname.c_str()).send(gui);
  clearClientSelector();
  CPP::IocSensor::instance().send(this,CLIENTS_LOAD);
}

/// High level change of the selector. Erase all content
void OptionsExplorer::clearClientSelector()    {
  if ( rpcClients.isValid    )  {
    rpcClients.input->RemoveAll();
    _clr(rpcClients.input);
  }
  if ( dimClients.isValid    )  {
    dimClients.input->RemoveAll();
    _clr(dimClients.input);
  }
  if ( bridgeClients.isValid )  {
    bridgeClients.input->RemoveAll();
    _clr(bridgeClients.input);
  }
}

/// Load the RPC client options on demand
void OptionsExplorer::loadRpcClientOptions()    {
  if ( gui )  {
    string  d = _selectedText(dns.input);
    string  n = _selectedText(node.input);
    string  c = _selectedText(rpcClients.input);
    GuiMsg("loadRpcClientOptions: %s -- %s -- %s",d.c_str(), n.c_str(), c.c_str()).send(gui);
    GuiCommand* cmd = new GuiCommand(d,n,c);
    CPP::IocSensor::instance().send(gui,XMLRPCGUI_SHOW_RPCCLIENT,cmd);
  }
}

/// Load the DIM client options on demand
void OptionsExplorer::loadDimClientOptions()    {
  if ( gui )  {
    string  d = _selectedText(dns.input);
    string  n = _selectedText(node.input);
    string  c = _selectedText(dimClients.input);
    GuiMsg("loadDimClientOptions: %s -- %s -- %s",d.c_str(), n.c_str(), c.c_str()).send(gui);
    GuiCommand* cmd = new GuiCommand(d,n,c);
    CPP::IocSensor::instance().send(gui,XMLRPCGUI_SHOW_DIMCLIENT,cmd);
  }
}

/// Load the bridge client options on demand
void OptionsExplorer::loadBridgeClientOptions()    {
  if ( gui )  {
    string  d = _selectedText(dns.input);
    string  n = _selectedText(node.input);
    string  c = _selectedText(bridgeClients.input);
    GuiMsg("loadBridgeClientOptions: %s -- %s -- %s",d.c_str(), n.c_str(), c.c_str()).send(gui);
    GuiCommand* cmd = new GuiCommand(d,n,c);
    CPP::IocSensor::instance().send(gui,XMLRPCGUI_SHOW_BRIDGERPCCLIENT,cmd);
  }
}

/// Load the victim node client options on demand
void OptionsExplorer::openVictimOptions()    {
  if ( gui )  {
    TGFileInfo fi;
    static TString dir(".");
    TGWindow* win = dynamic_cast<TGWindow*>(gui);

    fi.fFileTypes = Option_filetypes;
    fi.fIniDir    = StrDup(dir);
    printf("fIniDir = %s\n", fi.fIniDir);
    new TGFileDialog(gClient->GetRoot(), win ? win->GetParent() : GetParent(), kFDOpen, &fi);
    GuiMsg("Open file: %s (dir: %s)", fi.fFilename ? fi.fFilename : "(None)", fi.fIniDir).send(gui);
    dir = fi.fIniDir;
    if ( fi.fFilename != 0 )   {
      victimOpts.input->RemoveAll();
      victimOpts.input->AddEntry(gSystem->BaseName(fi.fFilename), 0);
      victimOpts.input->SetEnabled(kTRUE);
      victimOpts.input->Select(0, kFALSE);
    }
  }
}

/// Load the victim node client options on demand
void OptionsExplorer::loadVictimOptions()    {
  if ( gui )  {
    string  d = _selectedText(dns.input);
    string  n = _selectedText(node.input);
    string  c = "";
    string  o = _selectedText(victimOpts.input);
    GuiCommand* cmd = new GuiCommand(d,n,c,o);
    CPP::IocSensor::instance().send(gui,XMLRPCGUI_SHOW_VICTIMNODEPANE,cmd);
  }
}
