//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/XMLRPCGUI.h"
#include "RPC/GUI/GuiException.h"
#include "RPC/GUI/GuiCommand.h"
#include "RPC/GUI/GuiMsg.h"

#include "RPC/GUI/MessageBox.h"
#include "RPC/GUI/OutputWindow.h"
#include "RPC/GUI/RPCOptionsEditor.h"
#include "RPC/GUI/RPCBridgeOptionsEditor.h"
#include "RPC/GUI/DIMOptionsEditor.h"
#include "RPC/GUI/OptionsExplorer.h"
#include "RPC/GUI/VictimNodeEditor.h"
#include "CPP/IocSensor.h"
#include "CPP/Event.h"

/// ROOT include files
#include "TGTab.h"
#include "TGMenu.h"
#include "TApplication.h"

ClassImp(xmlrpc::XMLRPCGUI)

using namespace std;
using namespace xmlrpc;

/// Standard Initializing constructor
XMLRPCGUI::XMLRPCGUI(TGFrame* parent)
  : TGCompositeFrame(parent, 100, 100, kVerticalFrame), CPP::Interactor()
{
  tabs = new TGTab(this, 600, 600);
  TGCompositeFrame *tab_frame;

  tab_frame = tabs->AddTab("Connection");
  explorer = new OptionsExplorer(tab_frame, this);
  tab_frame->AddFrame(explorer, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));

  tab_frame = tabs->AddTab("Options");
  editor = new RPCOptionsEditor(tab_frame, this);
  editor->init(10);
  tab_frame->AddFrame(editor, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));

  tab_frame = tabs->AddTab("DIM");
  dim_editor = new DIMOptionsEditor(tab_frame, this);
  dim_editor->init(10);
  tab_frame->AddFrame(dim_editor, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));

  tab_frame = tabs->AddTab("Bridge");
  bridge_editor = new RPCBridgeOptionsEditor(tab_frame, this);
  bridge_editor->init(10);
  tab_frame->AddFrame(bridge_editor, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));

  tab_frame = tabs->AddTab("Victim Node");
  victim_editor = new VictimNodeEditor(tab_frame, this);
  victim_editor->init(10);
  tab_frame->AddFrame(victim_editor, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));

  output = new OutputWindow(this);
  AddFrame(tabs,   new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));
  AddFrame(output, new TGLayoutHints(kLHintsCenterX|kLHintsBottom|kLHintsExpandX,1,1,1,1));
  Connect("CloseWindow()", "xmlrpc::XMLRPCGUI", this, "CloseWindow()");
  tabs->Resize(tabs->GetDefaultWidth(),450);
}

/// Default destructor
XMLRPCGUI::~XMLRPCGUI()   {
  if ( menuFile      ) delete menuFile;
  if ( menuHelp      ) delete menuHelp;
  if ( guiMenuBar    ) delete guiMenuBar;
  if ( explorer      ) delete explorer;
  if ( editor        ) delete editor;
  if ( dim_editor    ) delete dim_editor;
  if ( bridge_editor ) delete bridge_editor;
  if ( victim_editor ) delete victim_editor;
  if ( output        ) delete output;
}

/// Interactor interrupt handler callback
void XMLRPCGUI::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case XMLRPCGUI_SEND_NODELIST:   {
      CPP::Interactor* actor = ev.iocPtr<Interactor>();
      auto* items = new set<string>(explorer->nodeList());
      IocSensor::instance().send(actor,XMLRPCGUI_HAVE_NODELIST,items); 
      break;
    }
    case XMLRPCGUI_SEND_TASKLIST:   {
      CPP::Interactor* actor = ev.iocPtr<Interactor>();
      auto* items = new set<string>(explorer->taskList());
      IocSensor::instance().send(actor,XMLRPCGUI_HAVE_TASKLIST,items); 
      break;
    }
    case XMLRPCGUI_SHOW_OUTPUTLINE:
      IocSensor::instance().send(output,ev.type,ev.data); 
      break;
    case XMLRPCGUI_UPDATE_OUTPUT:
      IocSensor::instance().send(output,ev.type,ev.data); 
      break;
    case XMLRPCGUI_EXCEPTION:  {
      unique_ptr<GuiException> e(ev.iocPtr<GuiException>());
      //GuiMsg("+======================================================================").send(this);
      //GuiMsg("|   GUI Exception: %s",e->title.c_str()).send(this);
      //GuiMsg("|   %s",e->msg.c_str()).send(this);
      //GuiMsg("+======================================================================").send(this);
      Int_t retval = 0;
      MessageBox(gClient->GetRoot(), GetParent(),
		 e->title.c_str(), e->msg.c_str(),
		 e->icon, e->buttons, &retval);
      break;
    }

    case XMLRPCGUI_SHOW_EXPLORERPANE:
      tabs->SetTab(0);
      break;
    case XMLRPCGUI_SHOW_EDITORPANE:
      tabs->SetTab(1);
      break;
    case XMLRPCGUI_SHOW_RPCCLIENT:
      tabs->SetTab(1);
      IocSensor::instance().send(editor,       XMLRPCGUI_SHOW_CLIENT,ev.data); 
     break;
    case XMLRPCGUI_SHOW_DIMCLIENT:
      tabs->SetTab(2);
      IocSensor::instance().send(dim_editor,   XMLRPCGUI_SHOW_CLIENT,ev.data); 
     break;
    case XMLRPCGUI_SHOW_BRIDGERPCCLIENT:
      tabs->SetTab(3);
      IocSensor::instance().send(bridge_editor,XMLRPCGUI_SHOW_CLIENT,ev.data); 
     break;
    case XMLRPCGUI_SHOW_VICTIMNODEPANE:
      tabs->SetTab(4);
      IocSensor::instance().send(victim_editor,XMLRPCGUI_SHOW_CLIENT,ev.data); 
      break;

    default:
      GuiMsg("XMLRPCGUI::handle..Unhandled IOC... %d\n",ev.type).send(this);
      break;
    }
    return;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
  GuiMsg("XMLRPCGUI::handle..Unhandled Event... %d\n",ev.eventtype).send(this);
}

/// Create a menu bar and add it to the parent
TGMenuBar* XMLRPCGUI::menuBar()   {
  if ( 0 == guiMenuBar )  {
    TGWindow* win = const_cast<TGWindow*>(GetMainFrame());
    TGCompositeFrame* frame = dynamic_cast<TGCompositeFrame*>(win);
    guiMenuBar = new TGMenuBar(GetParent(), 1, 1, kHorizontalFrame);
    frame->AddFrame(guiMenuBar, new TGLayoutHints(kLHintsTop | kLHintsExpandX));
  }
  return guiMenuBar;
}

/// Create the menues of the application
void XMLRPCGUI::createMenus(TGMenuBar* bar)   {
  TGPicturePool* p = gClient->GetPicturePool();
  // Create menubar and popup menus. 
  menuFile = new TGPopupMenu(gClient->GetRoot());
  menuFile->AddEntry(new TGHotString("&Open...\tCTRL-O"),        M_FILE_OPEN,   0, p->GetPicture("open.xpm"));
  menuFile->AddEntry(new TGHotString("&Save\tCTRL-S"),           M_FILE_SAVE,   0, p->GetPicture("save.xpm"));
  menuFile->AddEntry(new TGHotString("S&ave as...\tCTRL-a"),     M_FILE_SAVEAS, 0, p->GetPicture("filesaveas.xpm"));
  menuFile->AddEntry(new TGHotString("&Close\tCTRL-C"),          M_FILE_CLOSE,  0, p->GetPicture("ed_delete.png"));
  menuFile->AddSeparator();
  menuFile->AddEntry(new TGHotString("&Print\tCTRL-P"),          M_FILE_PRINT,  0, p->GetPicture("ed_print.png"));
  menuFile->AddEntry(new TGHotString("P&rint setup\tCTRL-r"),    M_FILE_PRINTSETUP, 0, p->GetPicture("ed_print.png"));
  menuFile->AddSeparator();
  menuFile->AddEntry(new TGHotString("E&xit\tCTRL-x"),           M_FILE_EXIT,   0, p->GetPicture("ed_delete.png"));

  menuHelp = new TGPopupMenu(gClient->GetRoot());
  menuHelp->AddEntry(new TGHotString("&Contents\tCTRL-C"),       M_HELP_CONTENTS, 0, p->GetPicture("ed_help.png"));
  menuHelp->AddEntry(new TGHotString("&Search...\tCTRL-S"),      M_HELP_SEARCH, 0, p->GetPicture("ed_find.png"));
  menuHelp->AddSeparator();
  menuHelp->AddEntry(new TGHotString("&About\tCTRL-A"),          M_HELP_ABOUT, 0, p->GetPicture("about.xpm"));

#if 0
  menuFile->DisableEntry(M_FILE_OPEN);
  menuFile->DisableEntry(M_FILE_SAVE);
  menuFile->DisableEntry(M_FILE_SAVEAS);
  menuFile->DisableEntry(M_FILE_PRINT);
  menuFile->DisableEntry(M_FILE_PRINTSETUP);

  menuHelp->DisableEntry(M_HELP_CONTENTS);
  menuHelp->DisableEntry(M_HELP_SEARCH);
#endif
  // The hint objects are used to place
  // and group the different menu widgets with respect to eachother.
  bar->AddPopup(new TGHotString("&File"), menuFile, new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0));
  bar->AddPopup(new TGHotString("&Help"), menuHelp, new TGLayoutHints(kLHintsTop | kLHintsRight));

  menuFile->Connect("Activated(Int_t)","xmlrpc::XMLRPCGUI",this,"handleMenu(Int_t)");
  menuHelp->Connect("Activated(Int_t)","xmlrpc::XMLRPCGUI",this,"handleMenu(Int_t)");
}

/// Menu callback handler
void XMLRPCGUI::handleMenu(Int_t id)   {
  switch(id)   {
  case M_FILE_OPEN:
    notImplemented("The \"File\" command is not implemented");
    break;
  case M_FILE_SAVE:
    notImplemented("The \"Save\" command is not implemented");
    break;
  case M_FILE_SAVEAS:
    notImplemented("The \"Save As\" command is not implemented");
    break;
  case M_FILE_PRINT:
    notImplemented("The \"Print\" command is not implemented");
    break;
  case M_FILE_PRINTSETUP:
    notImplemented("The \"Print Setup\" command is not implemented");
    break;
  case M_FILE_CLOSE:
    CloseWindow();
    break;
  case M_FILE_EXIT:
    terminate();   // terminate theApp no need to use SendCloseMessage()
    break;
  case M_HELP_CONTENTS:
    notImplemented("The \"Help Contents\" command is not implemented");
    break;
  case M_HELP_SEARCH:
    notImplemented("The \"Help Search\" command is not implemented");
    break;
  case M_HELP_ABOUT:
    aboutBox();
    break;
  default:
    break;
  }
}

/// Show a message box indicating this command is not implemented
void XMLRPCGUI::notImplemented(const std::string& msg)   {
  Int_t retval = 0;
  this->Disconnect("CloseWindow()");
  this->Connect("CloseWindow()", "xmlrpc::XMLRPCGUI", this, "tryToClose()");
  new TGMsgBox(gClient->GetRoot(), GetParent(),
	       "Command not implemented", msg.c_str(),
	       kMBIconAsterisk, kMBOk, &retval);
  this->Disconnect("CloseWindow()");
  this->Connect("CloseWindow()", "xmlrpc::XMLRPCGUI", this, "CloseWindow()");
}

/// Show the about box of this application
void XMLRPCGUI::aboutBox()   {
  Int_t retval = 0;
  this->Disconnect("CloseWindow()");
  this->Connect("CloseWindow()", "xmlrpc::XMLRPCGUI", this, "tryToClose()");
  new TGMsgBox(gClient->GetRoot(), GetParent(),
	       "About XMLRPCGUI....",
	       "XMLRPCGUI\n\n"
	       "The simple way to interact\n"
	       "with remote processes\n\n"
	       "M.Frank CERN/LHCb",
	       kMBIconExclamation, kMBOk, &retval);
  this->Disconnect("CloseWindow()");
  this->Connect("CloseWindow()", "xmlrpc::XMLRPCGUI", this, "CloseWindow()");
}

/// The user try to close the main window, while a message dialog box is still open.
void XMLRPCGUI::tryToClose()   {
  ::printf("Can't close the window '%s' : a message box is still open\n", GetMainFrame()->GetName());
}

/// Close the window
void XMLRPCGUI::CloseWindow()   {
  gApplication->Terminate();
  //CloseWindow();
}

/// Terminate the App no need to use SendCloseMessage()
void XMLRPCGUI::terminate()   {
  gApplication->Terminate();
  //CloseWindow();
}
