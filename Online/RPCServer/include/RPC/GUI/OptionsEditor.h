//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_OPTIONSEDITOR_H
#define RPC_OPTIONSEDITOR_H

/// Framework include files
#include <CPP/Interactor.h>
#include <CPP/ObjectProperty.h>
#include <RPC/DataflowRPC.h>
#include <RPC/RpcClientHandle.h>
#include <RPC/GUI/GuiCommand.h>

/// ROOT include files
#include <TGFrame.h>

// C++ ioncludes
#include <memory>
#include <mutex>

/// Forward declarations
class TGLabel;
class TGButton;
class TGComboBox;
class TGTextEdit;
class TGTextEntry;
class TGTextButton;
class TGGroupFrame;
class TGPictureButton;

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Job options editor panel
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class OptionsEditor : public TGCompositeFrame, public CPP::Interactor   {
  public:

    enum OptionEditorRowState   {
      OPTION_ENABLED  = 1<<0,
      OPTION_DISABLED = 1<<1,
      OPTION_CHANGED  = 1<<2,
      OPTION_SENT     = 1<<3
    };
    enum OptionEditorCommands {
      EDITOR_FIRST_ID = XMLRPCGUI_LAST+100,
      // Block interface commands
      EDITOR_LOAD_OPTIONS,
      EDITOR_CLEAR_OPTIONS,
      EDITOR_SAVE_OPTIONS,
      EDITOR_SEND_CHANGES,
      // Single option editor commands
      EDITOR_EDIT_OPTION,
      EDITOR_RELOAD_OPTION,
      EDITOR_SAVE_OPTION,
      EDITOR_SEND_OPTION,
      EDITOR_ADD_OPTION
    };
    typedef rpc::ObjectProperty         ObjectProperty;
    typedef std::vector<ObjectProperty> Properties;
    struct OptRow   {
      TGTextEntry*     dns  = 0;
      TGTextEntry*     node = 0;
      TGTextEntry*     proc = 0;
      TGComboBox*      combo = 0;
    };
    struct Row {
      Int_t            id      = 0;
      bool             isValid = false;
      TGTextEntry*     label   = 0;
      TGTextEdit*      editor  = 0;
      TGTextEntry*     option  = 0;
      TGTextButton*    edit    = 0;
      TGPictureButton* edit2   = 0;
      TGTextButton*    send    = 0;
      TGPictureButton* send2   = 0;
    };
    std::vector<Row>   rows;
    Properties         properties;
    TGGroupFrame*      editor;
    OptRow             source_label, source;
    Row                edit;
    int                optsBatch   = -1;
    Pixel_t            color_disabled, color_enabled, color_changed, color_sent;

    CPP::Interactor*   gui         = 0;
    TGGroupFrame*      prop_group  = 0;
    TGGroupFrame*      nav_group   = 0;
    TGTextButton*      nav_first   = 0;
    TGPictureButton*   nav_firstp  = 0;
    TGTextButton*      nav_prev    = 0;
    TGPictureButton*   nav_prevp   = 0;
    TGTextButton*      nav_next    = 0;
    TGPictureButton*   nav_nextp   = 0;
    TGTextButton*      nav_last    = 0;
    TGPictureButton*   nav_lastp   = 0;
    TGLabel*           nav_label   = 0;
    TGTextButton*      nav_explore = 0;
    TGTextButton*      nav_reload  = 0;
    TGTextButton*      nav_send    = 0;
    TGTextButton*      nav_save    = 0;
    TGPictureButton*   nav_savep   = 0;

    std::string        dns, node, process;
    xmlrpc::dataflow::PropertyContainer client;

    /// Lock to protect properties
    std::mutex              lock;
    /// Currently edited job option
    std::string        currentEdit;

  public:

    /// Standard Constructor
    OptionsEditor(TGWindow* p, CPP::Interactor* gui);
    /// Default destructor
    virtual ~OptionsEditor();
    void dummy();

    /// Translate options flag to row color
    Pixel_t getBackgroundColor(int flag)   const;
    /// Initialize the display, setup the widgets
    virtual void init(int num_rows);
    /// Initialize the table of job options with num_rows rows
    virtual void init_options_table(int num_rows);
    /// Initialize single row of the options table
    virtual Row  init_options_table_row(TGGroupFrame* grp, int irow);
    /// Clear the entire options table
    virtual void clear_options_table();
    /// Clear single row content in the options table
    virtual void clear_options_table_row(const Row& row);
    /// Show single row in the options table
    virtual void show_options_table_row(const Row& row, const ObjectProperty& p);
    /// Show a batch of options in the options table
    virtual void show_table_batch(int batch);
    /// Update after successful sending of the changes
    virtual void update_sent_options();
    /// Reset the content of the options editor
    virtual void reset_options_editor(const std::string& client, const std::string& opt_val);
    /// Save the not-disabled options to a file
    virtual void save_options_file(unsigned int mask, bool mask_exact)  const;

    /// Default implementation: Save the not-disabled options to a file
    virtual void saveOptionsFile();
    /// Edit option on request as indicated by the row
    virtual void editOption(const Row& row);
    /// Cancel editing of the option and reload current option into editor
    virtual void reloadOption();
    /// Save changed option from the editor back to table
    virtual void saveOption();
    /// Add an option to the table from the data in the editor GUI
    virtual void addOption();

    /** Interactor interface      */
    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;

    /** RPC interaction routines  */
    /// Create/Reset communication client. Basic implementation ONLY.
    virtual void createClient(GuiCommand* c);
    /// Load options of a single client
    virtual void loadOptions() = 0;
    /// Send single option to the target process
    virtual void sendOption(const Row& row) = 0;
    /// Send all changed options to the client process
    virtual void sendChanges() = 0;

    /** GUI Command handlers      */
    /// GUI handler: Save the not-disabled options to a file
    virtual void click_saveOptionsFile();
    /// GUI handler: Reload all client options
    virtual void click_reloadClientOptions();
    /// GUI handler: Back to explorer tab
    virtual void click_backToExplorer();
    /// GUI handler: Send all changed options
    virtual void click_sendChanges();
    /// GUI handler: Load selected option into editor
    virtual void click_editOption();
    /// GUI handler: Send option to the target
    virtual void click_sendOption();
    /// GUI handler: Cancel editing of the option
    virtual void click_reloadOption();
    /// GUI handler: Save changed option back to table
    virtual void click_saveOption();
    /// GUI handler: Add an option from the data in the editor GUI
    virtual void click_addOption();

    /// GUI handler: Load the next batch of options into the matrix
    virtual void show_next_table_batch();
    /// GUI handler: Load the previous batch of options into the matrix
    virtual void show_previous_table_batch();
    /// GUI handler: Load the first batch of options into the matrix
    virtual void show_first_table_batch();
    /// GUI handler: Load the last batch of options into the matrix
    virtual void show_last_table_batch();

    /// ROOT class definition
    ClassDefOverride(OptionsEditor,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_OPTIONSEDITOR_H  */
