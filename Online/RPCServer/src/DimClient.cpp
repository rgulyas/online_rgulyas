//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "RPC/DimClient.h"
#include "XML/Printout.h"

// C/C++ include files
#include <mutex>
#include <condition_variable>

/// Namespace for the dimrpc based implementation
namespace rpc  {

  class DimClient::Handler  {
  public:
    /// Lock for the conditions variable
    std::mutex              lock;
    /// Conditions variable to oprerly sequence the threads
    std::condition_variable condition;
    /// The method response of the server
    std::vector<unsigned char> response;
    /// Flag: set when data are ready
    int                     ready       = 0;
    /// Current message identifier to properly select the correct answer
    int                     message_id  = 0;
    /// DIM response service identifier
    int                     response_id = -1;
    /// Property: dim server name
    std::string             server;
    /// Property: dim dns name
    std::string             dns;
    /// Timeout waiting for messages
    int                     timeout = 5000;
    /// Debug flag
    int                     debug = 0;
    /// Standard constructor
    Handler() = default;
    /// Standard destructor
    ~Handler();
    /// Set the caller properties, open DIM connection
    void open(const std::string& server_name, const std::string& dns_name);
    /// Access name
    std::string name()  const;
    /// Connect client to given URI and execute RPC request
    std::vector<unsigned char> request(const void* call, size_t len);
    /// Execute RPC call
    xmlrpc::MethodResponse call(const xmlrpc::MethodCall& call);
    /// Execute RPC call
    jsonrpc::MethodResponse call(const jsonrpc::MethodCall& call);
  };
}
//==========================================================================

#include <cstring>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <netdb.h>

#ifdef HAVE_DIM
#include "dim/dic.h"
#else
#define  DIM_CLIENT
#include "fake_dim.h"
#endif

using namespace std;
using namespace rpc;
using namespace dd4hep;

namespace {
  vector<unsigned char> to_vector(const char* data)   {
    vector<unsigned char> r;
    if ( data )   {
      size_t len = ::strlen(data);
      r.reserve(len+1);
      for(const char *c=data; *c; ++c) r.push_back((unsigned char)(*c));
    }
    return r;
  }
  static DimRequest proc;
  void rpc_callback(void* tag, int* code)  {
    if ( tag && code )  {}
  }
  void feed_result(void* tag, void* buffer, int* size)   {
    if ( tag && buffer && size && *size )   {
      DimClient::Handler* client = *(DimClient::Handler**)tag;
      DimRequest* msg = (DimRequest*)buffer;
      if ( msg->pid == proc.pid )  {
        if ( msg->host == proc.host )  {
          if ( msg->mid == client->message_id )  {
            unique_lock<mutex> lk(client->lock);
            if ( !client->ready )  {
	      client->response.clear();
	      client->response   = to_vector(&msg->data[0]);
              client->ready      = 1;
              client->message_id = 0;
              client->condition.notify_one();
            }
          }
        }
      }
    }
  }
}

/// Standard destructor
DimClient::Handler::~Handler()  {
  if ( response_id != -1 ) ::dic_release_service(response_id);
}

/// Set the caller properties, open DIM connection
void DimClient::Handler::open(const string& server_name, const string& dns_name)   {
  /// Initialize structure
  if ( !proc.pid )  {
    char host[128];
    ::gethostname(host, sizeof(host));
    struct hostent *he = ::gethostbyname(host);
    if ( he )  {
      proc.pid  = ::getpid();
      proc.host = *(int*)he->h_addr;
      ++proc.mid;
    }
  }
  unique_lock<mutex> lck(lock);
  if ( dns != dns_name )   {
    dns = dns_name;
    if ( !dns.empty() ) ::dic_set_dns_node(dns.c_str());
  }
  if ( server != server_name )  {
    server = server_name;
    string name = server + "/RPC2/Reply";
    if ( response_id != -1 ) dic_release_service(response_id);
    response_id = ::dic_info_service(name.c_str(),MONITORED,timeout,0,0,feed_result,(long)this,0,0);
    if ( response_id <= 0 )   {
      throw runtime_error("Failed dic_info_service("+name+")");
    }
    printout(debug!=0 ? ALWAYS : DEBUG,"DimClient","Subscribed to response service: %s",name.c_str());
  }
}

/// Access name
string DimClient::Handler::name()  const   {
  return server+"@"+dns;
}

/// Connect client to given URI and execute RPC call
vector<unsigned char> DimClient::Handler::request(const void* req, size_t length)    {
  using namespace chrono;
  stringstream str;
  try  {
    size_t len = length+sizeof(DimRequest);
    unique_ptr<DimRequest> m((DimRequest*)::operator new(len));
    m->magic = DimRequest::MAGIC;
    m->pid   = proc.pid;
    m->host  = proc.host;
    m->mid   = proc.newID();
    m->size  = length+1;
    ::memcpy(m->data, req, length);
    string name = server + "/RPC2";
    {
      if ( debug>1 )  {
        printout(INFO,"RPCClient","Sending request: %s",m->data);
      }
      else if ( debug>0 )  {
	printout(INFO,"RPCClient","Sending request....");
      }  {
        unique_lock<mutex> lck(lock);
	
        ready = 0;
        message_id = m->mid;

        int sc = ::dic_cmnd_callback(name.c_str(),m.get(),len,rpc_callback,(long)this);
        if ( sc != 1 )   {
          throw runtime_error("Failed dic_cmnd_callback("+name+")");
        }
        if ( condition.wait_for(lck,chrono::milliseconds(timeout)) == cv_status::timeout ) {
          if ( !ready )  {
            message_id = 0;
            throw runtime_error("XML-RPC: Command timeout "+name);
          }
        }
      }
      message_id = 0;
      return response;
    }
  }
  catch(const std::exception& e)   {
    str << e.what();
  }
  catch( ... )   {
    std::error_code errcode(errno,std::system_category());
    str << "XMLRPC fault [" << errcode.value() << "] (UNKOWN Exception): " << errcode.message();
  }
  throw runtime_error(str.str());
}

namespace {

  template <typename RESPONSE, typename CALL>
  RESPONSE do_rpc(DimClient::Handler* client, const CALL& request)   {
    using namespace chrono;
    stringstream str;
    try  {
      string req = request.str();
      RESPONSE mr;
      try  {
	mr = RESPONSE::decode(client->request(req.c_str(), req.length()));
      }
      catch(const std::exception& e)   {
	std::error_code errcode(errno, std::system_category());
	mr = RESPONSE::makeFault(errcode.value(),e.what());
      }
      catch( ... )   {
	std::error_code errcode(errno, std::system_category());
	mr = RESPONSE::makeFault(errcode.value(),errcode.message());
      }
      if ( client->debug > 1 )  {
	printout(INFO,"RPCClient","Handling response: %s",mr.str().c_str());
      }
      else if ( client->debug > 0 )  {
	printout(INFO,"RPCClient","Handling response [Length:%ld bytes]",mr.str().length());
      }
      if ( !mr.isFault() )   {
	return mr;
      }
      str << "XMLRPC fault [" << mr.faultCode() << "]: " << mr.faultString();
    }
    catch(const std::exception& e)   {
      str << e.what();
    }
    catch( ... )   {
      std::error_code errcode(errno, std::system_category());
      str << "RPC fault [" << errcode.value() << "] (UNKOWN Exception): " << errcode.message();
    }
    throw runtime_error(str.str());
  }
}

/// Execute RPC call
xmlrpc::MethodResponse DimClient::Handler::call(const xmlrpc::MethodCall& request)   {
  return do_rpc<xmlrpc::MethodResponse,xmlrpc::MethodCall>(this, request);
}

/// Execute RPC call
jsonrpc::MethodResponse DimClient::Handler::call(const jsonrpc::MethodCall& request)   {
  return do_rpc<jsonrpc::MethodResponse,jsonrpc::MethodCall>(this, request);
}

/// Initializing constructor
DimClient::DimClient(const string& srv_name, const string& dns_name, int tmo)
{
  open(srv_name, dns_name, tmo);
}

/// Default destructor
DimClient::~DimClient() {
  if ( implementation ) delete implementation;
}

#define INSTALL   if ( !implementation ) implementation = new Handler()

/// Modify debug flag
int DimClient::setDebug(int value)  {
  INSTALL;
  int tmp = implementation->debug;
  implementation->debug = value;
  return tmp;
}

/// Access debug flag
int DimClient::debug()  const   {
  return (implementation) ? implementation->debug : 0;
}

/// Access name
string DimClient::name()  const   {
  return (implementation) ? implementation->name() : string("");
}

/// Set the caller properties
void DimClient::open(const string& server_name, const string& dns_name)   {
  INSTALL;
  implementation->open(server_name,dns_name);
}

/// Set the caller properties
void DimClient::open(const string& server_name, const string& dns_name, int tmo)   {
  INSTALL;
  implementation->timeout = tmo;
  open(server_name, dns_name);
}

/// Execute RPC call
vector<unsigned char> DimClient::request(const string& call)  const {
  return implementation->request(call.c_str(), call.length());
}

/// Connect client to given URI and execute RPC call
vector<unsigned char> DimClient::request(const void* call, size_t len)  const  {
  return implementation->request(call, len);
}

/// Execute RPC call
xmlrpc::MethodResponse DimClient::call(const xmlrpc::MethodCall& call)  const {
  return implementation->call(call);
}

/// Execute RPC call
jsonrpc::MethodResponse DimClient::call(const jsonrpc::MethodCall& call)  const {
  return implementation->call(call);
}

