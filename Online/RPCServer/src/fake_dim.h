//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
namespace {
#ifdef DIM_CLIENT
  void dic_release_service(int) {}
  void dic_set_dns_node(const char*) {}
  int  dic_info_service(const char*,...) { return 0; }
  int  dic_cmnd_callback(const char*,...) { return 0; }
  enum { MONITORED };
#endif
#ifdef DIM_SERVER
  void dis_remove_service(int)               {           }
  void dis_set_dns_node(const char*)         {           }
  int  dis_add_cmnd(const char*,...)         { return 0; }
  int  dis_add_service(const char*,...)      { return 0; }
  int  dis_selective_update_service(int,int*){ return 0; }
  int  dis_get_client(char*)                 { return 0; }
  void dis_start_serving(const char*)        {           }
#endif
}
