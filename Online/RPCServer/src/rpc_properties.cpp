//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//
//==========================================================================

/// Framework include files

/// C/C++ include files
#include <cctype>
#include <cstdlib>
#include <cstdio>

extern "C" {
#ifdef _POSIX_C_SOURCE
#undef _POSIX_C_SOURCE
#endif
#ifdef _XOPEN_SOURCE
#undef _XOPEN_SOURCE
#endif
#include <Python.h>
}
#if PY_MAJOR_VERSION >= 3
#define PyString_FromString          PyUnicode_FromString
#endif

#include <string>
namespace  {
  constexpr const char* module_version = "0.1";
}

static PyObject*
version(PyObject* /* self */, PyObject* /* args */)  {
  /** Return module version   */
  return PyString_FromString(module_version);
}

/// Initialize logger object from python
static PyObject* _initialize_rpc_properties(PyObject* /* self */, PyObject* /* args */)  {
  std::string err;
  Py_BEGIN_ALLOW_THREADS
    try    {

    }
    catch(const std::exception& e)    {
      err = e.what();
    }
  Py_END_ALLOW_THREADS
    if ( err.empty() ) {
      Py_RETURN_NONE;
    }
  PyErr_SetString(PyExc_RuntimeError, err.c_str());
  return nullptr;
}

/// Finalize logger object from python
static PyObject* _finalize_rpc_properties(PyObject* /* self */, PyObject* /* args */) {
  std::string err;
  Py_BEGIN_ALLOW_THREADS	
    try    {

    }
    catch(const std::exception& e)    {
      err = e.what();
    }
  Py_END_ALLOW_THREADS
    if ( err.empty() ) {
      Py_RETURN_NONE;
    }
  PyErr_SetString(PyExc_RuntimeError, err.c_str());
  return nullptr;
}

static PyMethodDef Rpc_PropertiesMethods[] = {
  { "version",
    version,
    METH_VARARGS,
    "Module version"
  },
  {    "rpc_properties_start",
       _initialize_rpc_properties,
       METH_VARARGS,
       "Start rpc_properties"
  },
  {    "rpc_properties_stop",
       _finalize_rpc_properties,
       METH_VARARGS,
       "Stop rpc_properties"
  },
  {NULL, NULL, 0, NULL}        /* Sentinel */
};

#if PY_MAJOR_VERSION >= 3
static struct PyModuleDef rpc_properties_module = {
    PyModuleDef_HEAD_INIT,
    "_rpc_properties",        /* name of module */
    "Rpc Properties Methods", /* module documentation, may be NULL */
    -1,                       /* size of per-interpreter state of the module,
                                 or -1 if the module keeps state in global variables. */
    Rpc_PropertiesMethods,
    NULL,                     /* m_reload */
    NULL,                     /* m_traverse */
    NULL,                     /* m_clear */
    NULL                      /* m_free */
};
  #define PyMODINIT_RETURN(x)   return x
#else
  #define PyMODINIT_RETURN(x)   return
#endif

#if PY_MAJOR_VERSION >= 3
PyMODINIT_FUNC    PyInit__fifo_log(void)
#else
PyMODINIT_FUNC    init_fifo_log(void)
#endif
{
#if PY_MAJOR_VERSION <=2 || (PY_MAJOR_VERSION == 3 && PY_MINOR_VERSION < 7)
  PyEval_InitThreads();
#endif
#if PY_MAJOR_VERSION >= 3
  PyObject *m = ::PyModule_Create(&rpc_properties_module);
#else
  PyObject *m = Py_InitModule3("_rpc_properties", Rpc_PropertiesMethods, "Rpc Properties methods");
#endif
  if (m == NULL)  {
    PyMODINIT_RETURN(m);
  }
  // Add constants for service type definitions
  PyModule_AddIntConstant (m, "INITIALIZED", 1);
  PyMODINIT_RETURN(m);
}
