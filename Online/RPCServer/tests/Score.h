//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================
#include "XML/config.h"

// C/C++ include files
#include <iostream>
#include <sstream>
#include <string>
#include <ctime>

namespace {
  std::ostream& logger(const std::string& prefix)  {
    time_t t = ::time(0);
    struct tm tm;
    ::localtime_r(&t, &tm);
    char buff[124];
    ::strftime(buff,sizeof(buff),"%H:%M:%S %d-%m-%Y ",&tm);
    return std::cout << buff << prefix;
  }
}

/// Namespace for the AIDA detector description toolkit
namespace Online {
  /// Namespace for the RPC implementation
  namespace rpc   {
    /// Testing namespace -- Not for public use!
    namespace test  {

      class _TestCount {
	explicit _TestCount(int i);
	virtual ~_TestCount();
      public:
	int counter = 0;
	static _TestCount& instance();
      };
    }
  }
}

namespace {
  int& s_test_count = Online::rpc::test::_TestCount::instance().counter;
}
