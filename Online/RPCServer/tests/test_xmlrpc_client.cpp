//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RPC/RpcClientHandle.h>
#include <RPC/XMLRPC.h>
#include <HTTP/HttpClient.h>
#define CallableHandler  rpc::XmlRpcClientH
#define Callable         HttpCallable

#include "Objects.h"
#include "test_rpc_client.h"

namespace  {
  void help_client()  {
    ::printf
      ("xmlrpc_client -opt [-opt]\n"
       "   -host=<host-name>     Client host name. Default: localhost\n"
       "   -port=<port-number>   Client port       Default: 8000     \n");
    ::exit(EINVAL);
  }
}

extern "C" int test_xmlrpc_client(int argc, char** argv)   {
  int loop  = 1, port  = 8000, debug = 0;
  std::string host = "localhost";
  for(int i = 0; i < argc && argv[i]; ++i)  {
    if ( 0 == ::strncmp("-host",argv[i],4) )
      host = argv[++i];
    else if ( 0 == ::strncmp("-port",argv[i],4) )
      port = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-debug",argv[i],4) )
      debug = 1;
    else if ( 0 == ::strncmp("-loop",argv[i],4) )
      loop  = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-help",argv[i],2) )
      help_client();
  }


  Callable client { rpc::client<http::HttpClient>(host,port) };
  client.handler.setDebug(debug != 0);
  logger("TESTClient") << "Starting test XMLRPC client with URI:" << host << ":" << port << endl;
  return call_client(argc, argv, client, loop);
}
