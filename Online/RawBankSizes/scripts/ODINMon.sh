#!/bin/bash
#
export CMTCONFIG=x86_64_v2-el9-gcc12-opt;
export CMTCONFIG=x86_64_v2-el9-gcc12-do0;
#
cd /group/online/dataflow/cmtuser/OnlineRelease;
. setup.${CMTCONFIG}.vars;
. ${FARMCONFIGROOT}/job/createEnvironment.sh  $*;
#
setup_options_path MONITORING;
#
if test "$PARTITION" = "FESTxxx"; then
exec -a ${UTGID} \
    /home/frankm/bin/valgrind --tool=memcheck --leak-check=yes --track-origins=yes \
    `which genPython.exe` `which gaudirun.py` ${RAWBANKSIZESROOT}/options/ODINMon.py --application=OnlineEvents;
else
exec -a ${UTGID} \
    `which genPython.exe` `which gaudirun.py` ${RAWBANKSIZESROOT}/options/ODINMon.py --application=OnlineEvents;
fi;
