#!/bin/bash
#
. /group/online/dataflow/scripts/preamble.sh;
. ${FARMCONFIGROOT}/job/createEnvironment.sh  $*;
#
`setup_options_pathes MONITORING`;
#
exec -a ${UTGID} genPython.exe `which gaudirun.py` ${RAWBANKSIZESROOT}/options/RawSizeMon.py --application=OnlineEvents;
