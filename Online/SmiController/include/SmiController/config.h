//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================
#ifndef ONLINE_FINITESTATEMACHINE_CONFIG_H
#define ONLINE_FINITESTATEMACHINE_CONFIG_H

#define DIM_MULTI_DNS 1
//#undef  DIM_MULTI_DNS

/// FiniteStateMachine namespace declaration
namespace FiniteStateMachine {

  struct Transition  {
    static constexpr const char* CONFIGURE  = "configure";
    static constexpr const char* INITIALIZE = "initialize";
    static constexpr const char* START      = "start";
    static constexpr const char* STOP       = "stop";
    static constexpr const char* RESET      = "reset";
    static constexpr const char* PAUSE      = "pause";
    static constexpr const char* LOAD       = "load";
    static constexpr const char* UNLOAD     = "unload";
    static constexpr const char* KILL       = "kill";
    static constexpr const char* DESTROY    = "destroy";
    static constexpr const char* RECOVER    = "recover";
    static constexpr const char* CREATE     = "create";
    static constexpr const char* CONTINUE   = "continue";
  };
  struct State  {
    static constexpr const char* UNKNOWN    = "UNKNOWN";
    static constexpr const char* OFFLINE    = "OFFLINE";
    static constexpr const char* NOT_READY  = "NOT_READY";
    static constexpr const char* READY      = "READY";
    static constexpr const char* RUNNING    = "RUNNING";
    static constexpr const char* STOPPED    = "STOPPED";
    static constexpr const char* PAUSED     = "PAUSED";
    static constexpr const char* ERROR      = "ERROR";
  };
  struct MonState  {
    static constexpr char UNKNOWN     = 'U';
    static constexpr char OFFLINE     = 'O';
    static constexpr char READY       = 'r';
    static constexpr char RUNNING     = 'R';
    static constexpr char STOPPED     = 'S';
    static constexpr char PAUSED      = 'P';
    static constexpr char ERROR       = 'E';
  };
  enum MonMetaState  {
    ACTION_SUCCESS = 'S',
    ACTION_EXEC    = 'E',
    ACTION_FAILED  = 'F',
    ACTION_UNKNOWN = 'U'
  };

  enum SmiErrCond   {
    SMI_CONTROLLER_ERROR = 0,
    SMI_CONTROLLER_SUCCESS = 1,
    LAST
  };


}      //  End namespace
#endif //  ONLINE_FINITESTATEMACHINE_CONFIG_H
