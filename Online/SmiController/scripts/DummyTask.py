"""
        Package module

        \author  M.Frank
        \version 1.0

"""
from __future__ import print_function
from builtins import str
from builtins import range
from builtins import object
import os
import sys
import time
import copy
import pydim
import threading


VERBOSE         = 1
DEBUG           = 2
INFO            = 3
WARNING         = 4
ERROR           = 5
ALWAYS          = 6

global minPrintLevel
minPrintLevel = INFO

utgid = ''
if 'UTGID' in os.environ: 
  utgid = os.environ['UTGID']+': '

# ------------------------------------------------------------------------------
def log(level,msg):
  """
  
  \author   M.Frank
  \version  1.0
  \date     30/06/2002
  """
  import sys
  try:
    f = sys.stdout
    if level < minPrintLevel:
      return
    elif level == DEBUG:
      print('DEBUG   %-24s %s'%(utgid, msg, ), file=f)
    elif level == INFO:
      print('INFO    %-24s %s'%(utgid, msg, ), file=f)
    elif level == WARNING:
      print('WARNING %-24s %s'%(utgid, msg, ), file=f)
    elif level == ERROR:
      print('ERROR   %-24s %s'%(utgid, msg, ), file=f)
    elif level == ALWAYS:
      print('SUCCESS %-24s %s'%(utgid, msg, ), file=f)
    elif level >  ALWAYS:
      print('INFO    %-24s %s'%(utgid, msg, ), file=f)
    f.flush()
  except Exception as X:
    print('Exception failure: '+str(X))

#----------------------------------------------------------------------------------------
class Monitor(object):
  def __init__(self, partitionid):
    self.done   = int(time.time())
    self.last   = int(time.time())
    self.pid    = os.getpid()
    self.partID = partitionid

#----------------------------------------------------------------------------------------
class FSM(object):
  """
    No need to take locks. Everything is protected by the global DIM lock.
  """
  ST_OFFLINE    = 'OFFLINE'
  ST_UNKNOWN    = 'UNKNOWN'
  ST_NOT_READY  = 'NOT_READY'
  ST_READY      = 'READY'
  ST_RUNNING    = 'RUNNING'
  ST_STOPPED    = 'READY'
  ST_PAUSED     = 'PAUSED'
  ST_ERROR      = 'ERROR'
  Meta = {ST_UNKNOWN: 'U', ST_NOT_READY: 'N', ST_READY: 'r', ST_RUNNING: 'R', ST_STOPPED: 'r', ST_PAUSED: 'r', ST_ERROR: 'E'}  
  CMD_CONFIGURE = 'configure'
  CMD_START     = 'start'
  CMD_STOP      = 'stop'
  CMD_RESET     = 'reset'
  CMD_CONTINUE  = 'continue'
  CMD_PAUSE     = 'pause'
  CMD_LOAD      = 'load'
  CMD_UNLOAD    = 'unload'
  CMD_START_TRIGGER = 'start_trigger'
  CMD_STOP_TRIGGER = 'stop_trigger'
  
  CMD_GET_STATE = '!state'
  CMD_FORCE_RESET = 'RESET'

  #--------------------------------------------------------------------------------------
  def __init__(self, name, partitionid=0, auto=False):
    self.monitor  = Monitor(partitionid)
    self.__name   = name
    self.__auto   = auto
    self.__keep   = True
    self.__state  = FSM.ST_NOT_READY
    self.previousState = FSM.ST_UNKNOWN
    self.__cmdID  = pydim.dis_add_cmnd(name,'C',self.callback,1)
    self.__svcID  = pydim.dis_add_service(name+'/status', 'C', self.service, 1)
    self.__fsmID  = pydim.dis_add_service(name+'/fsm_status','L:2;I:1;C:4;I:1',self.fsm_service, 4)
    log(INFO,'DIM Command starting.... ')
    pydim.dis_update_service(self.__svcID, (self.__state,))
    pydim.dis_update_service(self.__fsmID, self.fsm_service(self))
    log(INFO,'DIM Command starting....Done ')

  #--------------------------------------------------------------------------------------
  def state(self):
    return self.__state

  #--------------------------------------------------------------------------------------
  def keepAlive(self):
    return self.__keep

  #--------------------------------------------------------------------------------------
  def start(self):
    log(INFO,"Start serving DIM....")
    pydim.dis_start_serving(self.__name)
    ## self.handleCommand(FSM.CMD_LOAD)
    if self.__auto:
      self.handleCommand(FSM.CMD_CONFIGURE)
      self.handleCommand(FSM.CMD_START)

  #--------------------------------------------------------------------------------------
  def service(self,tag):
    return self.__state

  #--------------------------------------------------------------------------------------
  def inState(self, states):
    if type(states) is list:
      for s in states: 
        if self.__state == s: return True
    elif type(states) is tuple:
      for s in states: 
        if self.__state == s: return True
    elif type(states) is set:
      for s in states: 
        if self.__state == s: return True
    elif self.__state == states:
      return True
    return False

  #--------------------------------------------------------------------------------------
  def fsm_service(self,tag):
    meta = FSM.Meta[self.__state]
    self.monitor.metastate = meta+meta+'S'+' '
    self.monitor.last = self.monitor.done
    self.monitor.done = int(time.time())
    result = (int(self.monitor.last), 
              int(self.monitor.done), 
              int(self.monitor.pid), 
              self.monitor.metastate,
              int(self.monitor.partID),)
    log(INFO,'+++ Publish FSM state: %-16s metastate: %s'%(str(self.__state),str(result),))
    return result

  def _setState(self, new_state):
    self.previousState = self.__state
    self.__state = new_state

  #--------------------------------------------------------------------------------------
  def handleCommand(self,cmd):
    self.monitor.last = int(time.time())
    log(INFO,'+++ Handle DIM Command: '+str(cmd))
    sys.stdout.flush()
    cb = 'handle'+cmd[0].upper()+cmd[1:]
    if hasattr(self,cb):
      if not getattr(self,cb)():
        self.state = FSM.ST_ERROR
        if hasattr(self,'on'+FSM.ST_ERROR):
          getattr(self,'on'+FSM.ST_ERROR)()
        cmd = ''

    if cmd == FSM.CMD_CONFIGURE:
      self._setState(FSM.ST_READY)
    elif cmd == FSM.CMD_START:
      self._setState(FSM.ST_RUNNING)
    elif cmd == FSM.CMD_STOP:
      self._setState(FSM.ST_STOPPED)
    elif cmd == FSM.CMD_RESET:
      self._setState(FSM.ST_NOT_READY)
    elif cmd == FSM.CMD_CONTINUE:
      self._setState(FSM.ST_RUNNING)
    elif cmd == FSM.CMD_PAUSE:
      self._setState(FSM.ST_PAUSED)
    elif cmd == FSM.CMD_LOAD:
      self._setState(FSM.ST_NOT_READY)
    elif cmd == FSM.CMD_UNLOAD:
      self._setState(FSM.ST_UNKNOWN)
      self.__keep = False
    elif cmd == FSM.CMD_FORCE_RESET:
      self._setState(FSM.ST_UNKNOWN)
      self.__keep = False
    elif cmd == FSM.CMD_GET_STATE:
      pass
    elif cmd == FSM.CMD_STOP_TRIGGER:
      return
    elif cmd == FSM.CMD_START_TRIGGER:
      return
    else:
      self._setState(FSM.ST_ERROR)

    if hasattr(self,'on'+self.__state):
      getattr(self,'on'+self.__state)()

    pydim.dis_update_service(self.__svcID,(self.__state,))
    if self.__fsmID != 0: pydim.dis_update_service(self.__fsmID)


  #--------------------------------------------------------------------------------------
  def callback(self, *args):
    log(DEBUG,'++ FSM status callback. Args are %s'%str(args))
    r = args[0][0]
    r = r[:r.find('\0')]
    self.handleCommand(r)

def runVoidTask(name=None,auto=False):
  global minPrintLevel
  utgid = ''
  partition = 0x103
  if name is not None: utgid = name
  for i in range(len(sys.argv)):
    a = sys.argv[i]
    if a[:1] == '-a': auto = True
    if a[:2] == '-u': utgid = sys.argv[i+1]
    if a[:4] == '-par': partition = int(sys.argv[i+1])
    if a[:4] == '-pri': minPrintLevel = int(sys.argv[i+1])
    if a[:4] == '-sle': time.sleep(60)

  if not len(utgid) and 'UTGID' in os.environ:
    utgid = os.environ['UTGID']
  log(INFO,"FSM task: utgid:  %s   auto: %s  print level: %d"%(utgid,str(auto), minPrintLevel,))
  fsm = FSM(name=utgid, partitionid=partition, auto=auto)
  pydim.dis_start_serving(utgid)
  fsm.start()
  while fsm.keepAlive():
    time.sleep(1)

if __name__ == "__main__":
  import pdb
  #pdb.set_trace()
  runVoidTask()
