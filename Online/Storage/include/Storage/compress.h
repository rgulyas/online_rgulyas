//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_COMPRESS_H
#define ONLINE_STORAGE_COMPRESS_H

// Framework include files

// C/C++ include files
#include <vector>
#include <string>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {

    /// Namespace for the compression utilities
    namespace compress   {

      std::vector<unsigned char> to_vector(const char* data);
      std::vector<unsigned char> to_vector(const std::string& data);

      /// decompress a byte buffer
      std::vector<unsigned char> decompress(const std::string& content_encoding, 
					    const std::vector<unsigned char>& data);


      /// compress a byte buffer
      std::vector<unsigned char> compress(const std::string& accepted_encoding, 
					  const std::vector<unsigned char>& data,
					  std::string& used_encoding);

      /// Encode byte array using base64 encoding
      std::string base64_encode(const unsigned char* bytes_to_encode, unsigned int in_len);

      /// Decode base64 encoded string to byte array
      std::vector<unsigned char> base64_decode(const std::string& encoded_string);

    }   // End namespace compress
  }     // End namespace storage
}       // End namespace Online
#endif  // ONLINE_STORAGE_COMPRESS_H
