//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Setup.h"
#include <RTL/Logger.h>
#include <Storage/communication.h>

// C/C++ include files
#include <fstream>
#include <iostream>
#include <filesystem>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

using namespace std;
using namespace Online::storage;
using namespace Online::fdb_test;

Setup::Setup(const std::string& n, int argc, char* argv[], void (*help)(int,char**))
  : name(n)
{
  void (*hlp)(int,char**) = [](int, char**) {};
  RTL::CLI cli(argc, argv, help ? help : hlp);

  server = RTL::nodeName()+":80";
  cli.getopt("check",      4, check_file);
  cli.getopt("server",     4, server);
  cli.getopt("url",        3, url);
  cli.getopt("file",       4, file);
  cli.getopt("print",      4, prt);
  cli.getopt("debug",      4, debug);
  RTL::Logger::install_log(RTL::Logger::log_args(prt));
  ::lib_rtl_output(LIB_RTL_DEBUG,"%s: Test starting....", name.c_str());
}

Setup::~Setup()   {
  ::lib_rtl_output(LIB_RTL_DEBUG,"%s: Test finished!", name.c_str());
}

fdb_client Setup::client()    {
  fdb_client client(0);
  uri_t theurl(this->server);
  client.fdbclient     = client::create<client::sync>(theurl.host, theurl.port, 10000, debug);
  return client;
}

Setup& Setup::print(reply_t& reply)    {
  if ( reply.content.size() < 1024 )   {
    reply.content.push_back(0);
    reply.print(cout, this->name+": ");
  }
  return *this;
}

Setup& Setup::check(const reply_t& reply)    {
  if ( !this->check_file.empty() )   {
    check_reply(this->check_file, reply);
  }
  return *this;
}
    
vector<unsigned char> Setup::read_file(const string& fname)   {
  struct stat stat;
  int ret_stat = ::stat(fname.c_str(), &stat);
  if ( -1 == ret_stat )   {
    int err = errno;
    ::lib_rtl_output(LIB_RTL_ERROR, "%s: Cannot access file: %s [%s]",
		     name.c_str(), fname.c_str(), 
		     make_error_code(errc(err)).message().c_str());
    exit(err);
  }
  vector<unsigned char> data;
  size_t num_bytes = 0, num_total = stat.st_size;
  data.resize(stat.st_size);
  ifstream in(fname.c_str(), std::ifstream::in);
  while( in.good() && num_bytes < num_total )  {
    int len = in.readsome((char*)&data.at(0)+num_bytes, num_total-num_bytes);
    if ( len > 0 ) num_bytes += len;
  }
  if ( num_bytes != num_total )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "%s: Failed to read file: %s Got %ld out of %ld bytes",
		     name.c_str(), fname.c_str(), num_bytes, num_total);
    exit(EINVAL);
  }
  return data;
}
  
size_t Setup::write_file(const reply_t& reply)    {
  int fid = ::open(file.c_str(), O_LARGEFILE | O_WRONLY | O_CREAT, S_IRWXG | S_IRWXU);
  if ( -1 == fid )    {
    auto ec = make_error_code(errc(errno));
    ::lib_rtl_output(LIB_RTL_INFO,
		     "%s: CANNOT open output file %s  [%s]",
		     name.c_str(), file.c_str(), ec.message().c_str());
    return -1;
  }
  const unsigned char* p = (const unsigned char*)&reply.content[0];
  size_t len = long(reply.content.size());
  size_t tmp = len;
  while (tmp > 0)  {
    long sc = ::write(fid, p + len - tmp, tmp);
    if (sc > 0)
      tmp -= sc;
    else
      break;
  }
  ::close(fid);
  if ( tmp == 0 )   {
    ::lib_rtl_output(LIB_RTL_INFO,
		     "%s: Successfully downloaded %ld bytes from %s to file:%s",
		     name.c_str(), len, url.c_str(), file.c_str());
  }
  else  {
    ::lib_rtl_output(LIB_RTL_INFO,
		     "%s: FAILED to download %ld / %ld bytes from %s to file:%s",
		     name.c_str(), len-tmp, len, url.c_str(), file.c_str());
  }
  return len-tmp;
}

long Setup::check_reply(const string& thefile, const reply_t& reply)   {
  long ret = 0;
  if ( !this->check_file.empty() )   {
    size_t num_byte = 0;
    const unsigned char* p = &reply.content.at(0), *q = p + reply.content.size();
    ifstream in(this->check_file.c_str(), std::ifstream::in);
    while( in.good() && p < q )  {
      int c = in.get();
      if ( c != *p )   {
	++ret;
	if ( ret == 0 )   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "%s: Buffer diffs at byte %ld got: %02X expected: %02X",
			   name.c_str(), long(q-p), *p, c);
	}
      }
      ++p;
      ++num_byte;
    }
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "%s: Checked %ld / %ld data against file: %s  %ld errors",
		     name.c_str(), num_byte, reply.bytes_total, thefile.c_str(), ret);
  }
  return ret;
}
