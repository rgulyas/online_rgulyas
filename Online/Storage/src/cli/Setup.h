//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef STORAGE_FDB_TEST_SETUP_H
#define STORAGE_FDB_TEST_SETUP_H 1

// Framework inclde files
#include <Storage/fdb_client.h>
#include <RTL/rtl.h>

namespace Online  {

  namespace fdb_test  {

    class Setup   {
    public:
      typedef Online::storage::reply_t     reply_t;
      typedef Online::storage::fdb_client  fdb_client;

      std::string server, log, url, file, name, check_file;
      int         prt = LIB_RTL_INFO;
      int         debug = 0;

    public:
      Setup(const std::string& n, int argc, char* argv[], void (*help)(int,char**)=nullptr);
      ~Setup();
      fdb_client client();
      Setup& print(reply_t& reply);
      Setup& check(const reply_t& reply);
      std::vector<unsigned char> read_file(const std::string& fname);
      std::size_t write_file(const reply_t& reply);
      long check_reply(const std::string& file, const reply_t& reply);
    };
  }
}
#endif // STORAGE_FDB_TEST_SETUP_H
