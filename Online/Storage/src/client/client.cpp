//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Storage/client.h>
#include <RTL/rtl.h>

// C/C++ header files
#include <sstream>

using namespace Online::storage;

/// Initializing constructor
client::client(const std::string& h, const std::string& p, int tmo, int dbg)
  : host(h), port(p), timeout(tmo), debug(dbg)
{
}

/// Access name
std::string client::name()  const   {
  return host+':'+port;
}

/// Build generic HTTP request
std::string client::build_request(const std::string&           func,
				  const std::string&           url,
				  const std::vector<header_t>& headers)   const
{
  std::stringstream rq;
  rq << func << " "    << url << " HTTP/1.1\r\n"
     << "User-Agent: " << "FDB-1.0\r\n"
     << "Host: "       << RTL::nodeName() << "\r\n";
  for(const auto& h : headers)
    rq << h.name << ": " << h.value << "\r\n";
  for(const auto& h : user_headers)
    rq << h.name << ": " << h.value << "\r\n";
  rq << "\r\n";
  return rq.str();
}
