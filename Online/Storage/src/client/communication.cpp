//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Storage/communication.h>
#include <HTTP/Asio.h>

// C/C++ include files
#include <iostream>

namespace Online   {

  namespace storage   {

    static bool s_error_check = true;

    bool error_check_enable(bool value)    {
      bool tmp = s_error_check;
      s_error_check = value;
      return tmp;
    }

    bool error_code_ok(const boost::system::error_code& ec, bool debug)    {
      if ( !ec )   {
	return true;
      }
      if ( debug && s_error_check )   {
	std::cout << "Bad error code["
		  << ec.value() << "]  ("
		  << ec.category().name() << ") "
		  << ec.message() << std::endl;
      }
      return false;
    }

    bool error_code_ok(const std::error_code& ec, bool debug)    {
      if ( !ec )   {
	return true;
      }
      if ( debug && s_error_check )   {
	std::cout << "Bad error code["
		  << ec.value() << "]  ("
		  << ec.category().name() << ") "
		  << ec.message() << std::endl;
      }
      return false;
    }
  }
}
