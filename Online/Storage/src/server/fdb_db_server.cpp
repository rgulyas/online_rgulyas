//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/HttpReply.h>
#include <Storage/fdb_dbase.h>
#include <Storage/fdb_server.h>
#include <Storage/communication.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

// C/C++ include files

class Online::storage::db::traits {
public:
  typedef fdb_dbase_t         dbase_t;
  typedef fdb_dbase_monitor_t monitor_t;
};

using namespace std;
using namespace Online::storage;

static constexpr const double kByte = 1024e0;
static constexpr const double MByte = kByte*kByte;

static inline string object_name(const string& obj, string opt="")   {
  static const string  PATTERN_NEXT = "/next?prefix=";
  bool get_next = (0 == ::strncasecmp(obj.c_str(),
				      PATTERN_NEXT.c_str(),
				      PATTERN_NEXT.length())); 
  if ( get_next )  {
    return obj.substr(PATTERN_NEXT.length())+opt;
  }
  return obj;
}

/// Standard constructor
fdb_dbase_monitor_t::fdb_dbase_monitor_t()   {
  std::string nam = "/"+RTL::str_upper(RTL::nodeNameShort())+"/"+RTL::processName();
  add( ::dis_add_service((nam+"/DATA").c_str(),"C",&data,sizeof(data),0,0) );
  add( ::dis_add_service((nam+"/GET").c_str(),"L",
			 &data.num_get_success,sizeof(data.num_get_success),0,0) );
  add( ::dis_add_service((nam+"/GET_errors").c_str(),"L",
			 &data.num_get_errors,sizeof(data.num_get_errors),0,0) );
  add( ::dis_add_service((nam+"/GET_not_found").c_str(),"L",
			 &data.num_get_not_found,sizeof(data.num_get_not_found),0,0) );
  add( ::dis_add_service((nam+"/GET_unauthorized").c_str(),"L",
			 &data.num_get_unauthorized,sizeof(data.num_get_unauthorized),0,0) );

  add( ::dis_add_service((nam+"/DEL").c_str(),"L",
			 &data.num_del_success,sizeof(data.num_del_success),0,0) );
  add( ::dis_add_service((nam+"/DEL_errors").c_str(),"L",
			 &data.num_del_errors,sizeof(data.num_del_errors),0,0) );
  add( ::dis_add_service((nam+"/DEL_not_found").c_str(),"L",
			 &data.num_del_not_found,sizeof(data.num_del_not_found),0,0) );
  add( ::dis_add_service((nam+"/DEL_unauthorized").c_str(),"L",
			 &data.num_del_unauthorized,sizeof(data.num_del_unauthorized),0,0) );

  add( ::dis_add_service((nam+"/PUT").c_str(),"L",
			 &data.num_put_success,sizeof(data.num_put_success),0,0) );
  add( ::dis_add_service((nam+"/PUT_errors").c_str(),"L",
			 &data.num_put_errors,sizeof(data.num_put_errors),0,0) );
  add( ::dis_add_service((nam+"/PUT_bad_request").c_str(),"L",
			 &data.num_put_bad_request,sizeof(data.num_put_bad_request),0,0) );
  add( ::dis_add_service((nam+"/PUT_unauthorized").c_str(),"L",
			 &data.num_put_unauthorized,sizeof(data.num_put_unauthorized),0,0) );

  add( ::dis_add_service((nam+"/UPDA").c_str(),"L",
			 &data.num_upda_success,sizeof(data.num_upda_success),0,0) );
  add( ::dis_add_service((nam+"/UPDA_errors").c_str(),"L",
			 &data.num_upda_errors,sizeof(data.num_upda_errors),0,0) );
  add( ::dis_add_service((nam+"/UPDA_bad_request").c_str(),"L",
			 &data.num_upda_bad_request,sizeof(data.num_upda_bad_request),0,0) );
}

/// Default destructor
fdb_dbase_monitor_t::~fdb_dbase_monitor_t()   {
  for( auto svc : services ) ::dis_remove_service(svc);
}


/// Standard destructor
template <> http::basic_http_server<db>::handler_t::~handler_t()   {
}

/// Specific handler for GET requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_get(const request_t& req, reply_t& rep)    {
  size_t length = 0;
  string date, access_name, obj = object_name(req.uri);
  auto* hdr_location = req.header(http::constants::location);
  
  error_code ec;  {
    dbase_t::lock_t lck(dbase.get());
    ec = dbase->query_object(req.uri, access_name, date, length, hdr_location != nullptr);
  }
  if ( ec == std::errc::no_such_file_or_directory )   {
    rep = reply_t::stock_reply(reply_t::not_found);
    ++monitor->data.num_get_not_found;
  }
  else if ( ec == std::errc::permission_denied )  {
    rep = reply_t::stock_reply(reply_t::unauthorized);
    ++monitor->data.num_get_unauthorized;
  }
  else if ( !error_code_ok(ec, this->debug) )  {
    rep = reply_t::stock_reply(reply_t::not_found);
    ++monitor->data.num_get_not_found;
  }
  if ( !error_code_ok(ec, this->debug) )   {
    header_t h(http::constants::error_cause,"Failed to get "+req.uri+" ["+ec.message()+"]");
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %-6s %s %-20s = %s",
		     req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		     h.name.c_str(), h.value.c_str());
    rep.headers.emplace_back(move(h));
    ++monitor->data.num_get_errors;
  }
  else   {
    bool mb = length > 3*MByte;
    rep = reply_t::stock_reply(reply_t::permanent_redirect);
    rep.headers.emplace_back(http::constants::location,access_name);
    ::lib_rtl_output(LIB_RTL_INFO,"+++ %-6s '%s' %.1f %cB [%s]",
		     req.method.c_str(), access_name.c_str(),
		     double(length)/(mb ? MByte : kByte), mb ? 'M' : 'k',
		     req.remote_address().to_string().c_str());
    ++monitor->data.num_get_success;
  }
  return write;
}

/// Specific handler for DELETE requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_delete(const request_t& req, reply_t& rep)    {
  size_t length = 0;
  string date, access_name, obj = object_name(req.uri,"%");
  auto* hdr_location = req.header(http::constants::location);

  std::error_code ec;  {
    dbase_t::lock_t lck(dbase.get());
    ec = dbase->delete_object(obj, access_name, date, length, hdr_location != nullptr);
  }
  if ( ec == std::errc::no_such_file_or_directory )   {
    rep = reply_t::stock_reply(reply_t::not_found);
    ++monitor->data.num_del_not_found;
  }
  else if ( ec == std::errc::permission_denied )   {
    rep = reply_t::stock_reply(reply_t::unauthorized);
    ++monitor->data.num_del_unauthorized;
  }
  else if ( !error_code_ok(ec) )   {
    rep = reply_t::stock_reply(reply_t::not_found);
    ++monitor->data.num_del_not_found;
  }
  if ( !error_code_ok(ec, this->debug) )   {
    header_t h(http::constants::error_cause,"Failed to delete "+req.uri+" ["+ec.message()+"]");
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %-6s %s %-20s = %s",
		     req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		     h.name.c_str(), h.value.c_str());
    rep.headers.emplace_back(move(h));
    ++monitor->data.num_del_errors;
  }
  else   {
    bool mb = length > 3*MByte;
    rep = reply_t::stock_reply(reply_t::permanent_redirect);
    rep.headers.emplace_back(http::constants::location,access_name);
    ::lib_rtl_output(LIB_RTL_INFO,"+++ %-6s '%s' %.1f %cB [%s]",
		     req.method.c_str(), obj.c_str(),
		     double(length)/(mb ? MByte : kByte), mb ? 'M' : 'k',
		     req.remote_address().to_string().c_str());
    ++monitor->data.num_del_success;
  }
  return write;
}

/// Specific handler for PUT requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_put(const request_t& req, reply_t& rep)    {
  const header_t *hdr_date, *hdr_len, *hdr_location;
  hdr_location = req.header(http::constants::location);
  if ( (hdr_date=req.header(http::constants::date)) == nullptr )   {
    header_t h(http::constants::error_cause,"Missing date header");
    rep = reply_t::stock_reply(reply_t::bad_request);
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %-6s bad_request %-20s = %s",
		     req.method.c_str(), h.name.c_str(), h.value.c_str());
    ++monitor->data.num_put_bad_request;
    rep.headers.emplace_back(move(h));
  }
  else if ( (hdr_len=req.header(http::constants::content_length)) == nullptr )   {
    header_t h(http::constants::error_cause,"Missing content length header");
    rep = reply_t::stock_reply(reply_t::bad_request);
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %-6s bad_request %-20s = %s",
		     req.method.c_str(), h.name.c_str(), h.value.c_str());
    ++monitor->data.num_put_bad_request;
    rep.headers.emplace_back(move(h));
  }
  else   {
    const string&   date = hdr_date->value;
    size_t          len  = hdr_len->as<size_t>();
    string          access_name;
    std::error_code ec;  {
      dbase_t::lock_t lck(dbase.get());
      ec = dbase->add_object(req.uri, date, len, access_name, hdr_location != nullptr);
    }
    if ( ec == std::errc::permission_denied )  {
      header_t h(http::constants::error_cause,"Failed to add "+req.uri+" "+ec.message());
      rep = reply_t::stock_reply(reply_t::unauthorized);
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ %-6s %s  %-20s = %s",
		       req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		       h.name.c_str(), h.value.c_str());
      ++monitor->data.num_put_unauthorized;
      rep.headers.emplace_back(move(h));
    }
    else if ( !error_code_ok(ec, this->debug) )   {
      header_t h(http::constants::error_cause,
		 "Failed to add object "+req.uri+" in dbase: "+ec.message());
      rep = reply_t::stock_reply(reply_t::bad_request);
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ %-6s %s  %-20s = %s",
		       req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		       h.name.c_str(), h.value.c_str());
      ++monitor->data.num_put_bad_request;
      rep.headers.emplace_back(move(h));
    }
    else   {
      bool mb = len > 3*MByte;
      std::string remote = "UNKNOWN-ERROR";
      try  {
	remote = req.remote_address().to_string();
      }
      catch (...) {
      }
      rep = reply_t::stock_reply(reply_t::permanent_redirect);
      rep.content.clear();
      rep.headers.clear();
      rep.headers.emplace_back(http::constants::location,access_name);
      rep.headers.emplace_back(http::constants::data_length,len);
      rep.headers.emplace_back(http::constants::date,date);
      ::lib_rtl_output(LIB_RTL_INFO,"+++ %-6s '%s' %.1f %cB [%s]",
		       req.method.c_str(), req.uri.c_str(),
		       double(len)/(mb ? MByte : kByte), mb ? 'M' : 'k',
		       remote.c_str());
      ++monitor->data.num_put_success;
      return write;
    }
  }
  ++monitor->data.num_put_errors;
  return write;
}

/// Specific handler for POST requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_update(const request_t& req, reply_t& rep)    {
  const header_t *hdr_state;
  if ( (hdr_state=req.header(http::constants::state)) == nullptr )   {
    header_t h(http::constants::error_cause,"Missing state header");
    rep = reply_t::stock_reply(reply_t::bad_request);
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %-6s bad_request %-20s = %s",
		     req.method.c_str(), h.name.c_str(), h.value.c_str());
    ++monitor->data.num_upda_bad_request;
    rep.headers.emplace_back(move(h));
  }
  else   {
    std::error_code ec;  {
      dbase_t::lock_t lck(dbase.get());
      ec = dbase->update_object_state(req.uri, hdr_state->value);
    }
    if ( !error_code_ok(ec, this->debug) )   {
      header_t h(http::constants::error_cause,
		 "Failed to update object "+req.uri+" in dbase: "+ec.message());
      rep = reply_t::stock_reply(reply_t::bad_request);
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ %-6s %s  %-20s = %s",
		       req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		       h.name.c_str(), h.value.c_str());
      ++monitor->data.num_upda_errors;
      rep.headers.emplace_back(move(h));
    }
    else   {
      ++monitor->data.num_upda_success;
      rep = reply_t::stock_reply(reply_t::ok);
      rep.content.clear();
      rep.headers.clear();
    }
  }
  return write;
}

/// Handle a request and produce a reply.
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_request(const request_t& req, reply_t& rep)    {
  if ( 0 == rep.bytes_sent )   {
    auto ret = this->HttpRequestHandler::handle_request(req, rep);
    if ( this->debug > 0 )   {
      string status = reply_t::stock_status(rep.status);
      ::lib_rtl_output(LIB_RTL_INFO,"basic_http_server<db>::handle: Serving %s %s status: %d [%s]",
		       req.method.c_str(), req.uri.c_str(), rep.status, status.c_str());
      for ( const auto& h : rep.headers )
	::lib_rtl_output(LIB_RTL_INFO,"basic_http_server<db>::handle: %-20s = %s",
			 h.name.c_str(), h.value.c_str());
      for ( const auto& h : rep.userHeaders )
	::lib_rtl_output(LIB_RTL_INFO,"basic_http_server<db>::handle: %-20s = %s",
			 h.name.c_str(), h.value.c_str());
    }
    return ret;
  }
  return close;
}

/// Namespace for the http server and client apps
namespace http   {
  template class basic_http_server<Online::storage::db>;
}
