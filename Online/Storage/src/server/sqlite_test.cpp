//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
/*
gentest libStorageClientTest.so sqlite_create_database -database=sqlite_test.dbase

gentest libStorageClientTest.so sqlite_populate_database_direct -database=sqlite_test.dbase -stream=HLT1 -run=11223344 -count=100
gentest libStorageClientTest.so sqlite_populate_database_direct -database=sqlite_test.dbase -stream=HLT1 -run=11223345 -count=100
gentest libStorageClientTest.so sqlite_populate_database_direct -database=sqlite_test.dbase -stream=HLT1 -run=11223346 -count=100

gentest libStorageClientTest.so sqlite_update_database_direct   -database=sqlite_test.dbase -stream=HLT1 -run=11223346 -count=100

gentest libStorageServer.so fdb_cli_dumpdb -database=/home/frankm/storage_files.dbase
*/

// Framework inclde files
#include <sqldb/sqlite.h>
#include <HTTP/HttpHeader.h>  
#include <RTL/Logger.h>
#include "fdb_sqldb.h"

// C/C++ include files
#include <sstream>
#include <fstream>
#include <iostream>

using namespace std;
using namespace sqldb;

//  
//
//==========================================================================
namespace {
  
  struct Setup   {
    string name, database, stream, entry, match;
    size_t count = 0;
    long   run = 0;
    int    prt = LIB_RTL_INFO;

    Setup(const std::string& n, int argc, char* argv[],
	  void (*help)(int argc, char** argv)=nullptr)
      : name(n)
    {
      void (*hlp)(int,char**) = [](int, char**) {};
      RTL::CLI cli(argc, argv, help ? help : hlp);
      cli.getopt("database", 8, database);
      cli.getopt("stream",   5, stream);
      cli.getopt("count",    5, count);
      cli.getopt("entry",    5, entry);
      cli.getopt("run",      3, run);
      cli.getopt("print",    4, prt);
      cli.getopt("match",    4, match);
      RTL::Logger::install_log(RTL::Logger::log_args(prt));
      ::lib_rtl_output(LIB_RTL_ALWAYS,"%s: Test starting....", name.c_str());
    }
    ~Setup()   {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"%s: Test finished!", name.c_str());
    }
    sqldb::database db()  const    {
      sqldb::database d;
      d.open<sqlite>(this->database);
      if ( d.intern->db == nullptr )    {
	::lib_rtl_output(LIB_RTL_ERROR,"%s: Can't open database [%s]",
			 name.c_str(), d.errmsg().c_str());
      }
      return d;
    }
  };
}
//  
//
//==========================================================================
extern "C" int sqlite_create_database(int argc, char** argv)    {
  Setup setup("sqlite_create_database", argc, argv);
  auto db = setup.db();
  std::string error;
  const char* sql = "CREATE TABLE IF NOT EXISTS Files ("
    "Name   TEXT PRIMARY KEY, "
    "State  INT  NOT NULL, "
    "Size   INT  NOT NULL, "
    "Date   TEXT NOT NULL, "
    "Host   TEXT NOT NULL)";
  if ( db.execute(error, sql) != sqldb::OK )   {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: failed to execute %s :  %s",
		     db.name(), sql, error.c_str());
  }
  db.commit();
  db.close();
  return 0;
}
//  
//
//==========================================================================
extern "C" int sqlite_populate_database(int argc, char** argv)    {
  Setup setup("sqlite_populate_database", argc, argv);
  auto db = setup.db();
  try {
    sqldb::statement insert;
    insert.prepare(db,
		   "INSERT INTO Files  ('Name', 'State', 'Size', 'Date', 'Host') "
		   "VALUES ( ? , ? , ? , ? , ? )");
    for(size_t i=0; i<setup.count; ++i)    {
      std::string date   = http::HttpHeader::now();
      std::string host   = "http://127.0.0.1:8100";
      size_t      length = 4*1024;
      char        obj[1024];

      ::snprintf(obj, sizeof(obj), "/objects/%08ld/%s/%010ld_%08ld.raw",
		 setup.run, setup.stream.c_str(), setup.run, i);
      insert.reset();
      insert.bind(1, obj);
      insert.bind(2, 0);
      insert.bind(3, length);
      insert.bind(4, date);
      insert.bind(5, host);
      int ret = insert.execute();
      if ( ret != sqldb::OK )    {
	::lib_rtl_output(LIB_RTL_ERROR,"%s: failed INSERT: %s",
			 db.name(), insert.errmsg().c_str());
	return EINVAL;
      }
      insert.reset();
    }
    insert.finalize();
    db.commit();
    db.close();
  }
  catch(const exception& e)   {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: exception: %s",db.name(),e.what());
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int sqlite_populate_database_direct(int argc, char** argv)    {
  Setup setup("sqlite_populate_database", argc, argv);
  auto db = setup.db();
  try {
    for(size_t i=0; i<setup.count; ++i)    {
      std::string error, date = http::HttpHeader::now();
      std::string host = "http://127.0.0.1:8100";
      size_t      length = 4*1024;
      char        obj[1024];

      ::snprintf(obj, sizeof(obj), "/objects/%08ld/%s/%010ld_%08ld.raw",
		 setup.run, setup.stream.c_str(), setup.run, i);
      int ret = db.execute_sql(error,
			       "INSERT INTO Files  ('Name', 'State', 'Size', 'Date', 'Host') "
			       "VALUES ( '%s' , %d , %ld , '%s' , '%s' )",
			       obj, 0, length, date.c_str(), host.c_str());
      if ( ret != sqldb::OK )    {
	::lib_rtl_output(LIB_RTL_ERROR,"%s: failed INSERT: %s",
			 db.name(), db.errmsg().c_str());
	return EINVAL;
      }
    }
    db.commit();
    db.close();
  }
  catch(const exception& e)   {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: exception: %s", db.name(), e.what());
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int sqlite_read_database_direct(int argc, char** argv)    {
  typedef Online::storage::fdb_dbase_t::file_t file_t;
  Setup setup("sqlite_populate_database", argc, argv);
  string error;
  auto db = setup.db();
  if ( db.is_valid() )   {
    sqldb::statement query =
      sqldb::statement::create(db, "SELECT Name, State, Size, Date, Host"
			       " FROM Files"
			       " WHERE Size>%d",0);
    if ( query.is_valid() )  {
      int ret = query.execute();
      std::stringstream s;
      file_t f;
      while ( (ret = query.fetch_one()) == sqldb::OK )   {
	f.name  = query.get<std::string>(0);
	f.state = query.get<int>(1);
	f.size  = query.get<int64_t>(2);
	f.date  = query.get<std::string>(3);
	f.host  = query.get<std::string>(4);
	s << f.name << " '" << f.state << "' '" << f.size << "' '" << f.date << "' " << f.host;
	::lib_rtl_output(LIB_RTL_INFO, "TABLE: %s", s.str().c_str());      
	s.str("");
      }
    }
    db.commit();
    db.close();
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int sqlite_update_database_direct(int argc, char** argv)    {
  typedef Online::storage::fdb_dbase_t::file_t file_t;
  Setup setup("sqlite_populate_database", argc, argv);
  auto db = setup.db();
  try {
    int ret;
    string error;
    std::stringstream s;
    std::vector<file_t> files;
    sqldb::statement query =
      sqldb::statement::create(db,
			       "SELECT Name, State, Size, Date, Host"
			       " FROM Files"
			       " WHERE Size>%d",0);
    if ( query.is_valid() )  {
      file_t f;
      ret = query.execute();
      while ( (ret = query.fetch_one()) == sqldb::OK )   {
	f.name  = query.get<std::string>(0);
	f.state = query.get<int>(1);
	f.size  = query.get<int64_t>(2);
	f.date  = query.get<std::string>(3);
	f.host  = query.get<std::string>(4);
	files.push_back(f);
      }
    }
    for(size_t i=0; i<files.size(); ++i)   {
      const file_t& f = files[i];
      ret = sqldb::ERROR;
      if ( f.state == 0 )   {
	ret = db.execute_sql(error,
			     "UPDATE Files"
			     " SET State=1"
			     " WHERE Name='%s'",
			     f.name.c_str());
      }
      else if ( f.state == 1 )   {
	ret = db.execute_sql(error,
			     "UPDATE Files"
			     " SET State=0"
			     " WHERE Name='%s'",
			     f.name.c_str());
      }
      if ( ret != sqldb::OK )  {
	::lib_rtl_output(LIB_RTL_INFO, "SQL Failure: %s", error.c_str());      
      }
    }
    db.commit();
    db.close();
  }
  catch(const exception& e)   {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: exception: %s", db.name(), e.what());
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int fdb_cli_dumpdb(int argc, char** argv)    {
  typedef Online::storage::fdb_dbase_t::file_t file_t;
  Setup  setup("fdb_cli_dumpdb", argc, argv, [](int,char**)  {
    ::fprintf(stderr,
	      "fdb_cli_dumpdb -opt [-opt]                                       \n"
	      "     -database=<file-path>  Path to the SQLite file to dump.     \n"
	      "     -name=<match>          Print only files matching a pattern. \n"
	      "     -help                  Print this help.                     \n");
  });
  string error, sql;
  std::vector<file_t> files;
  sqldb::database database = setup.db();
  if ( !database.is_valid() )    {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: Can't open database [%s]",
		     setup.database.c_str(), database.errmsg().c_str());
    return EINVAL;
  }
  sql =
    "SELECT Name, State, Size, Date, Host"
    " FROM Files"
    " WHERE Size>?1";
  if ( !setup.match.empty() )
    sql += " AND Name LIKE ?2";

  sqldb::statement query = sqldb::statement::create(database, sql.c_str());
  ::lib_rtl_output(LIB_RTL_DEBUG, "SQL: %s", sql.c_str());

  if ( query.is_valid() )  {
    int ret = query.execute();
    int count = 0;
    int size = 0;
    file_t f;
    query.bind(0, size);
    if ( !setup.match.empty() ) query.bind(1, sql=setup.match+'%');
    while ( (ret = query.fetch_one()) == sqldb::OK )   {
      f.name  = query.get<std::string>(0);
      f.state = query.get<int>(1);
      f.size  = query.get<int64_t>(2);
      f.date  = query.get<std::string>(3);
      f.host  = query.get<std::string>(4);
      ::lib_rtl_output(LIB_RTL_INFO, "%4ld File: %s Host: %s %s",
		       count, f.name.c_str(), f.host.c_str(), f.date.c_str());
      ::lib_rtl_output(LIB_RTL_INFO, "           State:%d  %9ld bytes",
		       f.state, f.size);
      ++count;
    }
  }
  database.close();
  return 0;
}
