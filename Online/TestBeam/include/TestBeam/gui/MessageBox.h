//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TESTBEAM-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TESTBEAM_MESSAGEBOX_H
#define TESTBEAM_MESSAGEBOX_H

/// Framework include files
#include "TGMsgBox.h"
#include "TGPicture.h"

/// C++ include files

/// Namespace for the dimrpc based implementation
namespace testbeam  {

  /// Message Box class. TMsgBox does not work for me....
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    18.11.2018
   */
  class MessageBox {
  public:
    MessageBox(const TGWindow *p = 0, const TGWindow *main = 0,
		  const char *title = 0, const char *msg = 0, const TGPicture *icon = 0,
		  Int_t buttons = kMBDismiss, Int_t *ret_code = 0,
		  UInt_t options = kVerticalFrame,
		  Int_t text_align = kTextCenterX | kTextCenterY);
    MessageBox(const TGWindow *p, const TGWindow *main,
		  const char *title, const char *msg, EMsgBoxIcon icon,
		  Int_t buttons = kMBDismiss, Int_t *ret_code = 0,
		  UInt_t options = kVerticalFrame,
		  Int_t text_align = kTextCenterX | kTextCenterY);
    ~MessageBox();
  };
}       // End namespace testbeam
#endif  /* TESTBEAM_MESSAGEBOX_H         */
