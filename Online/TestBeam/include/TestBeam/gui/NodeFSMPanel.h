//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TESTBEAM-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TESTBEAM_NODEFSMPANEL_H
#define TESTBEAM_NODEFSMPANEL_H

/// Framework include files
#include <TestBeam/gui/TaskManager.h>
#include <TestBeam/gui/TaskHandler.h>
#include <CPP/Interactor.h>
#include <RTL/rtl.h>

/// ROOT include files
#include <TGFrame.h>

// C/C++ include files
#include <memory>
#include <mutex>

class TGGroupFrame;
class TGTextButton;
class TGComboBox;
class TGTextEntry;
class TGLabel;

/// Namespace for the TestBeam software
namespace testbeam  {

  /// Explorer of the readout clients at a given DNS node
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    18.11.2018
   */
  class NodeFSMPanel :
    public TGCompositeFrame,
    public CPP::Interactor  {
  public:
    struct Child  {
      int               id     = 0;
      TGTextEntry*      name   = nullptr;
      TGTextEntry*      state  = nullptr;
      TGTextEntry*      meta   = nullptr;
    };
    struct LineEntry  {
      TGLabel*          label  = nullptr;
      TGTextEntry*      input  = nullptr;
    };
    struct TaskEntry  {
      TGLabel*          label  = nullptr;
      TGTextEntry*      utgid  = nullptr;
      TGTextEntry*      status = nullptr;
      TGTextButton*     start  = nullptr;
      TGTextButton*     kill   = nullptr;
    };

    std::mutex          dim_protection;
    CPP::Interactor*    gui            = nullptr;
    TaskManager&        taskManager;
    TaskHandler         taskHandler;
    TGGroupFrame*       group          = nullptr;
    TGTextButton*       apply          = nullptr;
    LineEntry           maindns, dns, node, part, replace, runnumber, script, arch, numslave;
    TaskEntry           tmSrv, logSrv, logViewer, mbmmon, ctrl;
    std::vector<Child>  children;
    TGComboBox*         ctrl_command   = nullptr;
    TGLabel*            ctrl_label     = nullptr;
    TGTextEntry*        ctrl_status    = nullptr;
    TGTextButton*       ctrl_killall   = nullptr;
    TGTextButton*       ctrl_autostart = nullptr;
    TGTextButton*       ctrl_autostop  = nullptr;
    TGGroupFrame*       m_cmds_group   = nullptr;
    Pixel_t             disabled, enabled, executing, green, red, blue, yellow, white, black;

    long                m_numSlaves    = 0;
    int                 m_currentRun   = 1234;
    int                 m_autoRun      = 0;
    bool                m_applied      = false;
    int                 m_runnumber_id = 0;
    
  public:
    enum IDS {
      PART_ID_OFFSET          = 100,
      MAINDNS_LABEL           = PART_ID_OFFSET+1,
      MAINDNS_INPUT           = PART_ID_OFFSET+2,
      DNS_LABEL               = PART_ID_OFFSET+3,
      DNS_INPUT               = PART_ID_OFFSET+4,
      PART_LABEL              = PART_ID_OFFSET+5,
      PART_INPUT              = PART_ID_OFFSET+6,
      NODE_LABEL              = PART_ID_OFFSET+7,
      NODE_INPUT              = PART_ID_OFFSET+8,
      NUMSLAVE_LABEL          = PART_ID_OFFSET+9,
      NUMSLAVE_INPUT          = PART_ID_OFFSET+10,
      ARCH_LABEL              = PART_ID_OFFSET+11,
      ARCH_INPUT              = PART_ID_OFFSET+12,
      SCRIPT_LABEL            = PART_ID_OFFSET+13,
      SCRIPT_INPUT            = PART_ID_OFFSET+14,
      REPLACE_LABEL           = PART_ID_OFFSET+15,
      REPLACE_INPUT           = PART_ID_OFFSET+16,
      RUNNUMBER_LABEL         = PART_ID_OFFSET+15,
      RUNNUMBER_INPUT         = PART_ID_OFFSET+16,


      TMSRV_ID_OFFSET         = TaskManager::TMSRV_ID_OFFSET,
      TMSRV_STATUS,
      TMSRV_START,
      TMSRV_KILL,
      TMSRV_LIST,
      TMSRV_LABEL,

      LOGSRV_ID_OFFSET        = TaskManager::LOGSRV_ID_OFFSET,
      LOGSRV_STATUS,
      LOGSRV_START,
      LOGSRV_KILL,
      LOGSRV_LABEL,

      LOGVIEWER_ID_OFFSET     = TaskManager::LOGVIEWER_ID_OFFSET,
      LOGVIEWER_STATUS,
      LOGVIEWER_START,
      LOGVIEWER_KILL,
      LOGVIEWER_LABEL,

      MBMMON_ID_OFFSET        = TaskManager::MBMMON_ID_OFFSET,
      MBMMON_STATUS,
      MBMMON_START,
      MBMMON_KILL,
      MBMMON_LABEL,

      MBMDMP_ID_OFFSET        = TaskManager::MBMDMP_ID_OFFSET,
      MBMDMP_STATUS,
      MBMDMP_START,
      MBMDMP_KILL,
      MBMDMP_LABEL,

      TANSRV_ID_OFFSET        = TaskManager::TANSRV_ID_OFFSET,
      TANSRV_STATUS,
      TANSRV_START,
      TANSRV_KILL,
      TANSRV_LABEL,

      CTRL_ID_OFFSET          = TaskManager::CTRL_ID_OFFSET,
      CTRL_STATUS,
      CTRL_START,
      CTRL_KILL,
      CTRL_LABEL,
      CTRL_COMMAND,
      CTRL_AUTOSTART,
      CTRL_AUTOSTOP,

      CHECK_ENABLE_PARAM,
      NUM_ENTRIES_CHILDREN   = 30,
      LAST
    };
    /// Standard initializing constructor
    NodeFSMPanel(TGFrame* parent, CPP::Interactor* gui, TaskManager& tm);

    /// Default destructor
    virtual ~NodeFSMPanel();

    void init();
    void _configTask(TaskEntry& task);
    void updateChildren();
    void updateTaskStatus(TaskEntry& entry, const std::string& state);
    
    void runnumberChanged(const char* value);
    void architectureChanged(const char* value);
    void scriptChanged(const char* value);
    void partitionChanged(const char* value);
    void replacementChanged(const char* value);
    void hostChanged(const char* value);
    void mainDnsChanged(const char* value);
    void dnsChanged(const char* value);
    void setNumberOfSlaves(int value);
    void numslaveChanged(const char* value);
    void runInfoChanged(const char* value);
    void commandChanged(int value);

    void applyParams();
    void enableParams();

    void updateDependentValues();
    void updateCtrlCommand(std::string state);

    /// Get the controller running in auto mode
    void autoStart();
    /// Get the controller offline in auto mode
    void autoStop();

    void issueCommand(const std::string& cmd);

    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;

    /// ROOT class definition
    ClassDefOverride(NodeFSMPanel,0);
  };
}       // End namespace testbeam
#endif  /* TESTBEAM_NODEFSMPANEL_H  */
