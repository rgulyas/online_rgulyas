#!/bin/bash
# =========================================================================
#
#  Default script to start the HLT1 process on a farm node.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
cd /scratch/online/frankm/upgrade_moore/Moore;
#
. setup.x86_64-centos7-gcc9-opt.vars;
#
export PYTHONPATH=/home/frankm/upgrade_sw/Online/Online/TestBeam/options:${PYTHONPATH};
#
exec -a ${UTGID} `which python` `which gaudirun.py`  \
    $MOOREROOT/options/force_functor_cache.py        \
    $MOOREROOT/tests/options/mdf_input_and_conds.py  \
    ./add_online_changes.py                          \
    $HLT1CONFROOT/options/hlt1_pp_default.py         \
    --all-opt                                        \
    --application=OnlineEvents;
