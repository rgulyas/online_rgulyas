#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
exec -a ${UTGID} ${DATAFLOW_task} -class=Class2 -opts=../options/MonFeed.opts  ${AUTO_STARTUP} ${DEBUG_STARTUP}

