#!/bin/bash
# =========================================================================
#
#  Default script to start the passthrough process on a farm node.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
exec -a ${UTGID} `which python` `which gaudirun.py` /home/frankm/upgrade_sw/Online/gaudionline_net.py --application=Online::OnlineEventApp;


