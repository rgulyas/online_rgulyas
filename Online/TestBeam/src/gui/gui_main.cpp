//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam/gui
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <TestBeam/gui/config.h>
#include <TestBeam/gui/NodeGUI.h>
#include <TestBeam/gui/GuiMsg.h>
#include <CPP/IocSensor.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dic.h>
#include <dim/dis.h>

#include <TSystem.h>
#include <TGLayout.h>
#include <TGFrame.h>
#include <TRint.h>

// C/C++ include files
#include <iostream>
#include <memory>
#include <thread>

extern "C" void   XInitThreads();

extern "C" const char* g_task_set_name()   {
  return RTL::processName().c_str();
}

TGMainFrame* global_main_window = 0;

namespace {
  std::size_t gui_print(void* arg, int lvl, const char* fmt, va_list& args) {
    if ( lvl >= long(arg) ) {
      std::size_t result;
      std::string format;
      switch(lvl) {
      case LIB_RTL_VERBOSE:
	format = "VERBOSE ";
	break;
      case LIB_RTL_DEBUG:
	format = "DEBUG   ";
	break;
      case LIB_RTL_INFO:
	format = "INFO    ";
	break;
      case LIB_RTL_WARNING:
	format = "WARNING ";
	break;
      case LIB_RTL_ERROR:
	format = "ERROR   ";
	break;
      case LIB_RTL_FATAL:
	format = "FATAL   ";
	break;
      case LIB_RTL_ALWAYS:
	format = "ALWAYS  ";
	break;
      default:
	break;
      }
      format += fmt;
      format += "\n";
      char buffer[1024];
      result = ::vsnprintf(buffer,sizeof(buffer), format.c_str(), args);
      buffer[sizeof(buffer)-1] = 0;
      testbeam::GuiMsg("RTL: %s",buffer).send((CPP::Interactor*)arg);
      return result;
    }
    return 1;
  }
  void help_gui(int argc, char** argv)   {
    std::cout << 
      "testbeam_node_gui -opt [-opt]\n"
      "   -maindns=<node>         Main DNS to start infrastructure           \n"
      "   -dns=<node>             Working DNS node                           \n"
      "   -host=<node>            Worker node                                \n"
      "   -partition=<name>       Partition name                             \n"
      "   -architecture=<file>    Path to the architecture file              \n"
      "   -runinfo=<file>         Run-info file                              \n"
      "   -startup=<args>         Startup command (if not default controller)\n"
      "   -user<user-name>        User name used to start applications       \n"
      "   -instances=<number>     Number of parallel worker processes        \n"
      "   -controller=<string>    Controller type                            \n"
      "   -ctrl_script=<script>   Controller startup script.                 \n"
      "   -replacements=<string>  Replacement variables to interprete        \n"
      "                           the architecture file.                     \n"
      "                                                                      \n"
      "  Arguments given:"	 << std::endl;
    for(int i=0; i<argc; ++i)
      std::cout << "        " << argv[i] << std::endl;
  }
}

extern "C" void testbeam_node_gui(int argc, char** argv)   {
  std::pair<int, char**> a(0,{0});
  const char* dns_node = ::getenv("DIM_DNS_NODE");
  std::string runinfo       = "${TESTBEAMROOT}/options/OnlineEnvBase.py";
  std::string architecture  = "${TESTBEAMROOT}/options/lbDataflowArch_Monitoring.xml";
  std::string partition     = "Upgrade";
  std::string user          = "online";
  std::string controller    = CONTROLLER_TYPE;
  std::string startup, tmp, replacements;
  std::string script        = "/group/online/dataflow/scripts/runTestBeam.sh";

  std::string node          = RTL::str_upper(RTL::nodeNameShort());
  std::string dns           = RTL::str_upper(RTL::nodeNameShort());
  std::string maindns       = dns_node ? std::string(dns_node) : dns;
  int    instances = 20, max_instances = 20;

  runinfo      = gSystem->ExpandPathName(runinfo.c_str());
  architecture = gSystem->ExpandPathName(architecture.c_str());

  RTL::CLI cli(argc, argv, help_gui);
  cli.getopt("dns",          3, dns);
  cli.getopt("maindns",      4, maindns);
  cli.getopt("startup",      4, startup);
  cli.getopt("node",         2, node);
  cli.getopt("user",         2, user);
  cli.getopt("instances",    2, instances);
  cli.getopt("maxinstances", 4, max_instances);
  cli.getopt("partition",    2, partition);
  cli.getopt("runinfo",      2, runinfo);
  cli.getopt("architecture", 2, architecture);
  cli.getopt("controller",   2, controller);
  cli.getopt("ctrl_script",  9, script);
  cli.getopt("replacements", 9, replacements);

  std::cout << "Calling XInitThreads to support concurrency...." << std::endl;
  XInitThreads();
  gApplication = new TRint("TestBeamRint", &a.first, a.second);
  //::setenv("DIM_DNS_NODE",dns.c_str(),1);
  ::dic_set_dns_node(dns.c_str());
  ::dis_set_dns_node(dns.c_str());
  //                                                       geometry: x * y
  TGMainFrame *fMain = new TGMainFrame(gClient->GetRoot(), 1000, 1000);
  testbeam::NodeGUI* gui = new testbeam::NodeGUI(fMain, cli);
  gui->createMenus(gui->menuBar());
  fMain->AddFrame(gui, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));
  global_main_window = fMain;

  ::lib_rtl_install_printer(gui_print,gui);

  fMain->MapSubwindows();
  fMain->Layout();
  //                        geometry: x * y
  fMain->Resize(fMain->GetDefaultWidth(),1000);
  fMain->MapWindow();
  fMain->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");
  tmp = node + ": Readout Controller";
  fMain->SetWindowName(tmp.c_str());
  fMain->SetIconName(("NodeControl: "+RTL::nodeName()).c_str());
  fMain->SetIconPixmap("tmacro_t.xpm");
  new std::thread([] {
      ::lib_rtl_sleep(2000);
      CPP::IocSensor::instance().run();
    });
  gApplication->Run(kFALSE);
}
