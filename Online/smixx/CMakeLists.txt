#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/smixx
------------
#]=======================================================================]

add_definitions(-DPROTOCOL=1 -DMIPSEL -Df2cFortran -Dunix -Dlinux )

macro (apply_smixx_compile_opts name)
    # FIXME: we should fix the sources instead of adding directories ad hoc
    target_include_directories(${name} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include/smixx)
    target_include_directories(${name} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/commonSource)
    target_include_directories(${name} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../dim/include/dim)
    target_compile_options(${name} PUBLIC $<$<COMPILE_LANGUAGE:CXX>: -Wno-suggest-override>)
    target_compile_options(${name}
        PRIVATE
            -Wno-unused
            -Wno-format
            -Wno-format-security
            -Wno-array-bounds
            -Wno-write-strings
##		       -Wno-stringop-overflow
            $<$<COMPILE_LANGUAGE:C>:-Wno-parentheses>
            $<$<COMPILE_LANGUAGE:CXX>:-Wno-overloaded-virtual>
    )
    if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        target_compile_options(${name}
            PRIVATE
                -Wno-shadow
                -Wno-format-overflow
                -Wno-format-truncation
  	        -Wno-maybe-uninitialized
 	        -Wno-stringop-truncation
                -Wno-misleading-indentation
                $<$<COMPILE_LANGUAGE:C>:-Wno-discarded-qualifiers>
                $<$<COMPILE_LANGUAGE:CXX>:-Wno-deprecated-copy -Wno-overloaded-virtual>
		$<$<COMPILE_LANGUAGE:CXX>:-Wno-stringop-overflow>
        )
    else()
        target_compile_options(${name}
            PRIVATE
                -Wno-shadow
	        -Wno-self-assign
                -Wno-sometimes-uninitialized
                -Wno-incompatible-pointer-types
                -Wno-logical-not-parentheses
                $<$<COMPILE_LANGUAGE:C>:-Wno-header-guard -Wno-non-literal-null-conversion>
        )
        if( CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 10.0 )
            target_compile_options(${name} PRIVATE -Wno-misleading-indentation )
        endif()
    endif()
endmacro()

macro(smixx_exe name)
    cmake_parse_arguments( ARG "" "" "SOURCES;LINK" ${ARGN} )
    online_executable(${name} ${ARG_SOURCES})
    target_link_libraries(${name} PRIVATE Online::smixx_rtl Online::dim ${ARG_LINK})
    apply_smixx_compile_opts(${name})
    target_compile_options(${name} PRIVATE -Wno-parentheses -Wno-unused -Wno-sign-compare -Wno-uninitialized)
    target_compile_options(${name} PRIVATE $<$<COMPILE_LANGUAGE:CXX>: -Wno-suggest-override>)
    target_compile_options(${name} PRIVATE
            $<$<COMPILE_LANGUAGE:C>: -Wno-int-conversion -Wno-implicit-function-declaration -Wno-implicit-int>
    )
endmacro()

online_library(smixx_rtl
        src/rtl/smirtl.c
        src/rtl/smirtlcpp.cxx
        src/rtl/smiuirtl.c
        src/rtl/smiuirtlcpp.cxx
        src/commonSource/smixx_parstring_util.c)
target_link_libraries(smixx_rtl PUBLIC Online::dim PRIVATE ${CMAKE_DL_LIBS} -lrt)
apply_smixx_compile_opts(smixx_rtl)

online_library(smixx_SM
        src/commonSource/argname.cxx
        src/commonSource/errorwarning.cxx
        src/commonSource/examination_stage.cxx
        src/commonSource/indivalue.cxx
        src/commonSource/indivaluevector.cxx
        src/commonSource/name.cxx
        src/commonSource/namelist.cxx
        src/commonSource/namevector.cxx
        src/commonSource/nmdptnr.cxx
        src/commonSource/nmdptnrlist.cxx
        src/commonSource/nmdptnrvector.cxx
        src/commonSource/param.cxx
        src/commonSource/parmsbase.cxx
        src/commonSource/paroperand.cxx
        src/commonSource/ptrvector.cxx
        src/commonSource/registrar.cxx
        src/commonSource/smlline.cxx
        src/commonSource/smllinevector.cxx
        src/commonSource/utilities.cxx
        src/commonSource/varelement.cxx
        src/commonSource/varelementvector.cxx
	src/commonSource/reservednames.cxx
        src/stateManager/action.cxx
        src/stateManager/alarm.cxx
        src/stateManager/callins.cxx
        src/stateManager/clientobject.cxx
        src/stateManager/clientstate.cxx
        src/stateManager/clientwhens.cxx
        src/stateManager/commhandler.cxx
        src/stateManager/condition.cxx
        src/stateManager/createobjectins.cxx
        src/stateManager/destroyobjectins.cxx
        src/stateManager/diag.cxx
        src/stateManager/doins.cxx
        src/stateManager/forins.cxx
        src/stateManager/getvarelem.cxx
        src/stateManager/ifhandler.cxx
        src/stateManager/ifins.cxx
        src/stateManager/initiator.cxx
        src/stateManager/insertins.cxx
        src/stateManager/inslist.cxx
        src/stateManager/instruction.cxx
        src/stateManager/logic_engine.cxx
        src/stateManager/msg.cxx
        src/stateManager/objectregistrar.cxx
        src/stateManager/option.cxx
        src/stateManager/options.cxx
        src/stateManager/parms.cxx
        src/stateManager/queue_name.cxx
        src/stateManager/queue_twonames.cxx
        src/stateManager/report.cxx
        src/stateManager/reportins.cxx
        src/stateManager/reservednames_sm.cxx
        src/stateManager/resumehandler.cxx
        src/stateManager/scheduler.cxx
        src/stateManager/set_name.cxx
        src/stateManager/setins.cxx
        src/stateManager/sleepins.cxx
        src/stateManager/sleepinstimer.cxx
        src/stateManager/smiclass.cxx
        src/stateManager/smifrozenset.cxx
        src/stateManager/smiobject.cxx
        src/stateManager/smiobjectset.cxx
        src/stateManager/smiobjectsetsimple.cxx
        src/stateManager/smiobjectsetunion.cxx
        src/stateManager/smisetcontainer.cxx
        src/stateManager/smisetmember.cxx
        src/stateManager/smpcond.cxx
        src/stateManager/smpcondtyp1.cxx
        src/stateManager/smpcondtyp2.cxx
        src/stateManager/smpcondtyp3.cxx
        src/stateManager/smpcondtyp4.cxx
        src/stateManager/state.cxx
        src/stateManager/state_manager.cxx
        src/stateManager/swstay_in_state.cxx
        src/stateManager/termins.cxx
        src/stateManager/timehandling.cxx
        src/stateManager/twonames.cxx
        src/stateManager/ut_sm.cxx
        src/stateManager/waitforins.cxx
        src/stateManager/waitins.cxx
        src/stateManager/wfwcontinue.cxx
        src/stateManager/wfwmove_to.cxx
        src/stateManager/when.cxx
        src/stateManager/whenresponse.cxx)
target_include_directories(smixx_SM PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/commonSource)
target_include_directories(smixx_SM PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/stateManager)
target_link_libraries(smixx_SM PUBLIC Online::smixx_rtl)
apply_smixx_compile_opts(smixx_SM)

smixx_exe(smiSM SOURCES src/stateManager/state_manager.cxx src/commonSource/smixx_parstring_util.c
                LINK    Online::smixx_SM)

smixx_exe(smiTrans SOURCES
        src/commonSource/argname.cxx
        src/commonSource/errorwarning.cxx
        src/commonSource/examination_stage.cxx
        src/commonSource/indivalue.cxx
        src/commonSource/indivaluevector.cxx
        src/commonSource/name.cxx
        src/commonSource/namelist.cxx
        src/commonSource/namevector.cxx
        src/commonSource/nmdptnr.cxx
        src/commonSource/nmdptnrlist.cxx
        src/commonSource/nmdptnrvector.cxx
        src/commonSource/param.cxx
        src/commonSource/parmsbase.cxx
        src/commonSource/paroperand.cxx
        src/commonSource/ptrvector.cxx
        src/commonSource/registrar.cxx
	src/commonSource/reservednames.cxx
        src/commonSource/smlline.cxx
        src/commonSource/smllinevector.cxx
        src/commonSource/utilities.cxx
        src/commonSource/varelement.cxx
        src/commonSource/varelementvector.cxx
        src/translator/action.cxx
        src/translator/actionheadblock.cxx
        src/translator/attributeblock.cxx
        src/translator/attributes.cxx
        src/translator/boolitem.cxx
        src/translator/booloperation.cxx
        src/translator/callins.cxx
        src/translator/condition.cxx
        src/translator/createobjectins.cxx
        src/translator/destroyobjectins.cxx
        src/translator/doins.cxx
        src/translator/forins.cxx
        src/translator/getline_test.cxx
        src/translator/ifins.cxx
        src/translator/ifunit.cxx
        src/translator/ifunitheadblock.cxx
        src/translator/insertins.cxx
        src/translator/inslist.cxx
        src/translator/instruction.cxx
        src/translator/isofclassobject.cxx
        src/translator/main.cxx
        src/translator/objectregistrar.cxx
        src/translator/opermanager.cxx
        src/translator/parameterblock.cxx
        src/translator/parms.cxx
        src/translator/processcommandline.cxx
        src/translator/reportins.cxx
        src/translator/setins.cxx
        src/translator/sleepins.cxx
        src/translator/smiobject.cxx
        src/translator/smiobjectset.cxx
        src/translator/smiobjectsetunion.cxx
        src/translator/smlunit.cxx
        src/translator/smpcond.cxx
        src/translator/smpcondtyp1.cxx
        src/translator/smpcondtyp2.cxx
        src/translator/smpcondtyp3.cxx
        src/translator/smpcondtyp4.cxx
        src/translator/stack.cxx
        src/translator/stackitem.cxx
        src/translator/state.cxx
        src/translator/swmove_to.cxx
        src/translator/swstay_in_state.cxx
        src/translator/termins.cxx
        src/translator/ut_tr.cxx
        src/translator/waitforins.cxx
        src/translator/waitins.cxx
        src/translator/wfwcontinue.cxx
        src/translator/wfwmove_to.cxx
        src/translator/when.cxx
        src/commonSource/smixx_parstring_util.c
)
target_include_directories(smiTrans PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/translator)

smixx_exe(smiPreproc SOURCES
	src/commonSource/name.cxx
	src/commonSource/utilities.cxx
	src/commonSource/nmdptnr.cxx
        src/commonSource/nmdptnrvector.cxx
	src/commonSource/registrar.cxx
        src/commonSource/smlline.cxx
	src/commonSource/smllinevector.cxx
	#src/commonSource/reservednames.cxx
        src/preprocessor/block_name.cxx
        src/preprocessor/codeblock.cxx
        src/preprocessor/cpreproc.cxx
        src/preprocessor/generblock.cxx
        src/preprocessor/incfile.cxx
        src/preprocessor/incline.cxx
        src/preprocessor/macro.cxx
        src/preprocessor/main.cxx
        src/commonSource/smixx_parstring_util.c
)
target_include_directories(smiPreproc PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/preprocessor)

smixx_exe(smiSendCommand            SOURCES src/rtl/smi_send_command.c)
smixx_exe(smiKill                   SOURCES src/rtl/smi_kill.c)
smixx_exe(smiChangeOption           SOURCES src/rtl/smi_change_option.c)
smixx_exe(smiGen                    SOURCES src/generator/generator.c)
smixx_exe(smiMake                   SOURCES src/generator/smimake.c)
smixx_exe(smi_listDomain            SOURCES src/utilities/listdomain.cxx)
smixx_exe(smi_domainExists          SOURCES src/utilities/domainexists.cxx)
smixx_exe(smi_proxyExists           SOURCES src/utilities/proxyexists.cxx)
smixx_exe(smi_dnsDebugging          SOURCES src/utilities/dnsdebugging.cxx)
smixx_exe(smi_dnsRunning            SOURCES src/utilities/dnsRunning.cxx)
smixx_exe(smi_dnsExists             SOURCES src/utilities/dnsexists.cxx)
smixx_exe(smi_shellcmd              SOURCES src/utilities/shellcmd.c)
smixx_exe(smi_serverInError         SOURCES src/utilities/serverInError.cxx)
smixx_exe(smi_getDimVersions        SOURCES src/utilities/getDimVersions.cxx)
smixx_exe(smi_getSmiVersions        SOURCES src/utilities/getSmiVersions.cxx)
smixx_exe(smi_getObjectState        SOURCES src/utilities/getObjectState.cxx)
smixx_exe(smi_monObjectState        SOURCES src/utilities/monObjectState.cxx)
smixx_exe(smi_monObjects            SOURCES src/utilities/monObjects.cxx)
smixx_exe(smi_tellMonObjects        SOURCES src/utilities/tellMonObjects.cxx)
smixx_exe(smi_getDomains            SOURCES src/utilities/getDomains.cxx)
smixx_exe(smi_getDomainObjects      SOURCES src/utilities/getDomainObjects.cxx)
smixx_exe(smi_getDomainObjectSets   SOURCES src/utilities/getDomainObjectSets.cxx)
smixx_exe(smi_getSetObjects         SOURCES src/utilities/getSetObjects.c)
smixx_exe(smi_waitObjectNotBusy     SOURCES src/utilities/waitObjectNotBusy.cxx)
smixx_exe(smi_monObjectsServiceName SOURCES src/utilities/monObjectsServiceName.cxx)
smixx_exe(smi_tclTkGUI-Builder      SOURCES src/utilities/tclTkGUI-Builder.cxx)

smixx_exe(smiGui
    SOURCES src/gui/dui_util.c  src/gui/smid.c
    LINK    X11::Xt  ${MOTIF_LIBRARIES}
)
target_include_directories(smiGui PRIVATE ${MOTIF_INCLUDE_DIR})
