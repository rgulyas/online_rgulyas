#ifndef __SMIRTLHHDEFS
#define __SMIRTLHHDEFS

#include <dis.hxx>
#include <sllist.hxx>
#include <tokenstring.hxx>
#include "smirtl_core.hxx"

#ifdef WIN32
#ifdef SMILIB
#	define SmiDllExp __declspec(dllexport)
#else
#	define SmiDllExp __declspec(dllimport)
#endif
#else
#	define SmiDllExp
#endif

#ifndef SMI_PAR_TYPES
#ifndef WIN32
typedef enum { STRING, INTEGER, FLOAT } SHORT_PAR_TYPES;
#endif
typedef enum { SMI_STRING, SMI_INTEGER, SMI_FLOAT } PAR_TYPES;
#define SMI_PAR_TYPES
#endif

class ParInfo : public SLLItem {
	char *itsName;
	int itsType;
	char *itsData;
	char *itsEscData;
	char itsTypeStr[8];
public:
	ParInfo(const char *name, int type);
	~ParInfo();
	int setInfo(const void *data, int type);
	int setStrInfo(const char *data, int type);
	const char *getName() const { return itsName; }
	int getType() const { return itsType; }
	int getDataSize() const { return strlen(itsData)+1; }
	int getEscDataSize() const { return strlen(itsEscData)+1; }
	const char *getTypeStr() const { return itsTypeStr; }
	const char *getData() const { return itsData; }
	const char *getEscData() const { return itsEscData; }
	
};

class SmiDllExp SmiProxy: public DimCommandHandler, public DimClientExitHandler
{
private:
	char *itsDomain;
	char *itsName;
	char *itsFullName;
	char *itsState;
	char *itsShortState;
	char *itsAction;
	char *itsCommand;
	char *itsParValue;
	DimService *itsDimState;
	DimCommand *itsDimCmnd;
	DimService *itsDimVersion;
	void capitalize(char *str)  const;
	SLList itsParList;
	SLList itsCmndParList;
	ParInfo *currCmndPar;
//	int findPar(char *name);
	int addPar(const char *name, const void *value, int type);
	TokenString *cmndTokens;
	int getType(const char *str)  const;
	int getStateLen(const char *state);
	void formState(const char *state);
	int storeCmndPar();
	const char *getCmndPar(const char *name, int type);
	void publish_it(int later);
	void clientExitHandler() {smiDomainExitHandler();}
	void commandHandler();
	int infoPrint;
	DimServerDns *itsDns;
public:
	void attach(const char *domain);
	void attach(const char *domain, const char *name);
	void attach(const char *domain, const char *name, int later);
	void setVolatile();
	int setState(const char *state);
	const char *getState()  const;
	int setParameter(const char *parName, int parValue);
	int setParameter(const char *parName, double parValue);
	int setParameter(const char *parName, const char *parValue);
	int getParameterInt(const char *parName);
	double getParameterFloat(const char *parName);
	const char *getParameterString(const char *parName);
	int getParameterType(const char *parName);
	const char *getNextParameter();
	int testAction(const char *action) const;
	const char *getAction()  const;
	const char *getDomain() const {return itsDomain;}
	const char *getCommand() const {return itsCommand;}
	void setPrintOn() { infoPrint = 1;}
	void setPrintOff() { infoPrint = 0;}
	void printDateTime() const;
	SmiProxy(const char *name);
	SmiProxy();
	virtual ~SmiProxy(); 
	virtual void smiCommandHandler() {};
	virtual void smiDomainExitHandler(); 
	void setDns(const char *dnsName);
	void setDns(const char *dnsName, int dnsPort);
	SmiProxy(const char *dnsName, const char *name);
	SmiProxy(const char *dnsName, int dnsPort, const char *name);
};

#endif
