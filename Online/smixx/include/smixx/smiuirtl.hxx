#ifndef __SMIUIRTLHHDEFS
#define __SMIUIRTLHHDEFS

#include "smixx_common.hxx"
#include <dic.hxx>
#include <sllist.hxx>
#include "smiuirtl_core.hxx"

//using namespace std;

#ifndef SMI_PAR_TYPES
#ifndef WIN32
typedef enum { STRING, INTEGER, FLOAT } SHORT_PAR_TYPES;
#endif
typedef enum { SMI_STRING, SMI_INTEGER, SMI_FLOAT } PAR_TYPES;
#define SMI_PAR_TYPES
#endif

#ifdef WIN32
#ifdef SMIUILIB
#	define SmiDllExp __declspec(dllexport)
#else
#	define SmiDllExp __declspec(dllimport)
#endif
#else
#	define SmiDllExp
#endif

class SmiDllExp SmiParam : public SLLItem
{
	friend class SmiObject;
	friend class SmiAction;
	char *itsName;
	int itsDefault;
	void *itsValue;
	int itsValueSize;
	int itsType;
	int itisObj;
	void *itsEscValue;
	int itsEscValueSize;
public:
	SmiParam(const char *name);
	~SmiParam();
	const char *getName() const { return itsName; };
	int getType() const {return itsType; };
	int getValueInt() const { return *(int *)itsValue; };
	double getValueFloat() const { return *(double *)itsValue; };
	const char *getValueString() const { return (const char *)itsValue; };
	const char *getEscValueString() const { return (const char *)itsEscValue; }
	void convertToEsc();
	int hasValue() const {return itsDefault; };
	int setValue(int val);
	int setValue(double val);
	int setValue(const char *val);
	friend ostream& operator<<(ostream&, SmiParam&);
};

class SmiDllExp ParamHandler
{
	friend class SmiObject;
	friend class SmiAction;
	int itsNParams;
	SLList itsParamList;
	void addParam(SmiParam *ptr) { itsParamList.add(ptr);};
	SmiParam *remParam() { return (SmiParam *) itsParamList.removeHead(); };
public:
	virtual ~ParamHandler() {};
	int getNParams() { return itsNParams; };
	virtual SmiParam *getFirstParam();
	virtual SmiParam *getNextParam();
};

class SmiObject;

class SmiDllExp SmiAction : public SLLItem, public ParamHandler
{
	friend class SmiObject;
	char *itsName;
	SmiObject *itsObject;
	void getPars();
public:
	SmiAction(const char *name, int npars);
	virtual ~SmiAction();
	const char *getName() const { return itsName; };
	int setParam(SmiParam *param);
	int setParam(const char *name, int val);
	int setParam(const char *name, double val);
	int setParam(const char *name, char *val);
	int askParams();
	int send();
	friend ostream& operator<<(ostream&, SmiAction& );
};

class SmiDllExp SmiAttribute : public SLLItem
{
	char *itsName;
public:
	SmiAttribute(const char *name);
	~SmiAttribute();
	const char *getName() const { return itsName; };
	friend ostream& operator<<(ostream&, SmiAttribute& );
};

class SmiDomain;

class SmiDllExp SmiObject : public SLLItem, public ParamHandler
{
	char *itsName;
	char *itsState;
	char *itsActionInP;
	char *currActionName;
	int itsId;
	int itsBusy;
	int itsNActions;
	int itsOK;
	SLList itsActionList;
	void addAction(SmiAction *ptr) { itsActionList.add(ptr);};
	SmiAction *remAction() { return (SmiAction *) itsActionList.removeHead(); };
	SLList itsAttributeList;
        int gotAttributes;
	void getAttributes();
	void addAttribute(SmiAttribute *ptr) { itsAttributeList.add(ptr);};
	SmiAttribute *remAttribute() { return (SmiAttribute *) itsAttributeList.removeHead(); };
	int itsWakeup;
	dim_long itsDnsId;

public:
	SmiDomain *itsDomain;
	void getObjState(int id);
	void getObjPars(int id);
	void getObjActions(int id);
	SmiObject(const char *name, SmiDomain *ptr);
	SmiObject(const char *name);
	SmiObject(const char *domain, const char *name);
	virtual ~SmiObject();
	const char *getName() const { return itsName; };
	virtual void smiStateChangeHandler() {};
	virtual void smiExecutingHandler() {};
	const char *getState()  const;
	int getBusy();
	void setId(int id) {itsId = id; }
	int getId() const { return itsId; };
	int getNActions() const { return itsNActions; };
	const char *getActionInProgress();
	SmiAction *getNextAction();
	SmiAction *getAction(const char *name);
	SmiParam *getNextParam();
	SmiAttribute *getNextAttribute();
	int sendCommand(SmiAction *action);
	int sendCommand(const char *name);
	int sendCommand();
	friend ostream& operator<<(ostream&, SmiObject& );
	void waitUp()   const
	{
		while(!itsWakeup)
		{
#ifdef __VMS
			sys$hiber();
#else
			dim_wait();
#endif
		}
	}
	void wakeUp()
	{
		itsWakeup = 1;
#ifdef __VMS
		sys$wake(0,0);
#endif
#ifdef WIN32
		wake_up();
#endif
	}
	int wasOK() { return itsOK; };
	SmiObject(const char *dnsName, const char *domain, const char *name);
	SmiObject(const char *dnsName, int dnsPort, const char *domain, const char *name);
	dim_long getDnsId() const { return itsDnsId; }
};

class SmiDllExp SmiObjectPtr : public SLLItem
{
        // Unused: char *itsName;
	SmiObject *itsObject;
	
public:
	SmiObjectPtr(SmiObject *ptr)
	{
		itsObject = ptr;
	}
	~SmiObjectPtr()
	{
	}
	SmiObject *getObject() { return itsObject; }
};

class SmiDllExp SmiObjectSet : public SLLItem
{
	char *itsName;
	int itsId;
	int itsNObjects;
	int itsOK;
	SLList itsObjectList;
	void addObject(SmiObjectPtr *ptr) { itsObjectList.add(ptr);};
	SmiObjectPtr  *remObject() { return (SmiObjectPtr *) itsObjectList.removeHead(); };
	int itsWakeup;
	dim_long itsDnsId;
	
public:
	SmiDomain *itsDomain;
	void getObjects(int id);
	SmiObjectSet(const char *name, SmiDomain *ptr);
	SmiObjectSet(const char *name);
	SmiObjectSet(const char *domain, const char *name);
	virtual ~SmiObjectSet();
	const char *getName() const { return itsName; };
	virtual void smiObjSetChangeHandler() {};
	void setId(int id) {itsId = id; }
	int getId() const { return itsId; };
	int getNObjects() const { return itsNObjects; };
	SmiObject *getNextObject();
	friend ostream& operator<<(ostream&, SmiObjectSet& );
	void waitUp()  const
	{
		while(!itsWakeup)
		{
#ifdef __VMS
			sys$hiber();
#else
			dim_wait();
#endif
		}
	}
	void wakeUp()
	{
		itsWakeup = 1;
#ifdef __VMS
		sys$wake(0,0);
#endif
#ifdef WIN32
		wake_up();
#endif
	}
	SmiObjectSet(const char *dnsName, const char *domain, const char *name);
	SmiObjectSet(const char *dnsName, int dnsPort, const char *domain, const char *name);
};

class SmiDllExp SmiMessage
{
	char *itsName;
	int itsId;
	char *itsDomain;  // identical to itsName
	char *itsMessage;
	int itsWakeup;
	
public:
	SmiMessage(const char *name);
	virtual ~SmiMessage();
	
	const char *getDomainName() const { return itsName; };
	const char *getMessage() const { return itsMessage; };
	
	void setMessage();
	
	virtual void smiMessageHandler() {};
	void setId(int id) {itsId = id; }
	int getId() const { return itsId; };
	void waitUp()  const
	{
		while(!itsWakeup)
		{
#ifdef __VMS
			sys$hiber();
#else
			dim_wait();
#endif
		}
	}
	void wakeUp()
	{
		itsWakeup = 1;
#ifdef __VMS
		sys$wake(0,0);
#endif
#ifdef WIN32
		wake_up();
#endif
	}
};
//------------------------------------------------------------------------

class SmiDllExp SmiUserMessage
{
	char *itsName;
	int itsId;
	char *itsDomain;  // identical to itsName
	char *itsMessage;
	int itsWakeup;
	
public:
	SmiUserMessage(const char *name);
	virtual ~SmiUserMessage();
	
	const char *getDomainName() const { return itsName; };
	const char *getUserMessage() const { return itsMessage; };
	
	void setMessage();
	
	virtual void smiUserMessageHandler() {};
	void setId(int id) {itsId = id; }
	int getId() const { return itsId; };
	void waitUp()
	{
		while(!itsWakeup)
		{
#ifdef __VMS
			sys$hiber();
#else
			dim_wait();
#endif
		}
	}
	void wakeUp()
	{
		itsWakeup = 1;
#ifdef __VMS
		sys$wake(0,0);
#endif
#ifdef WIN32
		wake_up();
#endif
	}
};

class SmiDllExp SmiDomain
{
	char *itsName;
	int itsWakeup;
	int itsNObjects;
	SmiObject *allocObj;
	SLList itsObjList;
	void addObj(SmiObject *ptr) { itsObjList.add(ptr);};
	SmiObject *remObj() { return (SmiObject *) itsObjList.removeHead(); };
	SLList itsObjSetList;
	void addObjSet(SmiObjectSet *ptr) { itsObjSetList.add(ptr);};
	SmiObjectSet *remObjSet() { return (SmiObjectSet *) itsObjSetList.removeHead(); };
	dim_long itsDnsId;
public:
	SmiDomain(const char *name);
	virtual ~SmiDomain();
	SmiDomain *itsHandler;
	SmiObject *currObj;
	SmiObjectSet *currObjSet;
	void setNObjects(int nobjs) {itsNObjects = nobjs;};
	void setAllocation(int up);
	const char *getName() const { return itsName; };
	int getNObjects() const;
	void getObjects(int up);
	SmiObject *getNextObject();
	SmiObject *getObject() {return currObj; };
	virtual void smiStateChangeHandler() {};
	virtual void smiExecutingHandler() {};
	virtual void smiDomainHandler() {};
	void getObjectSets(int up);
	SmiObjectSet *getNextObjectSet();
	SmiObjectSet *getObjectSet() {return currObjSet; };
	virtual void smiObjSetChangeHandler() {};
	int sendCommand(const char *obj, const char *cmnd);
	int sendCommand();
	int shutdown();
	const char *getAllocationState() { return allocObj->getState(); };
	int allocate();
	int release();
	friend ostream& operator<<(ostream&, SmiDomain& );
	friend ostream& operator<<(ostream&, SmiDomain* );
	void waitUp()  const
	{
		while(!itsWakeup)
		{
#ifdef __VMS
			sys$hiber();
#else
			dim_wait();
#endif
		}
	}
	void wakeUp()
	{
		itsWakeup = 1;
#ifdef __VMS
		sys$wake(0,0);
#endif
#ifdef WIN32
		wake_up();
#endif
	}
	int isUp() { return itsWakeup;};
	SmiDomain(const char *dnsName, const char *name);
	SmiDomain(const char *dnsName, int dnsPort, const char *name);
	dim_long getDnsId() const { return itsDnsId; }
};


class SmiDllExp SmiUi
{
public:
	static dim_long itsDnsId;

	static int sendCommand(const char *obj, const char *cmnd);
	static int sendProxyCommand(const char *obj, const char *cmnd);
	static int checkDomain(const char *domain);
	static int checkProxy(const char *proxy);
//	static void setError(){};
//	static int printError(){};
	static void capitalize(char *name);
	static int changeOption(const char *domain, const char *option, const char *value);
	static int getOptions(const char *domain, const char *optionString);
	static void setDns(const char *dnsName);
	static void setDns(const char *dnsName, int dnsPort);
	static dim_long addDns(const char *dnsName);
	static dim_long addDns(const char *dnsName, int dnsPort);
};

#endif
