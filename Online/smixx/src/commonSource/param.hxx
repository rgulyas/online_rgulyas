
//----------------------  Class   Param  -----------------------------------
//
//
//                                                      Author: Boda Franek
//                                                      Date : October 2020
//----------------------------------------------------------------------------
#ifndef PARAM_HH
#define PARAM_HH
#include "typedefs.hxx"
#include "name.hxx"
#include "indivalue.hxx"

class ObjectRegistrar;
class SMIObject;
class State;
class Action;
//----------------------------------------------------------------------------
namespace paramcons
{
const Name noval = "&noval";
const Name undef = "&undef";
const Name notfound = "&notfound";
}
//-----------------------------------------------------------------------
class Param
{
	public :
		Param();
		~Param();
		
		Param(const Name& name, const Name& indiValue, const Name& type );

		void init(const Name& name, const Name& indiValue, const Name& type );
		
		Name paramName() const;
		Name paramDeclType() const;
		Name paramIndiValue() const;
		Name paramCurrValue() const;
/**
  return value is the actual value of the parameter
  return argument1 : actualType  is the actual type of the parameter
  return argument2 : err  should be zero when success, otherwise the return
                          value indicates the type of the error
	This method is used only by State Manager
*/
		Name paramValueAndType(ObjectRegistrar& objects,
                            SMIObject* pParentObject,
                            State*  pParentState,
                            Action* pParentAction,
                            Name& actualType, int& err) const;  
/**
  return argument1 : actualType  is the actual type of the parameter
        when "", it means it is not at the moment available
		theis method used only by Translator
*/
		bool paramValueAccessible(ObjectRegistrar& objects,
                            SMIObject* pParentObject,
                            State*  pParentState,
                            Action* pParentAction,
                            Name& actualType) const;  
		
		void setParamCurrValue(const Name& currValue);
/**
  returns parameter in one string. the 'format' determines what is printed
  format = 1     _name = _indiValue
  format = 2     _type _name = _indiValue
  format = 3     _type _name = _currValue
  NB: '=' is not printed when the corresp value is not there
      _type is not printed when it is  "STRING" 
*/		
		Name outString(int format = 1) const;
		
	private :
		Name indiValueForm() const;
		void checkData();
			
/**  Name of the parameter
*/
		Name _name;	
/** 
    If the parameter is Object or Action parameter, then this is its default
    value. It can only be a direct value, i.e. string, integer or floating point
	If the parameter does not have a dafault, it is set to 'noval'.
	If the parameter is either DO or CALL instruction parameter, then this
	value must be present and can be also an indirect value, i.e.
	'object-name'.'parameter-name'.
*/
		IndiValue _indiValue;
/**
   This indicates if the parameter is declaration type or do/call type
   Declaration Type: it is set to declared type ("STRING", "INT" or "FLOAT").
   Do/Call Type:  it is set to paramcons::undef
*/
		Name _type;
/**
   This has meaning only for Declaration Type. This is the only private data
    that may change. Initialy set to _indiValue (i.e. either default value
	or &noval).
*/
		Name _currValue;
};
#endif
