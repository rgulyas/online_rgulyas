//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  September 2020
//----------------------------------------------------------------------
#ifndef ACTIONRETURNSTATUS_HH
#define ACTIONRETURNSTATUS_HH

enum ActionReturnStatus_t 
{ actionTerminatedNoTermIns = 0, 
// action terminated without terminate_action instruction

  actionTerminated = 1, 
// action terminated with terminate_action instruction

  actionSuspended = 2 } ;
#endif
