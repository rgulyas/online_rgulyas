//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  April 2020
// Copyright Information:
//      Copyright (C) 1996-2020 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------


#include <stdlib.h>
#include "name.hxx"
#include "diag.hxx"

extern Name smiDomain;

#include "objectregistrar.hxx"
extern ObjectRegistrar allSMIObjects;

#include "smiobject.hxx"

#include "registrar.hxx"
extern Registrar allSMIObjectSets;

#include "smiobjectset.hxx"

#include "clientwhens.hxx"
 //--------------------------------------------------------------------------
void Diag::printClientWhensOfAllObjects()
{
	int numOfObjects; numOfObjects = allSMIObjects.numOfObjects();
	SMIObject* pObj; 
	
	cout << endl << endl << " OBJECT C L I E N T  Whens " << endl;
	
	for (int io=0; io < numOfObjects; io++) {
		pObj = allSMIObjects.gimePointer(io);
		cout << endl << " Object " << pObj->name() << endl;
		pObj->printClientWhens();
	}
       
       return; 
}
//------------------------------------------------------------------------
void Diag::printClientWhensOfAllObjectSets()
{
	int numOfSets = allSMIObjectSets.length();
	int is;
	SMIObjectSet* pSet;
	
	cout << endl << endl << " OBJECT SET C L I E N T  Whens " << endl;
	
	for (is = 0; is < numOfSets; is++ )
	{
		pSet = (SMIObjectSet*)allSMIObjectSets.gimePointer(is);
		cout << endl << " Object Set" << pSet->name() << endl;
		pSet->printClientWhens();
	}

	return;
} 
//----------------------------------------------------------------------
void Diag::printActualClientWhensOfAllObjects()
{
	int numOfObjects; numOfObjects = allSMIObjects.numOfObjects();
	SMIObject* pObj; 
	
	cout << endl << endl << "   Object's actual client whens" << endl;
	
	for (int io=0; io < numOfObjects; io++) {
		pObj = allSMIObjects.gimePointer(io);
		cout << endl << " Object " << pObj->name() << endl;
		ClientWhens clientWhens;
		pObj->gimeCurrentClientWhens(clientWhens);
		clientWhens.out("     ");
	}
	return;
}
