//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  April 2020
// Copyright Information:
//      Copyright (C) 1996-2020 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef DIAG_HH
#define DIAG_HH

/** @class Diag
    This class holds generaly useful diagnostic utilities
*/		       
class Diag {
public:

/**
  Will print Client Whens for all objects in the domain.
  For each object these are whens that refer to the object explicitly
*/
	static void printClientWhensOfAllObjects();

/**
  Will print Client Whens for all object sets in the domain.
  For each object these are the whens that refer to the object set.
*/
	static void printClientWhensOfAllObjectSets();
/**
  Will print the Actual Client Whens for all objects in the domain.
  For each object these are the whens that refer to the object explicitly
  plus those that only refer to object sets which happen to contain the object.
*/
	static void printActualClientWhensOfAllObjects();

	
private:

};

#endif
