//----------------------------- ForIns  Class ---------------------------------
#ifndef FORINS_HH
#define FORINS_HH

#include "parameters.hxx"
#include "instruction.hxx"

class State;
class Action;
class PtrVector;
#include "namevector.hxx"
#include "namelist.hxx"
#include <vector>
//
//                                                               April 2018
//                                                               B. Franek
// Copyright Information:
//      Copyright (C) 1996-2015 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------------

class ForIns : public Instruction
{
public :
	ForIns( char lines[][MAXRECL],int lev, PtrVector& allBlocks,
		int& no_lines, SMIObject *pobj, State* pstat, Action*);


	~ForIns();

	void whatAreYou();

	int execute( Name& endState );   // 0-normal,  2-suspended

	
private:


//-------------- data -------------------------------------------

	int _level;
/**
  Name of the for argument. There is only one so this seems to me redundant
*/
	Name _argName;
/**
  Name of the Object Set
*/	
	Name _setName;
/**
   Pointer to the FOR instruction block. The instruction block takes one
   argument and its meaning is a name of an object.
*/
	InsList* _pInsBlock;
	
/** Pointer to the parent object
*/
	SMIObject* _pParentObject; 

	State* _pParentState;
	
	Action* _pParentAction;

/**
    it is initialised to 'fresh' in the constructor.
*/
	enum ForInstructionStatus_t 
	{fresh =0 ,
	 suspendedDueToSuspendedBlock = 1 } _suspend_flag; 
/**
    when FOR instruction is suspended, this is set to the object index
    in the Object Set (_setContents)
*/	 
	int _suspendedAtObj; 
/**
    when FOR instruction starts executing, this is where the contents
    of the Object Set '_setName' is copied.
*/	
	std::vector<Name> _setContents; 
};

#endif
