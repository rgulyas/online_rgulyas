//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  April 2021
//----------------------------------------------------------------------

#include "getvarelem.hxx"
#include "name.hxx"
#include <stdlib.h>
#include "objectregistrar.hxx"
#include "registrar.hxx"
#include "action.hxx"
#include "parms.hxx"
#include "varelement.hxx"
#include "alarm.hxx"

extern ObjectRegistrar allSMIObjects;
extern Registrar allSMIObjectSets;

 //--------------------------------------------------------------------------
int GetVarElem::actualObjectName( const VarElement& objectId,
								        Action* pAct,
								  const Name& insname,
								  Name& actualName)
{	
	actualName ="";
	
	if ( pAct == 0 )
	{
		cout << "*** Warning : Executing ''" << insname <<
		"'' instruction" << endl <<
		" parent action unknown" << endl;
		return 0;	
	}
		
	Parms* pCurPars = pAct->pCurrentParameters();
	Name objNm = objectId.actualName(pCurPars);
	if ( objNm == paramcons::notfound )
	{
		cout << "*** Warning : Executing ''" << insname <<
		"'' instruction" << endl
		 << "         the value of parameter " << objectId.parName() 
		 << " not found" << endl;
		return 0;
	}
	
	if (!check_name(objNm) || objNm.length() <= 1)
	{
		cout << "*** Warning : Executing ''" << insname <<
		"'' instruction" << endl
		<< "        object name " << objNm << " is not a name" << endl;
		
		if ( objectId.parName() == "" )
		{
			cout << " FATAL error " << endl;
			Alarm::message("FATAL",objectId.name(),
			               " object name is not a name");
		}
		else
		{
			cout << " the instruction is ignored" << endl;
		}
		return 0;
	}

	if ( insname == "create_object" || insname == "destroy_object" )
	{
		actualName = objNm;
		return 1;
	}
	
	// for create/destroy instruction the existence of the object
	// is handled elsewhere
		
	SMIObject* pSMIObj = allSMIObjects.gimePointer(objNm);
	if (!pSMIObj)
	{
		cout << "*** Warning : Executing ''" << insname <<
		"'' instruction" << endl;
		cout << " Object " << objNm << " is not declared" << endl;
		
		if ( objectId.parName() == "" )
		{
			cout << " FATAL error " << endl;
			Alarm::message("FATAL",objectId.name(),
			               " object is not declared");
		}
		else
		{
			cout << " the instruction is ignored" << endl;
		}		
		return 0;
	}
	
	actualName = objNm;
	return 1;
}
 //--------------------------------------------------------------------------
int GetVarElem::actualSetName( const VarElement& setId,
								        Action* pAct,
								  const Name& insname,
								  Name& actualName, void* &ptnvSet)
{
	actualName ="";
	
	Parms* pCurPars = pAct->pCurrentParameters();
	Name setNm = setId.actualName(pCurPars);
	if ( setNm == paramcons::notfound )
	{
		cout << "*** Warning : Executing ''" << insname <<
		"'' instruction" << endl
		 << "         the value of parameter " << setId.parName() 
		 << " not found, the instruction is ignored" << endl;
		return 0;
	}
	
	if (!check_name(setNm) || setNm.length() <= 1)
	{
		cout << "*** Warning : Executing ''" << insname <<
		"'' instruction" << endl
		<< "        Set name " << setNm << " is not a name" << endl;
		
		if ( setId.parName() == "" )
		{
			cout << " FATAL error " << endl;
			Alarm::message("FATAL",setId.name(),
			               " Set name is not a name");
		}
		else
		{
			cout << " the instruction is ignored" << endl;
		}
		return 0;
	}

	ptnvSet = allSMIObjectSets.gimePointer(setNm);
	if (!ptnvSet)
	{
		cout << "*** Warning : Executing ''" << insname <<
		"'' instruction" << endl;
		cout << " Object Set " << setNm << " is not declared" << endl;
		
		if ( setId.parName() == "" )
		{
			cout << " FATAL error " << endl;
			Alarm::message("FATAL",setId.name(),
			               " object set is not declared");
		}
		else
		{
			cout << " the instruction is ignored" << endl;
		}		
		return 0;
	}
	
	actualName = setNm;
	return 1;
}
