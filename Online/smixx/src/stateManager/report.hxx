//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  August 2017
// Copyright Information:
//      Copyright (C) 1996-2017 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef REPORT_HH
#define REPORT_HH

class Name;

/** @class Report
    This class will deliver messages to an agent outside SMI world. It is then
    up to that agent to use them, most likely to display them on some
    ReportPanel or whatever.
    It is hidding the actual mechanism of delivering the messages from
    the rest of the SMI code.
    This mechanism was chosen as DIM Service
	               'SMI/domain-name/REPORT_MESSAGE'
*/		       
class Report {
public:

/**
  Call to this method is made at the start of the program.
    It will declare to DIM the Reporting service.
*/
	static void initialise();
/**
  Will deliver the message to the Reporting agent. It will siply publish it.
  @param  severity   is either INFO, WARNING, ERROR or FATAL
  @param  object   object name if there is a relevant one. It could be anything
  @param  mess   the relevant message
*/
	static void message
	   (const Name& severity, const Name& object, const Name& mess);
	
private:

};

#endif
