//-------------------------  Class   ReportIns  -------------------------------
//                                                  Date :    August 2017
//                                                  Author : Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2017 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------

#include "ut_sm.hxx"
#include "assert.h"
#include "reportins.hxx"
#include "alarm.hxx"
#include "report.hxx"
#include "smiobject.hxx"
#include "state.hxx"
#include "action.hxx"
#include "objectregistrar.hxx"
#include "options.hxx"

extern ObjectRegistrar allSMIObjects;

ReportIns::ReportIns
           ( char lines[][MAXRECL], int lev, int& no_lines,
            SMIObject* pobj, State *pstat, Action* pact)
{
//-------------------------------------------------------------------------
// Input :
// lines[0] ....... the first line after the 'report'
// lev    ....... the block level

// Output :
// no_lines ....... number of lines in 'report' (not counting the first //                         'report')
//----------------------------------------------------------------------------

	_level = lev;
	
	_pParentObject = pobj;
	_pParentState = pstat;
	_pParentAction = pact;
	
	
	int il = 0;
	_severity = lines[il];
	
	il++;
	int noElem;
	sscanf(lines[il],"%d",&noElem);
	assert (noElem > 0);
	
	Name tmpElem;
	ArgName tmpElem_gen;
	
	for (int ie=0; ie<noElem; ie++ )
	{
		il++;
		tmpElem = lines[il];
		_msgElements += tmpElem;
	}
	
	no_lines = il + 1;
	return;
}
//-----------------  Destructor  BF Mar 2020  ---------------------------
ReportIns::~ReportIns()
{
	return;
}
//--------------------------------------------------------------------------
void ReportIns::whatAreYou()
{
	Name temp = nBlanks(_level*4+10);
	char* ident = temp.getString();
	
	cout << ident
	     << "report ( " << _severity.getString() << "," ;
	
	int ie, numElem;
	
	numElem = _msgElements.length();
	
	for ( ie=0; ie<numElem; ie++ )
	{
		if (ie>0) cout << " + ";
		cout << (_msgElements[ie]).outString() ;
	}
	
	cout << " )" << endl;
	return;
}
//--------------------------------------------------------------------------
int ReportIns::execute( Name& endState )
{
	endState = "not changed"; // the instruction does not change state
	
//  Building the message from message elements in the report instruction
//  While doing it we shall strip enclosing double quotes from string
//  values and finish with only enclosing doublequotes on the complete
//  message
//-------------------------------------

	int dbg; Options::iValue("d",dbg);
	Name mainIndent = insDiagPrintOffset(_level, _pParentAction);
		
//debug beg
if ( dbg > 3 )
{
	cout << mainIndent;
	cout << "executing: report instruction" << endl;
	whatAreYou();
}	
//debug end

// the next two lines are temporary

	Name message = "";	
	int ie, numElem;
	
	numElem = _msgElements.length();
	IndiValue element;
	Name  value, valueType, result;
	Name actualType(""); int err;
	
	
	for ( ie=0; ie<numElem; ie++ )
	{
		element = _msgElements[ie];  //elements are indiValues, get the actual

		Name actualValue = element.actualValue
				(allSMIObjects,
				 _pParentObject,
				_pParentState,
				_pParentAction,
				 actualType,err);
				 
				 
		if ( err == 0 )
		{
			if ( actualType == "STRING" )
			{
				if ( stripEnclosingQuotes(actualValue) )
				{ message += actualValue; }
				else 
				{ 
			 		cout 
				 	<< " The indiValue has screwed up quotes : " 
					<< element.outString() << endl
					<< " this should not happen" << endl;

			 		Alarm::message("FATAL", _pParentObject->name(),
		        		" problem with message element");
					return 0;	
				}
			}
			else { message += actualValue; }
		}
		else
		{
			Alarm::message("FATAL", _pParentObject->name(),
		        " problem with message elements");

			// if actualIndiValue does not succeed, it is considered
			// fatal error and the program terminates.	
		}	
	}
	
//------------------------------------------------------------
		
//debug beg
if ( dbg > 3 )
{
	cout << mainIndent << "   " ;
	cout << " message: " << message << endl;
}

//debug end

// The message is successfully built, now we can send it

	Report::message(_severity,_pParentObject->name(),message);
	
	return 0;  // normal return from execute method
}
