//-------------------------  Class   ReportIns  -------------------------------
#ifndef REPORTINS_HH
#define REPORTINS_HH

#include "parameters.hxx"
#include "name.hxx"
#include "namevector.hxx"
#include "instruction.hxx"
#include "indivaluevector.hxx"

#include <vector>

class SMIObject;
class State;
class Action;
//                                                  Date :    August 2017
//                                                  Author : Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2017 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------


class ReportIns : public Instruction{
public:
/**  Constructor
    @param  lines[][MAXRECL]   lines[0] is a pointer to the line following the
                               'report' line on sobj input, i.e the line with
			       severity. This is then followed by no.of message
			       elements followed by elements themselves.
    @param  lev    block level, used only for printing
    @param  no_lines   no. of lines in the report section (not counting the                                                    first 'report' line.
    @param  pobj  pointer to the instruction's parent object
    @param  pstat  -dtto- for state
    @param  pact   -dtto- for action

*/
	ReportIns
           ( char lines[][MAXRECL], int lev, int& no_lines,
              SMIObject* pobj=0, State *pstat=0, Action* pact=0);

	~ReportIns();
/**
     Prints the instruction
*/
	void whatAreYou() ;
/**
     Executes the instruction
     @retval   always zero
     @param  endState  always "not changed"
*/
	int execute( Name& endState );   // 0-normal
	
	
private:
	int _level;

	SMIObject* _pParentObject;
	State*  _pParentState;
	Action* _pParentAction;

	Name _severity;  // either INFO, WARNING, ERROR or FATAL
	
	IndiValueVector _msgElements;
	
};

#endif
