//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  March 2017
// Copyright Information:
//      Copyright (C) 1996-2017 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
/*
#include "reservednames.hxx"

#include "smiobject.hxx"
extern Name smiDomain;

//-------------------------------------------------------------------------
int ReservedNames::getValue(const Name& name,
	                   SMIObject* pParentObject,
			   State* pParentState,
			   Action* pParentAction,
			   Name& value)
{
	value = "unknown";
	if ( name == "_DOMAIN_" )
	{
		value = smiDomain;
		return 1;
	}
	if ( name == "_OBJECT_" )
	{
		if ( pParentObject == 0 ) { return 0; }
		value = pParentObject->name();
		return 1;
	}
	if ( name == "_STATE_" )
	{
		if ( pParentState == 0 ) { return 0; }
		value = pParentState->stateName();
		return 1;
	}
	if ( name == "_ACTION_" )
	{
		if ( pParentAction == 0 ) { return 0; }
		value = pParentAction->actionName();
		return 1;
	}

	
	return 0;
}
*/
