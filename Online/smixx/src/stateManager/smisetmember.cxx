
//----------------------------- SMISetMember  Class --------------------------
#include "smisetmember.hxx"
//
//                                                               July 2018
//                                                               B. Franek
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------------


SMISetMember::SMISetMember()
: _objName(""), _pObj(NULL), _onFlg(false)
{
	return;
}
//-------------------------------------------------------------	
SMISetMember::SMISetMember(const Name& name, SMIObject* pObj, bool on)
: _objName(name), _pObj(pObj), _onFlg(on)
{
	return;
}
//---------------------------------------------------------------
SMISetMember::SMISetMember(const SMISetMember& source)
: _objName(source._objName), _pObj(source._pObj), _onFlg(source._onFlg)
{
	return;
}
//----------------- Destructor  BF Mar 2020  --------------------
SMISetMember::~SMISetMember() { return; }
//--------------------------------------------------------------	
Name SMISetMember::name() const
{
	return _objName;
}
	
SMIObject* SMISetMember::pointer() const
{
	return _pObj;
}
	
bool SMISetMember::isOn() const
{
	return _onFlg;
}
	

void SMISetMember::switchOn() {_onFlg = true;}

void SMISetMember::switchOff() {_onFlg = false;}

