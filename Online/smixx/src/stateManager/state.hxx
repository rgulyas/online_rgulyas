//---------------------- S t a t e   Class ---------------------------
#ifndef STATE_HH
#define STATE_HH

#include "parameters.hxx"
#include "action.hxx"
#include "when.hxx"
#include "commhandler.hxx"
#include "namevector.hxx"
#include "ptrvector.hxx"
#include "registrar.hxx"
#include <vector>
class SMIobject;
class When;
class CommHandler;
class Name;
//                                                 Author: B. Franek
//                                                 Date : May-1996
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------------

class State {
public :
  State ( char lines[][MAXRECL],
           SMIObject* pobj,
           int *st_lines);

  State ( const char* const state_name, SMIObject* pobj );
  
  virtual ~State();

  void whatAreYou() const;
//
  int execute( const Name& actionstr, Name& endState) const;
//
  int execute_assoc
      ( const Name& actionstr, const CommHandler* const pCommHandler) const;
//                       Special for associated objects 
//                       Return value
//                       is 1(OK) or 
//                       -1...action not found
//
/** will execute whens according to the 'relevant WHENS'  flag-list
     @param relevantWhensFlaglist ... integer vector of flags ( one per
            every WHEN in this state). Flag value is 1 or 0. 
	    Value 0 means that the particular WHEN does not refer to 'server'
	    object (see later). 
	    Value 1 is indicating that the particular WHEN refers to 'server'
	    object. We call such WHEN 'relevant'
            This list ends with the last flag of value 1.
	     
  when an object reaches a state then all the WHENS in that state are
  executed. The result of this is :
    1) one of the WHENS which is not special (stay_in_state) is executed and
       object will execute an action resulting in object immediately leaving
        the state. So this is quite a trivial case.(Even if it comes back to the
	same state, its WHENS sequence gets executed again and so it is
	a new story). 
       
    2) all the WHENS are 'dormant' - either their conditions are FALSE or it can
       not be executed. In this case _special_termination_when_inx is set to -1.
       
    3) the first WHEN that is 'alive' (its condition TRUE) is special 
      (stay_in_state). In this case _special_termination_when_inx is set to its
      index. All the WHENS before are 'dormant', but we know nothing about
      the subsequent ones.
      
  Every time any object (let's call it A) in the domain changes its state, then
   Scheduler first establishes its Client Database and contacts all A's clients.
   If our object is (in its current state) A's client,then A becomes our
   object's server and the Scheduler arranges through class ClientWhens,
   ClientObjects and ClientStates that this present method is called. It also
   supplies it with 'relevant WHENS list' (see above).
   The easiest thing would be to execute the WHENS sequence in this state. But 
   we can save some time by not executing the ones we know are 'dormant' and
   are not relevant. 
  There are two possibilities:
     A) _special_termination_when_inx is set to -1
        In this case we know that all WHENS are dormant and only the ones
	 marked as relevant in the flag list can bring different outcome
	 and we simply execute those. This results in 1 or 2 or 3 above.
	 
     B) _special_termination_when_inx is > 0 
         In this case we know that all the WHENS before the previously
	 terminating special one (SW) are dormant but we know nothing about
	 the WHENS following it. There are now two possibilities:
	 a) SW is relevant and could therefore now be dormant. We know nothing
	    about the status of WHENS following SW and therefore all 
	    the following WHENS all the way up to the last WHEN of
	    the state should be marked for execution. After executing the list
	    we get case 1) 2) or 3) above
	 b) SW is not relevant and therefore it would remain live and subsequent
	    WHENS would not be executed. But one of the relevant WHENS before
	    could become live. So we simply execute the flag list. This can 
	    only end up as 1) or 3)
*/
	void executeWhensFromFlagList
	( const std::vector<int>& relevantWhensFlaglist ) ;

/** executes all whens with non-busy objects
*/
	void executeAllWhens();
		
  bool initialState() const;

  bool deadState() const;

/** returns TRUE if the state has attribute 'undeclared_state'
    It only applies to associated objects. When the object returns
    a state, that was not declared, it will be set to this state.
*/
	bool undeclaredState() const;
	
  Name& stateName() ;

  int numActions() const;

  Name tagName() const;

  void changeName( const Name& name );

  void name(char* name, int *numActions) const;

  void action( const int index, Action** pAction) const;

  void actionString( const int index, char* actionstr, const int maxsize) const;

  Name actionString( const int index ) const;

  bool actionExists( const Name& actname) const;
  
/**
    Every object or object set which is reffered to by whens of this state is
    updated with their addresses. This method simply loops over the existing
    whens and for each of them calls method of the same name which actually
    does the job.
*/
	int updateReferencedObjectsandSets() const;
/**
  Will build a list of objects that are directly referenced in WHEN constructs.
  We used to call them 'referenced objects'
*/
	NameList gimeServerObjects() const;
/**
  Will build a list of object sets that are referenced in WHEN constructs.
  We used to call them 'referenced sets'
*/
	NameList gimeServerSets() const;	

private :
// methods:
/** will execute whens according to the supplied requestFlagList
	the flag list consists of ones and zeros.
	When flagList(inx) is 1, then _whens[inx] is executed.
	If none of the requested 'whens' is 'alive', it returns -1
	otherwise it returns the index of 'alive' 'when'.
	If the terminating when is special, terminatingWhenSpecial is set TRUE
*/
	int executeRequestedWhens
	     ( std::vector<int>& requestFlagList,
	       bool& terminatingWhenSpecial ) const;
	
// data

  Name _stateName;             // State name

  Name _stateNametag;   //e.g. &VARIABLE

    
    NameVector _attributes;

    PtrVector _whens;
    
/**
	when the 'when sequence' terminates by a 'special when', this
	is its index (0....). Otherwise it is -1
*/
	int _special_termination_when_inx;

    Registrar _actions;

  Name _objName;    // Name of the object to which 
//                                        the state belongs
  SMIObject *_pParentObject;
  
};
#endif
