// swstay_in_state.hxx: interface for the SWStay_in_State class.
//
//                                                  B. Franek
//                                                 October 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef SWSTAY_IN_STATE_HH
#define SWSTAY_IN_STATE_HH

#include "parameters.hxx"
#include "whenresponse.hxx"

class SWStay_in_State : public WhenResponse
{
public:
	SWStay_in_State( char lines[][MAXRECL], int &noLines);

	~SWStay_in_State();
	
	bool stay_in_state();
	
	Name outShort();
protected :

};

#endif 
