//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  August 2001
// Copyright Information:
//      Copyright (C) 1996-2013 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef UT_SM_HH
#define UT_SM_HH


class Name;
class NameVector;
class ParOperand;
class SMIObject;
class State;
class Action;

#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   
   
void gime_millis(char* millis);
void newTime(char *timeStr);

void indent(int blanks);
void print_msg(const char *msg);
void gime_date_time(char *timeStr);
void print_date_time();
void print_obj(char *obj_name);
void print_obj(const Name& obj_name);

void makeStateString(const NameVector& states, Name& stateString);

int stringParToIntPar(Name& in, Name& out);
int floatParToStringPar(Name& in, Name& out);
int floatParToIntPar(Name& in, Name& out);
int intParToFloatPar(Name& in, Name& out);
int intParToStringPar(Name& in, Name& out);

//-----------------------------------------------------------------------------
/** If the supplied string is enclosed in doublequotes, it will remove them.
   @retval <1>  doublequotes were present and removed
   @retval <0>  Something wrong with the quotes. The string is not modified.
*/
	int stripEnclosingQuotes( Name& string);	
//--------------------------------------------------------------------------------
/**  Given the operand, it returns value of the associated parameter. This 
     function is used by 'SetIns' class and 'SmpCondTyp4' class during
     execution of an instruction.
    @retval <1> success
    @retval <0> failure
    @param operand
    @param  pParentObject   pointer to the executed instruction's parent object
    @param  pParentState    -dtto- fot state
    @param  pParentAction   -dtto- for action 
    @param  value   value of the operand
    @param  type    type of the operand (INT,FLOAT or STRING)
*/

int getOperandValue
   ( ParOperand& operand,
    SMIObject* pParentObject, State* pParentState, Action* pParentAction,
    Name& value, Name& type);
		
#endif
