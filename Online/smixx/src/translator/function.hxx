/*
// This is now an obsolete class and can be deleted
//
// function.hxx: interface for the Action class.
//
//                                                  B. Franek
//                                                   August 2020
// Copyright Information:
//      Copyright (C) 1999-2020 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#ifndef FUNCTION_HH
#define FUNCTION_HH

#include "action.hxx"

#include "name.hxx"
#include "smlline.hxx"
#include "smlunit.hxx"
#include "actionheadblock.hxx"
#include "inslist.hxx"
#include "ptrvector.hxx"

class Function  : public Action 
{
public:
	Function(const Name&);

	virtual ~Function();
	
//	int examine() {return 0;}
	
	void outSobj(ofstream& sobj) const;	
	
//	void translate();
	
/*	virtual void translate();

	Name name() const;

        int getActionParameter(const Name& name, Name& value,
                               Name& typeNm) const;
			       
	int numParameters() const;
	
	Parms* pActionParameters();
*/

};

#endif 
*/
