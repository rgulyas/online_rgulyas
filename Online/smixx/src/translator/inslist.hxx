// inslist.hxx: interface for the Action class.
//
//                                                  B. Franek
//                                                28 September 1999
//
//////////////////////////////////////////////////////////////////////
#ifndef INSLIST_HH
#define INSLIST_HH

#include "name.hxx"
#include "smlunit.hxx"
#include "instruction.hxx"
class Action;

class InsList  : public SMLUnit 
{
public:
	
	InsList( Action* pParentAction, InsList *pParentList );

	virtual ~InsList();

	virtual void translate();
	
	virtual void outSobj(ofstream& sobj) const;
	
	int level() const;
	
	int id() const;
	
	void setId(const int id);
/**
    In all the relevant instructions it will replace the names in the
    args array by $(argi) where i is the position in the array.
    At the moment it concerns only 'do' and 'wait' instruction
*/
	void replaceArgs(const NameVector& args);	
	
	int numOfInstructions() const;
/**
    will remove 'sterile instructions
*/
	void removeSterileInstructions();	

protected :
/**
   Will collect and add lines to instruction pointed to by 'pInstr' beginning
   with line 'begLine'. This will continue until an instruction line is found.
   This line is not appended. It returns the line num of this next instruction
   in 'nextBeg' and its type in 'nextType'
*/
	void collectIns(SMLUnit* pInstr, int begLine,
			int& nextBeg, Name& nextType);
/**
   This is for collecting instructions that have terminator such as is...endif.
   Will collect and add lines to instruction pointed to by 'pInstr' beginning
   with line 'begLine'. This will continue until the instruction
   terminator is found.  This will get appended to the instruction and
   collection then continues until the next instruction is found. A care has
   to be taken about ....   
*/			
	void collectTermIns(SMLUnit* pInstr, Name instrType, int begLine,
			int& nextBeg, Name& nextType);

	Action* _pParentAction;
	InsList* _pParentList;
	
	int _id;
	int _level;
};

#endif 
