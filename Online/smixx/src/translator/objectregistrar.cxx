//---------------------- ObjectRegistrar   Class ---------------------------
//                                                          November 2020
//                                                            B. Franek
//--------------------------------------------------------------------------
//
#include "objectregistrar.hxx"
#include "registrar.hxx"
#include "name.hxx"
#include "ut_tr.hxx"

class SMIObject;

ObjectRegistrar::ObjectRegistrar()
{
	return;
}

void ObjectRegistrar::init(const Registrar& registrar)
{
	copy(registrar);
	return;
}
ObjectRegistrar::~ObjectRegistrar()
{
	return;
}
//-------------------------------------------------------------
SMIObject* ObjectRegistrar::gimePointer(const Name& objName) const
{
// in the Registrar there is a mixture of void pointers to either 'SMIObject'
//   or 'IsOfClassObject' instantiation. In the latter case, we shall set
//   pointer to its SMI class (also SMIObject c++ class)
//
	Name temp = objName;       
	SMIObject* ptn = gimeObjectPointer(temp);
	return ptn;
}
