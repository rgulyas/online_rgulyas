//---------------------- ObjectRegistrar   Class ---------------------------
#ifndef OBJECTREGISTRAR_HH
#define OBJECTREGISTRAR_HH

/**  Translator holds the list of SMI objects in class 'Reistrar'.
  Method 'ParamManager::actualIndiValue' requires this list as an argument
  of class 'ObjectRegistrar'. This is because we want to use this method
  also in State Manager and SM holds the objects in the list of this class.
  Historicaly stupid decision but too late.
  So that is the reason for existence of this class. 
*/
//                                                          November 2020
//                                                            B. Franek
//--------------------------------------------------------------------------
//
class Name;
class SMIObject;
#include "registrar.hxx"

class ObjectRegistrar : public Registrar
{
public :
	ObjectRegistrar();

	~ObjectRegistrar();

	void init(const Registrar& registrar);

	SMIObject *gimePointer(const Name& objname) const;

private :

};


#endif
