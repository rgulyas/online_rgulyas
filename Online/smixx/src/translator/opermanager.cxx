//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  August 2014
// Copyright Information:
//      Copyright (C) 1996-2014 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------

#include "smiobject.hxx"
#include "action.hxx"
#include "smlunit.hxx"
#include "paroperand.hxx"
#include "opermanager.hxx"
#include "ut_tr.hxx"
#include <stdlib.h>
#include "smlline.hxx"
#include "errorwarning.hxx"
#include "reservednames.hxx"
#include "indivalue.hxx"

#include "objectregistrar.hxx"
extern ObjectRegistrar allSMIObjects;

//------------------------------------------------------------------
int OperManager::getParValueType(  SMLUnit* pParentUnit, ParOperand& operand)
{
	SMIObject* parentObject;
	State* parentState;
	Action* parentAction;
	
	Name operandType, valueType, value, parName, ownerObjName;
	Name indiValue;
	
	parentObject = (SMIObject*) pParentUnit->parentPointer("Object");
	parentState = (State*) pParentUnit->parentPointer("State");  
	parentAction = (Action*) pParentUnit->parentPointer("Action");      

	
	operandType = operand.operandType();

//cout << " Entered getParValueType     operandType  " << operandType << endl;
	
	if ( operandType == "VALUE" )
	{  	
		return 1;
	}
	
	indiValue = "";
	
	if ( operandType == "NAME" )
	{
		parName = operand.parName();
		indiValue = parName;		
	}
		
	if ( operandType == "COMPNAME" )
	{
		parName = operand.parName();
		ownerObjName = operand.ownerObjName();

		indiValue = ownerObjName;
		indiValue += ".";
		indiValue += parName;
	}

	if ( indiValue == "")
	{
		cout << " !!!! internal error."
		<< " OperManager::getParValueType"
     	<< endl << " Unknown operand type " << operandType << endl
      	<< " Operand: " << endl; operand.out(" "); cout << endl;
		throw FATAL;	
	}

	IndiValue tempIndVal = indiValue;
	valueType = ""; 
	
	if ( tempIndVal.actualValueAccessible
			(allSMIObjects,
			 parentObject,
			 parentState,
			 parentAction,
			 valueType) )
	{
		operand.setParmValueType(valueType);
		return 1; // success
	}
	else
	{
		cout << " Error retrieving value . Error : " << value << endl;
		return 0;
	}
}
//----------------------------------------------------------------------------
int OperManager::getParValueAndOperValueTypes( SMLline& firstLine, SMLUnit* pParentUnit, ParOperand& operand)
{
	int retcode = 0;

//  If this is a constant, then everything is already set and user has no business
// request casting
	if ( operand.operandType() == "VALUE" )
	{
		if ( operand.userRequestedType() == "" )
		{
			return retcode;  // nothing to do, everything is already set
		}
		
		ErrorWarning::printHead("WARNING",firstLine);
		cout << " Parameter " << operand.printingName() << " is a constant"
		<< " and can not be casted. User request ignored and erased"
		<< endl;
		operand.eraseUserRequestedType();
		return retcode;
	}

// get the type of the value of the referenced parameter
	
	int flg = OperManager::getParValueType(pParentUnit, operand);
		
	if ( !flg ) 
	{
		retcode = 1;
		ErrorWarning::printHead("ERROR",firstLine);

		cout<< " Parameter " << operand.printingName() 
			<< " problem retrieving value" << endl;

		cout << endl; pParentUnit->printParents(); cout << endl;
		return retcode;				
	}

// now set the definite Operator Value Type taking into account possible user cast.
// if user did not request any cast, the parameter value type will be taken.
		
	Name userCast = operand.userRequestedType();
	int ret = operand.setOperValueType(userCast);
	if ( !ret )
	{
		retcode = 1;
		ErrorWarning::printHead("ERROR",firstLine);
		cout << " Parameter " << operand.printingName()
		 << " illegal cast STRING to FLOAT"
		<< endl;			
			
	}

	return retcode;

}
//==========================================================================
int OperManager::makeOperandsSameType
	           ( SMLline& firstLine, SMLUnit* pParentUnit,
		     ParOperand& operand1, ParOperand& operand2)
{ 
// We are assuming that at this point method 'getParValueAndOperValueTypes' was
// called for both parameters.

//  string float    .... not allowed
//  string int   ...  string -> int
//  int float ... int -> float

	Name operValTyp1 = operand1.operValueType();
	Name operValTyp2 = operand2.operValueType();
	
	if ( operValTyp1 == operValTyp2 )
	{ return 0; }

// the two operand types are different

	if ( operand1.operandType() == "VALUE" &&
	     operand2.operandType() == "VALUE" )
	{
		ErrorWarning::printHead("ERROR",firstLine);
		cout << "  operand types are different and"
		     << " both are constants. Ridiculous!" << endl;
		return 1;	
	}
// If user requested any casted then let him sort it out first

	if ( operand1.userRequestedType() == "" &&
	     operand2.userRequestedType() == "" ) {} 	
	else
	{
		ErrorWarning::printHead("ERROR",firstLine);
		cout << "  Sort out your casting first " << endl;
		return 1;
	
	}
	
// User did not request any casting so we can try to do it in order to
// make the operands the same type.

// As the user did not request any cast,we know, that the operand value type
//  is the same as ref par type

	if ( operand1.operandType() == "VALUE" )
	{
		return attemptCasting(firstLine,pParentUnit,operand2,operValTyp1.getString());	
	}

	if ( operand2.operandType() == "VALUE" )
	{
		return attemptCasting(firstLine,pParentUnit,operand1,operValTyp2.getString());	
	}
	
	if (    (operValTyp1 == "STRING" || operValTyp2 == "STRING")
	     && (operValTyp1 == "FLOAT" || operValTyp2 == "FLOAT") )
	{
		ErrorWarning::printHead("ERROR",firstLine);
		cout << " illegal type mixing STRING with FLOAT"
		<< endl;			
		return 1;
	
	}
	
	
	if ( operValTyp1 == "STRING" )
	{
		if ( operValTyp2 == "INT" )
		{
			return attemptCasting(firstLine,pParentUnit,operand1,"INT");
		}
		else
		{
			ErrorWarning::printHead("ERROR",firstLine);
			cout << " Parameter " << operand2.printingName() << " illegal operand type"
			<< endl << " internal error ...  aborting" ;
			throw FATAL;			
		}
	}
	else if ( operValTyp1 == "FLOAT" )
	{
		if ( operValTyp2 == "INT" )
		{
			return attemptCasting(firstLine,pParentUnit,operand2,"FLOAT");
		}			
		else
		{
			ErrorWarning::printHead("ERROR",firstLine);
			cout << " Parameter " << operand2.printingName() << " illegal operand type"
			<< endl << " internal error ...  aborting" ;
			throw FATAL;			
		}	
	
	}
	else if ( operValTyp1 == "INT" )
	{
		if ( operValTyp2 == "STRING" )
		{
			return attemptCasting(firstLine,pParentUnit,operand2,"INT");
		}
		else if ( operValTyp2 == "FLOAT" )
		{
			return attemptCasting(firstLine,pParentUnit,operand1,"FLOAT");
		}			
		else
		{
			ErrorWarning::printHead("ERROR",firstLine);
			cout << " Parameter " << operand2.printingName() << " illegal operand type"
			<< endl << " internal error ...  aborting" ;
			throw FATAL;			
		}	
	
	
	}
	else
	{
		ErrorWarning::printHead("ERROR",firstLine);
		cout << " Parameter " << operand1.printingName() << " illegal operand type"
		<< endl << " internal error ...  aborting" ;
		throw FATAL;			
	}
}
//==========================================================================
int OperManager::attemptCasting
	           ( SMLline& firstLine, SMLUnit* pParentUnit,
		     ParOperand& operand, const char castTo[])
{ 
	Name temp = castTo;

	if ( operand.setOperValueType(castTo) )
	{
		ErrorWarning::printHead("WARNING",firstLine);
		cout << " Parameter " << operand.printingName() << " casted to "
		<< castTo << endl;
		cout << endl; pParentUnit->printParents(); cout << endl;
		return 0;
	}
	ErrorWarning::printHead("ERROR",firstLine);
	cout << " Parameter " << operand.printingName() << " can not be casted to "
	<< castTo << endl;
	cout << endl; pParentUnit->printParents(); cout << endl;
	return 1;			

}
