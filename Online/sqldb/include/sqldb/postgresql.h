//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef SQLDB_POSTGRESQL_INTERFACE_H
#define SQLDB_POSTGRESQL_INTERFACE_H

/// Framework include files
#include <sqldb/sqldb.h>

typedef struct pg_conn   PGconn;
typedef struct pg_result PQresult;

///  Technology abstraction layer
/**
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class postgresql : public sqldb::traits  {
 public:
  typedef PGconn   dbase_t;
  typedef PQresult statement_t;
};

#endif // SQLDB_POSTGRESQL_INTERFACE_H
