//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef SQLDB_SQLDB_IMP_H
#define SQLDB_SQLDB_IMP_H

/// C/C++ include files
#include <sstream>

/// Framework include files

/// SQLDB namespace declaration
namespace sqldb  {

  /// Call on failure: Throws exception
  SQLDB_INLINE sqldb_status invalid_statement(const char* msg)    {
    throw std::runtime_error(std::string("Statement Error: ") + (msg ? msg : " [Unknwon Error]"));
  }
 
  /// Call on failure: Throws exception
  SQLDB_INLINE sqldb_status invalid_statement(const std::string& msg)    {
    throw std::runtime_error("Statement Error: " + msg);
  }

  /// Convert ascii string to time stamp
  SQLDB_INLINE time time::make_time(const struct tm& tm)    {
    return time(::mktime((struct tm*)&tm));
  }

  /// Convert ascii string to time stamp
  SQLDB_INLINE time time::make_time(const std::string& asc_time)    {
    return make_time(asc_time, time::default_format());
  }

  /// Convert ascii string to time stamp
  SQLDB_INLINE time time::make_time(const std::string& asc_time, const std::string& format)    {
    struct tm tm;
    if ( nullptr != ::strptime(asc_time.c_str(), format.c_str(), &tm) )   {
      return make_time(tm);
    }
    throw std::runtime_error("Invalid data conversion to time with format: "+format);
  }

  /// Conversion to local time in form of struct tm
  SQLDB_INLINE struct tm  time::local_time()   const   {
    struct tm tm;
    ::localtime_r(&this->stamp, &tm);
    return tm;
  }

  /// Conversion to greenwich mean time in form of struct tm
  SQLDB_INLINE struct tm  time::gm_time()   const   {
    struct tm tm;
    ::gmtime_r(&this->stamp, &tm);
    return tm;
  }

  /// Conversion to string
  SQLDB_INLINE std::string time::to_string()   const   {
    char buff[64];
    return ::ctime_r(&this->stamp, buff);
  }
  
  /// Conversion to string
  SQLDB_INLINE std::string time::to_string(const std::string& format)   const   {
    struct tm tm;
    char   buff[256];
    ::localtime_r(&this->stamp, &tm);
    std::size_t len = ::strftime(buff, sizeof(buff), format.c_str(), &tm);
    if ( len > 0 )  return buff;
    throw std::runtime_error("Invalid data conversion to time with format: "+format);
  }
  
  /// Generic conversion to integer number
  template <typename T> SQLDB_INLINE T to_integer(const column_data& column)  {
    switch( column.type )   {
    case TYPE_BLOB:
    case TYPE_STRING:
      return (T)::strtol(std::string(column.item.string, column.length).c_str(),nullptr,10);
    case TYPE_TINY:
    case TYPE_SHORT:
    case TYPE_LONG:
    case TYPE_LONGLONG:
      return (T)(column.item.uint64);
    case TYPE_FLOAT:
      return (T)(column.item.real32);
    case TYPE_DOUBLE:
      return (T)(column.item.real64);
    case TYPE_TIME:
      return column.item.time.stamp;
    default:
      throw std::runtime_error(std::string("Invalid data type conversion to ")+typeid(T).name());
      break;
    }
  }

  /// Generic conversion to floating point number
  template <typename T> SQLDB_INLINE T to_real(const column_data& column)  {
    switch( column.type )   {
    case TYPE_BLOB:
    case TYPE_STRING:
      return (T)::strtod((std::string(column.item.string, column.length)).c_str(), nullptr);
    case TYPE_TINY:
    case TYPE_SHORT:
    case TYPE_LONG:
    case TYPE_LONGLONG:
      return (T)column.item.int64;
    case TYPE_FLOAT:
      return column.item.real32;
    case TYPE_DOUBLE:
      return column.item.real64;
    case TYPE_TIME:
      return (T)column.item.time.stamp;
    default:
      throw std::runtime_error(std::string("Invalid data type conversion to ")+typeid(T).name());
      break;
    }
  }

  /// Generic conversion to integer number
  SQLDB_INLINE class time to_time(const column_data& column)  {
    switch( column.type )   {
    case TYPE_BLOB:
    case TYPE_STRING:
      return time::make_time(std::string(column.item.string, column.length));
    case TYPE_LONG:
    case TYPE_LONGLONG:
      return time::make_time(column.item.uint64);
    case TYPE_TINY:
    case TYPE_SHORT:
    case TYPE_FLOAT:
    case TYPE_DOUBLE:
    default:
      throw std::runtime_error("Invalid data type conversion to sqldb-time");
      break;
    }
  }

  /// Bind pointers for writing
  SQLDB_INLINE void column_data::bind_pointer(const void* ptr, std::size_t len)   {
    if ( len < sizeof(column_data::buf) )  {
      ::memcpy(this->buf, ptr, len);
      this->item.pointer = this->buf;
    }
    else  {
      pointers_t p(ptr);
      this->vbuf.resize(len);
      this->vbuf.assign(p.puint8, p.puint8 + len);
      this->item.pointer = &this->vbuf.front();
    }
    this->length = len;
  }
  
  /// Access the name of a database
  SQLDB_INLINE std::string_view database::name()   const   {
    return intern.get() ? intern->name.c_str() : "";
  }

  /// Access the name of a database
  SQLDB_INLINE std::string_view database::name(const database& dbase)   {
    return dbase.name();
  }

  /// Access error number of last failed statement
  SQLDB_INLINE int database::error()  const   {
    return intern->action(backend::ERRNO);
  }
  
  /// Access error message on failure
  SQLDB_INLINE std::string database::errmsg()  const   {
    return intern.get() ? intern->errmsg() : " --- no database handle --- ";
  }

  /// Access error message on failure
  SQLDB_INLINE std::string database::errmsg(const database& dbase)   {
    return dbase.errmsg();
  }
  
  /// Close the connection to the database
  SQLDB_INLINE sqldb_status database::close()   {
    if ( intern.get() )  {
      intern->action(backend::CLOSE);
      intern->db = nullptr;
      intern.reset();
    }
    return sqldb::OK;
  }
  
  /// Start transaction
  SQLDB_INLINE sqldb_status database::begin()    const   {
    return (sqldb_status)intern->action(backend::BEGIN);
  }
  
  /// Commit transaction
  SQLDB_INLINE sqldb_status database::commit()   const   {
    return (sqldb_status)intern->action(backend::COMMIT);
  }
  
  /// Roll-back transaction
  SQLDB_INLINE sqldb_status database::rollback()   const   {
    return (sqldb_status)intern->action(backend::ROLLBACK);
  }

  /// Access the number of rows affected by the last changes
  SQLDB_INLINE long database::rows_affected()  const   {
    return intern->rows_affected();
  }

  /// Execute SQL statement
  SQLDB_INLINE sqldb_status database::execute(std::string& error, const char* sql)  const  {
    return intern->execute(error, sql);
  }
  
  /// Execute SQL statement
  SQLDB_INLINE sqldb_status database::execute_sql(std::string& error, const char* fmt, ...)  const    {
    char str[4096];
    va_list args;
    va_start(args, fmt);
    ::vsnprintf(str, sizeof(str), fmt, args);
    va_end(args);
    return intern->execute(error, str);
  }

  /// Prepare a new statement and allocate resources.
  SQLDB_INLINE sqldb_status database::prepare(statement& stmt, const std::string& sql)  const  {
    return intern->prepare(stmt, sql);
  }

  /// Fetch next result row. Stop if result is <> traits::OK
  SQLDB_INLINE sqldb_status record_set::fetch_one()  const   {
    if ( this->stmt )  return this->stmt->fetch_one();
    return invalid_statement("fetch_one: Invalid recordset statement handle");
  }

  /// Access type specific field data
  template <> SQLDB_INLINE blob_t         record::get<blob_t>(std::size_t col)  const   {
    auto& column = this->field(col);
    switch( column.type )   {
    case TYPE_BLOB:
    case TYPE_STRING:
      return blob_t(column.item.blob, column.length);
    default:
      throw std::runtime_error("get<blob_t>: Invalid data type conversion to BLOB");
      break;
    }
  }

  /// Access type specific field data
  template <> SQLDB_INLINE vblob_t        record::get<vblob_t>(std::size_t col)  const   {
    const auto& column = this->field(col);
    switch( column.type )   {
    case TYPE_BLOB:
    case TYPE_STRING:
      return vblob_t(column.item.blob, column.item.blob+column.length);
    default:
      throw std::runtime_error("get<vblob_t>: Invalid data type conversion to BLOB");
      break;
    }
  }

  /// Access type specific field data
  template <> SQLDB_INLINE std::string    record::get<std::string>(std::size_t col)  const  {
    const auto& column = this->field(col);
    switch( column.type )   {
    case TYPE_BLOB:
    case TYPE_STRING:
      return column.item.string ? column.item.string : "";
    case TYPE_TINY:
    case TYPE_SHORT:
    case TYPE_LONG:
    case TYPE_LONGLONG:  {
      std::stringstream buf;
      buf << column.item.uint64 << std::ends;
      return buf.str();
    }
    case TYPE_FLOAT:  {
      std::stringstream buf;
      buf << column.item.real32 << std::ends;
      return buf.str();
    }
    case TYPE_DOUBLE:   {
      std::stringstream buf;
      buf << column.item.real64 << std::ends;
      return buf.str();
    }
    case TYPE_TIME:
      return column.item.time.to_string();
    default:
      throw std::runtime_error("Invalid data type conversion to STRING.");
      break;
    }
  }

  /// Access type specific field data
  template <> SQLDB_INLINE signed char    record::get<signed char>(std::size_t col)  const  {
    return to_integer<signed char>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE char           record::get<char>(std::size_t col)  const  {
    return to_integer<char>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE unsigned char  record::get<unsigned char>(std::size_t col)  const  {
    return to_integer<unsigned char>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE int16_t        record::get<int16_t>(std::size_t col) const  {
    return to_integer<int16_t>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE uint16_t       record::get<uint16_t>(std::size_t col) const  {
    return to_integer<uint16_t>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE int32_t        record::get<int32_t>(std::size_t col)   const  {
    return to_integer<int32_t>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE uint32_t       record::get<uint32_t>(std::size_t col)   const  {
    return to_integer<uint32_t>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE int64_t        record::get<int64_t>(std::size_t col)  const  {
    return to_integer<int64_t>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE uint64_t       record::get<uint64_t>(std::size_t col)  const  {
    return to_integer<uint64_t>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE float          record::get<float>(std::size_t col)  const  {
    return to_real<float>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE double         record::get<double>(std::size_t col)  const  {
    return to_real<double>(this->field(col));
  }

  /// Access type specific field data
  template <> SQLDB_INLINE time           record::get<class time>(std::size_t col)  const  {
    return to_time(this->field(col));
  }

  /// Generic bind of a single parameter column
  SQLDB_INLINE sqldb_status record_bind::bind (std::size_t column, field_type typ, const void* data, std::size_t len)  const  {
    return stmt->_bind(column, typ, data, len);
  }

  /// Type specific bind operator for blobs
  SQLDB_INLINE sqldb_status record_bind::bind(std::size_t col, const blob_t& data)  const  {
    return stmt->_bind(col, TYPE_BLOB, data.first, data.second);
  }

  /// Type specific bind operator for blobs
  SQLDB_INLINE sqldb_status record_bind::bind(std::size_t col, const vblob_t& data)  const  {
    return stmt->_bind(col, TYPE_BLOB, &data.at(0), data.size());
  }

  /// Type specific bind operator for blobs
  SQLDB_INLINE sqldb_status record_bind::bind(std::size_t col, const uint8_t* data, std::size_t len)  const  {
    return stmt->_bind(col, TYPE_BLOB, data, len);
  }

  /// Type specific bind operator for strings
  SQLDB_INLINE sqldb_status record_bind::bind(std::size_t col, std::string& data)  const  {
    return stmt->_bind(col, TYPE_STRING, data.c_str(), data.length());
  }

  /// Type specific bind operator for strings
  SQLDB_INLINE sqldb_status record_bind::bind(std::size_t col, const std::string& data)  const  {
    return stmt->_bind(col, TYPE_STRING, data.c_str(), data.length());
  }

  /// Type specific bind operator for strings
  SQLDB_INLINE sqldb_status record_bind::bind(std::size_t col, char* const data)  const  {
    return stmt->_bind(col, TYPE_STRING, data ? data : "", data ? ::strlen(data) : 0);
  }

  /// Type specific bind operator for strings
  SQLDB_INLINE sqldb_status record_bind::bind(std::size_t col, const char* const data)  const  {
    return stmt->_bind(col, TYPE_STRING, data ? data : "", data ? ::strlen(data) : 0);
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<char>(std::size_t col, char data)  const  {
    return stmt->_bind(col, TYPE_TINY, &data, sizeof(data));
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<signed char>(std::size_t col, signed char data)  const  {
    return stmt->_bind(col, TYPE_TINY, &data, sizeof(data));
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<unsigned char>(std::size_t col, unsigned char data)  const  {
    return stmt->_bind(col, TYPE_TINY, &data, sizeof(data));
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<int16_t>(std::size_t col, int16_t data)  const  {
    return stmt->_bind(col, TYPE_SHORT, &data, sizeof(data));
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<uint16_t>(std::size_t col, uint16_t data)  const  {
    return stmt->_bind(col, TYPE_SHORT, &data, sizeof(data));
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<int32_t>(std::size_t col, int32_t data)  const  {
    return stmt->_bind(col, TYPE_LONG, &data, sizeof(data));
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<uint32_t>(std::size_t col, uint32_t data)  const  {
    return stmt->_bind(col, TYPE_LONG, &data, sizeof(data));
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<int64_t>(std::size_t col, int64_t data)  const  {
    return stmt->_bind(col, TYPE_LONGLONG, &data, sizeof(data));
  }

  /// Type specific bind operator for integer type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<uint64_t>(std::size_t col, uint64_t data)  const  {
    return stmt->_bind(col, TYPE_LONGLONG, &data, sizeof(data));
  }

  /// Type specific bind operator for float type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<float>(std::size_t col, float data)  const  {
    return stmt->_bind(col, TYPE_FLOAT, &data, sizeof(data));
  }

  /// Type specific bind operator for double type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<double>(std::size_t col, double data)  const  {
    return stmt->_bind(col, TYPE_DOUBLE, &data, sizeof(data));
  }

  /// Type specific bind operator for time type
  template <> SQLDB_INLINE sqldb_status record_bind::bind<class time>(std::size_t col, class time data)  const  {
    return stmt->_bind(col, TYPE_TIME, &data.stamp, sizeof(data.stamp));
  }

  /// Copy assignment
  SQLDB_INLINE statement& statement::operator = (const statement& copy)   {
    if ( &copy != this )  {
      this->stmt   = this;
      this->intern = copy.intern;
    }
    return *this;
  }
  
  /// Access error number of last failed statement
  SQLDB_INLINE int statement::error()   const  {
    return intern->action(backend::ERRNO);
  }
  
  /// Create fully prepared statement in a database context
  SQLDB_INLINE sqldb_status statement::prepare(const database& db, const std::string& sql)  {
    return db.prepare(*this, sql);
  }
  
  /// Execute SQL statement with variable number of arguments
  SQLDB_INLINE sqldb_status statement::prepare(const database& db, const char* fmt, ...)   {
    char sql[4096];
    va_list args;
    va_start(args, fmt);
    ::vsnprintf(sql, sizeof(sql), fmt, args);
    va_end(args);
    return db.prepare(*this, sql);
  }
  
  /// Execute SQL statement with variable number of arguments
  SQLDB_INLINE statement statement::create(const database& db, const char* fmt, ...)   {
    char sql[4096];
    va_list args;
    va_start(args, fmt);
    ::vsnprintf(sql, sizeof(sql), fmt, args);
    va_end(args);
    statement stmt;
    sqldb_status ret = db.prepare(stmt, sql);
    if ( ret == sqldb::OK )
      return stmt;
    return statement();
  }
  
  /// Execute prepared statement
  SQLDB_INLINE sqldb_status statement::execute()  const   {
    return (sqldb_status)intern->action(backend::EXEC);
  }
  
  /// Fetch next result row. Stop if result is <> traits::OK
  SQLDB_INLINE sqldb_status statement::fetch_one()  const   {
    return (sqldb_status)intern->action(backend::FETCH);
  }
  
  /// Reset SQL statement: Reset all data to re-execute the statement with new parameters
  SQLDB_INLINE sqldb_status statement::reset()  const   {
    return (sqldb_status)intern->action(backend::RESET);
  }
  
  /// Finalize and run-down the statement. Release all resources.
  SQLDB_INLINE sqldb_status statement::finalize()   {
    if ( intern.get() )  {
      intern->action(backend::FINALIZE);
      intern.reset();
    }
    return sqldb::OK;
  }

  /// Generic bind of a single parameter column
  SQLDB_INLINE sqldb_status statement::_bind(std::size_t column, field_type typ, const void* data, std::size_t len)  const  {
    return intern->bind(column, typ, data, len);
  }

} /// End namespace sqldb
#endif  // SQLDB_SQLDB_IMP_H
