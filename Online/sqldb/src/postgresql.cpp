//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <sqldb/postgresql.h>

#if defined(SQLDB_HAVE_POSTGRESQL)

/// MySQL header
#include <libpq-fe.h>
#include <sql3types.h>

#define POSTGRESQL_TYPE_BLOB 123456

/// SQLDB namespace declaration
namespace sqldb  {

  namespace postgresql_imp  {

    struct statement_backend;
    
    ///
    std::string _escape(postgresql::dbase_t* hdl, const char* str, std::size_t length)   {
      std::vector<char> buf(2*length+1);
      std::size_t len = ::PQescapeStringConn(hdl, &buf.front(), str, length, 0);
      return std::string(&buf.front(), len);
    }
    ///
    void clear_result(PQresult*& result)   {
      if ( result != nullptr )  {
	::PQclear(result);
	result = nullptr;
      }
    }
    ///
    inline sqldb_status postgresql_ok(const PGresult* status)    {
      if ( status )  {
	ExecStatusType sc = ::PQresultStatus(status);
	switch( sc )   {
	case PGRES_COMMAND_OK:
	case PGRES_TUPLES_OK:
	case PGRES_COPY_OUT:
	case PGRES_COPY_IN:
	case PGRES_EMPTY_QUERY:
	  return sqldb::OK;
	case PGRES_NONFATAL_ERROR:
	  return sqldb::OK;
	case PGRES_BAD_RESPONSE:
	case PGRES_FATAL_ERROR:
	default:
	  return sqldb::ERROR;
	}
      }
      return sqldb::ERROR;
    }
    ///
    inline field_type gen_field_type(int typ)   {
      switch(typ)   {
      case SQL3_SMALLINT:                      return TYPE_SHORT;
      case SQL3_INTEGER:                       return TYPE_LONGLONG;
      case SQL3_FLOAT:                         return TYPE_FLOAT;
      case SQL3_DOUBLE_PRECISION:              return TYPE_DOUBLE;
      case SQL3_CHARACTER:                     return TYPE_STRING;
      case POSTGRESQL_TYPE_BLOB:               return TYPE_BLOB;
      case SQL3_DATE_TIME_TIMESTAMP:           return TYPE_TIME;
#if 0
      case SQL3_DDT_DATE:                      return TYPE_TIME;
      case SQL3_DDT_TIME:                      return TYPE_TIME;
      case SQL3_DDT_TIMESTAMP:                 return TYPE_TIME;
      case SQL3_DDT_TIME_WITH_TIME_ZONE:       return TYPE_TIME;
      case SQL3_DDT_TIMESTAMP_WITH_TIME_ZONE:  return TYPE_TIME;
#endif
      default:
	break;
      }
      throw std::runtime_error("Invalid data type encountered!");
    }
    ///
    inline int postgresql_field_type(field_type typ)   {
      switch(typ)   {
      case TYPE_TINY:      return SQL3_CHARACTER;
      case TYPE_SHORT:     return SQL3_SMALLINT;
      case TYPE_LONG:      return SQL3_INTEGER;
      case TYPE_LONGLONG:  return SQL3_INTEGER;
      case TYPE_FLOAT:     return SQL3_FLOAT;
      case TYPE_DOUBLE:    return SQL3_DOUBLE_PRECISION;
      case TYPE_STRING:    return SQL3_CHARACTER;
      case TYPE_BLOB:      return POSTGRESQL_TYPE_BLOB;
      case TYPE_TIME:      return SQL3_DATE_TIME_TIMESTAMP;
      default:
	break;
      }
      throw std::runtime_error("Invalid data type encountered!");
    }

    
    ///  Technology abstraction layer
    /**
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct column_bind   {
    public:
      statement_backend* stmt { nullptr };
      int                col  { 0 };
      
    public:
      /// Initializing constructor
      column_bind(statement_backend* s, int c) : stmt(s), col(c) {}
      /// Default constructor
      column_bind() = default;
      /// Move constructor
      column_bind(column_bind&& copy) = default;
      /// Copy constructor
      column_bind(const column_bind& copy) = default;
      /// Move assignment
      column_bind& operator=(column_bind&& copy) = default;
      /// Copy assigment
      column_bind& operator=(const column_bind& copy) = default;
      /// Default destructor
      ~column_bind() = default;
      /// Bind parameter to null value
      sqldb_status bind_store_null(column_data& d);
      /// Generic parameter binding
      sqldb_status bind_store(column_data& d, const char* ptr, std::size_t len);
      /// Generic parameter binding
      sqldb_status bind_store(column_data& d, const unsigned char* ptr, std::size_t len);
      /// Type specific parameter bind
      template <typename T> sqldb_status bind_store(column_data& d, T& to, const T* from);
      /// Bind output field
      sqldb_status bind_fetch_blob(column_data& d);
      /// Bind output field
      template <typename T> sqldb_status bind_fetch(column_data& d, T* to, std::size_t len);
    };

    ///  Technology abstraction layer for a Postgresql prepared statement
    /**
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct statement_backend : public statement::backend  {
    public:
      typedef enum {
	null_param,
	text_param,
	binary_param
      } param_type;
      
      typedef std::tuple<column_data*, column_bind*, int> field_t;
      postgresql::dbase_t*      _handle   { nullptr };
      postgresql::statement_t*  _result   { nullptr };
      std::string               _id;
      std::string               _sql;
      std::string               _err;
      std::stringstream         _fmt;
      std::vector<char const *> _param_ptr;
      std::vector<int>          _param_len;
      std::vector<int>          _param_fmt;
      std::vector<Oid>          _param_typ;
      std::vector<column_bind>  _fbind;
      std::size_t _current_row {0};
      std::size_t _num_param   {0};

      template <typename T> T _to(const char* s)   {
	T result;
	this->_fmt.clear();
	this->_fmt.str((s && *s) ? s : "-1");
	this->_fmt >> result;
	return result;
      }
      field_t field(int col)   {
	return field_t(&this->fields[col], &_fbind[col], col);
      }
      /// Access to the internal statement handle. Throws exception if the handle is invalid
      postgresql::dbase_t* handle()   const   {
	if ( this->_handle ) return this->_handle;
	throw std::runtime_error("Failed to access statement handle. [Invalid handle]");
      }
      /// Access error message on failure
      std::string error_msg(const postgresql::dbase_t* h)  {
	return ::PQerrorMessage(h);
      }
      /// Access error message on failure
      std::string error_msg(const postgresql::statement_t* h)  {
	return ::PQresultErrorMessage(h);
      }
      /// Access error message on failure
      virtual std::string errmsg()  override  {
	return (this->_result) ? this->error_msg(this->_result) : this->error_msg(this->_handle);
      }
      /// Character string of the prepared statement
      virtual const char* sql()  override  {
	return this->_sql.c_str();
      }
      /// Access the number of rows affected by the last statement
      virtual long  rows_affected()  override  {
	if ( this->_result )   {
	  const char* s = ::PQcmdTuples(this->_result);
	  if ( s && *s )
	    return _to<long>(s);
	}
	return -1;
      }
      /// 
      sqldb_status reset()  {
	if( this->_handle )  {
	  clear_result(this->_result);
	  std::vector<int>          lens(_num_param,0);
	  std::vector<char const *> pval(_num_param,0);
	  std::vector<Oid>          typs(_num_param,null_param);
	  this->_param_ptr.swap(pval);
	  this->_param_len.swap(lens);
	  this->_param_typ.swap(typs);
	  return sqldb::OK;
	}
	return sqldb::ERROR;
      }
      /// Prepare a new statement and allocate resources.
      sqldb_status prepare(postgresql::dbase_t* db, const std::string& sql_str, uint64_t id);
      /// Execute prepared statement
      virtual sqldb_status execute();
      /// Bind output field identified by column number
      sqldb_status bind_fetch(std::size_t col);
      /// Bind a single column to the data passed
      virtual sqldb_status bind(std::size_t col, field_type typ, const void* data, std::size_t len)  override;
      /// Action routine for various functions
      virtual int action(action_type type)   override;
    };

    /// Bind parameter to null value
    sqldb_status column_bind::bind_store_null(column_data& d)   {
      d.null   = true;
      d.length = 0;
      stmt->_param_ptr[col] = nullptr;
      stmt->_param_len[col] = d.length;
      stmt->_param_fmt[col] = 1;
      return sqldb::OK;
    }

    /// Generic parameter binding
    sqldb_status column_bind::bind_store(column_data& d, const unsigned char* ptr, std::size_t len)   {
      if ( ptr )  {
	d.bind_pointer(ptr, len);
	stmt->_param_ptr[col] = d.item.string;
	stmt->_param_len[col] = d.length;
	stmt->_param_fmt[col] = 1;
	return sqldb::OK;
      }
      return this->bind_store_null(d);
    }

    /// Generic parameter binding
    sqldb_status column_bind::bind_store(column_data& d, const char* ptr, std::size_t len)   {
      if ( ptr )  {
	d.bind_pointer((const unsigned char*)ptr, len);
	stmt->_param_ptr[col] = d.item.string;
	stmt->_param_len[col] = d.length;
	stmt->_param_fmt[col] = 0;
	return sqldb::OK;
      }
      return this->bind_store_null(d);
    }

    /// Type specific parameter bind
    template <typename T> sqldb_status column_bind::bind_store(column_data& d, T& to, const T* from)   {
      if ( from )   {
	to       = *from;
	d.length = sizeof(T);
	stmt->_param_ptr[col] = (char*)&to;
	stmt->_param_len[col] = d.length;
	stmt->_param_fmt[col] = 1;
	return sqldb::OK;
      }
      return this->bind_store_null(d);
    }

    /// Bind output field
    template <typename T> sqldb_status column_bind::bind_fetch(column_data& d, T* to, std::size_t len)    {
      T* ptr = (T*)::PQgetvalue(stmt->_result, stmt->_current_row, col);
      d.null   = false;
      d.error  = false;
      d.length = len;
      *to = *ptr;
      return sqldb::OK;
    }

    /// Bind output field
    sqldb_status column_bind::bind_fetch_blob(column_data& d)   {
      unsigned char* ptr = (unsigned char*)::PQgetvalue(stmt->_result, stmt->_current_row, col);
      std::size_t length = ::PQgetlength(stmt->_result, stmt->_current_row, col);
      d.null   = false;
      d.error  = false;
      d.length = length;
      d.vbuf.clear();
      if ( length <= sizeof(d.buf) )   {
	::memcpy(d.buf, ptr, length);
	d.item.string = d.buf;
	return sqldb::OK;
      }
      d.vbuf.insert(d.vbuf.begin(), ptr, ptr+length);
      d.item.blob = &d.vbuf.front();
      return sqldb::OK;
    }
    
    /// Prepare a new statement and allocate resources.
    sqldb_status statement_backend::prepare(postgresql::dbase_t* db, const std::string& sql_str, uint64_t id)    {
      this->_handle = db;
      this->_current_row = -1;
      this->_fmt.imbue(std::locale::classic());
      this->_fmt.str(std::string());
      this->_fmt.clear();
      this->_fmt << "sqldb_pq_stmt_" << id;
      this->_id = this->_fmt.str();
      this->_fmt.str(std::string());
      this->_fmt.clear();
      this->_sql.clear();
      this->_sql.reserve(sql_str.size());
      this->_num_param = 0;
      bool inside = false;
      for(unsigned i=0;i<sql_str.size();i++) {
	char c = sql_str[i];
	switch(c)   {
	case '\'':
	  inside = !inside;
	  break;
	case '?':
	  if( !inside )  {
	    this->_sql += '$';
	    ++this->_num_param;
	    this->_fmt << this->_num_param;
	    this->_sql += this->_fmt.str();
	    this->_fmt.str(std::string());
	    this->_fmt.clear();
	  }
	  break;
	default:
	  this->_sql += c;
	  break;
	}
      }
      this->_param_ptr.resize(  this->_num_param, nullptr);	
      this->_param_typ.resize(this->_num_param, null_param);
      this->_param_len.resize(this->_num_param, 0);
      this->reset();

      this->_result = ::PQprepare(this->_handle,this->_id.c_str(),this->_sql.c_str(),0,0);
      if( !this->_result )   {
	_err = "sqldb(postgres): Failed to create prepared statement object!";
	return sqldb::ERROR;
      }
      if ( ::PQresultStatus(this->_result) != PGRES_COMMAND_OK )   {
	_err = "sqldb(postgres): statement preparation failed: "+error_msg(this->_result);
	return sqldb::ERROR;
      }
	
      auto* res = ::PQdescribePrepared(this->_handle,this->_id.c_str());
      if( !this->_result )   {
	_err = "sqldb(postgres): Failed to inspect prepared statement object!";
	return sqldb::ERROR;
      }
      if ( ::PQresultStatus(this->_result) != PGRES_COMMAND_OK )   {
	_err = "sqldb(postgres): statement inspection failed: "+error_msg(res);
	return sqldb::ERROR;
      }
      this->params.resize(this->_num_param);
      for( std::size_t col=0; col < this->_num_param; ++col )  {
	Oid native_type = ::PQparamtype(res, col);
	this->_param_typ[col] = native_type;
	this->params[col].type = gen_field_type(native_type);
      }
      this->_err.clear();
      clear_result(res);
      clear_result(this->_result);
      return sqldb::OK;
    }

    /// Bind a single column to the data passed
    sqldb_status statement_backend::bind(std::size_t col, field_type typ, const void* data, std::size_t len)   {
      pointers_t ptr(data);
      column_bind b(this, col);
      auto& d = this->params[col];
      switch(typ)   {
      case TYPE_NULL:     return b.bind_store_null(d);
      case TYPE_TINY:     return b.bind_store(d, d.item.uint8,  ptr.puint8);
      case TYPE_SHORT:    return b.bind_store(d, d.item.uint16, ptr.puint16);
      case TYPE_LONG:     return b.bind_store(d, d.item.uint32, ptr.puint32);
      case TYPE_LONGLONG: return b.bind_store(d, d.item.uint64, ptr.puint64);
      case TYPE_FLOAT:    return b.bind_store(d, d.item.real32, ptr.preal32);
      case TYPE_DOUBLE:   return b.bind_store(d, d.item.real64, ptr.preal64);
      case TYPE_STRING:   return b.bind_store(d, ptr.pstring,   len);
      case TYPE_BLOB:     return b.bind_store(d, ptr.puint8,    len);
	/// For now store time stamps as long integers (time_t)
      case TYPE_TIME:     return b.bind_store(d, d.item.time.stamp, ptr.pint64);
      default:            return invalid_statement("Invalid Postgresql parameter data type");
      }
      return sqldb::OK;
    }

    /// Bind output field identified by column number
    sqldb_status statement_backend::bind_fetch(std::size_t col)  {
      auto  typ = ::PQftype(this->_result, col);
      auto& d   = this->fields[col];
      column_bind b(this, col);
      d.clear();
      d.type = gen_field_type(typ);
      switch(d.type)   {
      case TYPE_TINY:     return b.bind_fetch(d, &d.item.int8,   sizeof(d.item.int8));
      case TYPE_SHORT:    return b.bind_fetch(d, &d.item.int16,  sizeof(d.item.int16));
      case TYPE_LONG:     return b.bind_fetch(d, &d.item.int32,  sizeof(d.item.int32));
      case TYPE_LONGLONG: return b.bind_fetch(d, &d.item.int64,  sizeof(d.item.int64));
      case TYPE_FLOAT:    return b.bind_fetch(d, &d.item.real32, sizeof(d.item.real32));
      case TYPE_DOUBLE:   return b.bind_fetch(d, &d.item.real64, sizeof(d.item.real64));
      case TYPE_TIME:     return b.bind_fetch(d, &d.item.time.stamp, sizeof(d.item.int64));
      case TYPE_STRING:   return b.bind_fetch_blob(d);
      case TYPE_BLOB:     return b.bind_fetch_blob(d);
      default:            return invalid_statement("Invalid Postgresql Field data type");
      }
      return sqldb::OK;
    }
    
    /// Execute prepared statement
    sqldb_status statement_backend::execute()   {
      this->_current_row = -1;
      clear_result(this->_result);
      this->_result = ::PQexecPrepared(this->handle(),
				       this->_id.c_str(),
				       this->_num_param,
				       &this->_param_ptr[0],
				       &this->_param_len[0],
				       &this->_param_fmt[0],
				       1);
      if( !this->_result )   {
	_err = "sqldb(postgres): Failed to create prepared statement object!";
	return sqldb::ERROR;
      }
      if ( ::PQresultStatus(this->_result) != PGRES_COMMAND_OK )   {
	_err = "sqldb(postgres): statement preparation failed: "+error_msg(this->_result);
	return sqldb::ERROR;
      }
      return sqldb::OK;
    }

    /// Action routine for various functions
    int statement_backend::action(action_type type)   {
      switch(type)   {
      case EXEC:         /// Execute prepared statement
	return this->execute();
      case FETCH:        /// Fetch next result row. Stop if result is NON-zero
	if ( this->_result )   {
	  ++_current_row;
	  return ::PQgetisnull(this->_result, this->_current_row, 0) ? sqldb::OK : sqldb::ERROR;
	}
	return sqldb::ERROR;
      case RESET:        /// Reset all data to re-execute the statement with new parameters
	return this->reset();
      case FINALIZE:     /// Finalize and run-down the statement. Release all resources.
	if ( this->_handle != nullptr && !this->_id.empty() )   {
	  this->_result = ::PQexec(this->_handle, ("DEALLOCATE "+this->_id).c_str());
	  clear_result(this->_result);
	}
	return sqldb::OK;
      case ERRNO:        /// Access error number of last failed statement
	if ( this->_result != nullptr )  {
	  return ::PQresultStatus(this->_result);
	}
	return ::PQstatus(this->handle());
      default:
	throw std::runtime_error("Invalid action request received!");
      }
    }
    
    ///  Technology abstraction layer for a Postgresql database
    /**
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct dbase_backend : public database::backend  {
      postgresql::dbase_t* _handle { nullptr };
      ::PGresult*          _result { nullptr };
      unsigned long long   _prepared_id  { 0 };

      /// Access to the internal database handle. Throws exception if the handle is invalid
      postgresql::dbase_t* handle()   const   {
	if ( _handle ) return _handle;
	throw std::runtime_error("Invalid Postgresql database handle");
      }
      /// Access error message on failure
      virtual std::string errmsg()  override  {
	if ( this->_result )   {
	  return ::PQresultErrorMessage(this->_result);
	}
	return ::PQerrorMessage(this->handle());
      }
      /// Access the number of rows affected by the last statement
      virtual long rows_affected()  override  {
	if ( this->_result != nullptr )   {
	  return ::PQntuples(this->_result);
	}
	return -1;
      }
      /// Perform multiple actions
      virtual int action(action_type type)  override  {
	switch(type)    {
	case CLOSE:
	  clear_result(this->_result);
	  if ( this->_handle )  {
	    ::PQfinish(this->_handle);
	    this->_handle = nullptr;
	  }
	  return sqldb::OK;
	case BEGIN:       /// Start transaction
	  clear_result(this->_result);
	  this->_result = ::PQexec(this->handle(), "BEGIN");
	  return postgresql_ok(this->_result);
	case COMMIT:      /// Commit transaction
	  clear_result(this->_result);
	  this->_result = ::PQexec(this->handle(), "COMMIT");
	  return postgresql_ok(this->_result);
	case ROLLBACK:    /// Rollback transaction
	  clear_result(this->_result);
	  this->_result = ::PQexec(this->handle(), "ROLLBACK");
	  return postgresql_ok(this->_result);
	case ERRNO:       /// Access error number of last failed statement
	  if ( this->_result != nullptr )  {
	    return ::PQresultStatus(this->_result);
	  }
	  return ::PQstatus(this->handle());
	default:
	  throw std::runtime_error("Invalid action request received!");
	}
      }
      /// Execute prepared statement
      virtual sqldb_status execute(std::string& err, const std::string& sql)  override  {
	err = "";
	if( this->_handle )  {
	  clear_result(this->_result);
	  this->_result = ::PQexec(this->_handle, sql.c_str());
	  if ( postgresql_ok(this->_result) == sqldb::OK )  {
	    return sqldb::OK;
	  }
	  if ( this->_result )   {
	    ExecStatusType status = ::PQresultStatus(this->_result);
	    err = ::PQresStatus(status);
	    return sqldb::ERROR;
	  }
	  err = "Fatal PostgreSQL error";
	  return sqldb::ERROR;
	}
	err = "Invalid database connection";
	return sqldb::INVALID_HANDLE;
      }
      /// Prepare a new statement and allocate resources.
      virtual sqldb_status prepare(statement& bs, const std::string& sql_str)  override  {
	auto* stm_imp = new statement_backend();
	auto* h = this->handle();
	auto  sql = _escape(h, sql_str.c_str(), sql_str.length());
	bs.intern.reset(stm_imp);
	return stm_imp->prepare(h, sql, ++this->_prepared_id);
      }
      /// Open database
      sqldb_status open(const std::string& connect_string)   {
	auto args = connection_args().parse(connect_string);
	this->_handle = ::PQsetdbLogin(args["host"].c_str(),
				       args["port"].c_str(),
				       args["options"].c_str(),
				       nullptr,  // tty
				       args["database"].c_str(),
				       args["user"].c_str(),
				       args["password"].c_str());
	if ( this->_handle == nullptr )   {
	  return sqldb::ERROR;
	}
	this->name    = args["database"];
	return sqldb::OK;
      }
      
      /// Initializing constructor
      dbase_backend() = default;
    };
  }
}

/// SQLDB namespace declaration
namespace sqldb  {
  
  /// Open the database connection using all information in passed string
  template <>
  std::pair<std::shared_ptr<database::backend>, sqldb_status>
  database::backend::open<postgresql>(const std::string& connect_string)   {
    auto db = std::make_shared<postgresql_imp::dbase_backend>();
    sqldb_status ret = db->open(connect_string);
    if ( ret == sqldb::OK )
      return make_pair(db, sqldb::OK);
    return make_pair(std::shared_ptr<database::backend>(), ret);
  }

}       // End namespace sqldb
#endif
