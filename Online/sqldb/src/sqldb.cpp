//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <sqldb/sqldb.h>

#ifndef __INLINE__
#define __INLINE__
#endif

/// SQLDB namespace declaration
namespace sqldb  {

  /// Call on failure: Throws exception
  __INLINE__ int invalid_statement(const char* msg)    {
    throw std::runtime_error(std::string("Statement Error: ") + (msg ? msg : " [Unknwon Error]"));
  }
 
  /// Access the name of a database
  __INLINE__ std::string_view database::name()   const   {
    return intern.get() ? intern->name.c_str() : "";
  }

  /// Access the name of a database
  __INLINE__ std::string_view database::name(const database& dbase)   {
    return dbase.name();
  }

  /// Access error message on failure
  __INLINE__ std::string database::errmsg()  const   {
    return intern.get() ? intern->errmsg() : " --- no database handle --- ";
  }

  /// Access error message on failure
  __INLINE__ std::string database::errmsg(const database& dbase)   {
    return dbase.errmsg();
  }
  
  /// Close the connection to the database
  __INLINE__ int database::close()   {
    if ( intern.get() )  {
      intern->action(backend::CLOSE);
      intern->db = nullptr;
      intern.reset();
    }
    return sqldb::OK;
  }
  
  /// Start transaction
  __INLINE__ int database::begin()    const   {
    return intern->action(backend::BEGIN);
  }
  
  /// Commit transaction
  __INLINE__ int database::commit()   const   {
    return intern->action(backend::COMMIT);
  }
  
  /// Roll-back transaction
  __INLINE__ int database::rollback()   const   {
    return intern->action(backend::ROLLBACK);
  }
  
  /// Access the number of rows affected by the last changes
  __INLINE__ long database::rows_affected()  const   {
    return intern->rows_affected();
  }

  /// Execute SQL statement
  __INLINE__ int database::execute(std::string& error, const char* sql)  const  {
    return intern->execute(error, sql);
  }
  
  /// Execute SQL statement
  __INLINE__ int database::execute_sql(std::string& error, const char* fmt, ...)  const    {
    char str[4096];
    va_list args;
    va_start(args, fmt);
    ::vsnprintf(str, sizeof(str), fmt, args);
    va_end(args);
    return this->execute_sql(error, str);
  }

  /// Prepare a new statement and allocate resources.
  __INLINE__ int database::prepare(basic_statement& stmt, const std::string& sql)  const  {
    return intern->prepare(stmt, sql);
  }
  
  /// Fetch next result row. Stop if result is <> traits::OK
  __INLINE__ int record_set::fetch_one()  const   {
    if ( this->stmt )  return this->stmt->fetch_one();
    return invalid_statement("Invalid recordset statement handle");
  }

  template <> __INLINE__ blob_t record_set::get<blob_t>(std::size_t col)  const   {
    auto& column = this->field(col);
    return blob_t(column.item.blob, column.length);
  }

  template <> __INLINE__ vblob_t record_set::get<vblob_t>(std::size_t col)  const   {
    const auto& column = this->field(col);
    if ( column.item.blob )
      return vblob_t(column.item.blob, column.item.blob+column.length);
    return vblob_t();
  }

  template <> __INLINE__ std::string  record_set::get<std::string>(std::size_t col)  const  {
    const auto& column = this->field(col);
    return column.item.string ? column.item.string : "";
  }

  template <> __INLINE__ signed char    record_set::get<signed char>(std::size_t col)  const  {
    return this->field(col).item.int8;
  }

  template <> __INLINE__ char           record_set::get<char>(std::size_t col)  const  {
    return this->field(col).item.int8;
  }

  template <> __INLINE__ unsigned char  record_set::get<unsigned char>(std::size_t col)  const  {
    return this->field(col).item.uint8;
  }

  template <> __INLINE__ short          record_set::get<short>(std::size_t col) const  {
    return this->field(col).item.int16;
  }

  template <> __INLINE__ unsigned short record_set::get<unsigned short>(std::size_t col) const  {
    return this->field(col).item.uint16;
  }

  template <> __INLINE__ int            record_set::get<int>(std::size_t col)   const  {
    return this->field(col).item.int32;
  }

  template <> __INLINE__ unsigned int   record_set::get<unsigned int>(std::size_t col)   const  {
    return this->field(col).item.uint32;
  }

  template <> __INLINE__ long           record_set::get<long>(std::size_t col)  const  {
    return this->field(col).item.int64;
  }

  template <> __INLINE__ unsigned long  record_set::get<unsigned long>(std::size_t col)  const  {
    return this->field(col).item.uint64;
  }

  template <> __INLINE__ float          record_set::get<float>(std::size_t col)  const  {
    return this->field(col).item.real32;
  }

  template <> __INLINE__ double         record_set::get<double>(std::size_t col)  const  {
    return this->field(col).item.real64;
  }

  /// Create fully prepared statement in a database context
  __INLINE__ basic_statement basic_statement::prepare(const database& db, const std::string& sql)  {
    basic_statement stmt;
    if ( db.prepare(stmt, sql) == sqldb::OK )
      return stmt;
    return basic_statement();
  }
  
  /// Execute SQL statement with variable number of arguments
  __INLINE__ basic_statement basic_statement::prepare(const database& db, const char* fmt, ...)   {
    char sql[4096];
    va_list args;
    va_start(args, fmt);
    ::vsnprintf(sql, sizeof(sql), fmt, args);
    va_end(args);
    basic_statement stmt;
    if ( db.prepare(stmt, sql) == sqldb::OK )
      return stmt;
    return basic_statement();
  }
  
  /// Execute prepared statement
  __INLINE__ int basic_statement::execute()  const   {
    return intern->execute();
  }
  
  /// Fetch next result row. Stop if result is <> traits::OK
  __INLINE__ int basic_statement::fetch_one()  const   {
    return intern->fetch_one();
  }
  
  /// Reset SQL statement: Reset all data to re-execute the statement with new parameters
  __INLINE__ int basic_statement::reset()  const   {
    return intern->reset();
  }
  
  /// Finalize and run-down the statement. Release all resources.
  __INLINE__ int basic_statement::finalize()   {
    if ( intern.get() )  {
      intern->finalize();
      intern.reset();
    }
    return sqldb::OK;
  }
  
  template <> __INLINE__ blob_t basic_statement::get<blob_t>(std::size_t col)  const   {
    auto& column = this->field(col);
    return blob_t(column.item.blob, column.length);
  }

  template <> __INLINE__ vblob_t basic_statement::get<vblob_t>(std::size_t col)  const   {
    const auto& column = this->field(col);
    if ( column.item.blob )
      return vblob_t(column.item.blob, column.item.blob+column.length);
    return vblob_t();
  }

  template <> __INLINE__ std::string     basic_statement::get<std::string>(std::size_t col)  const  {
    const auto& column = this->field(col);
    return column.item.string ? column.item.string : "";
  }

  template <> __INLINE__ signed char    basic_statement::get<signed char>(std::size_t col)  const  {
    return this->field(col).item.int8;
  }

  template <> __INLINE__ char           basic_statement::get<char>(std::size_t col)  const  {
    return this->field(col).item.int8;
  }

  template <> __INLINE__ unsigned char  basic_statement::get<unsigned char>(std::size_t col)  const  {
    return this->field(col).item.uint8;
  }

  template <> __INLINE__ short          basic_statement::get<short>(std::size_t col) const  {
    return this->field(col).item.int16;
  }

  template <> __INLINE__ unsigned short basic_statement::get<unsigned short>(std::size_t col) const  {
    return this->field(col).item.uint16;
  }

  template <> __INLINE__ int            basic_statement::get<int>(std::size_t col)   const  {
    return this->field(col).item.int32;
  }

  template <> __INLINE__ unsigned int   basic_statement::get<unsigned int>(std::size_t col)   const  {
    return this->field(col).item.uint32;
  }

  template <> __INLINE__ long           basic_statement::get<long>(std::size_t col)  const  {
    return this->field(col).item.int64;
  }

  template <> __INLINE__ unsigned long  basic_statement::get<unsigned long>(std::size_t col)  const  {
    return this->field(col).item.uint64;
  }

  template <> __INLINE__ float          basic_statement::get<float>(std::size_t col)  const  {
    return this->field(col).item.real32;
  }

  template <> __INLINE__ double         basic_statement::get<double>(std::size_t col)  const  {
    return this->field(col).item.real64;
  }

  /// Generic bind of a single parameter column
  inline int basic_statement::bind (std::size_t column, field_type typ, const void* data, std::size_t len)  const
  {  return intern->bind(column, typ, data, len);          }

  template <> __INLINE__ int basic_statement::bind<char>(std::size_t col, char data)  const
  {  return intern->bind(col, TYPE_TINY, &data, sizeof(data));          }

  template <> __INLINE__ int basic_statement::bind<signed char>(std::size_t col, signed char data)  const
  {  return intern->bind(col, TYPE_TINY, &data, sizeof(data));          }

  template <> __INLINE__ int basic_statement::bind<unsigned char>(std::size_t col, unsigned char data)  const
  {  return intern->bind(col, TYPE_TINY, &data, sizeof(data));          }

  template <> __INLINE__ int basic_statement::bind<short>(std::size_t col, short data)  const
  {  return intern->bind(col, TYPE_SHORT, &data, sizeof(data));         }

  template <> __INLINE__ int basic_statement::bind<unsigned short>(std::size_t col, unsigned short data)  const
  {  return intern->bind(col, TYPE_SHORT, &data, sizeof(data));         }

  template <> __INLINE__ int basic_statement::bind<int>(std::size_t col, int data)  const
  {  return intern->bind(col, TYPE_LONG, &data, sizeof(data));          }

  template <> __INLINE__ int basic_statement::bind<unsigned int>(std::size_t col, unsigned int data)  const
  {  return intern->bind(col, TYPE_LONG, &data, sizeof(data));          }

  template <> __INLINE__ int basic_statement::bind<long>(std::size_t col, long data)  const
  {  return intern->bind(col, TYPE_LONGLONG, &data, sizeof(data));      }

  template <> __INLINE__ int basic_statement::bind<unsigned long>(std::size_t col, unsigned long data)  const
  {  return intern->bind(col, TYPE_LONGLONG, &data, sizeof(data));      }

  template <> __INLINE__ int basic_statement::bind<float>(std::size_t col, float data)  const
  {  return intern->bind(col, TYPE_FLOAT, &data, sizeof(data));         }

  template <> __INLINE__ int basic_statement::bind<double>(std::size_t col, double data)  const
  {  return intern->bind(col, TYPE_DOUBLE, &data, sizeof(data));        }

  template <> __INLINE__ int basic_statement::bind<char* const>(std::size_t col, char* const data)  const
  {  return intern->bind(col, TYPE_STRING, data ? data : "", data ? ::strlen(data) : 0); }

  template <> __INLINE__ int basic_statement::bind<const char*>(std::size_t col, const char* data)  const
  {  return intern->bind(col, TYPE_STRING, data ? data : "", data ? ::strlen(data) : 0); }

  inline int basic_statement::bind(std::size_t col, const blob_t& data)  const
  {  return intern->bind(col, TYPE_BLOB, data.first, data.second);  }

  inline int basic_statement::bind(std::size_t col, const vblob_t& data)  const
  {  return intern->bind(col, TYPE_BLOB, &data.at(0), data.size()); }

  inline int basic_statement::bind(std::size_t col, const uint8_t* data, std::size_t len)  const
  {  return intern->bind(col, TYPE_BLOB, data, len);         }

} /// End namespace sqldb

