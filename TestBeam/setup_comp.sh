#!/bin/bash
# =========================================================================
#
#  Default script to start the passthrough process on a farm node.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
export MONITOR_BASE=`realpath $( dirname "${BASH_SOURCE[0]}" )`;
export INSTALLATION=`dirname ${MONITOR_BASE}`;
echo "MONITOR_BASE = ${MONITOR_BASE}"
echo "INSTALLATION = ${INSTALLATION}"
#
COMP=gcc11;
if test -n "${1}"; then
    COMP="${1}";
fi;
#
if test "${COMP}" = "gcc10"; then
    export CMTDEB=x86_64_v2-centos7-${COMP}-do0;
    export CMTOPT=x86_64_v2-centos7-${COMP}-opt;
elif test "${COMP}" = "gcc11"; then
    export CMTDEB=x86_64_v2-centos7-${COMP}-do0;
    export CMTOPT=x86_64_v2-centos7-${COMP}-opt;
elif test "${COMP}" = "gcc12"; then
    export CMTDEB=x86_64_v2-el9-${COMP}-do0;
    export CMTOPT=x86_64_v2-el9-${COMP}-opt;
elif test "${COMP}" = "clang11"; then
    export CMTDEB=x86_64_v2-centos7-${COMP}-dbg;
    export CMTOPT=x86_64_v2-centos7-${COMP}-opt;
elif test "${COMP}" = "clang12"; then
    export CMTDEB=x86_64_v2-centos7-${COMP}-dbg;
    export CMTOPT=x86_64_v2-centos7-${COMP}-opt;
else
    export CMTDEB=x86_64-centos7-${COMP}-do0;
    export CMTOPT=x86_64-centos7-${COMP}-opt;
fi;
export CMTCONFIG=$CMTDEB;
export BINARY_TAG=$CMTDEB;
export ALL_CMT_PLATFORMS="${CMTDEB} ${CMTOPT}";
#
#
if test "${COMP}" = "gcc9"; then
    export RDKAFKA_DIR=/group/online/dataflow/cmtuser/libraries/kafka/rdkafka-gcc-9.2.0-opt;
else
    export RDKAFKA_DIR=/group/online/dataflow/cmtuser/libraries/kafka/rdkafka-gcc-10.1.0-opt;
fi;
#
MAINDNS=${DIM_DNS_NODE};
#echo "+++ VARS:            ${EB}  ${H3} ${H4}";
if test "`hostname -s | tr a-z A-Z | cut -b 3-4`" = "EB"; then
    MAINDNS="ecstms01";
elif test "`echo $DIM_HOST_NODE | tr a-z A-Z | cut -b 1-3`" = "HLT"; then
    MAINDNS="ecstms01";
fi;
#
#
export SETUP_INSTALLATION=${INSTALLATION}/setup.${BINARY_TAG}.vars;
#
echo "+++ Base:            ${MONITOR_BASE}";
echo "+++ Installation:    ${INSTALLATION}";
echo "+++ Kafka directory: ${RDKAFKA_DIR}";
echo "+++ Compiling for:   $COMP";
echo "+++ Binary tag:      $BINARY_TAG";
echo "+++ MAIN DNS node:   $MAINDNS";
#
start_gui()   {
    arg_1="${1}";
    shift ;
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${SETUP_INSTALLATION}; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS} \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=\${MONITOR_BASE}/options/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_${arg_1}.xml  $*" &
}
start_gui_dbg()   {
    arg_1="${1}";
    shift ;
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${SETUP_INSTALLATION}; \
     gdb --args gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS} \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=\${MONITOR_BASE}/options/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_${arg_1}.xml $*" &
}
#
start_gui_FPGA_Source()   {    start_gui FPGA_Source;        }
start_gui_FPGA_Target()   {    start_gui FPGA_Target;        }
start_gui_MUON()          {    start_gui MUON;               }
start_gui_RichMon()       {    start_gui RichMon;            }
start_gui_Velo()          {    start_gui VeloHalfVtxMon;     }
start_gui_Align()         {    start_gui Align;              }
#
start_gui_Allen()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=ALLEN -host=${HOST} -maindns=${MAINDNS}  \
        -ctrl_script=/group/online/dataflow/scripts/runFarmTask.sh        \
	-runinfo=/group/online/dataflow/options/ALLEN/OnlineEnvBase.py    \
	-architecture=/group/online/dataflow/options/ALLEN/Architecture.xml" &
}
