#####################################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration                 #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################
# - Locate Oracle library
# Defines:
#
#  Oracle_FOUND
#  Oracle_INCLUDE_DIR
#  Oracle_INCLUDE_DIRS (not cached)
#  Oracle_oci_LIBRARY
#  Oracle_LIBRARIES (not cached)
#  Oracle_LIBRARY_DIRS (not cached)
#
# Imports:
#
#  Oracle::oci
#
# Usage of the target instead of the variables is advised

find_path(Oracle_INCLUDE_DIR oci.h)
find_library(Oracle_oci_LIBRARY NAMES clntsh oci)

# handle the QUIETLY and REQUIRED arguments and set Oracle_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Oracle DEFAULT_MSG Oracle_INCLUDE_DIR Oracle_oci_LIBRARY)

mark_as_advanced(Oracle_FOUND Oracle_INCLUDE_DIR Oracle_oci_LIBRARY)

set(Oracle_INCLUDE_DIRS ${Oracle_INCLUDE_DIR})
set(Oracle_LIBRARIES ${Oracle_oci_LIBRARY})
get_filename_component(Oracle_LIBRARY_DIRS ${Oracle_oci_LIBRARY} DIRECTORY)

if(NOT TARGET Oracle::oci AND Oracle_FOUND)
  add_library(Oracle::oci IMPORTED INTERFACE)
  target_include_directories(Oracle::oci SYSTEM INTERFACE "${Oracle_INCLUDE_DIR}")
  target_link_libraries(Oracle::oci INTERFACE "${Oracle_oci_LIBRARY}")
  # Display the imported target for the user to know
  if(NOT ${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    message(STATUS "  Import target: Oracle::oci")
  endif()
endif()
