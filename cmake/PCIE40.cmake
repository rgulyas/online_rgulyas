if (NOT DEFINED PCIE40_ROOT)
  set(PCIE40_ROOT "/usr")
endif()

set(PCIE_40_LIBRARIES)
foreach(lib IN ITEMS daq40 pcie40_daq pcie40_id pcie40 pcie40_ecs)
  find_library(${lib}_path ${lib}
    HINTS
      ${PCIE40_ROOT}/lib64
      ${PCIE40_ROOT}/lib
    NO_DEFAULT_PATH)
  if (${${lib}_path} STREQUAL "${lib}_path-NOTFOUND")
    set(PCIE40_FOUND FALSE)
  else()
    list(APPEND PCIE_40_LIBRARIES ${${lib}_path})
  endif()
endforeach()

find_path(PCIE40_DAQ_INC NAMES "daq40.hpp" "daq.h" "id.h" PATHS ${PCIE40_ROOT}/include/lhcb/daq40)
if ( "${PCIE40_DAQ_INC}" STREQUAL "PCIE40_DAQ_INC-NOTFOUND" )
  set(PCIE40_FOUND FALSE)
else()
  set(PCIE_40_INCLUDE_DIRS ${PCIE40_ROOT}/include/lhcb/daq40 ${PCIE40_ROOT}/include/lhcb)
  set(PCIE40_FOUND TRUE)
endif()

if (PCIE40_FOUND)
  add_library(lhcb_pcie40 INTERFACE)
  target_include_directories(lhcb_pcie40 INTERFACE ${PCIE_40_INCLUDE_DIRS})
  target_link_libraries(lhcb_pcie40 INTERFACE
    ${PCIE_40_LIBRARIES})
  install(TARGETS lhcb_pcie40 EXPORT Online)
endif()
